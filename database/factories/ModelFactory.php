<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Order_Information::class, function (Faker\Generator $faker) {
    return [
        'id_bill' =>$faker->randomDigit,
        'state' => 2,
        'type' => 'fabirc',
        'id_customer'=>11,
       'id_employees'=>$faker->randomDigit ,
    ];
});
$factory->define(App\Order_detail::class, function (Faker\Generator $faker) {
    return [
        'id_design' =>3,
        'id_sleeve' => 3,
        'id_button' => 3,
        'id_neck'=>3,
        'id_pockets'=>3,
        'measurement_id'=>11,
         'number_column'=>1,

    ];
});
$factory->define(App\Bill::class, function (Faker\Generator $faker) {
    return [
        'payment_method_id' =>3,
        'paid_up' => 40,
        'Residual' => 0,
        'paid_up_card'=>800,
        'state'=>3 ,
        'price'=> 840
    ];
});
$factory->define(App\Order::class, function (Faker\Generator $faker) {
    return [
        'id_fabric' =>$faker->randomDigit,
        'number' => $faker->randomDigit,
        'number_column' => $faker->randomDigit,
        'number_column'=>$faker->randomDigit,
        'price'=>$faker->randomDigit ,
        'type'=> $faker->sentence
    ];
});
$factory->define(App\Date::class,function (Faker\Generator $faker){
return[
    'date' =>'2019-12-12 05:00:00',
    'type' => 1,
];

});


$factory->define(App\Reservation::class,function (Faker\Generator $faker){
return[

  'id_customer'=>1,
  'state'=>$faker->randomElement(['active','canceled','done']),
  'DateTime'=>$faker->randomElement(['2018-11-29 07:58:00','2017-11-27 00:00:00']),
  'phone'=>'0504271935',
  'name'=>$faker->name,
  'lan'=>'46.716667',
  'lat'=>'24.633333',

];
});
