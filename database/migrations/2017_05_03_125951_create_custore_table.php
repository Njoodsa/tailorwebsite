<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('id');
            $table->double('Length');
            $table->double('ShlR');
            $table->double('ShlL');
            $table->double('Slv_w');
            $table->double('Chest');
            $table->double('Waist');
            $table->double('Bottom');
            $table->double('CollarGalab');
            $table->double('FrontLonger');
            $table->double('FakeButton');
            $table->double('ElbowSlv_w');
            $table->double('Cuffs');
            $table->double('ElbowSlvSada');
            $table->double('Sada');
            $table->double('Bar');
            $table->double('SIDE');
            $table->double('MIDDLE');
            $table->double('Back');
            $table->double('CollarSada');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
