<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_industries')->usigned()->index();
            $table->string('name');
            $table->integer('first_balance');
            $table->integer('first_price');
            $table->integer('demand_limit');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventor_balances');
    }
}
