<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_fabric')->usigned()->index();
            $table->integer('id_customer')->usigned()->index();
            $table->integer('id_employees')->usigned()->index();
            $table->integer('id_designer')->usigned()->index();
            $table->integer('id_tailor')->usigned()->index();
            $table->integer('id_cutter')->usigned()->index();
            $table->integer('id_sew')->usigned()->index();
            $table->integer('id_button')->usigned()->index();
            $table->integer('id_neck')->usigned()->index();
            $table->integer('id_pockets')->usigned()->index();
            $table->double('price');
            $table->string('state');
            $table->double('yards');
            $table->date('end_date');
            $table->date('start_date');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
