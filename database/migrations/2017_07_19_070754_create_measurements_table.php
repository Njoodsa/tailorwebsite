<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Length');
            $table->string('ShlR');
            $table->string('ShlL');
            $table->string('Slv_w');
            $table->string('SlvSada');
            $table->string('Chest');
            $table->string('Waist');
            $table->string('Bottom');
            $table->string('CollarGalab');
            $table->string('FrontLonger');
            $table->string('FakeButton');
            $table->string('Snp_in');
            $table->string('ElbowCuffs');
            $table->string('Cuffs');
            $table->string('ElbowSlvSada');
            $table->string('Sada');
            $table->string('Bar');
            $table->string('SIDE');
            $table->string('MIDDLE');
            $table->string('Back');
            $table->string('CollarSada');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
