<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('id_industries')->usigned()->index();
            $table->integer('id_fabrics')->usigned()->index();
            $table->integer('retail_unit')->usigned()->index();
            $table->integer('unit_wholesale1')->usigned()->index();
            $table->integer('unit_wholesale2')->usigned()->index();
            $table->integer('cost_price');
            $table->integer('lowest_price');
            $table->integer('Equation1');
            $table->integer('Equation2');
            $table->integer('Barcode number');
            $table->integer('price_retail');
            $table->integer('price_wholesale1');
            $table->integer('price_wholesale2');
            $table->string('barcode_number');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productـdata');
    }
}
