@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title"> المشتريات المؤكده </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                            </div>
                        </div>
                        <br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder='اسم المورد ' disabled></th>
                                <th class="hide_to_phone"><input type="text" class="form-control" placeholder='رقم الجوال' disabled></th>
                                <th><input type="text" class="form-control" placeholder="تاريخ العمليه " disabled></th>
                                <th><input type="text" class="form-control" placeholder=" حالة الفاتوره " disabled></th>
                                <th><input type="text" class="form-control" placeholder="النوع" disabled></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($supplier_invoices as $supplier_invoice)
                                <tr>
                                    <td>{{$supplier_invoice->supplier->name}}</td>
                                    <td class="hide_to_phone">0{{ $supplier_invoice->supplier->phone}}</td>
                                    <td>{{$supplier_invoice->created_at}}</td>
                                    @if($supplier_invoice->state ==1)
                                    <td>مؤكده</td>
                                    @endif
                                    @if($supplier_invoice->type == 'product')
                                        <td>منتج </td>
                                        <td>
                                            <a class="btn btn-info btn-xs" href="detailCompleted_product/{{$supplier_invoice->id}}"><span
                                                        class=" glyphicon glyphicon-fullscreen "></span></a>
                                        </td>

                                    @else
                                        <td>قماش</td>
                                        <td>
                                            <a class="btn btn-info btn-xs" href="detailInvoice/{{$supplier_invoice->id}}"><span
                                                        class=" glyphicon glyphicon-fullscreen "></span></a>
                                        </td>
                                    @endif


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

