@extends('layouts.app')
@section('content')

    <style media="screen">
        .hide {
            display: none;
        }
        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 147px;
        }


    </style>
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6  margin-SidBar"">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">فاتورة جديده</h2>
                <form action="insertInvoice" method="post">
                    {{ csrf_field() }}


                    <br/>
                    <div class="panel panel-info form-group row panel_detail_order">
                        <br/>
                        <div class="panel-heading">المورد</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6 col-xs-6">
                                    <label>المورد</label>
                                    <select name="supplier_id" class='form-control'required>
                                        @foreach($suppliers as $supplier)
                                            <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <label>تاريخ الفاتوره </label>
                                    <input type="date" name="date" class='form-control'>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5% ; text-align: center;">
                                <div class="col-lg-12">
                                    <input type="radio" name="type" value="product" >&nbsp;   منتج
                                    <input type="radio" name="type" checked value="fabrics" style="margin-right: 10%;"> &nbsp;&nbsp;قماش
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info form-group row panel_detail_order">
                        <div class="panel-heading"> الفاتوره</div>
                        <div class="panel-body">
                            <div class="row"  >

                                <div class="col-lg-12 table-responsive"  >
                                    <button class="btn btn-success btn-xs add_new_fabric_row" id="add_new" type="button"><span class="glyphicon glyphicon-plus"></span></button>
                                    <table class="table">
                                        <thead class="fabrics">
                                        <th> اسم القماش</th>
                                        <th> المخزن</th>
                                        <th>عدد الطاقات</th>
                                        <th>سعر الطاقة</th>
                                        <th>الاجمالي</th>
                                        </thead>
                                        <thead class="product" hidden>
                                        <th> اسم المنتج</th>
                                        <th> المخزن</th>
                                        <th>العدد</th>
                                        <th>سعر المنتج </th>
                                        <th>الاجمالي</th>
                                        </thead>
                                        <tbody id="fabric_list">
                                        <!-- the default one -->
                                        <tr class="fabric_item">
                                            <td>
                                                <select name="id_item[]"
                                                        class="id_fabric"  style="width: 150px;">
                                                    @foreach($Fabrics as $Fabric)
                                                        <option value="{{$Fabric->id}}">{{$Fabric->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <select style="width: 100px;" name="warehouses_id[]" class="form-control"
                                                        required>
                                                    @foreach($Warehouses as $Warehouse)
                                                        <option value="{{$Warehouse->id}} ">{{$Warehouse->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="number" name='number[]' id="taga_number"
                                                       class="form-control taga_number IsChange">
                                            </td>
                                            <td><input name="price[]" id="price" type="number"
                                                       class="form-control price IsChange">
                                            </td>
                                            <td><input class="form-control totalPrice" id="totalPrice" type="text"
                                                       readonly></td>
                                            <td>
                                                <button class="btn btn-danger btn-xs delete_fabric_row" type="button">
                                                    <span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>

                                      

                                        <!-- add btn -->

                                        </tbody>
                                    </table>

                                    <hr>

                                    <div class="row">

                                        <div class="col-xs-6 col-sm-3 col-lg-3">
                                            الاجمالي : <input class="form-control" id="total_cost" readonly>
                                        </div>

                                        <div class="col-xs-6 col-sm-3 col-lg-3">
                                            الخصم : <input class="form-control" value="0" id="discount" name="discount">
                                        </div>

                                        <div class="col-xs-6 col-sm-3 col-lg-3">
                                            السعر : <input class="form-control" id="finally_price" name="finally_price"
                                                           readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info form-group row panel_detail_order">
                        <br/>
                        <div class="panel-heading"> الدفع</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4 col-xs-4">
                                    <label> طريقة الدفع </label>
                                    <select name="payment_method_id" class="form-control" required>
                                        @foreach($Payment_methods as $Payment_method)
                                            <option value="{{$Payment_method->id}}">{{$Payment_method->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 col-xs-4">
                                    <label>* المدفوع : </label>
                                    <input type='number' name='paid_up' id='paid_up' class='form-control'
                                           value='{{old('paid_up') }}'
                                           required>
                                </div>

                                <div class="col-lg-4 col-xs-4">
                                    <label>* المتبقي : </label>
                                    <input type='number' name='Residual' id='Residual' class='form-control'
                                           value='{{old('Residual') }}'
                                           required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info form-group row panel_detail_order">
                        <br/>
                        <div class="panel-heading"> حالة الفاتوره * في حال تم تاكيد الفاتوره فلن تستطيع التعديل عليها مرة
                            اخرى وسيضاف الرصيد الى المخزون
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>حالة الفاتوره </label>
                                    <select name="state" id='state' class="selectpicker" data-show-subtext="true"
                                            data-live-search="true" required>
                                        <option value=""> -- اختر--</option>
                                        <option value="1 ">مؤكده</option>
                                        <option value="0">غير مؤكده</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success btn-block" type="submit ">إضافه</button>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {


            // Add new fabric row
            $(".add_new_fabric_row").on("click", function() {
                $(".fabric_item").first()
                    .clone(true)
                    .find("input:text").val("").end()
                    .appendTo("#fabric_list")
            })

            // Delete fabric row
            $(".delete_fabric_row").on("click", function() {
                var tr = $(this).closest("tr")

                // if this is the last row then just empty its fields
                if ($('#fabric_list tr').length === 1) {
                    tr.find("input:text").val("").end()
                } else {
                    tr.remove()
                }

                updateNumbers()

            });

            function updatedRowPrice(columns, callback) {
                var count = Number($(columns[2]).find(".taga_number").val())
                var price = Number($(columns[3]).find(".price").val())
                return callback($(columns[4]).find(".totalPrice"), count * price)
            }

            $(".taga_number, .price").on("input", function(){
                var row = $(this).closest("tr").children()
                updatedRowPrice(row, function(input, cost){
                    input.val(cost);
                })
            })

            $(".taga_number,.price").on("change", function(){
                var row = $(this).closest("tr").children()
                updatedRowPrice(row, function(input, cost){
                    input.val(cost)
                    updateNumbers();
                })

                $("#discount").on("input", function(){
                    $("#finally_price").val(Number($("#total_cost").val())- Number($("#discount").val()))
                });

                function updateNumbers(){
                    // updating thop total count
                    var i = 0
                    $("#fabric_list").find(".taga_number").each(function() {
                        i += Number($( this ).val())
                    })
                    $("#total_thop_count").val(i)

                    // updating grand total price with discount
                    i = 0
                    $("#fabric_list").find(".totalPrice").each(function() {
                        i += Number($(this).val())
                    })
                    $("#total_cost").val(i)
                    $("#finally_price").val(i-Number($("#discount").val()))

                }



                $('#Residual').on('change', function () {
                var result = Number($('#Residual').val()) + Number($('#paid_up').val());
                if (result != $('#finally_price').val()) {
                    alert('يجب ان يكون مجموع المدفوع و المتبقي يساوي السعر ')
                    $('#Residual').val('');
                }
                console.log(result);

            });

            $('#state').on('change', function () {
                var result = $('#paid_up').val();
                if (result != $('#finally_price').val() && $('#state').val() == 1) {
                    alert(' لم يتم دفع كافة المبلغ لتاكيد الفاتورة  ')
                    $('#state').val('');
                }

            });


            });
            $('#paid_up').on('change', function () {
                var paidUp = $(this).val();
                var result = Number($('#finally_price').val()) - Number($('#paid_up').val());
                $('#Residual').val(result);


            });
        });




        $('input:radio[name="type"]').on("click", function () {
            if (this.checked && this.value == 'product') {
                $('.product').show();
                $('.fabrics').hide();
                $.ajax({
                    url: 'GetProductAJax',
                    Type: 'GET',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        console.log(data)
                        $('select[name="id_item[]"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="id_item[]"]').append('<option value="' + value.id + '">' + value.name + '</option>');

                        });
                    }
                })

            }
            if (this.checked && this.value == 'fabrics') {
                $('.product').hide();
                $('.fabrics').show();
                $.ajax({
                    url: 'GetFabricsAJax',
                    Type: 'GET',
                    dataType: 'json',
                    async: false,
                    success: function (data) {

                        $('select[name="id_item[]"]').empty();
                        $.each(data, function (key, value) {
                            console.log(value.id)
                            $('select[name="id_item[]"] ').append('<option value="' + value.id + '" >' + value.name + '</option>');
                        });
                    }
                })
            }
        });


    </script>
@endsection