@extends('layouts.app')
@section('content')
    <style media="screen">
        .hide {
            display: none;
        }

        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 135px;
        }


    </style>
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6  margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">فاتورة مؤكدة</h2>
                <form action="insertInvoice" method="post">
                    {{ csrf_field() }}


                    <div>
                        <br/>
                        <div class="panel panel-info form-group row panel_detail_order">
                            <br/>
                            <div class="panel-heading">المورد</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6 col-xs-6">
                                        <label>المورد</label>
                                        <input value="{{$data->supplier->name}}" class="form-control">
                                    </div>
                                    <div class="col-lg-6 col-xs-6">
                                        <label>تاريخ الفاتوره </label>
                                        <input type="date" name="date" value="{{$data->date}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info form-group row panel_detail_order">
                            <div class="panel-heading"> الفاتوره</div>
                            <div class="panel-body">
                                <div class="row" >
                                    <div class="col-lg-12 table-responsive" >
                                        <table class="table">
                                            <thead>
                                            <th>اسم المنتج </th>
                                            <th> المخزن</th>
                                            <th>الكمية  </th>
                                            <th>سعر المنتج </th>
                                            <th>الاجمالي</th>
                                            </thead>
                                            <tbody >
                                            <!-- the default one -->
                                            <?php $n = 0; ?>
                                            @foreach($data->invoice_items as $item)
                                                <tr class="fabric_item">
                                                    <td>
                                                        <input name="id_fabric[]" class="id_fabric form-control" value ='{{$item->product->name}}'>
                                                    </td>
                                                    <td>
                                                        <input class="form-control " value ='{{$item->warehouse->name}}'>
                                                    </td>
                                                    <td><input type="number" value='{{$item->number}}' name='number[]'
                                                               id="taga_number" class=" IsChange">
                                                    </td>
                                                    <td><input name="price[]" value='{{$item->price}}' id="price"
                                                               type="text" class=" price IsChange">
                                                    </td>
                                                    <td><input class="totalPrice" value=" {{$n+=($item->number * $item->price)}}" type="text" readonly></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                        <div class="col-lg-3 col-xs-4">
                                            الاجمالي : <input value="{{$n}}" readonly>
                                        </div>
                                        <div class="col-lg-3 col-xs-4">
                                            الخصم : <input name="discount" id='discount' value="{{$data->discount}}">
                                        </div>
                                        <div class="col-lg-3 col-xs-4">
                                            السعر : <input name="finally_price" id='finally_price' value="{{$data->price}}"
                                                           readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info form-group row panel_detail_order">
                            <br/>
                            <div class="panel-heading"> الدفع</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-4 col-xs-4">
                                        <label> طريقة الدفع </label>
                                        <input name="payment_method_id"  class="form-control" value="{{$data->payment_method->name}}">

                                    </div>
                                    <div class="col-lg-4 col-xs-4">
                                        <label>* المدفوع : </label>
                                        <input type='number' name='paid_up' id='paid_up' class='form-control'
                                               value='{{$data->paid_up}}'
                                               required>
                                    </div>

                                    <div class="col-lg-4 col-xs-4">
                                        <label>* المتبقي : </label>
                                        <input type='number' name='Residual' id='Residual' class='form-control'
                                               value='{{$data->Residual}}'
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info form-group row panel_detail_order">
                            <br/>
                            <div class="panel-heading"> حالة الفاتوره</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>حالة الفاتوره </label>
                                        <select name="state" class="form-control" required>
                                            <option value="1 ">مؤكده</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </form>
                <button class="btn btn-success btn-block" onclick=printDiv("cn")>طباعة الفاتوره</button>
            </div>
        </div>
    </div>
    @include('layouts.helper.prientToInvoic')
@endsection