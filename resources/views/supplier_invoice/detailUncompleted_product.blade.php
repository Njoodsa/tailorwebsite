@extends('layouts.app')
@section('content')
    <style media="screen">
        .hide {
            display: none;
        }
        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 135px;
        }
        }
    </style>
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">فاتورة غير مؤكده</h2>
                <form action="../updateInvoice/{{$data->id}}" method="post">
                    {{ csrf_field() }}

                    <br/>
                    <div class="panel panel-info form-group row panel_detail_order">
                        <br/>
                        <div class="panel-heading">المورد</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6 col-xs-6">
                                    <label>المورد</label>
                                    <select name="supplier_id" class="form-control" required>
                                        @foreach($suppliers as $supplier)
                                            <option @if($data->supplier_id ==$supplier->id ) selected
                                                    @endif
                                                    value="{{$supplier->id}}">{{$supplier->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <label>تاريخ الفاتوره </label>
                                    <input type="date" class="form-control" name="date" value="{{$data->date}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info form-group row panel_detail_order">
                        <div class="panel-heading"> الفاتوره</div>
                        <div class="panel-body">
                            <div class="row" >

                                <div class="col-lg-12 table-responsive" >
                                    <table class="table">
                                        <thead>
                                        <th> اسم المنتج </th>
                                        <th> المخزن</th>
                                        <th>الكمية  </th>
                                        <th>سعر المنتج </th>
                                        <th>الاجمالي</th>
                                        </thead>
                                        <tbody id="fabric_list">
                                        <!-- the default one -->
                                        @foreach($data->invoice_items as $item)
                                            <input type="hidden" value="{{$item->id}}" name="id_item[]">
                                            <tr class="fabric_item">
                                                <td>
                                                    <select name="id_fabric[]" class="id_fabric selectpicker "
                                                            data-show-subtext="true" data-live-search="true">

                                                        @foreach($products as $product)
                                                            <option @if($item->id_fabric ==$product->id )selected
                                                                    @endif
                                                                    value="{{$product->id}}">{{$product->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select
                                                            name="warehouses_id[]" class="selectpicker"
                                                            data-show-subtext="true"
                                                            data-live-search="true" required>
                                                        @foreach($Warehouses as $Warehouse)
                                                            <option @if($item->warehouses_id ==$Warehouse->id)  selected
                                                                    @endif
                                                                    value="{{$Warehouse->id}} ">{{$Warehouse->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="number" value='{{$item->number}}' name='number[]'
                                                           id="taga_number"
                                                           class="form-control taga_number IsChange">
                                                </td>
                                                <td><input name="price[]" value='{{$item->price}}' id="price"
                                                           type="text" class="form-control price IsChange">
                                                </td>
                                                <td><input class="form-control totalPrice" id="totalPrice"
                                                           value='{{$item->price * $item->number}} ' type="text" readonly>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger btn-xs delete_fabric_row" type="button">
                                                        <span class="glyphicon glyphicon-trash"></span></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <hr>

                                    <div class="row">

                                        <div class="col-xs-4 col-sm-3 col-lg-3">
                                            الاجمالي : <input class="form-control" id="total_cost" readonly>
                                        </div>

                                        <div class="col-xs-4 col-sm-3 col-lg-3">
                                            الخصم : <input class="form-control" id="discount" value="{{$data->discount}}"
                                                           name="discount">
                                        </div>

                                        <div class="col-xs-4 col-sm-3 col-lg-3">
                                            السعر : <input class="form-control" id="finally_price" name="finally_price"
                                                           value="{{$data->price}}"  readonly>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info form-group row panel_detail_order">
                        <br/>
                        <div class="panel-heading"> الدفع</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4 col-xs-4">
                                    <label> طريقة الدفع </label>
                                    <select name="payment_method_id" class="form-control" required>
                                        @foreach($Payment_methods as $Payment_method)
                                            <option @if($data->payment_method_id == $Payment_method->id) selected
                                                    @endif value="{{$Payment_method->id}}">{{$Payment_method->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 col-xs-4">
                                    <label>* المدفوع : </label>
                                    <input type='number' name='paid_up' id='paid_up' class='form-control'
                                           value='{{$data->paid_up}}'
                                           required>
                                </div>

                                <div class="col-lg-4 col-xs-4">
                                    <label>* المتبقي : </label>
                                    <input type='number' name='Residual' id='Residual' class='form-control'
                                           value='{{$data->Residual}}'
                                           required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info form-group row panel_detail_order">
                        <br/>
                        <div class="panel-heading"> حالة الفاتوره</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>حالة الفاتوره </label>
                                    <select name="state" id="state" class="selectpicker" data-show-subtext="true"
                                            data-live-search="true" required>

                                        <option @if($data->state==0) selected @endif
                                        value="0">غير مؤكده</option>
                                        <option @if($data->state==1) selected @endif
                                        value="1"> مؤكده</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-info btn-block" type="submit">تعديل</button>
                    <button class="btn btn-success btn-block" onclick=printDiv("cn")>طباعة الفاتوره</button>

                    </br>
                </form>
            </div>
        </div>

    </div>
    @include('layouts.helper.prientToInvoic')

    <script type="text/javascript">
        $(document).ready(function () {
            // Add new fabric row
            $(".add_new_row").on("click", function () {
                $(".clone_me").clone(true).removeClass("hide clone_me").appendTo("#fabric_list")
            });

            // Delete fabric row
            $(".delete_fabric_row").on("click", function () {
                // removes first parent row <tr>
                $(this).closest("tr").remove()

                // if this is the last row, clone a new one
                if ($('#fabric_list').find("tr").length === 1) {
                    $(".clone_me").clone(true).removeClass("hide clone_me").appendTo("#fabric_list")
                }

                updateNumbers()

            });

            $(".taga_number").on("input", function () {
                var col = $(this).closest("tr").children()
                // col is now an array of n size, where n is the number of columns
                // col[2] is the third column which is "thop count"
                // col[3] is the fourth column which is "thop price"
                // col[4] is the fifth column which is "row total price"

                var thopcount = Number($(col[2]).find(".taga_number").val())
                var thopprice = Number($(col[3]).find(".price").val())
                var cost = thopcount * thopprice
                $(col[4]).find(".totalPrice").val(cost)

                updateNumbers()

            });

            $(".price").on("input", function () {
                var col = $(this).closest("tr").children()
                var thopcount = Number($(col[2]).find(".taga_number").val())
                var thopprice = Number($(col[3]).find(".price").val())
                var cost = thopcount * thopprice
                $(col[4]).find(".totalPrice").val(cost)

                updateNumbers()

            });

            $("#discount").on("input", function () {
                $("#finally_price").val(Number($("#total_cost").val()) - Number($("#discount").val()))
            });

            function updateNumbers() {

                // update thop total count
                //  var i = 0
                // $("#fabric_list").find(".thop_count").each(function() {
                //   i += Number($( this ).val())
                //    });
                // update grand total price with discount
                var i = 0
                $("#fabric_list").find(".totalPrice").each(function () {
                    i += Number($(this).val())
                });
                $("#total_cost").val(i)
                $("#finally_price").val(i - Number($("#discount").val()))

            }


            $('#Residual').on('change', function () {
                var result = Number($('#Residual').val()) + Number($('#paid_up').val());
                if (result != $('#finally_price').val()) {
                    alert('يجب ان يكون مجموع المدفوع و المتبقي يساوي السعر ')
                    $('#Residual').val('');
                }
                console.log(result);

            });

            $('#state').on('change', function () {
                var result = $('#paid_up').val();
                if (result != $('#finally_price').val() && $('#state').val() == 1) {
                    alert(' لم يتم دفع كافة المبلغ لتاكيد الفاتورة  ')
                    $('#state').val('');
                }

            });

            $('#paid_up').on('change', function () {
                var result = $('#paid_up').val();
                if (result != $('#finally_price').val() && $('#state').val() == 1) {
                    alert(' لم يتم دفع كافة المبلغ لتاكيد الفاتورة  ')
                    $('#paid_up').val('');
                }
                var result = Number($('#finally_price').val()) - Number($('#paid_up').val());
                $('#Residual').val(result);


            });


            //FUNCTION WILL WORK AFERT PAGE LOAD
            $(window).bind("load", function () {
                var result = $('#taga_number').val() * $('#price').val();
                $('#totalPrice').val(result);
                var total = 0;
                $('#totalPrice').each(function () {
                    total += parseInt($(this).val());
                });
                $('#total_cost').val(total)
                //count finally price

                $('#finally_price').val(Number($('#total_cost').val()) - Number($('#discount').val()));
                updateNumbers();
            });
        });

    </script>

@endsection
