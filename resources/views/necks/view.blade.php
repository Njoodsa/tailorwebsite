@component('layouts.include.views')
    @slot('title')
        تصميم الرقبه
    @endslot

    @slot('linkAdd')
        <a href="createNeck" class="btn btn-success btn-xs "><span
                    class="glyphicon glyphicon-plus-sign"></span> إضافه
        </a>
    @endslot

    @slot('thead')
        <th><input type="text" class="form-control" placeholder=الاسم disabled></th>
        <th><input type="text" class="form-control" placeholder=" الملاحظات" disabled></th>
        <th></th>
    @endslot

    @slot('tbody')
        @foreach($data as $Neck)
            <tr>

                <td>{{$Neck->name}}</td>
                <td>{{$Neck->note}}</td>
                <td>
                    <a class="btn btn-info btn-xs" href="editNeck/{{$Neck->id}}"><span
                                class=" glyphicon glyphicon-edit "></span></a>
                </td>
            </tr>
        @endforeach
    @endslot
@endcomponent
