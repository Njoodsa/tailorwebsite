@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar" >
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.flash')
                <h2 class="main-title">تعديل نوع الرقبه</h2>
                <form action="../updateNeck/{{$data->id}}" method="post" class="contentForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type='hidden' name='id'  value='{{$data->id}}'>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label> الاسم : </label>
                                <input type='text' name='name' class='form-control' value='{{$data->name}}'>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        * <strong>{{ $errors->first('name') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                                <label>ملاحظات : </label>
                                <input type='text' name='note' class='form-control' value='{{$data->note}}'>
                                @if ($errors->has('note'))
                                    <span class="help-block">
                                        * <strong>{{ $errors->first('note') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        <br/>
                        <button class="btn btn-block  btn-info">تعديل</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection