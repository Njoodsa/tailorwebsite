@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">إضافة وحدة جديد</h2>
                <form action="insertUnit" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label> * الاسم : </label>
                        <input type='text' name='name' class='form-control' value='{{old('name') }}' required>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>
@endsection