@component('layouts.include.views')
    @slot('title')
        بيانات الوحده
    @endslot

    @slot('linkAdd')
        <a href="createUnit" class="btn btn-success btn-xs "><span
                    class="glyphicon glyphicon-plus-sign"></span> إضافه
        </a>
    @endslot

    @slot('thead')
        <th><input type="text" class="form-control" placeholder=الوحدة disabled></th>
        <th></th>
    @endslot

    @slot('tbody')
        @foreach($data as $unit)
            <tr>
                <td>{{$unit->name}}</td>
                <td>
                    <a class="btn btn-info btn-xs" href="editUnit/{{$unit->id}}"><span
                                class=" glyphicon glyphicon-fullscreen "></span></a>
                </td>
            </tr>
        @endforeach
    @endslot
@endcomponent
