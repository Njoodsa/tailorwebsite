@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6" style="margin-left :-10% ">

                @includeIf('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">بياناتي</h2>
                <form action="update_hisInformation/{{Auth::user()->id}}" method="post">
                    {{ csrf_field() }}
                    <center style="margin: 4%"> <!-- make  form center  -->
                        <div class="form-group">
                            <input type="hidden" name="id" value="{{Auth::user()->id}}">
                            <label for="usr">الايميل </label>
                            <input type="email" name="email" class="form-control" value="{{Auth::user()->email}}" required>
                        </div>
                        <div class="form-group">
                            <label for="pwd">كلمة المرور</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <label> * الفرع   : </label>
                            <select name='branches_id' class='form-control' required>
                                <option value="">-- اختر الفرع  -- </option>
                                @foreach($Branches as $Branch)
                                    <option @if(Auth::user()->branches_id ==$Branch->id ) selected @endif value="{{$Branch->id}}">{{$Branch->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" name=" update" class="btn btn-info  btn-md">تعديل</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
@endsection