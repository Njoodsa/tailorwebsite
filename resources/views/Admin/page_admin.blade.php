@extends('layouts.app')
@section('content')
    <title>صفحتي</title>
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar" >

        </div>

        <div >
            <form action="edit_hisInformation/{{Auth::user()->id}}" method="post">
                {{ csrf_field() }}
                    @include('layouts.error')
                    <h2 class="main-title">بياناتي</h2>
                    <div class="form-group">
                        <input type="hidden" name="id" value="{{Auth::user()->id}}">
                        <label for="usr">الايميل </label>
                        <input type="email" name="email" class="form-control" value="{{Auth::user()->email}}" required>
                    </div>
                    <div class="form-group">
                        <label for="pwd">كلمة المرور</label>
                        <input type="password" class="form-control" name="password">
                    </div>
                    <button type="submit" name=" update" class="btn btn-info  btn-md">تعديل</button>
            </form>
        </div> <!-- class body-left -->
    </div> <!-- class container -->


@endsection