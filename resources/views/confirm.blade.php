@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
    <div class="col-md-7 ">
    <form method="POST" action="{{url('api/confirmed')}}">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="name">mobile_code:{{$phone}}</label>
        <input type="text" class="form-control" id="name" name="mobile_code">
    </div>
    <button  type="button" id='resend' value="{{$phone}}"  >إعادة ارسال كود التفعيل </button>
    <div class="form-group">
        <button style="cursor:pointer" type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

    </div>
        </div>
    </div>
   <script>
    $(document).ready(function() {
        $('#resend').on("click", function () {
            $.ajax({
                url: 'ResendCode/' + $(this).val(),
                Type: 'Post',
                dataType: 'json',
            })
        })
    });
</script>
@stop