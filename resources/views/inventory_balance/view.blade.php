@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title"> ارصدة المخازن   </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                               <!-- <a href="inventory_balanceCreate" class="btn btn-success btn-xs "><span
                                            class="glyphicon glyphicon-plus-sign"></span> إضافه</a>-->
                            </div>
                        </div><br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder="المخزن " disabled></th>
                                <th><input type="text" class="form-control" placeholder=" القماش " disabled></th>
                                <th><input type="text" class="form-control" placeholder=" الرصيد الحالي " disabled></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>
                                        {{$item->warehouse->name}} <!-- name warehouse -->
                                      <!-- check if warehouse balance -->
                                            @if( $item->current_balance <= 15)
                                                &nbsp; <span style="color: red" class="glyphicon glyphicon-warning-sign"></span>
                                                @endif
                                       </td>

                                    <td>{{$item->fabrics->name}}</td>

                                    <td>
                                        {{$item->current_balance}}
                                    </td>
                                    <td>
                                        <a class="btn btn-info btn-xs" href="editInventory_balance/{{$item->id}}"><span
                                                    class=" glyphicon glyphicon-fullscreen "></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection

