@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title"> ارصدة المخزون</h2>
                <form action="../updateInventory_balance/{{$data->id}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <input name='id' value="{{$data->id}}" hidden>
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * المخزن : {{$data->warehouse->name}} </label>

                                <input type="hidden" name='fabrics_id' id='fabrics_id' value="{{$data->fabrics_id}}">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * القماش : {{$data->fabrics->name}} </label>

                                <input type="hidden" name='fabrics_id' id='fabrics_id' value="{{$data->fabrics_id}}">
                            </div>
                        </div>
                        <p id='test '></p>
                        <div class="col-lg-12">
                            <table class="table">
                                <thead>
                                <th>*</th>
                                <th>الحالي</th>
                                </thead>
                                <tbody>
                                <td style="width: 100px">الرصيد لطاقة</td>
                                <td>
                                    <?php $yeardh = 0 ?>

                                    @foreach($data->fabrics->Fabrics_unit as $n)
                                        @if($n->unit_id == 1 )
                                            <input type='number' id='current_balanceToTaga'
                                                   class='form-control'
                                                   value='{{floor($data->current_balance / $yeardh =$n->Equation)}}'
                                                   readonly>
                                        @endif
                                    @endforeach
                                </td>
                                <tr>
                                    <td>الرصيد للياردة:</td>
                                    <td>
                                        <input id="current_balanceToYarda" class='form-control'
                                               value='{{$data->current_balance}}'
                                               readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td> سعر للياردة :</td>
                                    <td><input type='number' id='current_price' class='form-control'
                                               readonly></td>
                                </tr>
                                <tr>
                                    <td> التكلفة للياردة :</td>
                                    <td><input class='form-control' id='current_Total_price' readonly></td>
                                </tr>
                                <tr>
                                    <td> حد الطلب لليادرة :</td>
                                    <td><input id='demand_limitTaga' class='form-control' name="demand_limit"
                                               value='{{$data->demand_limit }}' required></td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">تعديل</button>
                </form>
                <div class="panel panel-info specific_details_item">
                    <div class="panel-heading ">الإجمالي </div>
                    <div class="panel-body specific_details_body" >
                        <div class="col-lg-4 col-xs-12">
                            مجموع الخصم:
                        </div>
                        <div class="col-lg-4 col-xs-12">
                           مجموع الإضافة:
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            مجموع الثياب:
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            {{$total_sales_yeardh}}
                        </div>
                        <div class="col-lg-4 col-xs-12">
                            {{$Purchase->sum('number') * $yeardh}}
                        </div>
                        <div class="col-lg-4 col-xs-12">
                             {{$bills->sum('number')}}
                        </div>
                    </div>
                </div>

                @foreach($result as $item)
                    <div class="panel panel-info specific_details_item">
                        <div class="panel-heading ">
                            <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>&nbsp;
  السنة
                            :<?php echo date('Y', strtotime($item[0]->created_at)); ?>
                        </div>
                        <div class="panel-body specific_details_body" hidden>
                            <?php $total_bill = 0 ;
                            $total_invoice=0;
                            $total_thop=0
                            ?>
                            @foreach($item as $i)
                                <!-- test -->
                                    @if($i->Bill_id)
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p> خصم <b>({{ $i->yards *$i->number}}) ياردة</b> بتاريخ
                                                    <b>{{$i->created_at->format('d- m -Y ')}}</b>
                                                    طلب رقم
                                                    <input type="hidden" value="{{$total_bill+= $i->yards *$i->number}}">
                                                    <input type="hidden" value="{{$total_thop += $i->number}}">

                                                    @if( $i->bill->order_information->state == 1)
                                                        <a href="../DetailPending_measurement/{{$i->bill->order_information->id}}">
                                                           <b>{{$i->bill->order_information->id}}</b>
                                                        </a>
                                                @elseif($i->bill->order_information->state == 2)
                                                        <a href="../editOrder/{{$i->bill->order_information->id}}">
                                                            <b>{{$i->bill->order_information->id}}</b>
                                                        </a>
                                                    @elseif($i->bill->order_information->state == 3)
                                                        <a href="../DetailWorked_order/{{$i->bill->order_information->id}}">
                                                            <b>{{$i->bill->order_information->id}}</b>
                                                        </a>
                                                    @elseif($i->bill->order_information->state == 4)
                                                        <a href="../detail_orders_completed/{{$i->bill->order_information->id}}">
                                                            <b>{{$i->bill->order_information->id}}</b>
                                                        </a>
                                                    @elseif($i->bill->order_information->state == 6)
                                                        <a href="../detailDelivered/{{$i->bill->order_information->id}}">
                                                            <b>{{$i->bill->order_information->id}}</b>
                                                        </a>
                                                    @else
                                                        <b>{{$i->bill->order_information->id}}</b>
                                                @endif
                                                    <b> عدد الثياب  :     {{$i->number }}</b>

                                            </div>
                                        </div>
                                    @elseif($i->supplier_invoices_id)
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p> إضافة <b>({{$i->number * $yeardh}}) ياردة </b>مشتريات بتاريخ
                                                    <b>{{$i->created_at->format('d - m -Y ')}}</b>
                                                    فاتورة رقم <b> <a href="../detailInvoice/{{$i->supplier_invoices_id}}">
                                                            {{$i->supplier_invoices_id}}</a></b></p>
                                            </div>
                                        </div>
                                        <input  type="hidden" value="{{$total_invoice+=$i->number * $yeardh}}">
                                    @endif
                            @endforeach
                                <div class="col-lg-4 col-xs-12">
                                مجموع الخصم : {{$total_bill}}
                                </div>
                                <div class="col-lg-4 col-xs-12">
                                    مجموع الإضافة:
                                    {{$total_invoice}}
                                </div>
                                <div class="col-lg-4 col-xs-12">
                                    مجموع الثياب: {{$total_thop}}
                                </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function visalibaty(show1) {
            $('#' + show1).fadeToggle();
        }

        var demand_limitTaga = document.getElementById('demand_limitTaga');
        var Equation;
        //current
        var current_balanceToTaga = document.getElementById('current_balanceToTaga');
        var current_balanceToYarda = document.getElementById('current_balanceToYarda');
        var current_Total_price = document.getElementById('current_Total_price');
        var current_price = document.getElementById('current_price');


        function loadajax() {
            $(document).ready(function () {
                var fabrics = $("#fabrics_id").val()
                $.ajax({
                    url: '../GetFabrics/ajax/' + fabrics,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        Equation = parseInt(data['Equation']);
                        var price = parseInt(data['price']);
                        /// current

                        current_price.value = parseInt(data['price']);
                        current_Total_price.value = Math.round(price * current_balanceToYarda.value).toFixed(2) ;
                    }
                });

            });
        }
        $(function () {
            setTimeout(loadajax, 1000);
        });


    </script>
    @include('layouts.javascript')
@endsection

