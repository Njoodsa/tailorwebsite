@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">إضافة مستخدم </h2>
                <form action="insertUser" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type='hidden' name='access'  value='2' required>
                                <label> * الايميل  : </label>
                                <input type='text' name='email' class='form-control' value='{{old('email')}}' required>

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * كلمة المرور  : </label>
                                <input type='password' name='password' class='form-control' value='{{old('password')}}' required>

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * العميل  : </label>
                                <select name='employee_datas_id' class='form-control' required>
                                    <option value="">-- اختر العميل -- </option>
                                    @foreach($Employee_data as $employee)
                                        <option value="{{$employee->id}}">{{$employee->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * الفرع   : </label>
                                <select name='branches_id' class='form-control' required>
                                    <option value="">-- اختر الفرع  -- </option>
                                    @foreach($Branches as $Branch)
                                        <option value="{{$Branch->id}}">{{$Branch->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <input type='hidden' name='validity'  value='2' required>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>
@endsection