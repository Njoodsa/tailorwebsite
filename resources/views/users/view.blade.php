@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title"> المستخدمين </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                                <a href="createUser" class="btn btn-success btn-xs "><span
                                            class="glyphicon glyphicon-plus-sign"></span> إضافه</a>
                            </div>
                        </div>
                        <br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder=الاسم disabled></th>
                                <th><input type="text" class="form-control" placeholder=الفرع disabled></th>
                                <th><input type="text" class="form-control" placeholder=الايميل disabled></th>

                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td> {{$user->employee_datas->name}}  </td>
                                    <td> {{$user->branch->name}}  </td>
                                    <td> {{$user->email}}  </td>
                                    <td>
                                        @if($user->access == 2)
                                            <a  href="access0/{{$user->id}}">
                                            <button class="btn btn-danger btn-xs"
                                                    onclick="return confirm('هل انت متاكد ازالة صلاحية   - {{$user->employee_datas->name}}?') ">
                                                <span > ازالة صلاحياته </span>
                                            </button>
                                            </a>
                                            @endif
                                            @if($user->access == 0)
                                                <a  href="access2/{{$user->id}}">
                                                    <button class="btn btn-success btn-xs"
                                                            onclick="return confirm('هل انت متاكد من تمكين الصلاحية   - {{$user->employee_datas->name}}?') ">
                                                        <span > تمكين صلاحياته </span>
                                                    </button>
                                                </a>
                                                @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')

@endsection

