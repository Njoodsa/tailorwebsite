@extends('layouts.app')
<style>
    @media only screen and (max-width: 768px) {
        .image-circular {
            top: 101px!important;
        }
    }
</style>

@section('content')
    <div class="back-login">
        <div class=" back-login2">
            <div class="container ">
                <div class="image-circular ">
                </div>
                <div class="login-form">
                    <div style="margin-top:20%">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label id="login-float" for="email" class="col-md-4 control-label">البريد الالكتروني
                                    <span class="glyphicon glyphicon-envelope"></span></label>
                                <div class="col-md-6 ">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                     </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label id="login-float" for="password" class="col-md-4 control-label">كلمة المرور <span
                                            class="glyphicon  glyphicon-link"></span></label>
                                <div class="col-md-6 col-sm-3">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                     </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">دخول</button>
                                    <a class="btn btn-link" href="{{ route('password.request') }}">نسيت كلمة المرور</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
