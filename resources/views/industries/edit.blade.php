@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.flash')
                <h2 class="main-title">تعديل الصناعه</h2>
                <form action="../updateIndustry/{{$data->id}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label> الاسم : </label>
                            <input type='hidden' name='id' value='{{$data->id}}'>
                            <input type='text' name='name' class='form-control' value='{{$data->name}}' required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        * <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                    </div>
                    <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <label>ملاحظات : </label>
                            <input type='text' name='note' class='form-control' value='{{$data->note}}' >
                            @if ($errors->has('note'))
                                <span class="help-block">
                                        * <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-info">تعديل</button>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection