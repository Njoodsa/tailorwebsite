@component('layouts.include.views')
    @slot('title')
        الصناعه
    @endslot

    @slot('linkAdd')
        <a href="createIndustry" class="btn btn-success btn-xs ">
            <span class="glyphicon glyphicon-plus-sign"></span> إضافه
        </a>
    @endslot

    @slot('thead')
        <th><input type="text" class="form-control" placeholder=الاسم disabled></th>
        <th><input type="text" class="form-control" placeholder=" الملاحظات" disabled></th>
        <th></th>
    @endslot

    @slot('tbody')
        @foreach($data as $industry)
            <tr>
                <td>{{$industry->name}}</td>
                <td>{{$industry->note}}</td>
                <td>
                    <a class="btn btn-info btn-xs" href="editIndustry/{{$industry->id}}"><span
                                class=" glyphicon glyphicon-edit "></span></a>
                </td>
            </tr>
        @endforeach
    @endslot
@endcomponent
