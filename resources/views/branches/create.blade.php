@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">إضافة فرع جديد</h2>
                <form action="{{route('Branch.store')}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label> * الاسم : </label>
                        <input type='text' name='name' class="form-control {{$errors->has('name') ? 'has-error' : ''}}" value='{{old('name') }}' required>
                    </div>
                    <div class="form-group">
                        <label> * الهاتف : </label>
                        <input type='number' name='phone' class='form-control' value='{{old('phone') }}' required>
                    </div>
                    <div class="form-group">
                        <label> جوال : </label>
                        <input type='number' name='cell_phone' class='form-control' value='{{old('cell_phone') }}'>
                    </div>
                    <div class="form-group">
                        <label> فاكس : </label>
                        <input type='number' name='fax' class='form-control' value='{{old('fax') }}'>
                    </div>
                    <div class="form-group">
                        <label> صندوق بريد : </label>
                        <input type='number' name='mailbox' class='form-control' value='{{old('mailbox') }}'>
                    </div>
                    <div class="form-group">
                        <label> العنوان : </label>
                        <input type='text' name='address' class='form-control' value='{{old('address') }}'>
                    </div>
                    <div class="form-group">
                        <label> المخزن  : </label>
                        <select class='form-control' name='warehouses_id' required>
                            <option value="">-- اختر المخزن -- </option>
                            @foreach($warehouses as $warehouse)
                                <option value=" {{$warehouse->id }}"> {{$warehouse->name }} </option>
                            @endforeach
                        </select>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection