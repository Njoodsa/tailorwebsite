@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title"> الفرع </h2>
                <form action="{{route('Branch.update', [$branch->id])}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    {{ @method_field('PATCH') }}
                    <input type="hidden" name="id" value="{{$branch->id}}">
                    <div class="form-group">
                        <label> * الاسم : </label>
                        <input type='text' name='name' class='form-control' value='{{$branch->name }}' required>
                    </div>
                    <div class="form-group">
                        <label> * الهاتف : </label>
                        <input type='number' name='phone' class='form-control' value='{{$branch->phone}}' required>
                    </div>
                    <div class="form-group">
                        <label> جوال : </label>
                        <input type='number' name='cell_phone' class='form-control' value='{{$branch->cell_phone}}'>
                    </div>
                    <div class="form-group">
                        <label> فاكس : </label>
                        <input type='number' name='fax' class='form-control' value='{{$branch->fax }}'>
                    </div>
                    <div class="form-group">
                        <label> صندوق بريد : </label>
                        <input type='number' name='mailbox' class='form-control' value='{{$branch->mailbox}}'>
                    </div>
                    <div class="form-group">
                        <label> العنوان : </label>
                        <input type='hidden' name='address' class='form-control' value='{{$branch->address }}'>
                    </div>
                    <div class="form-group">
                        <label> المخزن  :{{$branch->Warehouses->name }} </label>
                        <select class='form-control' name='warehouses_id' required>
                            <option value="">-- اختر المخزن -- </option>
                            @foreach($warehouses as $warehouse)
                                <option @if($warehouse->id == $branch->warehouses_id) selected @endif
                                value=" {{$warehouse->id }}"> {{$warehouse->name }} </option>
                            @endforeach
                        </select>
                    </div>
                    <br/>

                    <button class="btn btn-block  btn-success">تعديل</button>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection