
@component('layouts.include.views')
    @slot('title')
        الفروع
    @endslot

    @slot('linkAdd')
        <a href="{{route('Branch.create')}}" class="btn btn-success btn-xs "><span
                    class="glyphicon glyphicon-plus-sign"></span> إضافه</a>
    @endslot

    @slot('thead')
        <tr class="filters">
            <th><input type="text" class="form-control" placeholder='اسم الفررع' disabled></th>
            <th><input type="text" class="form-control" placeholder=الهاتف disabled></th>
            <th><input type="text" class="form-control" placeholder=المخزن  disabled></th>
            <th></th>
        </tr>
    @endslot

    @slot('tbody')
        @foreach($data as $Branch)
            <tr>
                <td>{{$Branch->name}}</td>
                <td>{{$Branch->phone}}</td>
                <td>{{$Branch->Warehouses->name}}</td>
                <td>
                    <a class="btn btn-info btn-xs" href="{{route('Branch.edit',$Branch->id)}}"><span
                                class=" glyphicon glyphicon-fullscreen "></span></a>
                </td>
            </tr>
        @endforeach
    @endslot
@endcomponent

