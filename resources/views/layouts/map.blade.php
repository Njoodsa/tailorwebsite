
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDHA-osC-LQFLU_oDYg6VkxGy3pGKN-hAI"></script>
        <script type="text/javascript">


            var map;
            var zoom=5;

            var lat = 24.730165162514723; //default latitude
            var lng = 46.817501068114325; //default longitude
            var homeLatlng = new google.maps.LatLng(lat, lng); //set default coordinates

            var myOptions = {
                center: new google.maps.LatLng(lat, lng), //set map center
                zoom: zoom, //set zoom level to 17
                mapTypeId: google.maps.MapTypeId.ROADMAP //set map type to road map
            };

            map = new google.maps.Map(document.getElementById("map_canvas"),
                myOptions); //initialize the map

            var homeMarker = new google.maps.Marker({ //set marker
                position: homeLatlng, //set marker position equal to the default coordinates
                map: map, //set map to be used by the marker
                draggable: true //make the marker draggable
            });


            //if the position of the marker changes set latitude and longitude to
            //current position of the marker
            google.maps.event.addListener(homeMarker, 'position_changed', function(){
                lat = homeMarker.getPosition().lat(); //set lat current latitude where the marker is plotted
                lng = homeMarker.getPosition().lng(); //set lat current longitude where the marker is plotted
                zoom = map.getZoom();


                document.getElementById('coords').value=lat+","+lng;
                document.getElementById('map_url').value=lat+","+lng+zoom;
            });

            //if the center of the map has changed
            google.maps.event.addListener(map, 'center_changed', function(){
                lat = homeMarker.getPosition().lat(); //set lat to current latitude where the marker is plotted
                lng = homeMarker.getPosition().lng(); //set lng current latitude where the marker is plotted
                zoom = map.getZoom();

                document.getElementById('coords').value=lat+","+lng;
                document.getElementById('map_url').value="http://maps.google.com/?q="+lat+","+lng+"&z="+zoom;
            });

        </script>
