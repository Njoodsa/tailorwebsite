<div >
    <!-- Message for any process-->
    @if($flash=session('messageT'))
        <div id="flash-massage" class=" alert alert-success" role=alert" style="padding: 0px">
            <center><h5> {{$flash}}</h5></center>
        </div>
    @endif
    @if($flash=session('messageF'))
        <div id="flash-massage" class=" alert alert-danger" role=alert">
            <h5> {{$flash}}</h5>
        </div>
@endif
<!--End  Message -->
</div>