<table class="table">
    <thead >

    <tr>
        <th> اسم القماش</th>
        <th>إضافات</th>
        <th > عدد الياردات</th>
        <th> عدد الثياب</th>
        <th> سعر الثوب</th>
        <th> سعر الإضافة</th>
        <th>السعر</th>
    </tr>


    </thead>
    <tbody id="fabric_list">
    <!-- the default one -->
    <tr class="fabric_item">
        <td>
            <select class="id_fabric " data-live-search="true"
                    data-show-subtext="true"
                    name="id_fabric[]" required>
                @foreach($Fabrics as $Fabric)
                    @if($Fabric->fabrics->type =='fabirc')
                        @if($Fabric->current_balance < $Fabric->demand_limit)
                            <option value="{{$Fabric->fabrics_id}}"
                                    disabled>{{$Fabric->fabrics->name}}
                                -نفذ من المخزون -
                            </option>
                        @elseif($Fabric->current_balance ==  $Fabric->demand_limit)
                            <option value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}
                                -٥ ياردة متبقى فقط -
                            </option>
                        @else
                            <option value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}</option>

                        @endif
                    @endif
                @endforeach
            </select>
        </td>
        <td>
            <select class="addition_id" name="addition_id[]" required>
                @foreach($Additions as $Addition)
                    <option value="{{$Addition->id}}">{{$Addition->name}}</option>
                @endforeach
            </select>
        </td>
        <td>
            <input class="form-control yards" name="yards[]" type="text"
                   value="5" required>
        </td>
        <td>
            <input class="form-control thop_count" name="number[]" type="text"
                   value="1" required>
        </td>
        <td>
            <input class="form-control thop_price" name="price[]" type="text"
                   value="" required>
        </td>
        <td>
            <input class="form-control price_addition" name="price_addition[]" type="text"
                   value="0" required disabled>
        </td>
        <td>
            <input class="form-control totalPriceThop" name="totalPriceThop[]"
                   type="text" value="" readonly>
        </td>
        <td>
            <button class="btn btn-danger btn-xs delete_fabric_row" type="button">
                <span class="glyphicon glyphicon-trash"></span></button>
        </td>
    </tr>
    </tbody>
</table>
