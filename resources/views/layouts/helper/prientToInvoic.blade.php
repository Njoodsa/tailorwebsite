<!-- TO PRINT BILL -->
<div id="cn" name="cn" hidden>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <img class="img-responsive" src="{{asset("images/logoman.png")}}" width="20%"
                             style="margin-right: 35%;">
                    </div>
                </div>
                <br><br>
            </div>
        </div>

        <div style="text-align: center">
            <h3>فاتورة مشتريات </h3>
            <h4>Cash/Credit Invoice</h4>
        </div>
        <div>

            <p style="float: left">رقم المورد :{{$data->supplier->phone}}  </p>
            <p>اسم المورد :{{$data->supplier->name}}</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <?php
                            $price = 0;
                            ?>
                            <table class="table table-condensed">
                                <thead>
                                @if($data->type == 'product')
                                <tr>
                                    <td><strong>اسم المنتج  </strong></td>
                                    <td><strong>الكمية  </strong></td>
                                    <td><strong>سعر المنتج </strong></td>
                                    <td><strong>الاجمالي </strong></td>

                                </tr>
                                @else
                                <tr>
                                        <td><strong>اسم القماش </strong></td>
                                        <td><strong>عدد الطاقات </strong></td>
                                        <td><strong>سعر الطاقه</strong></td>
                                        <td><strong>الاجمالي </strong></td>

                                </tr>
                                    @endif
                                </thead>
                                <tbody>

                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                @foreach($data->invoice_items as $item)
                                    <tr>
                                        <input type="hidden" value ='{{$price +=$item->price * $item->number}}'>
                                        @if($data->type == 'product')
                                            <td>{{$item->product->name}}</td>
                                            @else
                                            <td>{{$item->fabrics->name}}</td>
                                            @endif
                                        <td>{{$item->number}}</td>
                                        <td>{{$item->price}}</td>
                                        <td>{{$item->price * $item->number}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td><strong>الاجمالي  </strong></td>
                                    <td ><strong>السعر بعد الخصم  </strong></td>
                                    <td ><strong>المدفوع </strong></td>
                                    <td ><strong>النتبقى   </strong></td>


                                </tr>
                                <tr>
                                    <td><strong>{{$price}} </strong></td>
                                    <td ><strong> {{$data->price}} </strong></td>
                                    <td ><strong> {{$data->paid_up}} </strong></td>
                                    <td ><strong>ا{{$data->Residual}}  </strong></td>


                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div>
                <p style="float: left">توقيع المسئول : </p>
                <p>تاريخ الفاتوره :{{$data->date}} </p>
                <hr>
            </div>
            <div style="text-align: center">
                <p>الرياض - طريق الملك فهد - غرب مكتبة الملك فهد الوطنيه . هاتف : ٠١١٢٠٠٠٥٦٧ جوال : ٠٥٣٠١٠٨٥٤٩</p>
                <p>Riyadh -King Fahad Road- west of king fahad national library. Tel: 011 2000567 Mobile : 0530108549</p>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">

    //print Bill



    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

</script>