<div id="cn" name="cn" hidden>
    <div class="container" >
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <img class="img-responsive" src="{{asset("images/logoman.png")}}" width="20%"
                             style="margin-right: 24%;">
                    </div>
                </div>
                <br><br>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9 col-sm-9 col-xs-9" style="font-size: 10px;  border-left: #ece8e8 1px solid;padding: 3px;     padding-left: 3%;  font-size: 9px;
        ">
                <div class="row" style="margin-top: 4%">
                    <div class="col-lg-4 col-sm-4 col-xs-4">
                        <p >موعد التسليم :{{$data->bill->end_date}} </p>
                        <p>التاريخ :{{$data->created_at}}</p>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-xs-4" style="text-align: center">
                        <p >  رقم الفاتورة - {{$data->id}} </p>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-xs-4" style="text-align: left">
                        <p >اسم العميل :{{$data->customer->name}} </p>
                        <p>رقم العميل :0{{$data->customer->mobile->phone}} </p>
                    </div>
                </div>
            <?php $numberProduct = 0;
            $price = 0;
            $finallyPrice = 0;

            ?>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div>
                            <table class="table table-condensed" style="font-size: 11px;">
                                <thead>
                                <tr align="center">
                                    <td><strong>*  </strong></td>
                                    <td><strong>اسم المنتج  </strong></td>
                                    <td><strong>العدد  </strong></td>
                                    <td><strong>سعر المنتج </strong></td>
                                    <td><strong>الاجمالي </strong></td>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing 1here -->
                                @foreach($data->bill->order as $order)
                                    <tr align="center">
                                        <input type="hidden" value='{{$numberProduct += $order->number}}'>
                                        <td>{{$order->number_column}}</td>
                                        <td>{{$order->products->name}}</td>
                                        <td>{{$order->number}}</td>
                                        <td>{{$order->price}}</td>
                                        <td>{{$order->price*$order->number}}</td>
                                        <input type="hidden" value='{{$price += $order->price*$order->number}}'>
                                    </tr>
                                @endforeach
                                <tr align="center">
                                    <td>  </td>
                                    <td> الإجمالي </td>
                                    <td>  </td>
                                    <td>  </td>
                                    <td> {{$data->bill->price}}</td>
                                </tr>

                                <tr align="center">
                                    <td>  </td>
                                    <td> ضريبة القيمة المضافة  </td>
                                    <td>  </td>
                                    <td>  </td>
                                    <td>{{floor($data->bill->price *0.05)}}</td>
                                </tr>
                                <tr align="center">
                                                                       <td>  </td>
                                                                      <td> الخصم </td>
                                                                       <td>  </td>
                                                                        <td>  </td>

                                                                      <td> {{$data->bill->discount}}</td>
                                                                  </tr>
                                <tr align="center">
                                    <td>  </td>
                                    <td> الإجمالي شامل الضريبة  (5%)
                                        <br>
                                        <h5>الرقم الضريبي (300935822700003)</h5></td>
                                    <td>  </td>
                                    <td>  </td>
                                    <td> {{floor(($data->bill->price - $data->bill->discount)+$data->bill->price*0.05)}}</td>
                                </tr>
                                <tr align="center">
                                    <td>  </td>
                                    <td> المدفوع </td>
                                    <td> {{$data->bill->paid_up + $data->bill->paid_up_card}} </td>
                                    <td>  المتبقي</td>
                                    <td> {{$data->bill->Residual}}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

                <div class="col-md-3 col-xs-3"style="font-size: 8.5px;margin-top: 10%; ">
                    <p>- المرجو من عملالنا االكرام مراجعة بيانات الفاتورة قبل التوقيع و مغادرة المعرض .</p>
                    <p>- الادارة ترحب باي مقترحات أو ملاحظات من العملاء هدفنا ثقتكم بنا ونحن في خدمتكم. </p>
                    <p>- المعرض غير مسئول عن لأقمشة و الملابس أذا مضى عليها ثلاثة شهور. </p>
                    <br>
                    <p>توقيع العميل بالموافقة على الاسعار و المواصفات بناء على رغبته وطلبه. </p>
                    <br>
                    <p ><b>توقيع العميل : </b></p>
                    <p >توقيع المسئول : </p>

                </div>
            </div>

            <div class="row" style="margin-top: 20% ;   position: absolute;  bottom: 0px;     margin-right: 20%;">
                <hr>
                <div  class="col-md-12 col-xs-12 col-lg-12" style="text-align: center ;font-size: 8px">
                    <p style="float: right">
                        الرياض - طريق الملك فهد - غرب مكتبة الملك فهد الوطنيه . هاتف : ٠١١٢٠٠٠٥٦٧ جوال : ٠٥٣٠١٠٨٥٤٩</p>
                    &nbsp; Riyadh -King Fahad Road- west of king fahad national library. Tel: 011 2000567 Mobile : 0530108549

                </div>

            </div>
        </div>
    </div>
</div>
