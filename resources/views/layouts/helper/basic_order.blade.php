<div class="panel panel-info form-group row panel_detail_order">
    <div class="panel-body ">
        <div class="row table-responsive">

            <div class="col-lg-12">
                <table class="table">
                    <thead>
                    <th>*</th>
                    <th> اسم القماش</th>
                    <th>النوع</th>
                    <th>إضافات</th>
                    <th>عدد الياردات</th>
                    <th>عدد الثياب</th>
                    <th>سعر الثوب</th>
                    <th>سعر الإضافة</th>
                    <th>السعر</th>
                    </thead>
                    <tbody id="fabric_list">
                    <!-- the default one -->
                    @foreach($data->bill->order as $order)
                        <tr class="fabric_item">
                            <td style="width: 33px;"><input name='number_column[]' value="{{$order->number_column}}"
                                                            class="number_column" readonly></td>

                            <td>
                                <select name="id_fabric[]"
                                        class="id_fabric  "
                                >
                                    <option>{{$order->fabrics->name}}</option>
                                </select>
                            </td>
                            <td>
                                <select class="type_design" data-live-search="true"
                                        data-show-subtext="true"
                                        name="type_design[]" required>
                                    <option value="" selected="selected">select</option>
                                    <option @if($order->type == 'Thop') selected @endif value="Thop">Thop</option>
                                    <option @if($order->type == 'VEST') selected @endif value="VEST">VEST</option>
                                    <option @if($order->type == 'Pants') selected @endif value="Pants">Pants</option>
                                    <option @if($order->type == 'Sroal') selected @endif value="Sroal">Sroal</option>

                                </select>
                            </td>

                            <td>
                                <select class="addition_id" name="addition_id[]" required>
                                    @foreach($order->additions as $order_addition)
                                        <option>{{$order_addition->name}}</option>
                                    @endforeach
                                </select>
                            </td>

                            <td>
                                <input step="0.01" type='number' name='yards[]' class="form-control"
                                       value='{{$order->yards}}' id="yardh_number"
                                       readonly>
                            </td>
                            <td>
                                <input class="form-control" value='{{$order->number}}'
                                       name="number[]"
                                       id="thop_number"
                                       type="text" readonly class="GivePrice">
                            </td>
                            <td>
                                <input class="form-control" name="price[]" value='{{$order->price}}'
                                       id="price"
                                       type="text" readonly class=" price GivePrice"
                                >
                            </td>
                            <td>
                                <input class="form-control price_addition" name="price_addition[]" type="text"
                                       value="{{$order->additions->sum('price')}}" readonly disabled>
                            </td>
                            <td><input id="totalPriceThop"
                                       name='totalPriceThop[]'
                                       readonly
                                       value='{{$order->price*$order->number + ($order->number *$order->additions->sum('price'))}}'
                                       type="text"

                                       type="text"
                                       readonly class="form-control"></td>
                        </tr>
                        <input type="hidden" value="{{$order->id}}" name="id_order_bill[]">
                    @endforeach


                    </tbody>
                </table>
            </div>
            <div class="col-lg-12 col-xs-12 ">
                <div class="col-lg-3 col-xs-4">
                    العدد : <input class="form-control" id="total_number" readonly>

                </div>
                <div class="col-lg-3 col-xs-4">
                    السعر : <input
                            name="discount" class="form-control cost" readonly>
                </div>

                <div class="col-lg-3 col-xs-4">
                    الضريبة : <input class="form-control" id="tax" readonly>
                </div>
                <div class="col-lg-3 col-xs-4">
                الخصم : <input value='{{$data->bill->discount}}'
                              name="discount"
                              id='discount' class="form-control" readonly>
                </div>
                <div class="col-lg-3 col-xs-6">
                    الاجمالي : <input class=' form-control' id="finally_price" readonly>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class=" col-lg-5">
                <label>الاجمالي يشمل ضريبة القيمة المضافة (5%) </label>
            </div>
        </div>
    </div>
</div>

<!-- BILL -->

<div class="panel panel-info form-group row  panel_detail_order">
    <div class="panel-heading"> الدفع</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3 col-xs-12">
                <label> طريقة الدفع </label>
                <select name="payment_method_id" class="form-control payment_method" readonly>
                    @foreach($payment_methods as $Payment_method)
                        <option @if($data->bill->payment_method_id ==$Payment_method->id)
                                selected
                                @endif value="{{$Payment_method->id}}">{{$Payment_method->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-3 col-xs-6">
                <label>* المدفوع : </label>
                <input class='form-control' name='paid_up' id="paid_up" value='{{$data->bill->paid_up}}' readonly>
            </div>
            <div class="col-lg-3 col-xs-6 paid_up_card" @if($data->bill->payment_method_id != 3) hidden @endif>
                <div class="form-group">
                    <label>* المدفوع / بطاقة : </label>
                    <input type='number' step="0.01" id="paid_up_card" name='paid_up_card' class='form-control'
                           value='{{$data->bill->paid_up_card}}' readonly>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <label>* المتبقي : </label>
                <input type='number' @if($data->state != 4) id="Residual" @endif name='Residual' class='form-control'
                       @if($data->state == 6) value=0 @else value= '{{$data->bill->Residual}}' @endif
                       >
            </div>

            <input type="hidden" name='bill_id' value="{{$data->bill->id }}"
                   readonly>
        </div>

        @if($data->state == 4 || $data->state == 6)
        <div  class="row" style="margin-top:1%">
            <div class="col-lg-3 col-xs-6">
                <label>* المدفوع آجل بطاقة: </label>
                <input class='form-control' type='number' step="0.01"  name='paid_card_later'  value='{{$data->bill->paid_card_later}}' >
            </div>
            <div class="col-lg-3 col-xs-6 " >
                    <label>* المدفوع آجل كاش   : </label>
                    <input type='number' step="0.01"  name='paid_later' class='form-control'
                           value='{{$data->bill->paid_later}}' >
            </div>
            <div class="col-lg-3 col-xs-6 " >
                    <label>*  طريقة دفع اخرى     : </label>
                    <input type='number' step="0.01"  name='other_payment_method' class='form-control'
                           value='{{$data->bill->other_payment_method}}' >
            </div>
        </div>
        @endif
    </div>
</div>
<script>


    //FUNCTION WILL WORKE AFTER LOAD PAGE

    $(window).bind("load", function () {
        var total = 0;
        $('input[name="number[]"]').each(function () {
            total += parseInt($(this).val());
            console.log(total);
        });
        $('#total_number').val(total);
        console.log(total);
    });
</script>
