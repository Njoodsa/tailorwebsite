@include('layouts.helper.style_prinet')
<div id="div_2" hidden>
    <div class="row"  style="margin-right:3%">
        <div class="col-xs-4" >العميل  : <b> {{$data->customer->name}} </b></div>
        <div class="col-xs-4">رقم جوال : <b> {{$data->customer->mobile->phone}} </b></div>
        <div class="col-xs-4">رقم الطلب: <b> {{$data->id}} </b></div>
    <hr>
  </div>
</div>
<div class="row table-responsive" id="orders" hidden>
  <div class="col-lg-2 col-xs-2 hide_to_phone" >
    <img src="{{asset('images/thop_print2.jpg')}}" height="150px" style="float: right;margin-top: 80px">
  </div>
  <div name="order" class="col-lg-10 col-xs-10" >
    @foreach($data->order_detail as $item)
        <?php $measurement = $item->measurement; ?>
      <p style="font-size: 9px; margin-top:10px"> رقم التفصيل: {{$item->number_column}}</p>
            <table class="table-bordered M_R" style="font-size: 12px;margin-top: 10px;">
                <tr align="center">
                    @if($item->fabric != null)
                        <td class="col-lg-2 col-xs-2">{{$item->fabric->name}}</td>
                    @else
                        <td class="col-lg-2 col-xs-2">Fabric : -</td>
                    @endif
                    <td class="col-lg-2 col-xs-2 "> {{$item->design->name}}  </td>
                    <td class="col-lg-2 col-xs-2 "> STYLE</td>
                    @if($item->design->has_bar_pocket == 0)
                        <td class="col-lg-1 col-xs-1"> {{ $measurement->Sada}}</td>
                        <td class="col-lg-1 col-xs-1">H</td>
                         <td class="col-lg-1 col-xs-1">{{$measurement->Armhole}}</td>
                        <td class="col-lg-2 col-xs-1">W</td>
                        <td class="col-lg-2 col-xs-1"> {{$item->pocket->name}} </td>
                        <td class="col-lg-1 col-xs-1.5">POCKET </td>
                    @else
                        <td class="col-lg-2 col-xs-2"> -</td>
                        <td class="col-lg-1 col-xs-1">H</td>
                        <td class="col-lg-2 col-xs-2"> -</td>
                        <td class="col-lg-2 col-xs-2">W</td>
                        <td class="col-lg-2 col-xs-2"> -</td>
                        <td class="col-lg-3 col-xs-2 ">POCKET </td>

                    @endif
                </tr>
                <tr align="center">
                    <td rowspan="" class="col-lg-2 col-xs-2">: Note {{$measurement->note}}</td>
                    <td class="col-lg-2 col-xs-2">{{$item->button_detail}}</td>
                    <td class="col-lg-2 col-xs-2" >FILLING BUTTON</td>
                    @if($item->design->has_bar_pocket == 0)
                        <td class="col-lg-1 col-xs-1">{{$measurement->FakeButton}}</td>
                        <td class="col-lg-1 col-xs-1">H</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->Bar}}</td>
                        <td class="col-lg-1 col-xs-1">W</td>
                        <td class="col-lg-2 col-xs-2">  {{$item->button->name}}  </td>
                        <td class="col-lg-2 col-xs-1"> BAR</td>
                    @else
                        <td class="col-lg-2 col-xs-2"> -</td>
                        <td class="col-lg-1 col-xs-1">H</td>
                        <td class="col-lg-2 col-xs-2"></td>
                        <td class="col-lg-1 col-xs-1">W</td>
                        <td class="col-lg-2 col-xs-2"> -</td>
                        <td class="col-lg-2 col-xs-1"> BAR</td>
                    @endif
                </tr>
                <tr align="center">
                    <td rowspan="" class="col-lg-2 col-xs-2"> Button neck {{$item->button_neck}}</td>
                    <td class="col-lg-2 col-xs-2">  {{$item->collar_detail}} </td>
                    <td class="col-lg-2 col-xs-2">FILLING COLLAR</td>
                    <td class="col-lg-2 col-xs-2"> {{ $measurement-> ElbowSlvSada}}</td>
                    <td class="col-lg-1 col-xs-1"> E</td>
                    <td class="col-lg-2 col-xs-2"> {{$measurement->SlvSada}}</td>
                    <td class="col-lg-1 col-xs-1"> M</td>
                    <td class="col-lg-2 col-xs-2"> {{$item->sleeve->name}} </td>
                    <td class="col-lg-2 col-xs-2">SELLVES</td>
                </tr>
                <tr align="left">
                    <td  colspan="2" class="col-lg-2 col-xs-2 ">

                        @if($item->detail_pocket == 'Right pocket')
                            R
                        @elseif($item->detail_pocket == 'Left pocket')
                            L
                        @elseif($item->detail_pocket == 'Left and right pocket')
                            L&R
                        @elseif($item->detail_pocket == 'Without')
                            Without
                        @endif
                    </td>
                    <td class="col-lg-2 col-xs-2 ">INSIDE POCKET</td>
                    <td colspan="3" class="col-lg-2 col-xs-2 ">{{ $measurement->SnpIN}}</td>
                    <td class="col-lg-2 col-xs-2">H</td>
                    <td class="col-lg-3 col-xs-3"> {{$item->neck->name }} </td>
                    <td class="col-lg-2 col-xs-1">NECK</td>
                </tr>
            </table>

    @endforeach
  </div>
</div>
