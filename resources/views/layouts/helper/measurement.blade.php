<style>
    .table_width {
        padding: 1px;
        width: 48px;
    }

</style>
<div class="panel panel-info form-group row panel_detail_order specific_details_item">
    <div class="panel-heading"> القياسات</div>
    <div class="specific_details_body panel-body">
        <div class="form-group row table-responsive">
            <div class="col-lg-12 col-xs-12">
                <table class="table-bordered " style="font-size: 12px;">
                    <tr align="left">
                        <td class="col-lg-2 col-xs-2">121</td>
                        <td class="col-lg-2 col-xs-2">NECK</td>
                        <td class="col-lg-2 col-xs-2">SHL</td>
                        <td class="col-lg-2 col-xs-2">SLV</td>
                        <td class="col-lg-2 col-xs-2">CHEST</td>
                        <td class="col-lg-2 col-xs-2">WAIST</td>
                        <td class="col-lg-2 col-xs-2">LENGTH</td>
                        <td class="col-lg-2 col-xs-2">HIPS</td>
                        <td class="col-lg-2 col-xs-2">BOTTOM</td>
                    </tr>
                    <tr align="left">
                        <td class="col-lg-2 col-xs-2">
                            <input step="0.01" type='number' class='table_width col-lg-2 col-xs-2' value='1'
                                   name="MIDDLE">
                        </td>
                        <td class="col-lg-2 col-xs-2">
                            <input type="text" class="table_width col-lg-2 col-xs-2" value='1' name="CollarSada">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="Shl">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="Slv_w">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="Chest">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="Waist">
                        </td>
                        <td class="col-lg-2 col-xs-2{{$errors->has('FrontLonger') ? ' has-error' : '' }}"><input
                                    step="0.01" type='number' class='table_width' value="1" name="FrontLonger"></td>

                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="HIPS">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="Bottom">
                        </td>

                    </tr>
                    <tr align="left">
                        <td class="col-lg-2 col-xs-2"></td>

                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="CollarGalab"></td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="ShlL">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input type="text" class='table_width' value="1" name='Cuffs'>
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="FrontChest">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="Back">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="Length">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="SIDE">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="ShlR">
                        </td>
                    </tr>
                </table>
                <hr>
                <table class="table-bordered hide_print" style="font-size: 12px;margin-top: 5px;margin-right:40px">
                    <tr align="left">
                        <td class="col-lg-2 col-xs-2"><input type="text" class='table_width' value="1" name="Sada">
                        </td>
                        <td colspan="2" class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                                         value="1" name="Armhole">
                        </td>

                        <td class="col-lg-2 col-xs-2"> POCKET
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="ElbowSlvSada">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="SlvSada">
                        </td>

                        <td class="col-lg-2 col-xs-2"> SELLVES
                        </td>
                    </tr>
                    <tr align="left">

                        <td class="col-lg-2 col-xs-2"><input type="text" name="note" value="1"></td>
                        <td class="col-lg-2 col-xs-2">NOTE</td>
                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="SnpIN"></td>
                        <td class="col-lg-2 col-xs-2">NECK</td>

                        <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="1"
                                                             name="FakeButton">
                        </td>
                        <td class="col-lg-2 col-xs-2"><input type="text" class='table_width' value="1" name="Bar">
                        </td>
                        <td class="col-lg-2 col-xs-2">BAR</td>

                    </tr>
                </table>
                <hr>
                <div class="row" style="text-align: center ;margin-right: 30%">
                    <div class="col-lg-6 col-xs-6{{ $errors->has('id_cutter') ? 'has-error' : '' }}">
                        <label>اخذالمقاسات </label>
                        <select name="id_cutter" class="form-control" required>
                            <option selected disabled>اختر القصاص</option>
                            @foreach($cutters as $cutter )
                                <option value="{{$cutter->id}}">{{$cutter->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('id_cutter'))
                            <span class="help-block">
                                                              <strong>{{ $errors->first('id_cutter') }}</strong>
                                                        </span>
                        @endif
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>