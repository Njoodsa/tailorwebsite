<div class="panel panel-info form-group row panel_detail_order">
    <div class="panel-body toggle_specific_details">
        <div class="row">

            <div class="col-lg-12 table-responsive">
                <table class="table">
                    <thead>
                    <th> اسم المنتج</th>
                    <th>الكمية</th>
                    <th>سعر المنتج</th>
                    <th>الإجمالي</th>
                    </thead>
                    <tbody id="fabric_list">
                    <!-- the default one -->
                    @foreach($data->bill->order as $order)
                        <tr class="fabric_item">
                            <td>
                                <select name="id_fabric[]"
                                        class="id_fabric  ">
                                    <option>{{$order->products->name}}</option>
                                </select>
                            </td>

                            <td>
                                <input class="form-control" value='{{$order->number}}'
                                       name="number[]"
                                       id="thop_number"
                                       type="text" class="GivePrice">
                            </td>
                            <td>
                                <input class="form-control" name="price[]" value='{{$order->price}}'
                                       id="price"
                                       type="text" class=" price GivePrice"
                                >
                            </td>
                            <td><input id="totalPriceThop"
                                       name='totalPriceThop[]'
                                       value='{{$order->price*$order->number }}' type="text"

                                       type="text"
                                       readonly class="form-control"></td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
                <div class="col-lg-3 col-xs-6">
                    العدد : <input id="total_number">

                </div>
                <div class="col-lg-3  col-xs-6">
                    السعر : <input class="form-control" value='{{$data->bill->price}}'
                                   step="0.01" name="discount" readonly>
                </div>
                <div class="col-lg-3  col-xs-6">
                الخصم : <input value='{{$data->bill->discount}}'
                               class="form-control" name="discount"
                               id='discount' readonly>
                </div>
                <div class="col-lg-3  col-xs-6">
                    الضريبة <input class="form-control" id="tax"
                                   value='{{number_format((float)($data->bill->price * 0.05), 2, '.', '')}}' readonly>

                </div>
                <div class="col-lg-3  col-xs-6">
                    الاجمالي: <input step="0.01" class='finally_price form-control'
                                     value="{{number_format((float)(($data->bill->price- $data->bill->discount )+ $data->bill->price*0.05), 2, '.', '')}}">
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class=" col-lg-5">
                <label>الاجمالي يشمل ضريبة القيمة المضافة (5%) </label>
            </div>
        </div>
    </div>
</div>

<!-- BILL -->

<div class="panel panel-info form-group row panel_detail_order">
    <div class="panel-heading"> الدفع</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <label> طريقة الدفع </label>
                <input value="{{$data->bill->payment_method->name}}"
                       class='form-control'
                       readonly
                >

            </div>
            <div class="col-lg-3 col-xs-6">
                <label>* المدفوع : </label>
                <input class='form-control' value='{{$data->bill->paid_up}}'
                       readonly>
            </div>
            <div class="col-lg-3 col-xs-6 paid_up_card" @if($data->bill->payment_method_id != 3) hidden @endif>
                <div class="form-group">
                    <label>* المدفوع / بطاقة : </label>
                    <input type='number' step="0.01" id="paid_up_card" name='paid_up_card' class='form-control'
                           value='{{$data->bill->paid_up_card}}'>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <label>* المتبقي : </label>
                <input type='number' name='Residual' class='form-control'
                       value='{{$data->bill->Residual}}'
                       required>
            </div>
            <input type="hidden" name='bill_id' value="{{$data->bill->id }}"
                   readonly>
        </div>
    </div>
</div>
<script>


    //FUNCTION WILL WORKE AFTER LOAD PAGE

    $(window).bind("load", function () {
        var total = 0;
        $('input[name="number[]"]').each(function () {
            total += parseInt($(this).val());
            console.log(total);
        });
        $('#total_number').val(total);
        console.log(total);
    });
</script>