
<div id="cn" name="cn" hidden>
    <div class="container" >
        <div class="row">
            <div class="col-xs-12">
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <img class="img-responsive" src="{{asset("images/logoman.png")}}" width="20%"
                             style="margin-right: 37%;">
                    </div>
                </div>
                <br><br>
            </div>
            <div style="text-align: center">
                <h3>تقرير الفواتير  </h3>
                <p > الفترة من :
                    <b>  {{$first_date}} الى  {{$second_date}}</b>
                </p>

            </div>
        </div>
        <div style="font-size: 0.5cm;">

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed" style="font-size: 0.5cm;">
                                <thead>
                                <tr align="center">
                                    <th> الطلب </th>
                                    <th> الحالة  </th>
                                    <th> العميل </th>
                                    <th> المدفوع/ كاش  </th>
                                    <th>  بطاقة   </th>
                                    <th> اجل /كاش</th>
                                    <th>  بطاقة   </th>
                                    <td>اخرى</td>
                                    <th>المتبقي  </th>
                                    <th> الإجمالي    </th>
                                    <th>طريقة الدفع   </th>
                                    <th> عدد الثياب   </th>


                                </tr>
                                </thead>
                                <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing 1here -->
                                @foreach($data as $Order)
                                    <tr>
                                        @if($Order->state == 3)
                                            <td>

                                                    {{$Order->id}}
                                            </td>
                                            <td>قيد العمل </td>
                                        @elseif($Order->state == 4)
                                            <td>
                                                {{$Order->id}}

                                            </td>
                                            <td> منجز  </td>
                                        @elseif($Order->state == 6 && $Order->type == 'product')
                                            <td>
                                                {{$Order->id}}

                                            </td>
                                            <td>مستلم  </td>
                                        @elseif($Order->state == 6 && $Order->type == 'fabirc')
                                            <td>
                                                {{$Order->id}}

                                            </td>
                                            <td>مستلم  </td>
                                        @endif
                                        <td>{{$Order->customer->name}}</td>
                                             <td>{{$Order->bill->paid_up}}</td>
                                             <td>{{$Order->bill->paid_up_card }}</td>
                                             <td>{{$Order->bill->paid_later }}</td>
                                             <td>{{$Order->bill->paid_card_later}}</td>
                                             <td>{{$Order->bill->other_payment_method}}</td>
                                             @if($Order->state == 6 )
                                                 <td>0</td>
                                            @elseif($Order->state !=5 && $Order->state != 6 )
                                                  <td>{{ $Order->bill->Residual}}</td>

                                             @endif
                                            <td>{{((($Order->bill->price )+$Order->bill->price*0.05)- $Order->bill->discount)}}</td>
                                        <td>{{$Order->bill->payment_method->name}}</td>
                                            <td>{{ $Order->bill->order->sum('number')}}</td>

                                    </tr>
                                         @endforeach

                                </tbody>
                            </table>
                            <br/>
                            <table class="table table-condensed" style="font-size: 0.5cm;">
                                <tbody>
                                <tr align="right">
                                    <td> مجموع المدفوع : {{$totalPaid }} </td>
                                    <td>   مجموع المتبقي: {{$total_residual_not_deliver}}</td>
                                    <td>  عدد الثياب: {{$totalThop}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div style="text-align: center">
                <p>الرياض - طريق الملك فهد - غرب مكتبة الملك فهد الوطنيه . هاتف : ٠١١٢٠٠٠٥٦٧ جوال : ٠٥٣٠١٠٨٥٤٩</p>
                <p>Riyadh -King Fahad Road- west of king fahad national library. Tel: 011 2000567 Mobile : 0530108549</p>
            </div>
        </div>
    </div>
</div>
