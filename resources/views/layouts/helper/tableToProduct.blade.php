<table class="table">
    <thead >

    <tr>
        <th> اسم المنتج </th>
        <th> العدد </th>
        <th> السعر </th>
        <th>الإجمالي</th>
    </tr>


    </thead>
    <tbody id="fabric_list">
    <!-- the default one -->
    <tr class="fabric_item">
        <td>
            <select class="id_fabric " data-live-search="true"
                    data-show-subtext="true"
                    name="id_fabric[]" required>
                @foreach($Fabrics as $Fabric)
                    @if($Fabric->fabrics->type =='fabirc')
                        @if($Fabric->current_balance < $Fabric->demand_limit)
                            <option value="{{$Fabric->fabrics_id}}"
                                    disabled>{{$Fabric->fabrics->name}}
                                -نفذ من المخزون -
                            </option>
                        @elseif($Fabric->current_balance ==  $Fabric->demand_limit)
                            <option value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}
                                -٥ ياردة متبقى فقط -
                            </option>
                        @else
                            <option value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}</option>

                        @endif
                    @endif
                @endforeach
            </select>
        </td>

        <td>
            <input class="form-control countPoduct" name="number[]" type="text"
                   value="1" required>
        </td>
        <td>
            <input class="form-control product_price" name="price[]" type="text"
                   value="" required>
        </td>
        <td>
            <input class="form-control totalPriceThop" name="totalPriceProuduct[]"
                   type="text" value="" readonly>
        </td>
        <td>
            <button class="btn btn-danger btn-xs delete_fabric_row" type="button">
                <span class="glyphicon glyphicon-trash"></span></button>
        </td>
    </tr>
    </tbody>
</table>
