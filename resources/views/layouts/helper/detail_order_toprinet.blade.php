<input type="hidden"value="{{$item->id}}" name="id_order[]">
<div class="panel panel-info form-group row panel_detail_order specific_details_item" >

    <div id='detail' class="panel-heading" >

        <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>&nbsp;

        تفاصيل الطلب
        <input name='number_detail[]' class="input_number" value="{{$item->number_column}}">

    </div>
    <div class="panel-body specific_details_body" >
        <div class='row'>
            <div class="col-lg-3 col-xs-4">
                <div id="titleInOrder"> Style </div>
                <label>{{$item->design->name}}</label>
            </div>
            <div class="col-lg-3 col-xs-4">
                <div id="titleInOrder"> Neck</div>
                <label>{{$item->neck->name }}</label>
            </div>
            <div class="col-lg-3 col-xs-4">
                <div id="titleInOrder"> Sleeves</div>
                <label>{{$item->sleeve->name}}</label>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div id="titleInOrder"> Button neck</div>
                <label>{{$item->button_neck}}</label>
            </div>
        </div>
        <div class='row'>
            <div class="col-lg-3 col-xs-3">
                <div id="titleInOrder"> Filling button</div>
                <label>{{$item->button_detail}}</label>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div id="titleInOrder"> Inside pocket</div>
                <label>{{$item->detail_pocket}}</label>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div id="titleInOrder"> Filling collar</div>
                <label>{{$item->collar_detail}}</label>
            </div>
            @if($item->fabric != null)
                <div class="col-lg-3 col-xs-3">
                    <div style="width: 66%;" id="titleInOrder">Fabric </div>
                    <label> {{$item->fabric->name}}</label>
                </div>
            @endif

        </div>
        <div class='row'>
            @if($item->design->has_bar_pocket == 0)
            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder"> Pocket </div>
                <lable>{{$item->pocket->name}}</lable>
            </div>
            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder">  Bar</div>
                {{$item->button->name}}
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder"> Start Date</div>
                <label> Start Date : {{$data->bill->created_at}}</label>
            </div>
            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder"> Due Date</div>
                <label> {{$data->bill->end_date}}</label>
            </div>
        </div>
    </div>
    <button class="btn btn-info btn-xs btn-print"  type="button" onclick="test('div_measurement')">طباعة الطلب </button>

</div>
@include('layouts.helper.print_bill_customer')

<script type="text/javascript">
    function test(measurnment) {

        var div_1 = document.getElementById('div_1');
        var div_2 = document.getElementById('div_2');
        var div_3 = document.getElementById(measurnment);
        var div_4 = document.getElementById('orders');
        var combined = document.createElement('div');
        var printContents = combined.innerHTML = div_1.innerHTML + " " + div_2.innerHTML + " " +
            div_3.innerHTML + " " + div_4.innerHTML; // a little spacing
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

    }
    $(".my_selection").change(function () {
        window.open( this.children[this.selectedIndex].getAttribute('href') );
        return false;
    })
</script>
