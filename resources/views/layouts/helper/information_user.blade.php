
    <div class="panel panel-success panel_detail_order">
        <div class="panel-heading"> العميل
            <div style="float: left;"><a href="../editCustomer/{{$data->customer->id}}">رؤية قياسات العميل </a> </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-4">
                    <label> * الاسم : </label>
                    <input type='text' name='name' class='form-control'
                           value="{{$data->customer->name}}" readonly>
                </div>
                <div class="col-lg-4">
                    <label>* جوال : </label>
                    <input type='number' name='phone' class='form-control'
                           value='{{$data->customer->mobile->phone}}' readonly>
                </div>
                <div class="col-lg-4">
                    <label> ملاحظات  : </label>
                    <input type='text' name='name' class='form-control'
                           value="{{$data->customer->note}}" readonly>
                </div>
            </div>
        </div>
    </div>