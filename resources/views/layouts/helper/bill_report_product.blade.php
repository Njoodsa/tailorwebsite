
<div id="cn" name="cn" hidden>
    <div class="container" >
        <div class="row">
            <div class="col-xs-12">
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <img class="img-responsive" src="{{asset("images/logoman.png")}}" width="20%"
                             style="margin-right: 37%;">
                    </div>
                </div>
                <br><br>
            </div>
            <div style="text-align: center">
                <h3>تقرير الفواتير  </h3>
                <p > الفترة من :
                    <b>  {{$first_date}} الى  {{$second_date}}</b>
                </p>

            </div>
        </div>
        <div style="font-size: 0.5cm;">

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed" style="font-size: 0.5cm;">
                                <thead>
                                <tr align="center">
                                    <th>رقم الطلب </th>
                                    <th> الحالة  </th>
                                    <th>اسم العميل </th>
                                    <th>تاريخ الطلب</th>
                                    <th> المدفوع/ كاش  </th>
                                    <th>  بطاقة   </th>
                                    <th>المتبقي  </th>
                                    <th>طريقة الدفع   </th>

                                </tr>
                                </thead>
                                <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing 1here -->
                                @foreach($data as $Order)
                                    <tr>
                                        @if($Order->state == 7)
                                            <td>

                                                {{$Order->id}}
                                            </td>
                                            <td> غير مكتمل</td>
                                        @elseif($Order->state == 6)
                                            <td>
                                                {{$Order->id}}

                                            </td>
                                            <td>مستلم</td>
                                        @endif
                                        <td>{{$Order->customer->name}}</td>
                                        <td>{{ $Order->created_at}}</td>
                                        @if($Order->state == 6 )
                                            <td>{{$Order->bill->paid_up + $Order->bill->Residual}}</td>
                                            <td>{{$Order->bill->paid_up_card }}</td>
                                            <td>0</td>
                                        @else
                                            <td>{{$Order->bill->paid_up}}</td>
                                            <td>{{$Order->bill->paid_up_card }}</td>
                                            <td>{{ $Order->bill->Residual}}</td>
                                        @endif
                                        <td>{{$Order->bill->payment_method->name}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <br/>
                            <table class="table table-condensed" style="font-size: 0.5cm;">
                                <tbody>
                                <tr align="right">
                                    <td> مجموع المدفوع : {{$totalPaid + $residual_order_deliver}} </td>
                                    <td>   مجموع المتبقي: {{$total_residual_not_deliver}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div style="text-align: center">
                <p>الرياض - طريق الملك فهد - غرب مكتبة الملك فهد الوطنيه . هاتف : ٠١١٢٠٠٠٥٦٧ جوال : ٠٥٣٠١٠٨٥٤٩</p>
                <p>Riyadh -King Fahad Road- west of king fahad national library. Tel: 011 2000567 Mobile : 0530108549</p>
            </div>
        </div>
    </div>
</div>