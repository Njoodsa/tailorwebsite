<style>
    .table_width {
        padding: 1px;
        width: 48px;
    }

    #date_print {
        margin-right: 70%;
        margin-bottom: -20px;
        font-size: 11px;
    }
    .M_R{
        font-size: 10px;
        margin-right: -21%;
    }
    td.col-lg-2.col-xs-2 {
        /* padding: 11px; */
        padding-right: 8px;
    }


    @media print {

        .col-xs-2 > img {
            margin-right: 0px !important;
        }

        input {
            border: none !important;
            box-shadow: none !important;
            outline: none !important;
            text-align: center;
        }

        .hide_print {
            display: none;
            font-size: 12px;
            margin-top: 5px;
            margin-right: 40px
        }

        #margin_print {
            margin-right: -20px;
        }

        td.col-lg-2.col-xs-2 {
            padding-right: 0px;
            padding-left: 0px;
            text-align: center;
            padding-bottom: 4px;
            padding-top:4px;
        }
        .table{
            margin-right: 40px;


        }

        .hide_to_phone {
            display: block;
        }

        .cutter_date {
            font-size: 12px;
            margin-top: 7px;
            margin-right: 30px!important;
        }
        .M_R{
            table-layout: fixed;
            width: 770px;
            font-size: 13px!important;
            margin-right: 0%;
        }

    }
</style>
<?php $measurement = $data->order_detail[0]->measurement ?>
<div class="row">
  <div class="col-lg-12 col-xs-12 cutter_date">
      <input class='table_width' type="hidden" name="measurement_id" value="{{$measurement->id}}">&nbsp; قياسات بتاريخ
      {{$measurement->created_at}}
  </div>
  <br><br><br>
    <div class="col-lg-2 col-xs-2 " >
    </div>
  <div class="col-lg-10 col-xs-10" >
      <table class="table-bordered M_R" >
          <tr align="left">
              <td class="col-lg-2 col-xs-2">
                <input step="0.01" type='number' class='table_width'
                       value="{{$measurement->Shafa121}}" name="Shafa121">
              </td>
              <td class="col-lg-2 col-xs-2">(SHAFA(121</td>
              <td class="col-lg-2 col-xs-2">NECK</td>
              <td class="col-lg-2 col-xs-2">SHL</td>
              <td class="col-lg-2 col-xs-2">SLV</td>
              <td class="col-lg-2 col-xs-2">CHEST</td>
              <td class="col-lg-2 col-xs-2">WAIST</td>
              <td class="col-lg-2 col-xs-2">LENGTH</td>
              <td class="col-lg-2 col-xs-2">HIPS</td>
              <td class="col-lg-2 col-xs-2">BOTTOM</td>
          </tr>
          <tr align="left">
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{ $measurement->Cuff57}}" name="Cuff57">
                  </td>
              <td class="col-lg-2 col-xs-2">(CUFF(57</td>
              <td class="col-lg-2 col-xs-2">
                  <input type="text" class="table_width col-lg-2 col-xs-2" value='{{ $measurement->CollarSada}}'
                         name="CollarSada">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{ $measurement->Shl}}" name="Shl">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{ $measurement->Slv_w}}" name="Slv_w">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->Chest}}" name="Chest">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->Waist}}" name="Waist">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->FrontLonger}}" name="FrontLonger"></td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->HIPS}}" name="HIPS">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{ $measurement->Bottom}}" name="Bottom">
              </td>

          </tr>
          <tr align="left">
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->Elpw}}" name="Elpw">
                 </td>
              <td class="col-lg-2 col-xs-2">ELPW </td>

              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->CollarGalab}}" name="CollarGalab"></td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->ShlL}}" name="ShlL">
              </td>
              <td class="col-lg-2 col-xs-2"><input type="text" class='table_width' value="{{ $measurement->Cuffs}}"
                                                   name='Cuffs'>
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{ $measurement->FrontChest}}" name="FrontChest">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{ $measurement->Back}}" name="Back">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->Length}}" name="Length">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->SIDE}}" name="SIDE">
              </td>
              <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                   value="{{$measurement->ShlR}}" name="ShlR">
              </td>
          </tr>
      </table>
  </div>
      <div class="hide_print">
          <div class="col-lg-12 col-xs-12" style="margin-top: 10px;">
          <table class="table-bordered " style="font-size: 12px;">
              <tr align="left">

                  <td class="col-lg-2 col-xs-2"><input type="text" class='table_width' value="{{ $measurement->Sada}}"
                                                       name="Sada">
                  </td>
                  <td class="col-lg-1 col-xs-1">H</td>
                  <td  class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width' value="{{$measurement->Armhole}}" name="Armhole">
                  </td>
                  <td class="col-lg-1 col-xs-1">W</td>
                  <td class="col-lg-2 col-xs-2"> POCKET
                  </td>

                  <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                       value="{{$measurement->FakeButton}}" name="FakeButton">
                  </td>
                  <td class="col-lg-1 col-xs-1">H</td>
                  <td class="col-lg-2 col-xs-2"><input  name ="Bar" type="text" class='table_width' value="{{ $measurement->Bar}}">
                  </td>
                  <td class="col-lg-1 col-xs-1">W</td>
                  <td class="col-lg-2 col-xs-2">BAR</td>
              </tr>
              <tr align="left">

                  <td class="col-lg-2 col-xs-2"><input type="text" name="note" value="{{$measurement->note}}"></td>
                  <td class="col-lg-2 col-xs-2">NOTE</td>
                  <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                       value="{{ $measurement->SnpIN}}" name="SnpIN"></td>
                  <td class="col-lg-1 col-xs-1">W</td>
                  <td  class="col-lg-2 col-xs-2">NECK</td>

                  <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                       value="{{ $measurement-> ElbowSlvSada}}" name="ElbowSlvSada">
                  </td>
                  <td class="col-lg-1 col-xs-1">E</td>
                  <td class="col-lg-2 col-xs-2"><input step="0.01" type='number' class='table_width'
                                                       value="{{$measurement->SlvSada}}" name="SlvSada">
                  </td>
                  <td class="col-lg-1 col-xs-1">M</td>
                  <td class="col-lg-2 col-xs-2">SELLVES</td>

              </tr>
          </table>
      </div>
  </div>
</div>
