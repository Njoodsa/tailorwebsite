
        <div class="form-group row table-responsive">
            <div class="col-lg-12 col-xs-12">
                <table class="table-bordered " style="font-size: 12px;">
                    <tr align="left">
                        <td   class="col-lg-2 col-xs-2">{{$measurement->Shafa121}}</td>
                        <td   class="col-lg-2 col-xs-2">Shafa(121)</td>
                        <td   class="col-lg-2 col-xs-2">NECK</td>
                        <td   class="col-lg-2 col-xs-2">SHL</td>
                        <td   class="col-lg-2 col-xs-2">SLV</td>
                        <td   class="col-lg-2 col-xs-2">CHEST</td>
                        <td  class="col-lg-2 col-xs-2">WAIST</td>
                        <td  class="col-lg-2 col-xs-2">LENGTH</td>
                        <td  class="col-lg-2 col-xs-2">HIPS</td>
                        <td  class="col-lg-2 col-xs-2">BOTTOM</td>
                    </tr>
                    <tr align="left">
                        <td class="col-lg-2 col-xs-2">{{ $measurement->Cuff57}}</td>
                        <td class="col-lg-2 col-xs-2">Cuff(57)</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->CollarSada}}</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->Shl}}</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->Slv_w}}</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->Chest}}</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->Waist}} </td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->FrontLonger}}</td>
                        <td class="col-lg-2 col-xs-2"> {{ $measurement->HIPS}}</td>
                        <td class="col-lg-2 col-xs-2"> {{ $measurement->Bottom}} </td>

                    </tr>
                    <tr align="left">
                        <td class="col-lg-2 col-xs-2">{{$measurement->Elpw}}</td>
                        <td class="col-lg-2 col-xs-2">Elpw</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->CollarGalab}}</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->ShlL}}</td>
                        <td class="col-lg-2 col-xs-2"> {{ $measurement->Cuffs}} </td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->FrontChest}}</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->Back}}</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->Length}}</td>
                        <td class="col-lg-2 col-xs-2"> {{ $measurement->SIDE}}</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->ShlR}}</td>
                    </tr>
                </table>
                <hr>
                <table class="table-bordered hide_print" style="font-size: 12px;margin-top: 5px;margin-right:40px" >
                    <tr align="left">
                        <td colspan="2" class="col-lg-2 col-xs-2">{{ $measurement->Sada}}</td>
                        <td  class="col-lg-2 col-xs-2">{{ $measurement->Armhole}} </td>
                        <td class="col-lg-2 col-xs-2"> POCKET</td>
                        <td class="col-lg-2 col-xs-2">{{$measurement->ElbowSlvSada}}</td>
                        <td class="col-lg-2 col-xs-2">{{$measurement->SlvSada}} </td>
                        <td class="col-lg-2 col-xs-2"> SELLVES
                        </td>
                    </tr>
                    <tr align="left">

                        <td    class="col-lg-2 col-xs-2">{{$measurement->note}}</td>
                        <td class="col-lg-2 col-xs-2">NOTE</td>
                        <td    class="col-lg-2 col-xs-2">{{$measurement->SnpIN}}</td>
                        <td    class="col-lg-2 col-xs-2">NECK</td>
                        <td class="col-lg-2 col-xs-2">{{ $measurement->FakeButton}}</td>
                        <td  class="col-lg-2 col-xs-2">{{ $measurement->Bar}} </td>
                        <td class="col-lg-2 col-xs-2">BAR</td>

                    </tr>
                </table>
                <hr>
            </div>
        </div>

