<input type="hidden"value="{{$item->id}}" name="id_order[]">
<div class="panel panel-info form-group row panel_detail_order specific_details_item" >

    <div id='detail' class="panel-heading" >

        <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>&nbsp;
        تفاصيل الطلب
    </div>
    <div class="panel-body specific_details_body" hidden >
        <div class='row'>
            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder"> Style </div>
                   <label>{{$item->design->name}}</label>
            </div>
            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder">Neck</div>
                <label>{{$item->neck->name }}</label>
            </div>
            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder"> Inside pocket </div>
                <label>{{$item->detail_pocket}}</label>
            </div>

        </div>
            <div class='row'>
            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder"> Sleeves  </div>
                <label>{{$item->sleeve->name}}</label>
            </div>
                <div class="col-lg-4 col-xs-4">
                    <div id="titleInOrder"> Filling button </div>
                    <label>{{$item->button_detail}}</label>
                </div>

            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder">Filling collar </div>
                <label>{{$item->collar_detail}}</label>
            </div>
            </div>
            <div class='row'>
                @if($item->design->has_bar_pocket == 0)
                <div class="col-lg-4 col-xs-4">
                    <div id="titleInOrder"> Bar </div>
                    {{$item->button->name}}
                </div>
                <div class="col-lg-4 col-xs-4">
                    <div id="titleInOrder">Pocket</div>
                    <lable>{{$item->pocket->name}}</lable>
                </div>
            @endif

            @if($item->fabric != null)
            <div class="col-lg-4 col-xs-4">
                <div style="width: 66%;" id="titleInOrder">Fabric </div>
                <label> {{$item->fabric->name}}</label>
            </div>
            @endif
        </div>

        <div class="page-header"></div>
        <div class="row">
            <div class="col-lg-4 col-xs-4">
            <div id="titleInOrder"> Start Date </div>
                <label>  {{$data->bill->created_at}}</label>
            </div>
            <div class="col-lg-4 col-xs-4">
                <div id="titleInOrder"> Due Date </div>
                <label> {{$data->bill->end_date}}</label>
            </div>
        </div>

    </div>
</div>

