<link rel="stylesheet" href={{asset("css/select2.min.css")}}>
<script src={{asset("js/select2.min.js")}} charset="utf-8"></script>
<script src={{asset("js/select2-ar-language.js")}} charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script type="text/javascript">

    /// this to search user in customer/view
    var path = "autocomplete";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
    /*
     Please consider that the JS part isn't production ready at all, I just code it to show the concept of merging filters and titles together !
     */
    $(document).ready(function(){
        $('.filterable .btn-filter').click(function(){
            var $panel = $(this).parents('.filterable'),
                $filters = $panel.find('.filters input'),
                $tbody = $panel.find('.table tbody');
            if ($filters.prop('disabled') == true) {
                $filters.prop('disabled', false);
                $filters.first().focus();
            } else {
                $filters.val('').prop('disabled', true);
                $tbody.find('.no-result').remove();
                $tbody.find('tr').show();
            }
        });

        $('.filterable .filters input').keyup(function(e){
            /* Ignore tab key */
            var code = e.keyCode || e.which;
            if (code == '9') return;
            /* Useful DOM data and selectors */
            var $input = $(this),
                inputContent = $input.val().toLowerCase(),
                $panel = $input.parents('.filterable'),
                column = $panel.find('.filters th').index($input.parents('th')),
                $table = $panel.find('.table'),
                $rows = $table.find('tbody tr');
            /* Dirtiest filter function ever ;) */
            var $filteredRows = $rows.filter(function(){
                var value = $(this).find('td').eq(column).text().toLowerCase();
                return value.indexOf(inputContent) === -1;
            });
            /* Clean previous no-result if exist */
            $table.find('tbody .no-result').remove();
            /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
            $rows.show();
            $filteredRows.hide();
            /* Prepend no-result row if all rows are filtered */
            if ($filteredRows.length === $rows.length) {
                $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
            }
        });


    /// conver number from arabic to english


    String.prototype.toEnglishDigits = function () {
        var persian = {
            '۰': '0',
            '۱': '1',
            '۲': '2',
            '۳': '3',
            '۴': '4',
            '۵': '5',
            '۶': '6',
            '۷': '7',
            '۸': '8',
            '۹': '9'
        };
        var arabic = {
            '٠': '0',
            '١': '1',
            '٢': '2',
            '٣': '3',
            '٤': '4',
            '٥': '5',
            '٦': '6',
            '٧': '7',
            '٨': '8',
            '٩': '9'
        };
        return this.replace(/[^0-9.]/g, function (w) {
            return persian[w] || arabic[w] || w;
        });
    };

    $(window).bind("load", function () {
        var total2 = 0;
        $('input[name="totalPriceThop[]"]').each(function () {
            total2 += parseFloat($(this).val());
              console.log($(this).val()  )
        });
        $('.cost').val(total2.toFixed(2));
        $('#tax').val((total2*0.05).toFixed(2))
        console.log(total2  )
        $('#finally_price').val( Math.round((Number($('.cost').val()) + Number($('#tax').val()))  - Number($("#discount").val().toEnglishDigits())));
    });

    $(".toggle_specific_details").on("click", function () {
            $(this).closest('.specific_details_item').find('.specific_details_body').fadeToggle()
        });


        $(".add_new_fabric_row").on("click", function () {
           var count =  +$('#fabric_list tr').length + 1
            $(".fabric_item").first()
                .clone(true)
                .find("input:text").val("").end()
                .appendTo("#fabric_list")
                .find('.number_column').val(count)
                .end()
                .find('.addition_id').attr('name', count+'_addition_id[]')
                .end()
                .find('.order_id').val(0) // it is mean this is a new item

        })


        // Delete fabric row
        $(".delete_fabric_row").on("click", function () {
            var tr = $(this).closest("tr")

            // if this is the last row then just empty its fields
            if ($('#fabric_list tr').length === 1) {
                tr.find("input:text").val("").end()
            } else {
                tr.remove()
            }

            updateNumbers()

        });



        $(".add_specific_details_box").on("click", function () {
           var count = +$('.specific_details_item').length+ 1  // To count number of form
           // var count =  $('.specific_details_item').length ;
            $(".specific_details_item").first()
                .clone(true)
                .appendTo(".specific_details_block")
                .find('.input_number').val(count)
                 .end()
                .find('.id_detailOrder').val(0) // it is mean this is a new item
        });


        $(".delete_specific_details_row").on("click", function () {
            var item = $(this).closest(".specific_details_item")
            // if this is the last row then just empty its fields
            if ($('.specific_details_item').length == 1) {
                item.find("input").val("").end();
            } else {
                item.remove();
            }
        });



        function updatedRowPrice(columns, callback) {
            var count = Number($(columns[5]).find(".thop_count").val().toEnglishDigits())
            var price = Number($(columns[6]).find(".thop_price").val().toEnglishDigits())
            var PriceAddition =  Number($(columns[7]).find(".price_addition").val().toEnglishDigits())
            return callback($(columns[8]).find(".totalPriceThop"), (count * price)  + (count * PriceAddition))

        }
        $('.id_fabric').on('change', function () {
            var row = $(this).closest("tr").children()
            updatedRowPrice(row, function (input, cost) {
                input.val(cost)
                updateNumbers()

            })
            var id_fabric = $(row[1]).find(".id_fabric").val();
            var thopprice = Number($(row[6]).find(".thop_price").val().toEnglishDigits());
                console.log(id_fabric);
                FAKEcheckFabricPrice(id_fabric, thopprice, row)

        });

        $('.type_design').on('change', function () {
            var row = $(this).closest("tr").children()
            updatedRowPrice(row, function (input, cost) {
                input.val(cost)
                updateNumbers()

            })
            var type = $(row[2]).find(".type_design").val();
            if( type == 'Pants')
            {
                $(row[7]).find(".price_addition").val(0);
                $(row[3]).find(".here").attr("disabled",true);
                // here it is mean all option

            }else{
                $(row[3]).find(".here").attr("disabled",false);


            }
            var id_fabric = $(row[1]).find(".id_fabric").val();
            var thopprice = Number($(row[6]).find(".thop_price").val().toEnglishDigits());
                console.log(id_fabric);
                FAKEcheckFabricPrice(id_fabric, thopprice, row)
        });

        $(".thop_count, .thop_price").on("input", function () {
            var row = $(this).closest("tr").children();
            updatedRowPrice(row, function (input, cost) {
                input.val(cost)
                updateNumbers()
            })
        });



        $(".thop_price").on("change", function () {
            var row = $(this).closest("tr").children()
            updatedRowPrice(row, function (input, cost) {
                input.val(cost)
                updateNumbers()
            })

            var id_fabric = $(row[1]).find(".id_fabric").val();
            var thopprice = Number($(row[6]).find(".thop_price").val().toEnglishDigits());
            console.log(id_fabric);
            FAKEcheckFabricPrice(id_fabric, thopprice, row)
            // checkFabricPrice(id_fabric, thopprice, row)
        })

        // additions
        $('.addition_id').on('change', function () {
            var row = $(this).closest("tr").children()
            var id_addition = $(row[3]).find(".addition_id").val();

            if(id_addition != null){
                    FAKEcheckAdditionPrice(id_addition, row)
            }
            updatedRowPrice(row, function (input, cost) {
                input.val(cost)
                updateNumbers()
            })

        });
        function FAKEcheckAdditionPrice(addition_number ,columns) {
            $.ajax({
                url: '../GetPriceAddition/' + addition_number,
                Type: 'GET',
                dataType: 'json',
                async: false,
                success: function (data) {
                    $(columns[7]).find(".price_addition").val(data)

                }
            })
        }

        function FAKEcheckFabricPrice(fabric_number, selected_price, columns) {
            var type =  ($(columns[2]).find(".type_design").val());
           if($('input:radio[name="type"]:checked').val() == 'product'){
               var link = '../GetProductPrice/' + fabric_number ;
           }else if($('input:radio[name="type"]:checked').val() == 'fabrics' || type == 'Thop' ||  type == 'VEST'
           || type == 'Pants' || type == 'Sroal'){
               var link = '../GetLowestPrice/' + fabric_number ;
           }
            $.ajax({
                url: link ,
                Type: 'GET',
                dataType: 'json',
                success: function (data) {
                    var lowest_price = data
                    if( type == 'Pants' || type == 'VEST' ) {
                        // if design and Vest = sadreiah
                        var fabric_price = lowest_price - 200;
                        var price_VEST = Math.round(fabric_price / 2);
                        var result = price_VEST + 200;
                        $(columns[6]).find(".thop_price").val(result)
                        console.log(lowest_price)
                        var count = Number($(columns[5]).find(".thop_count").val().toEnglishDigits())
                        updatedRowPrice(columns, function (cost) {
                            $(columns[8]).find(".totalPriceThop").val(result * count)
                        })
                    }else if(type =='Sroal'){
                        var result = (lowest_price - 200).toFixed(2);
                        console.log(lowest_price ,result )
                        $(columns[6]).find(".thop_price").val(result)
                        var count = Number($(columns[5]).find(".thop_count").val().toEnglishDigits())
                        updatedRowPrice(columns, function (cost) {
                            $(columns[8]).find(".totalPriceThop").val(result * count)
                        })

                    }else if(type != 'Pants' || $('input:radio[name="type"]:checked').val() == 'product'){
                            $(columns[6]).find(".thop_price").val(lowest_price)
                            var count = Number($(columns[5]).find(".thop_count").val().toEnglishDigits())
                            var PriceAddition =  Number($(columns[7]).find(".price_addition").val().toEnglishDigits())
                            updatedRowPrice(columns, function (cost) {
                                if($('input:radio[name="type"]:checked').val() == 'product') {
                                    $(columns[8]).find(".totalPriceThop").val(lowest_price * count)
                                }else{
                                    $(columns[8]).find(".totalPriceThop").val(lowest_price + (count * PriceAddition))
                                }

                            })
                    }
                    updateNumbers();
                }
            })
        }


        function updateNumbers() {
            // updating thop total count
            var i = 0
            $("#fabric_list").find(".thop_count").each(function () {
                i += Number($(this).val().toEnglishDigits())
            })
            $("#total_thop_count").val(i)

            // updating grand total price with discount
            i = 0
            $("#fabric_list").find(".totalPriceThop").each(function () {
                i += Number($(this).val())

            })

            $("#total_thop_cost").val((i).toFixed(2))

            $("#finally_price").val(Math.round((i+(i*0.05)) - Number($("#discount").val())))
            $('#tax').val((i*0.05).toFixed(2))
            $('#Residual').val(Number($("#finally_price").val()) - (Number($('#paid_up').val()) +
                    Number($('#paid_up_card').val())))

        }

        $('#paid_up').on('change', function () {
            var paidUp = $(this).val();
                var result = Number($('#finally_price').val()) - (Number($('#paid_up').val())+
                    Number($('#paid_up_card').val()));
                $('#Residual').val(result);

        });
        $('#paid_up_card').on('change', function () {
            var paidUp = $(this).val();
            var result = Number($('#finally_price').val()) - (Number($('#paid_up').val()) +
                Number($('#paid_up_card').val()));
            $('#Residual').val(result);

        });
        function check_Residual() {
            var result = Number($('#Residual').val()) + (Number($('#paid_up').val()) + Number($('#paid_up_card').val()));
            if (result != $('#finally_price').val()) {
                alert('يجب ان يكون مجموع المدفوع و المتبقي يساوي السعر ')
                var result = Number($('#finally_price').val()) - (Number($('#paid_up').val()) +
                        Number($('#paid_up_card').val()));
                $('#Residual').val(result);

            }
            console.log(result);

        }
        $('#Residual').on('change', function () {
            check_Residual();
        });

        /**$('#bill_state').on('change', function () {
            var result = $('#paid_up').val();
            if (result != $('#finally_price').val() && $('#bill_state').val() == 1) {
                alert(' لم يتم دفع كافة المبلغ لتاكيد الفاتورة ')
                $('#bill_state').val('');
            }

        });
         */


    });

    /*
    function formatState(state) {
        if (!state.id) {
            return state.text;
        }

      var $state = $(
            '<span><img src="' + $(state.element).data('imagesrc') + '" width="24px"  /> ' + state.text + '</span>'
        );
        return $state;
    };
    $('.select2').select2({templateResult: formatState, language: "ar"});
*/
    /*
     * Specific Details Block
     */


    ///
    function test_style(style ,row ) {
        $.ajax({
            url: '../check_style/'+ style ,
            Type: 'Get',
            dataType :'json',
            success:function (data) {
                if(data == 1 ){
                    var h       =   $(row[5]).find(".h").hide();
                    var bar     =   $(row[5]).find("#bar");
                    var Pocket  =   $(row[5]).find("#Pocket");

                    bar.removeAttr('required')
                    Pocket.removeAttr('required')


                }else if(data==0){
                    var h       =   $(row[5]).find(".h").show();
                    var bar     =   $(row[5]).find("#bar");
                    var Pocket  =   $(row[5]).find("#Pocket");
                    bar.prop('required', true);
                    Pocket.prop('required', true);
                }

            }
        })
    }
    $('.style').on('change', function () {
        var style=  $(this).closest('.check').find('.style').val();
        var row = $(this).closest(".check").children();
        console.log(row)
        test_style(style , row );
    });


    ///




    $('input:radio[name="type"]').on("click", function () {
        if (this.checked && this.value == 'product') {

            $('.fabrics').hide();
            $('.fabrics :input ,.yards , .type_design' ).removeAttr('required')
            $('.trToFabircs').hide().attr("disabled",true);
            $('.h').hide().attr("disabled",true);
            $('.trToProduct').show().attr("disabled",false);
            $.ajax({
                url: '../GetProductAJaxToOrder',
                Type: 'GET',
                dataType: 'json',
                async: false,
                success: function (data) {

                    $('select[name="id_fabric[]"]').empty();
                    $.each(data, function (key, value) {
                        $('select[name="id_fabric[]"]').append('<option value="' + value.fabrics_id + '">' + value['products']["name"] + '</option>');

                    });
                }
            })

        }
        if (this.checked && this.value == 'fabrics') {
            $('.fabrics ,.yards,.type_design').show().attr("disabled",false);
            $('.trToProduct').hide().attr("disabled",true);
            $('.h').show().attr("disabled",false);
            $('.trToFabircs').show().attr("disabled",false);
            $.ajax({
                url: '../GetFabricsAJax',
                Type: 'GET',
                dataType: 'json',
                async: false,
                success: function (data) {
                    $('select[name="id_fabric[]"]').empty();
                    $.each(data, function (key, value) {
                        $('select[name="id_fabric[]"]').append('<option value="' + value.id + '">' + value.name + '</option>');

                    });
                }
            })
        }
    });






    /**   TO PRODUCT  **/



    function totalThop() {
        var total = 0;
        $('input[name="number[]"]').each(function () {
            total += parseInt($(this).val());
        });
        $('.total_number').val(total);
       // totalPriceThop();
    }

    //FUNCTION WILL WORKE AFTER LOAD PAGE




    $(".product_count,.product_price").on("input", function () {
        var row = $(this).closest("tr").children();
        console.log( $(this).closest("tr").children())
        updatedRowPrice(row, function (input, cost) {
            input.val(cost)
            updateNumbers()
        })
    });

    function updatedRowPrice(columns, callback) {
        var count = Number($(columns[2]).find(".product_count").val().toEnglishDigits())
        var price = Number($(columns[3]).find(".product_price").val().toEnglishDigits())
        return callback($(columns[4]).find(".totalPriceProduct"), (count * price))

    }
    function updateNumbers() {
        // updating thop total count
        var i = 0
        $("#fabric_list").find(".product_count").each(function () {
            i += Number($(this).val().toEnglishDigits())
        })
        $("#total_product_count").val(i)

        // updating grand total price with discount
        i = 0
        $("#fabric_list").find(".totalPriceProduct").each(function () {
            i += Number($(this).val())

        })

        $("#total_product_cost").val(Math.round(i))
        $("#finally_price").val(Number((i+(i*0.05)).toFixed(2) -Number($("#discount").val())))
        $('#tax').val((i*0.05).toFixed(2))
        $('#Residual').val(Number($("#finally_price").val()) - (Number($('#paid_up').val()) +
            Number($('#paid_up_card').val())))

    }

    $('.id_fabric_product').on('change', function () {
        var row = $(this).closest("tr").children()
        updatedRowPrice(row, function (input, cost) {
            input.val(cost)
            updateNumbers()

        })

        var id_fabric = $(row[0]).find(".id_fabric_product").val();
        var productPrice = Number($(row[2]).find(".product_price").val().toEnglishDigits());
        if (productPrice != 0) {
            console.log(id_fabric);
            checkProductPrice(id_fabric, productPrice, row)
        }
    });

    $(".product_price").on("change", function () {
        var row = $(this).closest("tr").children()
        updatedRowPrice(row, function (input, cost) {
            input.val(cost)
            updateNumbers()
        })

        var id_fabric = $(row[0]).find(".id_fabric_product").val();
        var productPrice = Number($(row[2]).find(".product_price").val().toEnglishDigits());
        console.log(id_fabric);

        checkProductPrice(id_fabric, productPrice, row)
        // checkFabricPrice(id_fabric, thopprice, row)

    })

    function checkProductPrice(product_number, selected_price, columns) {

        $.ajax({
            url: '../GetProductPrice/' + product_number,
            Type: 'GET',
            dataType: 'json',
            success: function (data) {
                var lowest_price = data['lowest_price']
                if (selected_price < lowest_price) {
                    alert('ادنى سعر للبيع هو ' + lowest_price)
                    $(columns[2]).find(".product_price").val(lowest_price)
                    var count = Number($(columns[1]).find(".product_count").val().toEnglishDigits())
                    updatedRowPrice(columns, function (cost) {
                        $(columns[3]).find(".totalPriceProduct").val(lowest_price +(count))
                        updateNumbers()
                    })
                }
            }
        })
    }

    // payment method
    $('.payment_method').on('change',function () {
        var type_payment = $(this).val() ;
        if(type_payment == 3 )
        {
            $('.paid_up_card').show();
            $('#paid_up_card').prop('required', true);
        }else{
            $('.paid_up_card').hide();
            $('#paid_up_card').removeAttr('required');
            $('#paid_up_card').val(null);
        }
    })
    // display hour
    $('.type_display').on('change',function () {
        var type = $(this).val() ;
        if(type == 0 )
        {
            $('.select_hour').show();
            $('#hour').prop('required', true);
        }else{
            $('.select_hour').hide();
            $('#hour').removeAttr('required');
        }
    })

    //datepicker

    var array = $('#dates').val()
    $('#datepicker').datepicker({
        showAnim: 'drop',
        dateFormat: 'yy-mm-dd',
        beforeShowDay: function (date) {
            var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
            return [array.indexOf(string) == -1]
        }
    });

    $('#datepicker').on('change', function () {
        $.ajax({
            url: 'AjaxGetHour/' + $(this).val(),
            Type: 'GET',
            dataType: 'json',
            success: function (date) {
              $('#hour > option').removeAttr("disabled")
                if (date != '') {
                    var HourBocked = date
                    $.each($('#hour > option'), function () {
                        var BasicHour = this.value;
                        $.each(HourBocked, function (key, value) {
                            if (value == BasicHour) {
                                return $("#hour option[value='" + BasicHour + "']").attr('disabled', 'disabled')
                            }
                        });
                    });
                }
            }
        })
    })
    /* end */



    $("#discount").on("blur", function () {
                    var discount = $(this).val() ;
                    var t = $("#total_thop_cost").val();
                    var x = $("#tax").val();
                    var sum  =Number(t) + Number(x) ;
                   if($('#state_percentage').val() != 0 ) {
                            var max_discount = Math.round(sum * ($('#percentage').val() / 100));
                            if (max_discount < discount) {
                                    alert('الخصم يجب ان لا يكون اكثر من ' + ($('#percentage').val()) + '%');
                                    $("#discount").val(0);
                                    resultAfterEnterDiscount(sum)
                                } else if (max_discount >= discount) {
                                    resultAfterEnterDiscount(sum)
                                }
                        }else{
                            resultAfterEnterDiscount(sum)
                        }
                });
            $("#discount").on("blur", function () {
                    var discount = $(this).val() ;

                    var t = $("#total_product_cost").val();
                    var x = $("#tax").val();
                    var sum  =Number(t) + Number(x) ;
                    if($('#state_percentage').val() != 0 ) {
                            var max_discount = Math.round(sum * ($('#percentage').val() / 100));
                            if (max_discount < discount) {
                                    alert('الخصم يجب ان لا يكون اكثر من ' + ($('#percentage').val()) + '%');
                                    $("#discount").val(0);
                                    resultAfterEnterDiscount_product(sum)
                                } else if (max_discount >= discount) {
                                    resultAfterEnterDiscount_product(sum)
                                }
                        }else{
                            resultAfterEnterDiscount_product(sum)
                        }
                });

                function resultAfterEnterDiscount(sum) {
                        $("#finally_price").val(Number(sum) - Number($("#discount").val().toEnglishDigits()))
                        $('#Residual').val(Number($("#finally_price").val()) - Number($('#paid_up').val()))
                    }
            function resultAfterEnterDiscount_product(sum) {
                    $("#finally_price").val(Number(sum) - Number($("#discount").val().toEnglishDigits()))
                    $('#Residual').val(Number($("#finally_price").val()) - Number($('#paid_up').val()))
                }



</script>
