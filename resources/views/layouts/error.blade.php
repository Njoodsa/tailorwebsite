<div>
    @if(session('message_success'))
        <div class=" alert alert-success flash_massage" r >
            <h5> {{ session('message_success') }}</h5>
        </div>
    @elseif(session('message_fail'))
        <div class=" alert alert-danger flash_massage" >
            <h5> {{ session('message_fail') }}</h5>
        </div>
    @endif
</div>


@if(count($errors))
    <div class="alert alert-danger">
        <ul>
            <?php $errorRequired = 0; $measurementUnCurrect = 0;  ?>
            @foreach($errors->all() as $error)
                @if($error =='جميع الحقول مطلوبة')
                    <?php $errorRequired++?>
                @elseif($error == 'ادخل القياسات بشكل صحيح' )
                    <?php $measurementUnCurrect++ ?>
                @else
                    <p style="text-align: center">
                        * {{$error}}
                    </p>

                @endif
            @endforeach
            @if($errorRequired >=1 )
                <p style="text-align: center"> * جميع الحقول مطلوبة</p>
            @endif

            @if($measurementUnCurrect >=1 )
                <p style="text-align: center"> *ادخل القياسات بشكل صحيح </p>
            @endif
        </ul>
    </div>

@endif
