<style>
    .ul_menu{
        margin-top: -2%!important;
        margin-right: 5%;
    }
    .glyphicon {
        font-size: 9px;
    }
    .none_to_lap{
        display: none;
        margin-top: 2%;
        color: #1a3842;
    }
    .start_work{
        float: left; margin-left: 28%;
    }

    @media only screen and (max-width: 768px) {
        .For_Phone {
            display: none;
        }

        .none_to_lap {
            display: block;
        }
        .table_phone{
            overflow-x: auto!important;
            white-space: nowrap !important;
        }
        .table-responsive .table {
            max-width: none;
        }

    .btn-block{
        width: 50%;
        /* text-align: center; */
        margin-right: 21%;

    }

        .margin-SidBar {
            margin-left: 0px !important;
        }

        .filterable {
            width: 97%;
            margin-right: 7px;
        }

        .start_work {
            float: left;
            margin-left: 7% !important;
        }

        .panel_detail_order {
            width: 348px !important;
            /* margin-right: -92px; */
            margin-right: 10px !important;
        }

        .deleter_box {
            margin-top: -14%;
        }
        #add-button {
            margin-right: 4%;
            margin-bottom: 1%;
        }
        .hide_to_phone{
            display: none;
        }
        .info_order {
            margin-right: 6px;
            margin-left: 4px;
            width: 96%;
        }
        #titleInOrder {
            width: 108%;
            font-size: 14px;

        }
        .btn-print {
            margin-right: 81%;
            margin-bottom: 2%;
            margin-top: -19%;
        }

        .back_detail_price{
            background: #f6f6f6;
            padding-bottom: 10px;
    }
    }


</style>
<button class=" none_to_lap" onclick="view_it('menu')">  القائمة <span class="glyphicon glyphicon-align-right"></span></button>
<div class="container menu-list For_Phone" id="menu">
        <div class="row inbox">
            <div class="col-md-3">

                @if (auth::check())
                    @if(Auth::user()->access == 1 )
                    <div class="panel panel-default">
                        <div class="panel-body contacts">
                            <a href="#" class="btn btn-danger btn-block"> البيانات الأساسية </a>
                            <ul >
                                <li><a href="{{route('companyInformation.edit',1)}}"><span class="label label-danger"></span> بيانات الشركة </a></li>
                                <li><a href="{{route('Branch.index')}}"><span class="label label-danger"></span> الفروع  </a></li>
                                <li><a href="{{url('Boxes')}}"><span class="label label-danger"></span> الصندوق  </a></li>
                                <li><a href="{{url('Payment_method')}}"><span class="label label-danger"></span> طرق الدفع</a></li>
                               <!-- <li><a href="{{url('send_massage')}}"><span class="label label-danger"></span> الرسائل  </a></li>-->
                                <li onclick="view_it('design')">
                                    <a ><span class="label label-danger"></span>  التفصيل  <span class="glyphicon glyphicon-chevron-down"> </span></a>
                                </li>
                                <ul class="ul_menu" id="design" hidden>
                                    <li><a href="{{url('design')}}"> <span class="label label-danger"></span> بيانات التفصيل </a></li>
                                    <li><a href="{{url('industries')}}"><span class="label label-danger"></span> بياتات الصناعة </a></li>
                                    <li><a href="{{url('buttons')}}"><span class="label label-danger"></span> تصميمات الازرار </a></li>
                                    <li><a href="{{url('sleeve')}}"><span class="label label-danger"></span> تصميمات الآكمام </a></li>
                                    <li><a href="{{url('pockets')}}"><span class="label label-danger"></span> تصميمات الجيب </a></li>
                                    <li><a href="{{url('necks')}}"><span class="label label-danger"></span> تصميمات الرقبة </a></li>
                                    <li><a href="{{url('Unit')}}"><span class="label label-danger"></span>  بيانات الوحده</a></li>
                                    <li><a href="{{url('Addition')}}"><span class="label label-danger"></span> الإضافات  </a></li>
                                    <li><a href="{{url('fabrics')}}"><span class="label label-danger"></span> كرت الصنف </a></li>
                                    <li><a href="{{url('Discount')}}"><span class="label label-danger"></span> الخصم  </a></li>
                                </ul>

                                <li onclick="view_it('Warehouses')">
                                    <a ><span class="label label-danger"></span>  المخازن   <span class="	glyphicon glyphicon-chevron-down"></span></a>
                                </li>
                                <ul class="ul_menu " id="Warehouses" hidden>
                                    <li><a href="{{url('Warehouses')}}"><span class="label label-danger"></span> المخازن </a></li>
                                    <li><a href="{{url('inventory_balance')}}"><span class="label label-danger"></span>  ارصدة المخازن (للأقمشة)  </a>
                                    <li><a href="{{url('inventory_balance_product')}}"><span class="label label-danger"></span>  ارصدة المخازن (للمنتجات)   </a>
                                </ul>
                                <li onclick="view_it('product')">
                                    <a ><span class="label label-danger"></span>  المنتجات    <span class="	glyphicon glyphicon-chevron-down"></span></a>
                                </li>
                                <ul class="ul_menu " id="product" hidden>
                                    <li  ><a href="{{url('Sections')}}"><span class="label label-danger"></span> الاقسام  </a></li>
                                    <li ><a href="{{url('products')}}"><span class="label label-danger"></span> المنتجات    </a></li>

                                </ul>
                                </li>


                                <li onclick="view_it('app')">
                                    <a ><span class="label label-danger"></span>  تطبيق العملاء  <span class="glyphicon glyphicon-chevron-down"> </span></a>
                                </li>
                                <ul class="ul_menu" id="app" hidden>
                                  <li><a href="{{url('dates')}}"><span class="label label-danger"></span> التواريخ المحجوبة </a></li>
                                  <li><a href="{{url('hours')}}"><span class="label label-danger"></span> الساعات المتاحة </a></li>
                                  <li><a href="{{url('reservations')}}"><span class="label label-danger"></span> الحجوزات   </a></li>
                                      </ul>

                            </ul>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-body contacts">
                            <a href="#" class="btn btn-info btn-block"> مستخدمين النظام </a>
                            <ul>
                            <li onclick="view_it('employee')">
                                <a ><span class="label label-info"></span>  الموظفين    <span class="	glyphicon glyphicon-chevron-down"></span></a>
                            </li>
                            <ul class="ul_menu " id="employee" hidden>
                                <li><a href="{{url('employeeType')}}"><span class="label label-info"></span> أنواع الموظفين</a></li>
                                <li><a href="{{url('employeeData')}}"><span class="label label-info"></span> الموظفين</a></li>
                                <li><a href="{{url('users')}}"><span class="label label-info"></span> المستخدمين </a></li>
                            </ul>
                                <li><a href="{{url('Customer')}}"><span class="label label-info"></span>  العملاء</a></li>
                                <li><a href="{{url('suppliers')}}"><span class="label label-info"></span> الموردين </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body contacts">
                            <a href="#" class="btn btn-info btn-block">  الطلبات  </a>
                            <ul>
                                <li onclick="view_it('order')">
                                    <a ><span class="label label-info"></span>  طلبات التفصيل     <span class="	glyphicon glyphicon-chevron-down"></span></a>
                                </li>
                                <ul class="ul_menu " id="order" hidden>
                                    <li><a href="{{url('pending_review')}}"><span class="label label-info"></span> طلبات قيد الإنتظار (إنتظار المراجعة)  </a></li>
                                    <li><a href="{{url('orders')}}"><span class="label label-info"></span> طلبات قيد العمل  </a></li>
                                    <li><a href="{{url('orders_completed')}}"><span class="label label-info"></span>  الطلبات المنجزة  </a></li>
                                    <li><a href="{{url('order_delivered')}}"><span class="label label-info"></span> الطلبات المسلمة  </a></li>
                                    <li><a href="{{url('orders_canceled')}}"><span class="label label-info"></span> الطلبات الملغية  </a></li>
                                </ul>
                                <li onclick="view_it('order_product')">
                                    <a ><span class="label label-info"></span>  طلبات المنتجات      <span class="	glyphicon glyphicon-chevron-down"></span></a>
                                </li>
                                <ul class="ul_menu " id="order_product" hidden>
                                    <li><a href="{{url('view_product_unCompleted')}}"><span class="label label-info"></span> الطلبات الغير مكتملة  </a></li>
                                    <li><a href="{{url('View_product_delivered')}}"><span class="label label-info"></span> الطلبات المسلمة  </a></li>
                                    <li><a href="{{url('viewProduct_cancel')}}"><span class="label label-info"></span> الطلبات الملغية  </a></li>
                                   </ul>
                                <li onclick="view_it('Reports')">
                                    <a ><span class="label label-info"></span>  التقارير      <span class="	glyphicon glyphicon-chevron-down"></span></a>
                                </li>
                                <ul class="ul_menu " id="Reports" hidden>
                                    <li><a href="{{url('report/search/fabric')}}"><span class="label label-info"></span> مبيعات الثياب </a></li>
                                    <li><a href="{{url('report/search/product')}}"><span class="label label-info"></span>  مبيعات المنتجات   </a></li>
                                    <li><a href="{{url('report/warehouses/fabric')}}"><span class="label label-info"></span> ارصدة المخازن أقمشة  </a></li>
                                    <li><a href="{{url('report/warehouses/product')}}"><span class="label label-info"></span> ارصدة المخازن منتجات   </a></li>
                                </ul>
                               </ul>
                        </div>
                    </div>

                        <div class="panel panel-default">
                            <div class="panel-body contacts">
                                <a href="#" class="btn btn-info btn-block">  الحركات   </a>
                                <ul>
                                    <li onclick="view_it('purchases')">
                                        <a ><span class="label label-info"></span>  المشتريات     <span class="	glyphicon glyphicon-chevron-down"></span></a>
                                    </li>

                                    <ul class="ul_menu " id="purchases" hidden>
                                        <li><a href="{{url('purchases')}}"><span class="label label-info"></span> المشتريات  </a></li>
                                        <li><a href="{{url('createSupplier_invoice')}}"><span class="label label-info"></span> إضافة مشتريات </a></li>
                                        <li><a href="{{url('invoiceUncompleted')}}"><span class="label label-info"></span> المشتريات الحالية </a></li>
                                        <li><a href="{{url('invoice')}}"><span class="label label-info"></span> المشتريات المؤكدة </a></li>
                                    </ul>
                                    <li><a href="{{url('Revenue')}}"><span class="label label-info"></span> الإرادات </a></li>
                                   </ul>
                            </div>
                        </div>
                        @endif
                        @if(Auth::user()->access == 2 )
                            <div class="panel panel-default">
                                <div class="panel-body contacts">
                                    <a href="#" class="btn btn-info btn-block"> العملاء </a>
                                    <ul>
                                        <li><a href="{{url('Customer')}}"><span class="label label-info"></span> بيانات العملاء</a></li>
                                        <!-- <li><a href="{{url('dates')}}"><span class="label label-danger"></span> التواريخ</a></li>
                                        <li><a href="{{url('hours')}}"><span class="label label-danger"></span> الساعات</a></li> -->

                                    </ul>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-body contacts">
                                    <a href="#" class="btn btn-info btn-block">  الطلبات  </a>
                                    <ul>
                                        <li onclick="view_it('order')">
                                            <a ><span class="label label-info"></span>  طلبات التفصيل     <span class="	glyphicon glyphicon-chevron-down"></span></a>
                                        </li>
                                        <ul class="ul_menu " id="order" hidden>
                                             <li><a href="{{url('pending_review')}}"><span class="label label-info"></span> طلبات قيد الإنتظار (إنتظار المراجعة)  </a></li>
                                            <li><a href="{{url('orders')}}"><span class="label label-info"></span> طلبات قيد العمل  </a></li>
                                            <li><a href="{{url('orders_completed')}}"><span class="label label-info"></span>  الطلبات المنجزه  </a></li>
                                            <li><a href="{{url('order_delivered')}}"><span class="label label-info"></span> الطلبات المسلمة  </a></li>
                                            <li><a href="{{url('orders_canceled')}}"><span class="label label-info"></span> الطلبات الملغية  </a></li>
                                        </ul>
                                        <li onclick="view_it('order_product')">
                                            <a ><span class="label label-info"></span>  طلبات المنتجات      <span class="	glyphicon glyphicon-chevron-down"></span></a>
                                        </li>
                                        <ul class="ul_menu " id="order_product" hidden>
                                            <li><a href="{{url('view_product_unCompleted')}}"><span class="label label-info"></span> الطلبات الغير مكتملة  </a></li>
                                            <li><a href="{{url('View_product_delivered')}}"><span class="label label-info"></span> الطلبات المسلمة  </a></li>
                                            <li><a href="{{url('viewProduct_cancel')}}"><span class="label label-info"></span> الطلبات الملغية  </a></li>
                                        </ul>
                                          <li onclick="view_it('app')">
                                              <a ><span class="label label-info"></span>  تطبيق العملاء  <span class="glyphicon glyphicon-chevron-down"> </span></a>
                                          </li>
                                          <ul class="ul_menu" id="app" hidden>
                                            <li><a href="{{url('dates')}}"><span class="label label-danger"></span> التواريخ المحجوبة </a></li>
                                            <li><a href="{{url('hours')}}"><span class="label label-danger"></span> الساعات المتاحة </a></li>
                                            <li><a href="{{url('reservations')}}"><span class="label label-danger"></span> الحجوزات   </a></li>
                                                </ul>
                                        <li onclick="view_it('Reports')">
                                            <a ><span class="label label-info"></span>  التقارير      <span class="	glyphicon glyphicon-chevron-down"></span></a>
                                        </li>
                                        <ul class="ul_menu " id="Reports" hidden>
                                            <li><a href="{{url('report/search/fabric')}}"><span class="label label-info"></span> مبيعات الثياب </a></li>
                                            <li><a href="{{url('report/search/product')}}"><span class="label label-info"></span>  مبيعات المنتجات   </a></li>
                                            <li><a href="{{url('report/warehouses/fabric')}}"><span class="label label-info"></span> ارصدة المخازن أقمشة  </a></li>
                                            <li><a href="{{url('report/warehouses/product')}}"><span class="label label-info"></span> ارصدة المخازن منتجات   </a></li>
                                        </ul>

                                    </ul>

                                </div>
                            </div>



                            @endif
                        @if(Auth::user()->access == 0 )
                        @endif
                @endif
            </div>
        </div>
    </div>
<script>
    function view_it(item) {
        if($('#'+item).is(':visible'))
        {
            $('#'+item).hide('slow');
        }else if(!$('#'+item).is(':visible'))
        {
            $('#'+item).show('slow');
        }

    }


</script>
