<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>

    <title>ماوان</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link href="{{asset('css/style.css?v=849' )}}" rel="stylesheet" type="text/css" >
    <link href="{{asset('js/javascript.js?v=543')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
     <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/prashantchaudhary/ddslick/master/jquery.ddslick.min.js" ></script>
</head>

<body>
<header>


    <nav class="[ navbar navbar-fixed-top ][ navbar-bootsnipp animate ] " role="navigation">
        <div class="container">
            <div class="[ navbar-header ]">
                <button type="button" class="[ navbar-toggle ]" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="[ sr-only ]">Toggle navigation</span>
                    <span class="[ icon-bar ]"></span>
                    <span class="[ icon-bar ]"></span>
                    <span class="[ icon-bar ]"></span>
                </button>

            </div>
            <div class="[ collapse navbar-collapse ]" id="bs-example-navbar-collapse-1">
                <ul class="[ nav navbar-nav navbar-right ]">
                    <li><a href="{{url('/')}}" class="[ animate ] ">الرئيسية </a></li>
                    <li><a class="animate" href="http://marn.co/" style="font-family:njood">من نحن </a></li>
                    <li><a href="{{url('/mawan/request-visit')}}" >طلب زيارة الخياط </a></li>

                    @auth
                        @if (Auth::user()->access == '1')
                            <li><a class="animate" href="{{url('Admin')}}">صفحتي</a></li>
                        @endif
                        @if (Auth::user()->access == '2')
                                <li><a class="animate" href="{{url('Profile')}}">صفحتي</a></li>
                        @endif
                        @if (Auth::user()->access == '0')
                                <li><a class="animate" href="{{url('ERRORE')}}">صفحتي</a></li>
                        @endif
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">تسجيل خروج
                            </a>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>
</header>
@yield('content')
<div class="container">
    <div class='navbar navbar-bottom'>
        <p class='navbar-text'>جميع الحقوق محفوظة لشركة مارن للمقاولات &copy; <?php echo date("Y") ?></p>
    </div>
</div>
</body>
</html>
