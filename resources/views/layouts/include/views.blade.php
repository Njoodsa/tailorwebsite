@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar" >
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title">{{$title}}  </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                               {{$linkAdd}}
                            </div>
                        </div><br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                               {{ $thead }}
                            </tr>
                            </thead>
                            <tbody>
                             {{ $tbody }}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection

