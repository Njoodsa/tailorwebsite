@extends('layouts.app')
@section('content')
    <div class="row margi-top-mobile">
        <div class="col-lg-12 col-sm-12">
            <section style="margin-top:30px;">
                <img class="img-responsive" src="{{asset("images/nm.png")}}">
                <div class="wrap"></div>
            </section>
        </div>
        <div class=" col-lg-12 col-sm-12">
            <article>
                <div class="row" id="row-section">
                    <div class="col-lg-3 col-sm-3">
                        <img src="{{asset("images/phone.png")}} " width="30%">
                        <br/>
                        <p><strong id="contact-text"> هاتف </strong></p>
                        <p> {{$data->phone}}  </p>
                    </div>
                    <div class="col-lg-3 col-sm-3">
                        <img src="{{asset("images/earth.png")}} " width="30%">
                        <p><strong id="contact-text" >الموقع </strong></p>
                        <p>{{$data->location}}</p>
                    </div>
                    <div class="col-lg-3 col-sm-3">
                        <img src="{{asset("images/time.png")}} " width="30%">
                        <p><strong id="contact-text">أوقات العمل</strong></p>
                        <p>{{$data->work_hours}}</p>
                    </div>
                    <div class="col-lg-3 col-sm-3">
                        <img src="{{asset("images/day.png")}} " width="30%">
                        <p><strong id="contact-text">أيام العمل </strong></p>
                        <p> {{$data->work_days}}</p>
                    </div>
                </div>
            </article>
            <article style=" padding-top: 80px; padding-bottom:80px; width:80%;margin-right:10%">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3625.2109483699574!2d46.68280321499952!3d24.68527418413689!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f0344082e66a5%3A0x330256deb67b682!2z2K7Zitin2Lcg2YXYp9mI2KfZhg!5e0!3m2!1sen!2ssa!4v1517321513280"
                        width="100%"
                        height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </article>
        </div>
    </div>
@endsection
