@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">حجب تاريخ </h2>
                <form action="displayDate" method="post" class="contentForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="form-group">
                            <label> التاريخ : </label>
                            <input name='date' class='form-control' id="datepicker" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>نوع التعطيل :</label>&nbsp;
                        <input type="radio" name="type" class="type_display" value="1" checked required> &nbsp;يوم كامل
                        <input type="radio" name="type" class="type_display" value="0" required>&nbsp; وقت محدد
                    </div>
                    <div class="form-group select_hour" hidden>
                        <div class="form-group">
                            <label> الوقت : </label>
                            <input value="{{$hours}}" class="r" type="hidden">
                            <select name="hours" id='hour'>
                                @foreach($hours as $hour)
                                    <option value="{{$hour}}">{{$hour}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input id='dates' type="hidden" value="{{$date}}">
                    <br/>
                    <button class="btn btn-block  btn-success">تعطيل</button>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection