@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6 ">
                <h2 class="main-title">الحجب</h2>
                <div class="row">
                    <div class="pull-right">
                        <a href="{{url('displayDate')}}" class="btn btn-success btn-xs btn-add">
                            <span class="glyphicon glyphicon-plus-sign"></span>
                            حجب
                        </a>
                    </div>
                    <br/>
                    <table class="table filterable center-content">
                        <thead>
                        <tr>
                            <th>اليوم </th>
                            <th>الساعه </th>
                            <th>نوع الحجب  </th>
                            <th>إلغاء الحجب  </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dates as $item)
                            <tr>
                                <td>{{$item->date->format('Y-m-d')}}</td>
                                <td>{{$item->date->format('H:i')}}</td>
                                @if($item->type == 1 )
                                    <td>يوم كامل</td>
                                    @else
                                    <td>وقت محدد</td>
                                @endif
                                <td>
                                    <form action="deleteDate/{{$item->id}}" method="post">
                                        @csrf
                                        <button class="btn btn-danger btn-xs" href="deleteDate/"><span
                                                    class=" glyphicon glyphicon-trash "></span></button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection
