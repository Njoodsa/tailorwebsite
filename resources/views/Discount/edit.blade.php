@extends('layouts.app')
@section('content')
    <Style>
        .margin-SidBar{
            margin-left :-12% !important;
        }
    </Style>
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.flash')
                <h2 class="main-title"> نسبة الخصم  </h2>
                <form action="updateDiscount/{{$data->id}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <label> ادخل نسبة الخصم بالمئة: </label>
                            <input type='text' name='percentage' class='form-control' value='{{$data->percentage}}' required>
                            @if ($errors->has('percentage'))
                                <span class="help-block">
                                        * <strong>{{ $errors->first('percentage') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label>   تفعيل الخصم : <input @if($data->state == 1) checked @endif type='checkbox' name='state'>   </label>
                            @if ($errors->has('state'))
                                <span class="help-block">
                                        * <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <br/>
                    <button class="btn btn-block  btn-info">تعديل</button>
                </form>
            </div>
        </div>
    </div>
@endsection