@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title"> بيانات الموظف  </h2>
                <form action="../updateSuppliers/{{$data->id}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <input type='hidden' name='id' class='form-control' value='{{$data->id}}' >
                    <div class="form-group">
                        <label> * الاسم : </label>
                        <input type='text' name='name' class='form-control' value='{{$data->name}}' required>
                    </div>
                    <div class="form-group">
                        <label>* الهاتف : </label>
                        <input type='text' name='phone' class='form-control' value='0{{$data->phone}}' required>
                    </div>
                    <div class="form-group">
                        <label> العنوان : </label>
                        <input type='text' name='address' class='form-control'  value='{{$data->address}}'>
                    </div>
                    <div class="form-group">
                        <label> الايميل : </label>
                        <input type='text' name='email' class='form-control' value='{{$data->email}}'>
                    </div>
                    <div class="form-group">
                        <label> ملاحظات : </label>
                        <input type='text' name='note' class='form-control'   value='{{$data->note}}'>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">تعديل</button>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection