@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">إضافة مورد جديد</h2>
                <form action="insertSuppliers" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label> * الاسم : </label>
                        <input type='text' name='name' class='form-control' value='{{old('name') }}' required>
                    </div>
                    <div class="form-group">
                        <label>* جوال  : </label>
                        <input type='text' name='phone' class='form-control' value='{{old('phone') }}' placeholder="0505555555"  required>
                    </div>
                    <div class="form-group" >
                        <label> العنوان : </label>
                        <input type='text' name='address' class='form-control' value='{{old('address') }}'>
                    </div>
                    <div class="form-group">
                        <label> الايميل : </label>
                        <input type='text' name='email' class='form-control' value='{{old('email') }}'>
                    </div>
                    <div class="form-group">
                        <label> ملاحظات : </label>
                        <input type='text' name='note' class='form-control' value='{{old('note') }}'>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>
@endsection