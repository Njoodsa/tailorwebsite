@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title"> بيانات المخازن </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                                <a href="createWarehouse" class="btn btn-success btn-xs "><span
                                            class="glyphicon glyphicon-plus-sign"></span> إضافه</a>
                            </div>
                        </div>
                        <br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder=رمز disabled></th>
                                <th><input type="text" class="form-control" placeholder=الاسم disabled></th>
                                <th><input type="text" class="form-control" placeholder=الهاتف disabled></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $Warehouse)
                                <tr>
                                    <td> {{$Warehouse->id}}</td>
                                    <td>{{$Warehouse->name}}</td>
                                    <td>{{$Warehouse->phone}}</td>

                                    <td>
                                        <a class="btn btn-info btn-xs" href="editWarehouse/{{$Warehouse->id}}"><span
                                                    class=" glyphicon glyphicon-fullscreen "></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')

@endsection

