@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar" >
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.flash')
                <h2 class="main-title">إضافة جديدة  </h2>
                <form action="insertAddition" method="post" class="contentForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <label> الاسم : </label>
                            <input type='text' name='name' class='form-control' value='{{old('name') }}' required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        * <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <label>السعر : </label>
                            <input type='number' name='price' class='form-control' value='{{old('price')}}'>
                            @if($errors->has('price'))
                                <span class="help-block">
                                    * <strong>{{ $errors->first('price') }}</strong>
                                </span>
                                @endif
                        </div>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>
@endsection