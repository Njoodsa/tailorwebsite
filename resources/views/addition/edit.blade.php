@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6  margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.flash')
                <h2 class="main-title">تعديل الإضافة</h2>
                        <form action="../updateAddition/{{$data->id}}" method="post" class="contentForm"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label> الاسم : </label>
                                <input type='hidden' name='id' class='form-control' value='{{$data->id}}'>
                                <input type='text' name='name' class='form-control' value='{{$data->name}}'>
                                @if($errors->has('name'))
                                    <span class="help-block">
                                                * <strong>{{ $errors->first('name') }}</strong>
                                                   </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label>السعر : </label>
                                <input type='number' name='price' class='form-control' value='{{$data->price}}'>
                            </div>
                            <br/>
                            <button class="btn btn-block  btn-info">تعديل</button>
                    </form>
        </div>
    </div>
    </div>
@endsection