@component('layouts.include.views')
    @slot('title')
        الإضافات
    @endslot

    @slot('linkAdd')
        <a href="createAddition" class="btn btn-success btn-xs "><span
                    class="glyphicon glyphicon-plus-sign"></span> إضافه
        </a>
    @endslot

    @slot('thead')
        <th><input type="text" class="form-control" placeholder=الاسم disabled></th>
        <th><input type="text" class="form-control" placeholder=" السعر" disabled></th>
        <th></th>
    @endslot

    @slot('tbody')
        @foreach($data as $item)
            <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->price}}</td>
                <td>
                    <a class="btn btn-info btn-xs" href="editAddition/{{$item->id}}"><span
                                class=" glyphicon glyphicon-edit "></span></a>
                </td>
            </tr>
        @endforeach
    @endslot
@endcomponent
