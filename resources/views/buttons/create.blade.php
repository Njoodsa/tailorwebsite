@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar" >
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.flash')
                <h2 class="main-title">إضافه نوع ازرار جديد </h2>
                <form action="insertButton" method="post" class="contentForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <label> الاسم : </label>
                            <input type='text' name='name' class='form-control' value='{{old('name') }}' required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        * <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <label>ملاحظات : </label>
                            <input type='text' name='note' class='form-control' value='{{old('note')}}'>
                            @if($errors->has('note'))
                                <span class="help-block">
                                    * <strong>{{ $errors->first('note') }}</strong>
                                </span>
                                @endif
                        </div>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>
@endsection