@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title">الارادات لسنة :  {{$year}}  </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                            </div>
                            <div class="pull-left">
                                <button class="btn btn-success btn-xs "> مجموع الارادات :
                                    {{$total}}
                                </button>
                            </div>
                        </div><br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder= العميل   disabled></th>
                                <th><input type="text" class="form-control" placeholder=" التاريخ " disabled></th>
                                <th><input type="text" class="form-control" placeholder=" المبلغ  " disabled></th>
                                <th><input type="text" class="form-control" placeholder=" **  " disabled></th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>{{$item->order_information->customer->name}}</td>
                                    <td>{{$item->created_at}}</td>
                                    <td>{{$item->price - $item->discount }}</td>
                                    @if($item->order_information->type == 'product')
                                        <td>منتج </td>
                                        <td><a class="btn btn-info btn-xs"
                                           href="../detailProduct_delivered/{{$item->order_information->id}}"><span
                                                    class=" glyphicon glyphicon-edit "></span></a>
                                        </td>

                                        @else
                                        <td> تفصيل </td>
                                        <td>
                                            <a class="btn btn-info btn-xs"
                                               href="../detailDelivered/{{$item->order_information->id}}"><span
                                                        class=" glyphicon glyphicon-edit "></span></a>
                                        </td>
                                        @endif

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection

