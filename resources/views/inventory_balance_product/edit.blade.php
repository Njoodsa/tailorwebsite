@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title"> ارصدة المخزون</h2>
                <form action="../updateInventory_balance/{{$data->id}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <input name = 'id' value="{{$data->id}}" hidden>
                    <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label> * المخزن : {{$data->warehouse->name}} </label>

                                    <input type="hidden" name ='fabrics_id' id='fabrics_id' value="{{$data->fabrics_id}}">
                                </div>
                            </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * المنتج : {{$data->products->name}} </label>

                                <input type="hidden" name ='fabrics_id' id='fabrics_id' value="{{$data->fabrics_id}}">
                            </div>
                        </div>
                        <p id='test '></p>

                        <div class="col-lg-12">
                            <table class="table">
                                <thead>
                                <th>*</th>
                                <th>الحالي</th>
                                </thead>
                                <tbody>
                                <td style="width: 100px">الرصيد الحالي</td>
                                <td>
                                     <input type='text' id='current_balanceToTaga'
                                       class='form-control'  value = '{{$data->current_balance}}  {{$data->products->unit}}' readonly>


                                </td>
                                <tr>
                                    <td> سعر المنتج :</td>
                                     <td><input type='number' id='current_price' class='form-control'
                                              value = '{{$data->products->price}}' readonly></td>
                                </tr>
                                <tr>
                                    <td> التكلفة :</td>
                                    <td><input class='form-control' value="{{$data->current_balance * $data->products->price }}" readonly></td>
                                </tr>
                                <tr>
                                    <td> حد الطلب  :</td>
                                    <td><input id='demand_limitTaga' class='form-control' name="demand_limit"
                                               value='{{$data->demand_limit }}' required></td>

                                </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">تعديل </button>
                </form>
                @foreach($result as $item)
                    <div class="panel panel-info specific_details_item">
                        <div class="panel-heading ">
                            <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>&nbsp;
                            السنة
                            :<?php echo date('Y', strtotime($item[0]->created_at)); ?>
                        </div>
                        <div class="panel-body specific_details_body" hidden>
                        <?php $total_bill = 0 ;
                        $total_invoice=0;
                        ?>
                        @foreach($item as $i)
                            <!-- test -->
                                @if($i->Bill_id)
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <p> خصم <b>({{ $i->number}})  {{$data->products->unit}}</b> بتاريخ
                                                <b>{{$i->created_at->format('d- m -Y ')}}</b>
                                                طلب رقم
                                                <input type="hidden" value="{{$total_bill+= $i->number}}">
                                                @if( $i->bill->order_information->state == 7)
                                                    <a href="../editOrderProduct/{{$i->bill->order_information->id}}">
                                                        <b>{{$i->bill->order_information->id}}</b>
                                                    </a>
                                                 @elseif($i->bill->order_information->state == 6)
                                                    <a href="../detailProduct_delivered/{{$i->bill->order_information->id}}">
                                                        <b>{{$i->bill->order_information->id}}</b>
                                                    </a>
                                                @else
                                                    <b>{{$i->bill->order_information->id}}</b>
                                            @endif

                                        </div>
                                    </div>
                                @elseif($i->supplier_invoices_id)
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <p> إضافة <b>({{$i->number}}) {{$data->products->unit}} </b>مشتريات بتاريخ

                                                <b>{{$i->created_at->format('d - m -Y ')}}</b>
                                                فاتورة رقم <b> <a href="../detailCompleted_product/{{$i->supplier_invoices_id}}">
                                                        {{$i->supplier_invoices_id}}</a></b></p>
                                        </div>
                                    </div>
                                    <input  type="hidden" value="{{$total_invoice+=$i->number}}">
                                @endif
                            @endforeach
                            <div class="col-lg-6 col-xs-6">
                                <b>مجموع الخصم  = {{$total_bill}}</b>
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                <b>مجموع الإضافة = {{$total_invoice}}</b>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection

