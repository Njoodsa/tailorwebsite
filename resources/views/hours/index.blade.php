@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6 ">
                <h2 class="main-title">ساعات الحجز </h2>
                <div class="row">
                    <div class="pull-right">
                        <a href="{{url('add_hour')}}" class="btn btn-success btn-xs btn-add">
                            <span class="glyphicon glyphicon-plus-sign"></span>
                            إضافة
                        </a>
                    </div>
                    <br/>
                    <table class="table filterable center-content">
                        <thead>
                        <tr>
                            <th>الساعة</th>
                            <th>حذف</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($hours as $hour)
                            <tr>
                                <td>{{$hour->hour}}</td>
                                <td>
                                    <form action="deleteHour/{{$hour->id}}" method="post">
                                        @csrf
                                        <button class="btn btn-danger btn-xs" href="deleteHour/"><span
                                                    class=" glyphicon glyphicon-trash "></span></button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection
