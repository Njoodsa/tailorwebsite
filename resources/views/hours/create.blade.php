@extends('layouts.app')
@section('content')
<div class="container background-container">
    <div class="row">
        <div class="col-md-6 margin-SidBar">
            @include('layouts.SidBar')
        </div>
        <div class="col-md-6">
            @include('layouts.error')
            <h2 class="main-title">إضافة ساعة  </h2>
            <form action="insertHour" method="post" class="contentForm" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-group">
                        <label> الساعة : </label>
                        <input type='text' name='hour' class='form-control' placeholder="2:00" value='{{old('hour') }}'
                          required>
                    </div>
                </div>
                <br/>
                <button class="btn btn-block  btn-success">إضافة</button>
            </form>
        </div>
    </div>
</div>
@endsection