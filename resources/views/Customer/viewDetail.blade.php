@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6  margin-SidBar">
                <h2 class="main-title"> القياسات </h2>
                <div class="form-group row" id="cn" name="cn">
                        <div id='div_1' hidden>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <img class="img-responsive" src="{{asset("images/logoman.png")}}"
                                                 width="20%"
                                                 style="margin-right: 35%;">
                                        </div>
                                    </div>
                                    <br><br>
                                </div>
                            </div>
                            <div>
                                <p style="float: left">Licence No : 2161102 C.R. 1010017936</p>
                                <p>سجل رقم ١٠١٠٠١٧٩٣٦ ترخيص رقم ١٠٢/٦/٢</p>
                                <hr>
                            </div>
                        </div>
                        <form action="../update_measurement/{{$item->id}}" method="post">
                            {{ csrf_field() }}
                            <div  style="margin-bottom: -6%;">
                                <div class="panel panel-info" >
                                    <div class="panel-heading ">
                                        <button  class='btn btn-xs btn-info'  onclick=printDiv("NewDiv_{{ $item->id}}")>طباعة</button>
                                        <button  class='btn btn-xs btn-info' type="submit" style="float: left" >تعديل</button>
                                        <div style="float: left; margin-left: 35%;"> تاريخ الاضافة:<?php echo date('Y-m-d', strtotime($item->created_at)); ?> </div>

                                    </div>
                                    <div class="panel-body " id="NewDiv_{{ $item->id}}">
                                        @include('layouts.helper.basic_measurement')
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="div_4" hidden>
                                <hr>
                                <div style="text-align: center">
                                    <p>الرياض - طريق الملك فهد - غرب مكتبة الملك فهد الوطنيه . هاتف : ٠١١٢٠٠٠٥٦٧ جوال :
                                        ٠٥٣٠١٠٨٥٤٩</p>
                                    <p>Riyadh - King Fahad Road- west of king fahad national library. Tel: 011 2000567
                                        Mobile : 0530108549</p>
                                </div>
                            </div>
                            <br>
                        </form>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
    <script type="text/javascript">
        //print

        function printDiv(NewDiv) {
            var panel = document.getElementsByClassName('panel-body');

            var div_1 = document.getElementById('div_1');
            var div_3 = document.getElementById(NewDiv);
            var div_4 = document.getElementById('div_4');
            var combined = document.createElement('div');
            var printContents = combined.innerHTML = div_1.innerHTML + " "  +
                div_3.innerHTML + " " + div_4.innerHTML; // a little spacing
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }


    </script>
@endsection