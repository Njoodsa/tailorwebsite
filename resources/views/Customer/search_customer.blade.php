@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar ">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">

                <h2 class="main-title"> نتيجة البحث  </h2>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <a href="createCustomer" class="btn btn-info btn-xs "><span
                                            class="glyphicon glyphicon-plus-sign"></span> إضافه</a>
                                <a href="Customer" class="btn btn-info btn-xs "> عودة  <span
                                            class="glyphicon glyphicon-backward"></span>  </a>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <table class="table">
                    <thead>
                    <tr>
                        <th>الاسم </th>
                        <th>جوال </th>
                        <th>بداية التعامل </th>
                        <th></th>
                        <th ></th>
                        <th ></th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data->customer as $customer)
                        <tr>
                            <td>{{$customer->name}}</td>
                            <td>{{$data->phone}}</td>
                            <td>{{$customer->created_at}}</td>
                            <td>
                                <a class="btn btn-info btn-xs" href="editCustomer/{{$customer->id}}"><span
                                            class=" glyphicon glyphicon-fullscreen "></span></a>

                            </td>
                            <td>
                                <a class="btn btn-info btn-xs" href="createOrder/{{$customer->id }}">
                                    إضافة طلب </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    </div>
    </div>

    @include('layouts.javascript')
@endsection

