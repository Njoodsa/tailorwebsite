<style>
    #text {

        width: 57%;
        height: 36%;
        text-align: center;
        margin-right: 22%;
        padding: 1%;
        background: #f7f7f7;
        border-radius: 7px;
        border: 1px solid #e1e0e2;
    }

</style>
@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.flash')
                <h2 class="main-title">ارسال رسالة  </h2>
                <div class="form-group row" id="cn" name="cn">
                    <form action="send" method="post">
                        {{ csrf_field() }}

                        <div class="panel panel-success panel_detail_orderpanel panel-success panel_detail_order">
                            <div class="panel-heading">
                                ارسال رسالة للعملاء
                            </div>
                            <div class="panel-body">
<textarea name ='massage' id="text" placeholder="محتوى الرساله"></textarea>
                                <br/>
                                <button style="margin-right: 47%;" class="btn btn-success">ارسال </button>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    @include('layouts.javascript')
@endsection