@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar ">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title"> العملاء</h2>
                @include('layouts.error')
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <a href="createCustomer" class="btn btn-info btn-xs "><span
                                            class="glyphicon glyphicon-plus-sign"></span> إضافه</a>
                            </div>
                            @if (Auth::user()->access == '1')
                            &nbsp;
                            <a href="{{ URL::to('downloadExcel/xls') }}"><button class="btn btn-info btn-xs">Download Excel</button></a>
                             @endif
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <form class="navbar-form" role="search" method="post" action="search_number">
                            {{csrf_field() }}
                            <div class="input-group add-on">
                                <input class="form-control" placeholder="ادخل رقم الجوال " name="search" id="" type="number" required>
                                <div class="input-group-btn">
                                    <button class="btn btn-info" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                        </form>
                        <form class="navbar-form" role="search" method="post" action="search_name">
                            {{csrf_field() }}
                            <div class="input-group add-on">
                                <input class="form-control typeahead" placeholder="ادخل  الاسم " name="search" type="text" required>
                                <div class="input-group-btn">
                                    <button class="btn btn-info" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                    <br/>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>الاسم </th>
                                <th>جوال </th>
                                <th>بداية التعامل </th>
                                <th></th>
                                <th ></th>
                                <th ></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $customer)
                                <tr>
                                    <td>{{$customer->name}}</td>
                                    <td>{{$customer->mobile->phone}}</td>
                                    <td>{{$customer->created_at}}</td>
                                    <td>
                                        <a class="btn btn-info btn-xs" href="editCustomer/{{$customer->id}}"><span
                                                    class=" glyphicon glyphicon-fullscreen "></span></a>
                                    </td>
                                    <td>
                                        <a class="btn btn-info btn-xs" href="createOrder/{{$customer->id }}">
                                            إضافة طلب </a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $data->links() }}
                </div>
            </div>

    @include('layouts.javascript')
@endsection

