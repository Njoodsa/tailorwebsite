@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">إضافة عميل جديد</h2>
                <form action="insertCustomer" method="post" >
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6 col-xs-6">
                            <input value="new" type='radio' name="select" id='new'
                                   class="test-select" required>
                            <label> رقم جوال جديد </label>
                        </div>
                        <div class="col-lg-6  col-xs-6">
                             <input value="old" type='radio' name="select" id='old'
                                                              class="test-select" required>
                            <label>رقم جوال موجود سابقاً </label>
                        </div>
                    </div>
                    <br>
                    <div class="form-group row">
                        <div class="panel panel-success panel_detail_order">
                            <div class="panel-heading">البيانات</div>
                            <div class="panel-body row">
                                <div class="form-group col-lg-4">
                                    <label> * الاسم : </label>
                                    <input type='text' name='name' class='form-control' value='{{old('name') }}' required>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>* رقم الجوال  : </label>
                                    <input type='text' name='phone' class='form-control' value='{{old('phone') }}' placeholder="0505555555"  required>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>ملاحظات  : </label>
                                    <input type='text' name='note' class='form-control' value='{{old('note') }}'>
                                </div>
                            </div>
                        </div>

                    </div>
                    <button class="btn btn-success btn-block" type="submit ">إضافه</button>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection