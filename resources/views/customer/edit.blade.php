<style>
    .table_width {
        padding: 1px;
        width: 48px;
    }
</style>
@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title"> بيانات العملاء </h2>
                <div class="form-group row" id="cn" name="cn">
                    <form action="../update_information/{{$data->id}}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6  col-xs-6">
                                 <input value="old" type='radio' name="select" id='old'
                                                                  class="test-select" >
                                <label>رقم جوال موجود سابقاً </label>
                            </div>
                        </div>
                        <br/>

                        <input name='id' value='{{$data->id}}' hidden>
                        @include('layouts.helper.style_prinet')
                            <div class="panel panel-success panel_detail_orderpanel panel-success panel_detail_order">
                                <div class="panel-heading">
                                    <button class='btn btn-xs btn-success' type="submit" style="float: left">تعديل
                                    </button>
                                    البيانات
                                    <div class="start_work"> بداية التعامل :
                                        :{{$data->created_at}} </div>
                                </div>
                                <div id="div_2">
                                <div class="panel-body">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-xs-4">
                                        <label> * الاسم : </label>
                                        <input type='text' name='name' class='form-control' value='{{$data->name}}'
                                               required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} col-xs-4">
                                        <label>* جوال : </label>
                                        <input type='text' name='phone' class='form-control'
                                               value='0{{$data->mobile->phone}}'
                                               required>
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }} col-xs-4">
                                        <label> الملاحظات :</label>
                                        <input type='text' name='note' class='form-control' value='{{$data->note}}'
                                        >
                                        @if ($errors->has('note'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if($measurement)
                    <div style="margin-bottom: -6%;">
                        <div class="panel panel-info form-group row panel_detail_order specific_details_item">
                            <div class="panel-heading ">
                                        <span onclick="visalibaty('NewDiv_{{ $measurement->id}}')"
                                              class="glyphicon glyphicon-menu-down"></span>&nbsp;

                                القياسات رقم :
                                <button class='btn btn-xs btn-info'
                                        style="float: left ;margin-right: 3px;"
                                        onclick=printDiv("NewDiv_{{ $measurement->id}}")>طباعة
                                </button>
                            </div>
                            <div class="specific_details_body panel-body" id="NewDiv_{{ $measurement->id}}" >
                                @include('layouts.helper.basic_measurement')
                            </div>
                        </div>
                    </div>
@endif
<br/>
                    <div class="panel panel-info form-group row panel_detail_order specific_details_item">
                        <div class="panel-heading ">الطلبات </div>
                        <div class=" filterable">
                        <div class="pull-left" style="margin-left: 20px;margin-top: -46px;">
                            <button class="btn btn-info btn-xs btn-filter"><span
                                        class="glyphicon glyphicon-filter"></span> بحث
                            </button>
                            <a class="btn btn-info btn-xs" href="{{url('createOrder/'.$data->id)}}">
                                إضافة طلب </a>
                        </div>
                        <div class="specific_details_body panel-body" id="NewDiv_2851">
                            @if($data->orders ?? null)
                            <table class="table">
                                <thead>
                                <tr class="filters">
                                    <th><input type="text" class="form-control" placeholder="رقم الطلب " disabled=""></th>
                                    <th><input type="text" class="form-control" placeholder="تاريخ الطلب " disabled=""></th>
                                    <th ><input type="text" class="form-control" placeholder="حالة الطلب " disabled=""></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                              @foreach($data->orders as $order)
                                <tr>
                                    @if($order->state == 2 || $order->state == 3 || $order->state == 4 ||
                                    $order->state == 5 ||$order->state == 6)
                                    <td>{{$order->id}}</td>
                                    <td>{{$order->bill->created_at}}</td>
                                    @if($order->state == 2)
                                        <td>في إنتظار المراجعة </td>
                                        <td>
                                            <a class="btn btn-info " href="{{url('editOrder/'.$order->id)}}"><span class=" glyphicon glyphicon-fullscreen "></span></a>
                                        </td>
                                    @elseif($order->state == 3)
                                        <td>قيد العمل </td>
                                        <td>
                                            <a class="btn btn-info " href="{{url('DetailWorked_order/'.$order->id)}}"><span class=" glyphicon glyphicon-fullscreen "></span></a>
                                        </td>
                                    @elseif($order->state == 5)
                                        <td>ملغي</td>
                                        <td>
                                            <a class="btn btn-info " href="{{url('order_canceled/'.$order->id)}}"><span class=" glyphicon glyphicon-fullscreen "></span></a>
                                        </td>
                                    @elseif($order->state == 4)
                                        <td> منجز  </td>
                                        <td>
                                            <a class="btn btn-info " href="{{url('detail_orders_completed/'.$order->id)}}"><span class=" glyphicon glyphicon-fullscreen "></span></a>
                                        </td>

                                    @elseif($order->state == 6 && $order->type == 'product')
                                        <td>مستلم  </td>
                                        <td>
                                            <a class="btn btn-info " href="{{url('detailProduct_delivered/'.$order->id)}}"><span class=" glyphicon glyphicon-fullscreen "></span></a>
                                        </td>
                                    @elseif($order->state == 6 && $order->type == 'fabirc')
                                        <td>مستلم  </td>
                                        <td>
                                            <a class="btn btn-info " href="{{url('detailDelivered/'.$order->id)}}"><span class=" glyphicon glyphicon-fullscreen "></span></a>
                                        </td>
                                        @endif
                                    @endif
                                </tr>
                                  @endforeach
                                </tbody>
                            </table>
                                @endif
                    </div>
                        </div>
                    <div id="div_4" hidden>
                        <hr>
                        <div style="text-align: center">
                            <p>الرياض - طريق الملك فهد - غرب مكتبة الملك فهد الوطنيه . هاتف : ٠١١٢٠٠٠٥٦٧ جوال :
                                ٠٥٣٠١٠٨٥٤٩</p>
                            <p>Riyadh -King Fahad Road- west of king fahad national library. Tel: 011 2000567 Mobile :
                                0530108549</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
    <script type="text/javascript">
        //print

        function visalibaty(show1) {
            $('#' + show1).fadeToggle();
        }
        function printDiv(NewDiv) {
            var panel = document.getElementsByClassName('panel-body');

            var div_1 = document.getElementById('div_1');
            var div_2 = document.getElementById('div_2');
            var div_3 = document.getElementById(NewDiv);
            var div_4 = document.getElementById('div_4');
            var combined = document.createElement('div');
            var printContents = combined.innerHTML = div_1.innerHTML + " " + div_2.innerHTML + " " +
                div_3.innerHTML + " " + div_4.innerHTML; // a little spacing
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }


    </script>
@endsection
