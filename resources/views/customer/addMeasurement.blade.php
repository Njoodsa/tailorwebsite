@extends('layouts.app')
@section('content')
    <div class="container background-container">

        <div class="row">
            <div class="col-md-6  margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">إضافة قياسات</h2>
                <form action="../insertNewMeasurement/{{$data->id}}" method="post" >
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="panel panel-success panel_detail_order">
                            <div class="panel-heading">إضافة قياسات تابعه لللعميل :
                                <a class="btn btn-success btn-xs" style="float: left" href="../createOrder/{{$data->id }}">  إضافة طلب </a>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label> * الاسم : </label>
                                    <input name='id' value='{{$data->id }}'  hidden>
                                    <input type='text' name='name' class='form-control' value='{{$data->name }}'  readonly required>
                                </div>
                                <div class="form-group">
                                    <label>* جوال  : </label>
                                    <input type='text' name='phone' class='form-control' value='0{{$data->mobile->phone }}' readonly required>
                                </div>
                        </div>
                        </div>
                        @include('layouts.helper.measurement')
                    </div>
                    <button class="btn btn-success btn-block" type="submit ">إضافه</button>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection