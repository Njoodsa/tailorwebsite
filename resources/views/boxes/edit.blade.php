@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title"> بيانات الصندوق  </h2>
                <form action="../updateBox/{{$data->id}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label> * الاسم : </label>
                        <input type='text' name='name' class='form-control' value='{{$data->name}}' required>
                    </div>
                    <div class="form-group">
                        <label> الفرع  :{{$data->branches->name}} </label>
                        <select class='form-control' name='branches_id' required>
                            <option value="">-- اختر الفرع -- </option>
                            @foreach($branches as $branch)
                                <option value=" {{$branch->id }}"> {{$branch->name }} </option>
                            @endforeach
                        </select>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">تعديل</button>
                </form>
            </div>
        </div>
    </div>
@endsection