@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title">الصناديق </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                                <a href="createBox" class="btn btn-success btn-xs "><span
                                            class="glyphicon glyphicon-plus-sign"></span> إضافه</a>
                            </div>
                        </div>
                        <br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder=رمز disabled></th>
                                <th><input type="text" class="form-control" placeholder=الاسم disabled></th>
                                <th><input type="text" class="form-control" placeholder=الفرع disabled></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $box)
                                <tr>
                                    <td> {{$box->id}}</td>
                                    <td>{{$box->name}}</td>
                                    <td>{{$box->branches->name}}</td>
                                    <td>
                                        <a class="btn btn-info btn-xs" href="editBox/{{$box->id}}"><span
                                                    class=" glyphicon glyphicon-fullscreen "></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')

@endsection

