@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-7 ">
                <p class="title_result_search"> نتيجة البحث من الفترة:
                    <b>  {{$first_date}} الى {{$second_date}}</b>
                </p>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <button class="btn btn-info btn-xs btn-filter"><span
                                        class="glyphicon glyphicon-filter"></span> بحث
                            </button>
                        </div>
                        <br/>
                        <div style="overflow-x:auto;">
                            <table class="table">
                                <thead>
                                <tr class="filters" align="center">
                                    <th><input type="text" class="form-control" placeholder='*' disabled></th>
                                    <th><input type="text" class="form-control" placeholder=" الحالة   " disabled></th>
                                    <th><input type="text" class="form-control" placeholder=' المدفوع/ كاش    '
                                               disabled></th>
                                    <th><input type="text" class="form-control" placeholder='  بطاقة    ' disabled></th>
                                    <th><input type="text" class="form-control" placeholder="المتبقي  " disabled></th>
                                    <th><input type="text" class="form-control" placeholder="طريقة الدفع  " disabled>
                                    </th>
                                    </th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $totalPaid = 0; $total_residual_not_deliver = 0; $residual_order_deliver = 0; $totalThop = 0 ?>
                                @foreach($data as $Order)
                                    <tr >
                                        @if($Order->state == 7)
                                            <td>
                                                <a href="{{url('editOrderProduct/'.$Order->id)}}">
                                                    {{$Order->id}}
                                                </a>
                                            </td>
                                            <td> غير مكتمل</td>
                                        @elseif($Order->state == 6)
                                            <td>
                                                <a href="{{url('detailProduct_delivered/'.$Order->id)}}">
                                                    {{$Order->id}}
                                                </a>
                                            </td>
                                            <td>مستلم</td>
                                        @endif
                                        <td>{{$Order->bill->paid_up }}</td>
                                        <td>{{$Order->bill->paid_up_card ?? 0 }}</td>
                                        @if($Order->state == 6 )
                                            <td>0</td>
                                            <input type="hidden" value="{{$residual_order_deliver   += $Order->bill->Residual }}">
                                        @elseif($Order->state !=5 && $Order->state != 6 )
                                            <td>{{ $Order->bill->Residual}}</td>
                                            <input type="hidden" value="{{$total_residual_not_deliver += $Order->bill->Residual}}">
                                        @endif
                                        <td>{{$Order->bill->payment_method->name}}</td>
                                    </tr>
                                    <!-- hidden input -->
                                    <input type="hidden" value="{{$totalPaid+=$Order->bill->paid_up
                                            + $Order->bill->paid_up_card}}">
                                @endforeach
                                </tbody>

                            </table>
                            <table class="table">
                                <thead>
                                <tr align="center">
                                    <td><b>مجموع المدفوع </b></td>
                                    <td><b>مجموع المتبقي</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr align="center">
                                    <!-- N MEAN  PAID UP  , T  MEAN RESIDUAL -->
                                    <td>{{$totalPaid + $residual_order_deliver}}</td>
                                    <td>{{$total_residual_not_deliver}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="text-align: left">
                            <a href="{{url('report/search/fabric')}}" class="btn btn-info" name='type'>عودة </a>
                            <button class="btn btn-info " type="button" onclick=printDiv("cn")>طباعة التقرير</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.helper.bill_report_product')

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    @include('layouts.javascript')
@endsection