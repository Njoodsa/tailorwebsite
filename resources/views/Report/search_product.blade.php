@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title">تقارير الفواتير - للمنتجات  - </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading" style="background: #ecece6;">
                            <h3 class="panel-title"></h3>
                            <p> ادخل الفترة : </p>
                            <form class="navbar-form" method="post" action="{{url('report/result/product')}}">
                                {{csrf_field() }}
                                <select name="state" class="form-control" required>
                                    <option value="" disabled selected="selected">اختر الحالة</option>
                                    <option value="0">الكل</option>
                                    <option value="7">غير مكتمل</option>
                                    <option value="6">مستلم</option>
                                </select>
                                <input class="form-control typeahead" name="first_date" type="date"
                                       style="    margin-top: 1%;"
                                       required>
                                إلى
                                <div class="input-group add-on">
                                    <input class="form-control typeahead" name="second_date" type="date"
                                           required>
                                    <div class="input-group-btn">
                                        <button class="btn btn-info" type="submit"><i
                                                    class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection

