@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title"> تقرير الاقمشة  </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                            </div>
                        </div>
                        <br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder='القماش ' disabled></th>
                                <th></th>
                                <th><input type="text" class="form-control" placeholder=" مجموع الإضافة " disabled></th>
                                <th><input type="text" class="form-control" placeholder=" مجموع الخصم " disabled></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($result as $key=>$item)
                                <tr>
                                    <td colspan="2"> {{$key}}</td>
                                    <td>{{ array_sum(array_column($item, 'total_cost'))}}    </td>
                                    <td>{{ array_sum(array_column($item, 'total_yeardh'))}}    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.javascript')
@endsection

