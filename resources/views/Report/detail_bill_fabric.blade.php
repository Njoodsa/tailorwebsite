@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-7 ">
                <p class="title_result_search"> نتيجة البحث من الفترة:
                    <b>  {{$first_date}} الى {{$second_date}}</b>
                </p>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <button class="btn btn-info btn-xs btn-filter"><span
                                        class="glyphicon glyphicon-filter"></span> بحث
                            </button>
                        </div>
                        <br/>
                        <div style="overflow-x:auto;" class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr class="filters">
                                    <th><input type="text" class="form-control" placeholder='*' disabled></th>
                                    <th><input type="text" class="form-control" placeholder=" الحالة   " disabled></th>
                                    <th><input type="text" class="form-control" placeholder=" الإجمالي     " disabled></th>
                                    <th><input type="text" class="form-control" placeholder=" عدد الثياب   " disabled>
                                    <th><input type="text" class="form-control" placeholder='المقدم نقداً 'disabled></th>
                                    <th><input type="text" class="form-control" placeholder='  المقدم شبكة ' disabled></th>
                                    <th><input type="text" class="form-control" placeholder='  المؤخر نقدا' disabled></th>
                                    <th><input type="text" class="form-control" placeholder="المؤخر شبكة " disabled></th>
                                    <th><input type="text" class="form-control" placeholder="طريقة دفع اخرى  " disabled></th>
                                    <th><input type="text" class="form-control" placeholder="الخصم " disabled></th>
                                    <th><input type="text" class="form-control" placeholder="المتبقي  " disabled></th>
                                    <th><input type="text" class="form-control" placeholder="مجموع المدفوع شبكة   " disabled></th>
                                    <th><input type="text" class="form-control" placeholder="مجموع المدفوع نقد  " disabled></th>
                                    <th><input type="text" class="form-control" placeholder="طريقة الدفع  " disabled></th>
                                    </th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                               <?php $totalPaid = 0; $total_residual_not_deliver = 0; $residual_order_deliver = 0; $totalThop = 0 ?>
                                @foreach($data as $Order)
                                    <tr>
                                        @if($Order->state == 3)
                                            <td>
                                                <a href="{{url('DetailWorked_order/'.$Order->id)}}">
                                                    {{$Order->id}}
                                                </a>
                                            </td>
                                            <td>قيد العمل</td>
                                        @elseif($Order->state == 4)
                                            <td>
                                                <a href="{{url('detail_orders_completed/'.$Order->id)}}">
                                                    {{$Order->id}}
                                                </a>
                                            </td>
                                            <td> منجز</td>
                                        @elseif($Order->state == 6)
                                            <td>
                                                <a href="{{url('detailDelivered/'.$Order->id)}}">
                                                    {{$Order->id}}
                                                </a>
                                            </td>
                                            <td>مستلم</td>
                                        @endif
                                          <td> {{number_format((float)((($Order->bill->price )+$Order->bill->price*0.05) - $Order->bill->discount), 2, '.', '')
                                          }}</td>
                                            <td>{{$Order->bill->order->sum('number')}}</td>
                                            <td>{{$Order->bill->paid_up }}</td>
                                            <td>{{$Order->bill->paid_up_card }}</td>
                                            <td>{{$Order->bill->paid_later }}</td>
                                            <td>{{$Order->bill->paid_card_later}}</td>
                                            <td>{{$Order->bill->other_payment_method}}</td>
                                            <td>{{$Order->bill->discount}}</td>
                                        @if($Order->state == 6 )
                                            <td>0</td>
                                            <!-- <input type="hidden" value="{{$residual_order_deliver += $Order->bill->Residual }}"> -->
                                         @elseif($Order->state !=5 && $Order->state != 6  )
                                             <td>{{ $Order->bill->Residual}}</td>
                                            <input type="hidden" value="{{$total_residual_not_deliver += $Order->bill->Residual}}">
                                        @endif
                                        <td>{{$Order->bill->paid_up_card + $Order->bill->paid_card_later }}</td>
                                        <td>{{$Order->bill->paid_up + $Order->bill->paid_later }}</td>
                                        <td>{{$Order->bill->payment_method->name}}</td>



                                    </tr>
                                    <!-- hidden input -->
                                    <input type="hidden" value="{{$totalThop += $Order->bill->order->sum('number')}}">
                                    <input type="hidden" value="{{$totalPaid+= ($Order->bill->paid_up
                                            + $Order->bill->paid_up_card
                                            + $Order->bill->paid_card_later
                                            + $Order->bill->paid_later
                                            + $Order->bill->other_payment_method)}}">
                                @endforeach
                                </tbody>

                            </table>
                            <table class="table">
                                <thead>
                                <tr align="center">
                                    <!-- <td><b>مجموع المدفوع </b></td>
                                    <td><b>مجموع المتبقي</b></td> -->
                                    <td><b>الإجمالي</b></td>
                                    <td><b>مجموع الثياب</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr align="center">
                                    <!-- N MEAN  PAID UP  , T  MEAN RESIDUAL -->
                                    <!-- <td>{{$totalPaid }}</td>
                                    <td>{{$total_residual_not_deliver}}</td>-->
                                    <?php $price=0?>
                                    <td><b>{{number_format((float)(($price = $data->pluck('bill')->sum('price')) + ($price * 0.05)), 2, '.', '')}}</b></td>
                                    <td>{{$totalThop}}</td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="text-align: left">
                            <a href="{{url('report/search/fabric')}}" class="btn btn-info" name='type'>عودة </a>
                            <button class="btn btn-info " type="button" onclick=printDiv("cn")>طباعة التقرير</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.helper.bill_report')

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    @include('layouts.javascript')
@endsection
