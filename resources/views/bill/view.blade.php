@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title"> الفواتير الغير مكتمله  </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                                <a href="createOrder" class="btn btn-success btn-xs "><span
                                            class="glyphicon glyphicon-plus-sign"></span> إضافه</a>
                            </div>
                        </div>
                        <br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder='اسم العميل ' disabled></th>
                                <th><input type="text" class="form-control" placeholder='رقم الجوال' disabled></th>
                                <th><input type="text" class="form-control" placeholder="تاريخ الطلب " disabled></th>
                                <th><input type="text" class="form-control" placeholder=" الحاله " disabled></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bills as $bill)
                                <tr>
                                    <td>{{$bill->order_detail->customer->name}}</td>
                                    <td>0{{ $bill->order_detail->customer->mobile->phone}}</td>
                                    <td>{{$bill->created_at}}</td>
                                    @if($bill->state == 0)
                                    <td>غير مكتمل</td>
@endif
                                    <td>
                                        <a class="btn btn-info btn-xs" href="editOrder/{{$bill->order_detail_id}}"><span
                                                    class=" glyphicon glyphicon-fullscreen "></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.javascript')
@endsection

