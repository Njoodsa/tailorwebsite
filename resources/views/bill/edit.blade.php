@extends('layouts.app')
@section('content')
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error.error')
                <h2 class="main-title">رؤية الطلب </h2>
                <form action="../updateOrder/{{$data->id}}" method="post">
                    {{ csrf_field() }}
                    </br>
                    <div class="form-group row">
                        <div class="panel panel-success">
                            <div class="panel-heading"><a href="../editCustomer/{{$data->customer->id}}">رؤية قياسات
                                    العميل </a></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <input type='hidden' class='form-control' name="id_customer"
                                           value='{{$data->customer->id}} '>
                                    <label>اسم العميل : </label>
                                    <input type='text' class='form-control' value='{{$data->customer->name}} ' readonly>
                                </div>
                                <div class="form-group">
                                    <label> رقم الجوال: </label>
                                    <input type='text' class='form-control' value='{{$data->customer->mobile->phone}}'
                                           readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info form-group row">
                        <div class="panel-heading"> تفاصيل الطلب</div>
                        <div class="panel-body">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <select name="bar" id="selectpicker" required>
                                        <option @if($data->bar =='MAKFI ON SOFT') selected @endif
                                        value='MAKFI ON SOFT'>MAKFI ON SOFT
                                        </option>

                                        <option @if($data->bar =='MAKFI W-SOFT FAT') selected @endif
                                        value='MAKFI W-SOFT FAT'>MAKFI W-SOFT FAT
                                        </option>

                                        <option @if($data->bar =='SEC-SEWING') selected @endif
                                        value='SEC-SEWING'>SEC-SEWING
                                        </option>

                                        <option @if($data->bar =='KUWAITI W/PLEATS') selected @endif
                                        value='KUWAITI W/PLEATS'>KUWAITI W/PLEATS
                                        </option>
                                    </select>
                                    <label> : Bar</label>
                                </div>
                                <div class="col-lg-6">
                                    <select name='id_design' required>
                                        @foreach($Designs as $design)
                                            <option @if ($data->design->id ==$design->id)selected
                                                    @endif value='{{$design->id}}'>{{$design->name}}</option>
                                        @endforeach
                                    </select>
                                    <label>: Style </label>
                                </div>
                            </div>
                            <div class="page-header"></div>
                            <div class='row'>
                                <div id="titleInOrder">الرقبة :</div>
                                @foreach($Necks as $neck)
                                    <div class="col-lg-6 ">
                                        <input type="radio" value="{{$neck->id}}"
                                               @if ($data->neck->id ==$neck->id) checked @endif
                                               name="id_neck" required>
                                        {{$neck->name}}
                                        @if($neck->image != NULL)
                                            <img src="{{asset('images/button/'.$neck->image)}}" width="50%">
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                            <div class="page-header"></div>
                            <div class=row>
                                <div id="titleInOrder">الجيب :</div>
                                @foreach($Pockets as $Pocket)
                                    <div class="col-lg-6 ">
                                        <input type="radio" @if($data->pocket->id == $Pocket->id)checked @endif
                                        value="{{$Pocket->id}}" name="id_pockets" required>
                                        {{$Pocket->name}}
                                        @if($Pocket->image != NULL)
                                            <img src="{{asset('images/button/'.$Pocket->image)}}" width="50%">
                                        @endif
                                    </div>
                                @endforeach
                            </div>

                            <div class="page-header"></div>
                            <div class=row>

                                <div id="titleInOrder"> الاكمام :</div>
                                @foreach($Sleeves as $Sleeve)
                                    <div class="col-lg-6 ">
                                        <input type="radio" @if($data->sleeve->id == $Sleeve->id)checked @endif
                                        value="{{$Sleeve->id}}" name="id_sleeve" required>
                                        {{$Sleeve->name}}
                                        @if($Sleeve->image != NULL)
                                            <img src="{{asset('images/button/'.$Sleeve->image)}}" width="50%">
                                        @endif
                                    </div>
                                @endforeach
                            </div>

                            <div class="page-header"></div>
                            <div class=row>

                                <div id="titleInOrder"> الكبك :</div>
                                @foreach($Buttons as $Button)
                                    <div class="col-lg-6 ">
                                        <input type="radio" @if($data->button->id == $Button->id)checked @endif
                                        value="{{$Button->id}}" name="id_button" required>
                                        {{$Button->name}}
                                        @if($Button->image != NULL)
                                            <img src="{{asset('images/button/'.$Button->image)}}" width="50%">
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                            <div class="page-header"></div>
                            <div class="row">
                                <div id="titleInOrder"> حشوة الياقة :</div>
                                <div class="col-lg-3">
                                    <input type='radio' @if($data->collar_detail == 'دبل')checked @endif
                                    value="دبل" name="collar_detail" required> دبل
                                </div>
                                <div class="col-lg-3">
                                    <input type='radio' @if($data->collar_detail == 'صلبه')checked @endif
                                    value="صلبه" name="collar_detail" required> صلبه
                                </div>
                                <div class="col-lg-3">
                                    <input type='radio' @if($data->collar_detail == 'وسط')checked @endif
                                    value="وسط" name="collar_detail" required> وسط
                                </div>
                                <div class="col-lg-3">
                                    <input type='radio' @if($data->collar_detail == 'ناعمه')checked @endif
                                    value="ناعمه" name="collar_detail" required> ناعمه
                                </div>
                            </div>
                            <div class="page-header"></div>
                            <div class="row">
                                <div id="titleInOrder"> حشوة الكبك :</div>
                                <div class="col-lg-3">
                                    <input type='radio' value='دبل' @if($data->button_detail == 'دبل')checked @endif
                                    name="button_detail" required> دبل
                                </div>
                                <div class="col-lg-3">
                                    <input type='radio' value='صلبه' @if($data->button_detail == 'صلبه')checked @endif
                                    name="button_detail" required> صلبه
                                </div>
                                <div class="col-lg-3">
                                    <input type='radio' value='وسط' @if($data->button_detail == 'وسط')checked @endif
                                    name="button_detail" required> وسط
                                </div>
                                <div class="col-lg-3">
                                    <input type='radio' value='ناعمه' @if($data->button_detail == 'ناعمه')checked @endif
                                    name="button_detail" required> ناعمه
                                </div>
                            </div>
                            <div class="page-header"></div>
                            <div class="row">
                                <div id="titleInOrder"> الجيب :</div>
                                <div class="col-lg-6">
                                    <input type='radio' @if($data->right_pocket == 'جيب يمين مخفي')checked @endif
                                    value='جيب يمين مخفي ' name="right_pocket" required> جيب يمين
                                    مخفي
                                </div>
                                <div class="col-lg-6">
                                    <input type='radio' @if($data->right_pocket == 'جيب يمين غير مخفي')checked @endif
                                    value='جيب يمين غير مخفي' name="right_pocket" required> جيب يمين
                                    غير مخفي
                                </div>
                                <div class="col-lg-6">
                                    <input type='radio' @if($data->left_pocket == 'جيب يسار مخفي')checked @endif
                                    value='جيب يسار مخفي ' name="left_pocket" required> جيب يسار
                                    مخفي
                                </div>
                                <div class="col-lg-6">
                                    <input type='radio' @if($data->left_pocket == 'جيب يسار غير مخفي')checked @endif
                                    value='جيب يسار غير مخفي' name="left_pocket" required> جيب يسار
                                    غير مخفي
                                </div>
                            </div>
                            <div class="page-header"></div>
                            <div class="row">
                                <div id="titleInOrder"> التاريخ :</div>
                                <div class="col-lg-6">
                                    <label> Start Date</label>
                                    <input type='date' required class="form-control" name="start_date"
                                           value="{{$data->bill->start_date}}">
                                </div>
                                <div class="col-lg-6">
                                    <label>Due Date </label>
                                    <input type='date' required class="form-control" name="end_date"
                                           value="{{$data->bill->end_date}}">
                                </div>
                            </div>
                            <div class="page-header"></div>
                            <div class="row">
                                <div id="titleInOrder">الموظفين :</div>
                                <div class="col-lg-6">
                                    <label> القصاص</label>
                                    <select name="id_cutter" class="selectpicker" data-show-subtext="true"
                                            data-live-search="true" required>
                                        @foreach($cutters as $cutter)
                                            <option @if($data->id_cutter ==$cutter->id )selected @endif
                                            value="{{$cutter->id}}">{{$cutter->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>الخياط </label>
                                    <select name="id_tailor" class="selectpicker" data-show-subtext="true"
                                            data-live-search="true" required>
                                        @foreach($tailors as $tailor)
                                            <option @if($data->id_tailor ==$tailor->id )selected @endif
                                            value="{{$tailor->id}}">{{$tailor->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>اخذ المقاسات </label>
                                    <select name="Taking_measurements" class="selectpicker" data-show-subtext="true"
                                            data-live-search="true" required>
                                        @foreach($designs as $design)
                                            <option @if($data->Taking_measurements ==$design->id ) selected @endif
                                            value="{{$design->id}}">{{$design->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- order -->

                    <div class="panel panel-info form-group row" style="width: 700px;margin-right: -92px;">
                        <br/>
                        <div class="panel-heading"> الطلب</div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-lg-12">
                                    <table class="table">
                                        <thead>
                                        <th> اسم القماش</th>
                                        <th>عدد الياردات</th>
                                        <th>عدد الثياب</th>
                                        <th>سعر الثوب</th>
                                        <th>الاجمالي</th>
                                        </thead>
                                        <tbody id="fabric_list">
                                        <!-- the default one -->
                                        @foreach($data->bill->order as $order)
                                            <tr class="fabric_item">
                                                <td>
                                                    <select name="id_fabric[]" class="id_fabric selectpicker "
                                                            data-show-subtext="true" data-live-search="true">

                                                        @foreach($Fabrics as $Fabric)
                                                            <option @if($order->id_fabric ==$Fabric->id ) selected
                                                                    @endif
                                                                    value="{{$Fabric->id}}">{{$Fabric->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="order_id[]" value="{{$order->id}}">
                                                    <input type="number" name='yards[]'
                                                           value={{$order->yards}} id="yardh_number">
                                                </td>
                                                <td>
                                                    <input value='{{$order->number}}' name="number[]" id="thop_number"
                                                           type="text" class="GivePrice">
                                                </td>
                                                <td>
                                                    <input name="price[]" value='{{$order->price}}' id="price"
                                                           type="text" class=" price GivePrice">
                                                </td>
                                                <td><input id="totalPriceThop" name='totalPriceThop[]'
                                                           value='{{$order->price*$order->number }}' type="text"></td>
                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                    <div class="col-lg-3">
                                        العدد :
                                        <input class="total_number" readonly>
                                    </div>
                                    <div class="col-lg-3">
                                        الاجمالي : <input class="cost" readonly>
                                    </div>
                                    <div class="col-lg-3">
                                        الخصم : <input value='{{$data->bill->discount}}' name="discount" id='discount'>
                                    </div>
                                    <div class="col-lg-3">
                                        السعر : <input name="finally_price" class='finally_price'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- BILL -->

                    <div class="panel panel-info form-group row" style="width: 700px;margin-right: -92px;">
                        <br/>
                        <div class="panel-heading"> الدفع</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label> طريقة الدفع </label>
                                    <select name="payment_method_id" class="selectpicker" data-show-subtext="true"
                                            data-live-search="true" required>
                                        @foreach($Payment_methods as $Payment_method)
                                            <option @if($data->bill->payment_method_id ==$Payment_method->id)
                                                    selected
                                                    @endif value="{{$Payment_method->id}}">{{$Payment_method->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label>* المدفوع : </label>
                                    <input type='number' name='paid_up' id="paid_up" class='form-control '
                                           value='{{$data->bill->paid_up}}'
                                           required>
                                </div>

                                <div class="col-lg-4">
                                    <label>* المتبقي : </label>
                                    <input type='number' id="Residual" name='Residual' class='form-control '
                                           value='{{$data->bill->Residual}}'
                                           required>
                                </div>
                                <input type="hidden" name='bill_id' value="{{$data->bill->id }}">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info form-group row" style="width: 700px;margin-right: -92px;">
                        <br/>
                        <div class="panel-heading"> حالة الطلب * في حال تم تاكيد او الغاء الفاتوره و الطلب فلن تستطيع التعديل عليهامرة اخرى </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label> حالة الطلب </label>
                                    <select name="state" class="selectpicker" data-show-subtext="true"
                                            data-live-search="true" required>
                                        <option @if($data->state == 'قيد العمل')selected @endif value="قيد العمل ">قيد
                                            العمل
                                        </option>
                                        <option @if($data->state == 'منجز')selected @endif value="منجز">منجز</option>
                                        <option @if($data->state == 'ملغى')selected @endif value="ملغى">ملغى</option>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label> الفاتوره </label>
                                    <select name="bill_state" class="selectpicker" data-show-subtext="true"
                                            data-live-search="true" required>
                                        <option @if($data->bill->state == 1)selected @endif value="1 " >مكتمل</option>
                                        <option @if($data->bill->state == 0 )selected @endif value="0">غير مكتمل
                                         <option @if($data->bill->state == 2 )selected @endif value="2">ملغى </option>
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success btn-block" type="submit">تعديل</button>

                </form>

                <button class="btn btn-success btn-block" onclick=printDiv("cn")>طباعة الفاتوره</button>


            </div>
        </div>
    </div>

    <!-- TO PRINT BILL -->
    <div id="cn" name="cn">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <hr>
                    <div class="row">
                        <div class="col-xs-12">
                            <img class="img-responsive" src="{{asset("images/logoman.png")}}" width="20%"
                                 style="margin-right: 35%;">
                        </div>
                    </div>
                    <br><br>
                </div>
            </div>
            <div>
                <p style="float: left">Licence No : 2161102 C.R. 1010017936</p>
                <p>سجل رقم ١٠١٠٠١٧٩٣٦ ترخيص رقم ١٠٢/٦/٢</p>
                <hr>
            </div>
            <div style="text-align: center">
                <h3>فاتورة نقدية /أجلة </h3>
                <h4>Cash/Credit Invoice</h4>
            </div>
            <div>
                <p style="float: left">موعد التسليم :{{$data->end_date}} </p>
                <p>التاريخ :{{$data->start_date}}</p>
                <p style="float: left">اسم العميل :{{$data->customer->name}} </p>
                <p>رقم العميل :{{$data->customer->phone}} </p>
            </div>
            <div class="row">f
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-condensed">
                                    <thead>
                                    <tr>
                                        <td><strong>اسم القماش </strong></td>
                                        <td><strong>عدد الثياب </strong></td>
                                        <td><strong>سعر الثوب</strong></td>
                                        <td><strong>الاجمالي </strong></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                    @foreach($data->bill->order as $order)
                                        <tr>
                                            <td>{{$order->fabrics->name}}</td>
                                            <td>{{$order->number}}</td>
                                            <td>{{$order->price}}</td>
                                            <td>{{$order->price*$order->number}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <td><strong>العدد :<p></p> </strong></td>
                            <td class="text-right"><strong>الاجمالي :<input type='text' class="cost" readonly> </strong>
                            </td>
                            <td class="text-center"><strong>الخصم :{{$data->bill->discount}}  </strong></td>
                            <td class="text-center"><strong> السعر بعد الخصم : <input name="finally_price"
                                                                                      class='finally_price'> </strong>
                            </td>
                        </tr>
                        </thead>
                    </table>
                </div>

                <hr>
                <div>
                    <p style="float: left">توقيع المسئول : </p>
                    <p>توقيع العميل بالموافقة على الاسعار و المواصفات بناءا على رغبته وطلبه .................</p>
                    <p>- المرجو من عملالنا االكرام مراجعة بيانات الفاتورة قبل التوقيع و مغادرة المعرض </p>
                    <p>- الادارة ترحب باي مقترحات أو ملاخظات من العملاء هدفنا ثقتكم بنا ونحن في خدمتكم </p>
                    <p>- المعرض غير مسئولا عن اأقمشة و الملابس أذا مضى عليها ثلاثة شهور </p>

                    <hr>
                </div>
                <div style="text-align: center">
                    <p>الرياض - شارع التخصصي قبل محطة نفط ٢٠٠ متر - هاتف : ٠١١٢٠٠٠٥٦٧ جوال : ٠٥٣٠١٠٨٥٤٩</p>
                    <p>Riyadh - Takhassusi Road 200 Metter Befor Naft Station -Tel: 011 2000567 Mobile : 0530108549</p>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">


        // get lawest price
        //----------------
        function checkFabricPrice(fabric_number, price) {
            $.ajax({
                url: 'GetLowestPrice/' + fabric_number,
                Type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data['lowest_price']);
                    var lowest_price = data['lowest_price'];
                    if (lowest_price > price.val()) {
                        alert('ادنى سعر للبيع هو ' + lowest_price)
                        price.val(lowest_price) // set the lawest price
                        // price.value = lowest_price;
                    }
                }
            });
        }

        // whenever fabrics changed
        $('.id_fabric').on('change', function () {
            var fabric_number = $(this).val();
            var price = $(this).parent().parent('.price');
            if (price.val() != '') {
                checkFabricPrice(fabric_number, price)
            }
        });


        //print Bill


        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }


        // whenever price changed
        $('.price').on('blur', function () {
            var fabric_number = $(this).parent().parent('.id_fabric').val();
            var price = $(this);
            checkFabricPrice(fabric_number, price)
        });

        /**

         */
        //GIVE A PRICE TO NUMBER OF THOP
        $('.GivePrice').on('change', function () {
            var result = $('#thop_number').val() * $('#price').val();
            $('#totalPriceThop').val(result);
            var total2 = 0;
            $('input[name="totalPriceThop[]"]').each(function () {
                total2 += parseInt($(this).val());
            });
            $('.cost').val(total2);
        });

        //FUNCTION TO COUNT NUMBER OF THOP

        function totalThop() {
            var total = 0;
            $('input[name="number[]"]').each(function () {
                total += parseInt($(this).val());
                console.log(total);
            });
            $('.total_number').val(total);
            console.log(total);
            totalPriceThop();
        }

        //FUNCTION WILL WORKE AFTER LOAD PAGE

        $(window).bind("load", function () {
            totalThop();
        });

        //GIVE A NUMBER OF A TOTAL NUMBER OF THOP WHEN CHANGE
        $('input[name="number[]"]').on('change', function () {
            totalThop();
        });

        //FUNCTION TO CALCULATE THOP PRICE

        function totalPriceThop() {
            var total2 = 0;
            $('input[name="totalPriceThop[]"]').each(function () {
                total2 += parseInt($(this).val());
            });
            $('.cost').val(total2);
        }

        // check if the cudsr right

        $('#Residual').on('change', function () {
            var result = Number($('#Residual').val()) + Number($('#paid_up').val());
            if (result != $('.finally_price').val()) {
                alert('يجب ان يكون مجموع المدفوع و المتبقي يساوي السعر ')
                $('#Residual').val('');
            }
            console.log(result);

        });

        /*/GIVE A COST TO ORDER WHEN CHANGE

         $('input[name="totalPriceThop[]"]').on('change', function () {

         totalPriceThop();
         });
         */

        //FUNCTION WILL WORK AFERT PAGE LOAD
        $(window).bind("load", function () {
            totalPriceThop();
        });

        //GIVE A COST AFTER DISCOUNT
        $(window).bind("load", function () {
            $('.finally_price').val($('.cost').val() - $('#discount').val());
        });
        //GIVE A COST AFTER CHINGE DISCOUNT

        $('input[name="discount"]').on('change', function () {

            $('.finally_price').val($('.cost').val() - $('#discount').val());
        });


    </script>
@endsection
