@extends('layouts.app')
@section('content')
    <style>
        #cn{
            background-image:url('{{asset("images/k.png")}}');
            border-radius: 7%;
            margin-bottom: 2%;
        }
        @media print {
            /* style sheet for print goes here */
            body {
                background-image: url('{{asset("images/k.png")}}') !important;
                border-radius: 7%;
                margin-bottom: 2%;
            }
        }
        @print and @page
        #test{
            background-image:url('{{asset("images/k.png")}}');
            border-radius: 7%;
            margin-bottom: 2%;
        }
        

        @page {
            size: A4 landscape;
        }
        @page :left {
            margin-left: 100cm;
        }

        @page :right {
            margin-left: 10cm;

        }



    </style>
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6" >
                <div  id="cn" name="cn" >
                <!-- TO PRINT BILL -->
                <div   id="test" style=" padding: 3%;">
                    <div class="row">
                        <div class="col-xs-12">
                            <hr>
                            <div class="row">
                                <div class="col-xs-12">
                                    <img class="img-responsive" src="{{asset("images/logoman.png")}}" width="20%"
                                         style="margin-right: 35%;">
                                </div>
                            </div>
                            <br><br>
                        </div>
                    </div>
                    <div style="text-align: center">
                        <h3>سند قبض </h3>

                    </div>
                    <div>
                        <p>التاريخ :{{$data->bill->updated_at}}</p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div >
                                <div class="panel-body"
                                     style="background-color: rgba(255, 255, 255, 0.61);">
                                    <p>أستلمنا من السيد / السادة :<b> {{$data->customer->name}} </b> مبلغ وقدرة <b>{{$data->bill->Residual}}</b>
                                    وذلك عن المبلغ المتبقي من الطلب رقم <b>({{$data->id}}) </b> .</p>

                                </div>
                            </div>
                            <center>
                                @if($data->employee->id == 1 )
                                    <p>المحاسب : <b>المدير </b></p>
                                @else
                                    <p> المحاسب :  <b>{{$data->employee->employee_datas->name}}</b></p>
                                @endif
                            </center>

                            <hr>

                            <div style="text-align: center">
                                <p>الرياض - طريق الملك فهد - غرب مكتبة الملك فهد الوطنيه . هاتف : ٠١١٢٠٠٠٥٦٧ جوال : ٠٥٣٠١٠٨٥٤٩</p>
                                <p>Riyadh -King Fahad Road- west of king fahad national library. Tel: 011 2000567 Mobile : 0530108549</p>

                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <button class="btn btn-success btn-block" onclick=printDiv("cn")>طباعة السند</button>
            </div>
        </div>
    </div>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>


@endsection
