@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">إضافة موظف جديد</h2>
                <form action="insertEmployeeData" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label> * الاسم : </label>
                        <input type='text' name='name' class='form-control' value='{{old('name') }}' required>
                    </div>
                    <div class="form-group">
                        <label>* الهاتف : </label>
                        <input type='text' name='phone' class='form-control' value='{{old('phone') }}' required>
                    </div>
                    <div class="form-group">
                        <label> العنوان : </label>
                        <input type='text' name='address' class='form-control' value='{{old('address') }}'>
                    </div>
                    <div class="form-group">
                        <label> ملاحظات : </label>
                        <input type='text' name='note' class='form-control' value='{{old('note') }}'>
                    </div>
                    <div class="form-group">
                        <label>* الوظيفه : </label>
                        <select name="employee_type_id" class='form-control' required>
                            <option value="">--اختر الوظيفة--</option>
                            @foreach($data as $employee_type)
                                <option value="{{$employee_type->id}}">{{$employee_type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>
@endsection