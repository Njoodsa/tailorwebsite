@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title"> بيانات الموظف  </h2>
                <form action="../updateEmployeeData/{{$data->id}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label> * الاسم : </label>
                        <input type='hidden' name='id' value='{{$data->id}}' required>
                        <input type='text' name='name' class='form-control' value='{{$data->name}}' required>
                    </div>
                    <div class="form-group">
                        <label>* الهاتف : </label>
                        <input type='text' name='phone' class='form-control' value='0{{$data->phone}}' required>
                    </div>
                    <div class="form-group">
                        <label> العنوان : </label>
                        <input type='text' name='address' class='form-control'  value='{{$data->address}}'>
                    </div>
                    <div class="form-group">
                        <label> ملاحظات : </label>
                        <input type='text' name='note' class='form-control'   value='{{$data->note}}'>
                    </div>
                    <div class="form-group">
                        <label>* الوظيفه :  </label>
                        <div></div>
                        <select name="employee_type_id" class='form-control'  required>
                            <option value="">--اختر الوظيفة--</option>
                            @foreach($employee_types as $employee_type)
                                <option @if($data->employee_type_id == $employee_type->id) selected @endif value="{{$employee_type->id}}">{{$employee_type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">تعديل</button>
                </form>
            </div>
        </div>
    </div>
@endsection