@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title"> طريقة الدفع   </h2>
                <form action="../updatePaymentMethod/{{$data->id}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label> * الاسم : </label>
                        <input type='hidden' name='id'  value='{{$data->id}}' required>
                        <input type='text' name='name' class='form-control' value='{{$data->name}}' required>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">تعديل</button>
                </form>
            </div>
        </div>
    </div>
@endsection