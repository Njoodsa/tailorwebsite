@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <title>الموظفين </title>
        @include('layouts.saidBar')

        <div class="body-left">
            <!-- make  form center  -->
            <center style="margin: 4%">
                @include('layouts.error')
                <h2 class="main-title">الموظفين</h2>
                <!-- to load form add new tailor -->
                <div class="Form">
                </div>
                <div id="hiden">

                    <form method="post" action="destroy">
                        {{ csrf_field() }}
                        <table class="table table-bordered">
                            <thead class="thead-inverse">
                            <tr>
                                <th></th>
                                <th>الاسم</th>
                                <th>رقم الجوال</th>
                                <th>الايميل</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Employees as $Employee)
                                <tr>
                                    <td><input type="checkbox" value='{{$Employee->id}}' name="delete_Employee[]"></td>
                                    <td> {{$Employee->name}}</td>
                                    <td>{{$Employee->phone_number}}</td>
                                    <td>{{$Employee->user->email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <button class="btn btn-danger" class="btn btn-block" type="submit">حذف</button>
                        <button type="button" class="btn btn-success" id="AddNewEmployee">اضافه</button>
                    </form>
                </div>
            </center>
        </div> <!-- class body left -->
    </div> <!-- class container -->
    @include('layouts.javascript')
@endsection