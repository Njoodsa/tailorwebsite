<title>إضافة موظف </title>
<form action="insertNewEmployee" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label>اسم موظف : </label>
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" name="name" class="form-control" required>
        </div>
        <label>رقم االموظف : </label>
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
            <input type="number" name="phone_number" class="form-control" required>
        </div>
        <label>الايميل : </label>
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
            <input type="email" name="email" class="form-control" required>
        </div>
        <label>كلمة المرور : </label>
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-asterisk"></i></span>
            <input type="password" name="password" class="form-control" required>
        </div>
    </div>
    <button class="btn btn-success btn-block" type="submit ">إضافه</button>
</form>