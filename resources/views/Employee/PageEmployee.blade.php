@extends('layouts.app')
@section('content')
    <div class="container background-container">
        @include('layouts.saidBar')
        <div class="body-left">
            <center style="margin: 4%"> <!-- make  form center  -->
                @include('layouts.error')
                    <h2 class="main-title">بياناتي</h2>
                    <form action="update_employee/{{$Employee->id}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="hidden" name="id_user" value="{{$Employee->user->id}}">
                            <input type="hidden" name="id_number" value="{{$Employee->id}}">
                            <label for="pwd">الاسم</label>
                            <input type="text" class="form-control" value="{{ $Employee->name}}" name="name">
                        </div>
                        <div class="form-group">
                            <label for="pwd">رقم الجوال</label>
                            <input type="number" class="form-control" value="{{ $Employee->phone_number}}"
                                   name="phone_number">
                        </div>
                        <div class="form-group">
                            <label for="usr">الايميل </label>

                            <input type="email" name="email" value="{{$Employee->user->email}}" class="form-control"
                                   required>
                        </div>
                        <div class="form-group">
                            <label for="pwd">كلمة المرور</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <button type="submit" name=" update" class="btn btn-info  btn-md">تعديل</button>
                    </form>
            </center>
        </div> <!-- class body-left -->
    </div> <!-- class container -->
    @include('layouts.javascript')
@endsection