@extends('layouts.app')
@section('content')
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title">طلب قيد العمل  </h2>
                <form action="../check_complete_review/{{$data->id}}" method="post">
                    {{ csrf_field() }}
                    </br>
                    <!-- information user -->
                @include('layouts.helper.information_user')
                <!-- detail order -->
                @foreach($data->order_detail as $item)
                    @include('layouts.helper.detail_order_toprinet') <!-- basic detail  -->
                @endforeach
                    <div class="panel panel-info form-group row info_order ">
                        <br/>
                        <div class="panel-heading">  القياسات</div>
                        <div class="panel-body">
                            <div class="form-group row" id="div_measurement">
                                <p id="date_print"> Start Date : {{$data->bill->created_at}} | due Date : {{$data->bill->end_date}}</p>
                                @include('layouts.helper.measurement_toEdit_order')
                            </div>
                        </div><!--panel-body  -->
                    </div><!--panel info  -->
                <!-- order -->
                    <div class=" form-group row specific_details_item " style="    margin-right: 0%;">
                        <div id='detail' class="panel-heading heading_order panel_detail_order">
                            <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>
                            &nbsp; رؤية الطلب
                             </div> <!-- panel heading  -->
                        <div class="specific_details_body" >
                            <div >
                            @include('layouts.helper.basic_order') <!-- basic information order -->
                            </div>
                            <div class="panel panel-info form-group row info_order ">
                                <br/>
                                <div class="panel-heading"> حالة الطلب</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label> الطلب قيد العمل </label>
                                        </div><!--col-lg-6 -->
                                    </div><!--row -->
                                </div><!--panel-body  -->
                            </div><!--panel info  -->
                        </div><!-- specific_details_body-->
                    </div>
                    <br>
                    <button class="btn btn-success " type="button" onclick=printDiv("cn")>طباعة الفاتوره
                    </button>
                    <button class="btn btn-success "  value = 'complete_order' name="complete_order" type="submit">تم إنجاز الطلب</button>
                    <button class="btn btn-success "  value = 'review_order'   name ='review_order' type="submit">مراجعة الطلب</button>

                </form>
            </div>
        </div>
    </div>
    <!-- TO PRINT BILL -->
    @include('layouts.helper.measurement_toEdie')
    @include('layouts.helper.print_bill_customer')
    @include('layouts.javascript')
@endsection
