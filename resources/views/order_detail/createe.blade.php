@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6 ">
                @include('layouts.error')
                <h2 class="main-title">طلب جديد</h2>
                <form action="../insertOrder/{{$data->id}}" method="post">
                    {{ csrf_field() }}
                    </br>
                    <div class="form-group row">
                        <div class="panel panel-success panel_detail_order">
                            <div class="panel-heading"> الفرع : {{$user->branch->name}} </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label> * الاسم : </label>
                                        <input type='text' name='name' class='form-control'
                                               value="{{$data->name}}" readonly>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>* جوال : </label>
                                        <input type='number' name='phone' class='form-control'
                                               value='{{$data->mobile->phone}}' readonly>
                                    </div>
                                    <div class="col-lg-4">
                                        <label> ملاحظات : </label>
                                        <input type='text' name='name' class='form-control'
                                               value="{{$data->note}}" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="row" style="margin-top: 5% ; text-align: center;">
                                        <div class="col-lg-12">
                                            <input type="radio" class='type' name="type" value="product">&nbsp; منتج
                                            <input type="radio" class='type' name="type" checked value="fabrics"
                                                   style="margin-right: 10%;"> &nbsp;&nbsp;تفصيل
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- add btn -->
                        <div class="fabrics">
                            <button id="add-button" class="btn btn-info btn-xs add_specific_details_box"
                                    type="button">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>

                            <div class="specific_details_block ">
                                <div class="panel panel-info form-group row panel_detail_order specific_details_item">
                                    <div id='detail' class="panel-heading">
                                        <span class="glyphicon glyphicon-menu-down toggle_specific_details"> </span>&nbsp;
                                        تفاصيل الطلب
                                        <input name='number_detail[]' class="input_number" value="1">
                                    </div>

                                    <div class="specific_details_body panel-body check">

                                        <button
                                                class=" deleter_box btn-danger btn-xs delete_specific_details_row"
                                                type="button">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                        <div class="row">
                                            <div class="col-lg-2 col-xs-3">
                                                <div id="titleInOrder">Style</div>
                                                <select name='id_design[]' class="form-control style">
                                                    <option value=''>--select --</option>
                                                    @foreach($designs as $design)
                                                        <option value='{{$design->id}}'>{{$design->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-2 col-xs-3">
                                                <div id="titleInOrder">Filling button</div>
                                                <select name="button_detail[]" class=" form-control">
                                                    <option value="">--chose --</option>
                                                    <option value="Double">Double</option>
                                                    <option selected value="Medium">Medium</option>
                                                    <option value="Soft">Soft</option>
                                                    <option value="Without">Without</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-3 col-xs-3">
                                                <div id="titleInOrder"> Inside pocket</div>
                                                <select name="detail_pocket[]" class=" form-control">
                                                    <option value="">--chose --</option>
                                                    <option value='Right pocket'> Right pocket</option>
                                                    <option value='Left pocket'> Left pocket</option>
                                                    <option selected value='Left and right pocket'>Left and right
                                                        pocket
                                                    </option>
                                                    <option value='Without'>Without</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-3 col-xs-3">
                                                <div id="titleInOrder"> Filling collar</div>
                                                <select name="collar_detail[]" class=" form-control ">
                                                    <option value="">--chose --</option>
                                                    <option value="Double">Double</option>
                                                    <option selected value="medium">Medium</option>
                                                    <option value="Soft">Soft</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-2 col-xs-3">
                                                <div id="titleInOrder">button neck</div>
                                                <select class=" form-control " name="button_neck[]">
                                                    <option value="2SP">2SP</option>
                                                    <option value="2SP w/button">2SP w/button</option>
                                                    <option value="button hole">button hole</option>
                                                </select>
                                            </div>

                                        </div>

                                        <div class="page-header"></div>
                                        <div class=row>

                                        </div>
                                        <div class="page-header"></div>
                                        <div class="row">
                                            <div class="col-lg-2 h col-xs-3">
                                                <div id="titleInOrder">Bar</div>
                                                <select name='id_button[]' class="form-control" id='bar'>
                                                    <option value="" selected>-- chose --</option>
                                                    @foreach($buttons as $Button)
                                                        <option value="{{$Button->id}}">{{$Button->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-2 col-xs-3 h">
                                                <div id="titleInOrder">Pocket
                                                </div>
                                                <select name="id_pockets[]" class=" form-control " id='Pocket'>
                                                    <option value="" selected>-- chose --</option>
                                                    @foreach($pockets as $Pocket)
                                                        <option value="{{$Pocket->id}}">{{$Pocket->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-3 col-xs-3">
                                                <div id="titleInOrder">Sleeves</div>
                                                <select name="id_sleeve[]" class=" form-control">
                                                    <option value="" disabled selected="selected">-- chose --
                                                    </option>
                                                    @foreach($sleeves as $Sleeve)
                                                        <option value="{{$Sleeve->id}}">{{$Sleeve->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-3  col-xs-3">
                                                <div id="titleInOrder">Neck</div>
                                                <select name='id_neck[]' class=" form-control">
                                                    <option value="" disabled selected="selected"> --chose--</option>
                                                    @foreach($necks as $neck)
                                                        <option value="{{$neck->id}}">{{$neck->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-2 col-xs-3">
                                                <div id="titleInOrder">Fabric</div>
                                                <select class=" form-control" name="id_fabric_to_order[]">
                                                    <option value="" selected>--chose --</option>

                                                    @foreach($fabrics as $Fabric)
                                                        @if($Fabric->current_balance > $Fabric->demand_limit)
                                                            <option value="{{$Fabric->fabrics_id}}">
                                                                {{$Fabric->fabrics->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info form-group row panel_detail_order">
                        <div class="panel-heading"><p> الطلب @if( $n = $discount->state == 1)/ نسبة الخصم المتاحة
                               {{$percentage = $discount->percentage}}%</p>
                            @endif</div>
                        <div class="panel-body ">
                            <div class="row ">
                                <div class="col-lg-12 table-responsive">
                                    <button class="btn btn-success btn-xs add_new_fabric_row" id="add_new"
                                            type="button"><span class="glyphicon glyphicon-plus"></span></button>

                                    <table class="table">
                                        <thead class="trToFabircs">
                                        <tr>
                                            <th> *</th>
                                            <th> اسم القماش</th>
                                            <th>النوع</th>
                                            <th>إضافات</th>
                                            <th> عدد الياردات</th>
                                            <th> العدد</th>
                                            <th> سعر القطعة</th>
                                            <th> سعر الإضافة</th>
                                            <th>الإجمالي</th>
                                        </tr>
                                        </thead>
                                        <thead class="trToProduct" hidden>
                                        <tr>
                                            <th>*</th>
                                            <th> اسم المنتج</th>
                                            <th>العدد</th>
                                            <th> سعر المنتج</th>
                                            <th>الإجمالي</th>
                                        </tr>
                                        </thead>
                                        <tbody id="fabric_list">
                                        <!-- the default one -->
                                        <tr class="fabric_item">
                                            <td style="width: 4px;"><input name='number_column[]' value="1"
                                                                           class="number_column" readonly></td>
                                            <td>
                                                <select class="id_fabric " data-live-search="true"
                                                        data-show-subtext="true"
                                                        name="id_fabric[]" required>
                                                    <option selected="selected" disabled>select</option>
                                                    @foreach($fabrics as $Fabric)
                                                        @if($Fabric->current_balance > $Fabric->demand_limit)
                                                            <option value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="h">
                                                <select class="type_design" data-live-search="true"
                                                        data-show-subtext="true"
                                                        name="type_design[]" required>
                                                    <option value="" selected="selected">select</option>
                                                    <option value="Thop">Thop</option>
                                                    <option value="VEST">VEST</option>
                                                    <option value="Pants">Pants</option>
                                                    <option value="Sroal">Sroal</option>

                                                </select>
                                            </td>
                                            <td class="h">

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <select id=""
                                                                class=" js-example-basic-multiple form-control addition_id"
                                                                name="1_addition_id[]" multiple="multiple">
                                                            @foreach($additions as $Addition)
                                                                <option class='here'
                                                                        value="{{$Addition->id}}">{{$Addition->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h">
                                                <input class="form-control yards" name="yards[]" step="0.01"
                                                       type='number' disabled>
                                            </td>
                                            <td>
                                                <input class="form-control thop_count" name="number[]" type="text"
                                                       value="1" required>
                                            </td>
                                            <td>
                                                <input class="form-control thop_price" name="price[]" type="text"
                                                       value="" required readonly>
                                            </td>
                                            <td class="h ">
                                                <input class="form-control price_addition" name="price_addition[]"
                                                       type="text"
                                                       value="0" required disabled>
                                            </td>
                                            <td>
                                                <input class="form-control totalPriceThop" name="totalPriceThop[]"
                                                       type="text" value="" readonly>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-xs delete_fabric_row" type="button">
                                                    <span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-12">

                                    <hr>

                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-3">
                                            العدد : <input class="form-control" id="total_thop_count" readonly>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-3">
                                            السعر : <input class="form-control" id="total_thop_cost"
                                                           name="total_thop_cost" readonly>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-3">

                                            الخصم : <input @if($n == 0) readonly @endif class="form-control"
                                                           id="discount" name="discount" value="0">
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-3">
                                            الضريبة : <input class="form-control" id="tax" readonly>
                                        </div>

                                        <div class="col-xs-6 col-sm-3 col-lg-3">
                                            الاجمالي : <input class="form-control" id="finally_price"
                                                              name="finally_price"
                                                              readonly>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class=" col-lg-5">
                                            <label>الاجمالي يشمل ضريبة القيمة المضافة (5%) </label>
                                        </div>
                                        <div class=" col-lg-4">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="panel panel-info form-group row panel_detail_order">
                         <br/>
                         <div class="panel-heading"> تفاصيل</div>
                         <div class="panel-body">
                             <div class="row">
                                 <div id="titleInOrder">الموظفين :</div>
                                 <div class="col-lg-4">
                                     <label>الخياط </label>
                                     <select name="id_tailor" class="selectpicker" data-show-subtext="true"
                                             data-live-search="true" required>
                                         <option value="">اختر</option>

                                             <option value=""></option>

                                     </select>
                                 </div>
                                 <div class="col-lg-4">
                                     <label>اخذ المقاسات </label>
                                     <select name="Taking_measurements" class="selectpicker" data-show-subtext="true"
                                             data-live-search="true" required>
                                         <option value="">اختر</option>

                                             <option value=""></option>

                                     </select>
                                 </div>
                             </div>
                         </div>
                     </div>-->
                    <div class="panel panel-info form-group row panel_detail_order">
                        <div class="panel-heading"> الدفع</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-3 col-xs-6">
                                    <label> طريقة الدفع </label>
                                    <select name="payment_method_id" class="form-control payment_method" required>
                                        <option value="">اختر</option>
                                        @foreach($payment_methods as $Payment_method)
                                            <option value="{{$Payment_method->id}}">{{$Payment_method->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <div class="form-group{{ $errors->has('paid_up') ? ' has-error' : '' }}">
                                        <label>* المدفوع : </label>
                                        <input type='number' step="0.01" id="paid_up" name='paid_up'
                                               class='form-control'
                                               value='{{old('paid_up') }}'
                                               required>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-6 paid_up_card" hidden>
                                    <div class="form-group">
                                        <label>* المدفوع / بطاقة : </label>
                                        <input type='number' step="0.01" id="paid_up_card" name='paid_up_card'
                                               class='form-control'
                                        >
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <div class="form-group{{ $errors->has('Residual') ? ' has-error' : '' }}">

                                        <label>* المتبقي : </label>
                                        <input type='number' step="0.01" id="Residual" name='Residual'
                                               class='form-control'
                                               value='{{old('Residual') }}'
                                               required>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <label>* تاريخ التسليم : </label>
                                    <input type='date' required class="form-control" value="{{$date_deliver }}"
                                           name="end_date">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success btn-block" type="submit">إضافه</button>
            </div>
            </form>
        </div>
    </div>
    </div>
    <input name="state_percentage" value="{{$n}}" id="state_percentage" type="hidden">
                         @if($n != 0 )
                              <input type="hidden" value="{{$percentage}}" id="percentage" name="percentage">
                              @endif
    @include('layouts.javascript')
@endsection
