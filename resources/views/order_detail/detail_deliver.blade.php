@extends('layouts.app')
@section('content')
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">طلب مستلم  </h2>

                    {{ csrf_field() }}
                    </br>
                    <!-- information user -->
                @include('layouts.helper.information_user')
                <!-- detail order -->
                @foreach($data->order_detail as $item)
                    @include('layouts.helper.detail_order_toprinet') <!-- basic detail  -->
                @endforeach
                <div class="panel panel-info form-group row info_order ">
                    <br/>
                    <div class="panel-heading">  القياسات</div>
                    <div class="panel-body">
                        <div class="form-group row" id="div_measurement">
                            <p id="date_print"> Start Date : {{$data->bill->created_at}} | due Date : {{$data->bill->end_date}}</p>
                            @include('layouts.helper.measurement_toEdit_order')
                        </div>
                    </div><!--panel-body  -->
                </div><!--panel info  -->

                <!-- order -->
                    <div class=" form-group row specific_details_item  " style="margin-right: -1%">
                        <div id='detail' class="panel-heading heading_order panel_detail_order ">
                            <span class="glyphicon glyphicon-menu-down toggle_specific_details "></span>
                            &nbsp; رؤية الطلب
                        </div> <!-- panel heading  -->
                        <div class="specific_details_body" >
                            @include('layouts.helper.basic_order') <!-- basic information order -->
                        </div>
                            <div class="panel panel-info form-group row info_order ">
                                <br/>
                                <div class="panel-heading"> حالة الطلب</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label>تم تسليم الطلب</label>
                                        </div><!--col-lg-6 -->
                                    </div><!--row -->
                                </div><!--panel-body  -->
                            </div><!--panel info  -->
                        </div><!-- specific_details_body-->
                    </div>
            </div>
        </div>
    </div>
    @include('layouts.helper.measurement_toEdie')
    @include('layouts.javascript')
@endsection
