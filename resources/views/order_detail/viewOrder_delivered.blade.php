@extends('layouts.app')
@section('content')
<style>
    #search_order{
        display:none;
    }
</style>
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')

            </div>
            <div class="col-md-6 " >
                <h2 class="main-title"> الطلبات المسلمه</h2>
                <div class="row">
                    @include('layouts.error')
                            <div class="pull-right" style=" margin-top: 3%;margin-bottom: 2%">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>

                                @if(Request::path() == 'search_order')
                                <a href="{{url('order_delivered')}}" class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-back"></span> عودة
                                </a>
                                    @endif
                            </div>
                            <div class="pull-left" id="search_order">
                            <form class="navbar-form" role="search" method="post" action="search_order">
                                {{csrf_field() }}
                                <div class="input-group add-on">
                                    <input class="form-control" name="search" type="text" required>
                                    <input class="form-control" name="state"  type="hidden"  value="6"  >
                                    <div class="input-group-btn">
                                        <button class="btn btn-info" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                            </form>
                            </div>

                        <br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th> رقم الطلب </th>
                                <th>اسم العميل </th>
                                <th>رقم الجوال</th>
                                <th class="hide_to_phone"> عدد الثياب  </th>
                                <th>تاريخ الطلب  </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($Order_details as $Order_detail)
                                <tr>
                                    <td>
                                        <a  href="detailDelivered/{{$Order_detail->id}}">
                                            {{$Order_detail->id}} </a>
                                       </td>
                                    <td>{{$Order_detail->customer->name}}</td>
                                    <td>0{{ $Order_detail->customer->mobile->phone}}</td>
                                    <td class="hide_to_phone">
                                        <?php $n = 0 ?>
                                        @foreach($Order_detail->bill->order as $order)
                                            <input type="hidden" value=" {{$n+=$order->number}}">
                                        @endforeach
                                        <p class="f_number ">{{$n}}</p>
                                    </td>
                                    <td>{{$Order_detail->created_at}}</td>
                                    <td>
                                        <a class="btn btn-success btn-xs" href="billCompleted/{{$Order_detail->id}}">
                                          سند القبض </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
          {{ $Order_details->links() }}
        </div>
    </div>
<script>
    $('.pull-right').click(function () {
            $('#search_order').toggle( "slow")
    })
</script>
    @include('layouts.javascript')
@endsection

