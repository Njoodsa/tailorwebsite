@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6 ">
                <h2 class="main-title">الطلبات المعلقة ( إنتظار المراجعة )  </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                            </div>
                        </div>
                        <br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder='رقم الطلب ' disabled></th>
                                <th><input type="text" class="form-control" placeholder='اسم العميل ' disabled></th>
                                <th><input type="text" class="form-control" placeholder='رقم الجوال' disabled></th>
                                <th class="hide_to_phone"><input type="text" class="form-control" placeholder=' عدد الثياب  ' disabled></th>
                                <th><input type="text" class="form-control" placeholder="تاريخ الطلب " disabled></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($Order_details as $Order_detail)
                                <tr>
                                    <td>{{$Order_detail->id}}</td>
                                    <td>{{$Order_detail->customer->name}}</td>
                                    <td>0{{ $Order_detail->customer->mobile->phone}}</td>
                                    <td class="hide_to_phone">
                                        @if($Order_detail->bill->order ?? null)
                                            <p class="f_number "> {{$Order_detail->bill->order->sum('number') }}</p>
                                            @endif
                                    </td>
                                    <td>{{$Order_detail->bill->created_at ?? 'error'}} </td>
                                    <td>
                        <a class="btn btn-info btn-xs" href="editOrder/{{$Order_detail->id}}"><span
                                    class=" glyphicon glyphicon-fullscreen "></span></a>
                        </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{ $Order_details->links() }}
        </div>
    </div>

    @include('layouts.javascript')
@endsection

