@extends('layouts.app')
@section('content')
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">طلب غير مكتمل </h2>
                <form action="../updateOrderProduct/{{$data->id}}" method="post">
                    {{ csrf_field() }}
                    </br>
                    <div>
                        @include('layouts.helper.information_user')
                    </div>
                    <!-- order -->
                    <div class=" form-group row specific_details_item " style="margin-right: -1%;">
                        <div id='detail' class="panel-heading heading_order panel_detail_order">
                            <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>
                            &nbsp; رؤية الطلب
                            @if( $n = $discount->state == 1)/نسبة الخصم المتاحة
                            {{$percentage = $discount->percentage}}%</p>
                            @endif
                        </div> <!-- panel heading  -->
                        <div class="specific_details_body">

                            <div class="panel panel-info form-group row panel_detail_order">
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-lg-12 table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>*</th>
                                                    <th> اسم المنتج</th>
                                                    <th>العدد</th>
                                                    <th> سعر المنتج</th>
                                                    <th>الإجمالي</th>
                                                </tr>
                                                </thead>
                                                <tbody id="fabric_list">
                                                <!-- the default one -->

                                                @foreach($data->bill->order as $order)

                                                    <input type="hidden" name="order_id[]" value="{{$order->id}}">
                                                    <tr class="fabric_item">
                                                        <td style="width: 4px;"><input name='number_column[]'
                                                                                       value="{{$order->number_column}}"
                                                                                       class="number_column" readonly>
                                                        </td>
                                                        <td>
                                                            <select name="id_fabric[]" class="id_fabric_product">
                                                                @foreach($products as $product)
                                                                    <option @if($order->id_fabric ==$product->products->id ) selected
                                                                            @endif
                                                                            value="{{$product->products->id}}">{{$product->products->name}}
                                                                    </option>

                                                                @endforeach
                                                            </select>
                                                        </td>

                                                        <td>
                                                            <input value='{{$order->number}}' name="number[]"
                                                                   type="text"
                                                                   class=" product_count GivePrice form-control">
                                                        </td>
                                                        <td>
                                                            <input name="price[]" value='{{$order->price}}' id="price"
                                                                   type="text" class="product_price form-control">
                                                        </td>
                                                        <td><input class=" form-control totalPriceProduct"
                                                                   name='totalPriceThop[]'
                                                                   value='{{$order->price*$order->number}}' type="text"
                                                                   readonly></td>
                                                    </tr>

                                                @endforeach

                                                </tbody>
                                            </table>
                                            <div class="col-lg-3 col-xs-6">
                                                العدد :
                                                <input class="total_number" id="total_product_count">
                                            </div>
                                            <div class="col-lg-3 col-xs-6">
                                                الاجمالي : <input class="cost" name="total_thop_cost"
                                                                  value="{{$data->bill->price}}"
                                                                  id="total_product_cost">
                                            </div>
                                            <div class="col-lg-3 col-xs-6">
                                                الضريبة : <input class="form-control" id="tax"
                                                                 value="{{floor($data->bill->price *0.05)}}" readonly>

                                            </div>
                                            <div class="col-lg-3 col-xs-6">
                                                الخصم : <input @if($n == 0) readonly
                                                               @endif  value='{{$data->bill->discount}}' name="discount"
                                                               id='discount'>
                                            </div>
                                            <div class="col-lg-3 col-xs-6">
                                                السعر : <input name="finally_price" id="finally_price" value=""
                                                               readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class=" col-lg-5">
                                            <label>الاجمالي يشمل ضريبة القيمة المضافة (5%) </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- BILL -->

                            <div class="panel panel-info form-group row panel_detail_order">
                                <div class="panel-heading"> الدفع</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-3 col-xs-6">
                                            <label> طريقة الدفع </label>
                                            <select name="payment_method_id" class="form-control" required>
                                                @foreach($Payment_methods as $Payment_method)
                                                    <option @if($data->bill->payment_method_id ==$Payment_method->id)
                                                            selected
                                                            @endif value="{{$Payment_method->id}}">{{$Payment_method->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-xs-6">
                                            <label>* المدفوع : </label>
                                            <input type='number' name='paid_up' id="paid_up" class='form-control '
                                                   step="0.01" value='{{$data->bill->paid_up}}'
                                                   required>
                                        </div>
                                        <div class="col-lg-3 col-xs-6 paid_up_card"
                                             @if($data->bill->payment_method_id != 3) hidden @endif>
                                            <div class="form-group">
                                                <label>* المدفوع / بطاقة : </label>
                                                <input type='number' step="0.01" id="paid_up_card" name='paid_up_card'
                                                       class='form-control'
                                                       value='{{$data->bill->paid_up_card}}'>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <label>* المتبقي : </label>
                                            <input type='number' id="Residual" name='Residual' class='form-control '
                                                   step="0.01" value='{{$data->bill->Residual}}'
                                                   required>
                                        </div>
                                        <div class="col-lg-3 col-xs-6">
                                            <label>تاريخ التسليم </label>
                                            <input type='date' value="{{$data->bill->end_date}}" required
                                                   class="form-control"
                                                   name="end_date">
                                        </div>
                                    </div>
                                    <input type="hidden" name='bill_id' value="{{$data->bill->id }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info form-group row info_order ">
                        <br/>
                        <div class="panel-heading"> حالة الطلب</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>غير مكتمل </label>
                                </div><!--col-lg-6 -->
                            </div><!--row -->
                        </div><!--panel-body  -->
                    </div><!--panel info  -->
                    <button class="btn btn-success" name='type' value='done_review' type="submit">تعديل</button>
                    <button class="btn btn-danger " name='type' value="cancel" type="submit">حذف الطلب</button>
                    <button class="btn btn-success " type="button" onclick=printDiv("cn")>طباعة الفاتوره
                    </button>
                </form>

            </div><!-- specific_details_body-->
        </div>
    </div>
    <input name="state_percentage" value="{{$n}}" id="state_percentage" type="hidden">
                                @if($n != 0 )
                                        <input type="hidden" value="{{$percentage}}" id="percentage" name="percentage">
                                    @endif
    @include('layouts.helper.print_bill_customer_product')
    <!-- TO PRINT BILL -->


    <script>
        function test(divName, n) {

            var div_1 = document.getElementById('div_1');
            var div_2 = document.getElementById('div_2');
            var div_3 = document.getElementById(n);
            var div_4 = document.getElementById(divName);
            var combined = document.createElement('div');
            var printContents = combined.innerHTML = div_1.innerHTML + " " + div_2.innerHTML + " " +
                div_3.innerHTML + " " + div_4.innerHTML; // a little spacing
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }


        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
        $(window).bind("load", function () {
            updateNumbers();
        });


    </script>
    @include('layouts.javascript')
@endsection
