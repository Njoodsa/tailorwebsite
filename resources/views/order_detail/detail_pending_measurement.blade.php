@extends('layouts.app')
@section('content')

    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">طلب معلق( ادخل القياسات وعدد الياردات )  </h2>
                <form action="../AddMeasurement/{{$data->id}}" method="post">
                    {{ csrf_field() }}
                    </br>
                    <!-- information user -->
                @include('layouts.helper.information_user')
                <!-- detail order -->
                @foreach($data->order_detail as $item)
                    @include('layouts.helper.detail_order_toprinet')<!-- basic detail  -->

                        <div class="panel panel-info form-group panel_detail_order row ">
                            <!-- add measurement to order -->
                            <div id='detail' class="panel-heading">القياسات</div>
                            <div class="specific_details_body panel-body">
                                <div class="row">
                                    <div id="target">
                                    @foreach($data->customer->measurement as $measurement)
                                        <div class="col-lg-12">
                                            <input type="radio" name="id_measurement" value="{{$measurement->id}}"
                                            required>&nbsp; قياسات بتاريخ
                                            <b>{{$measurement->created_at}} / </b> للقصاص
                                            <b>{{$measurement->cutters->name}}</b>
                                            <br/> <br/>
                                                <table class="table-bordered" style="font-size: 12px; ">
                                                    <tr align="left">
                                                        <td   class="col-lg-2 col-xs-2">FORNT LONGER
                                                            :{{ $measurement->FrontLonger}}</td>
                                                        <td class="col-lg-2 col-xs-2">LENGTH
                                                            :{{ $measurement->Length}} </td>
                                                        <td colspan="2" class="col-lg-2 col-xs-2">COLLAR SADA
                                                            :{{ $measurement->CollarSada}}</td>
                                                        <td colspan="2" class="col-lg-2 col-xs-2">COLLAR GALAB
                                                            :{{$measurement->CollarGalab}} </td>
                                                        <td  class="col-lg-2 col-xs-2">SNPIN
                                                            : {{$measurement->SnpIN}}</td>



                                                    </tr>
                                                    <tr align="left">
                                                        <td class="col-lg-2 col-xs-2">L :{{ $measurement->ShlL}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">R :{{ $measurement->ShlR}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">SHL :{{ $measurement->Shl}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">Armhole :{{$measurement->Armhole}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">Front chest: {{$measurement->FrontChest}}</td>
                                                        <td class="col-lg-2 col-xs-2"> CHEST :{{$measurement->Chest}}
                                                        </td>
                                                        <td colspan="3" class="col-lg-2 col-xs-2">HIPS: {{ $measurement->HIPS}}
                                                        </td>

                                                    </tr>
                                                    <tr align="left">
                                                        <td class="col-lg-2 col-xs-2">CUFFS :{{$measurement->Cuffs}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">ELBOW : {{ $measurement->ElbowCuffs}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">SLV W /CUFFS : {{ $measurement->Slv_w}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">BACK :{{ $measurement->Back}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">MIDDLE :{{$measurement->MIDDLE}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">BOTTOM : {{$measurement->Bottom}}
                                                        </td>
                                                        <td colspan="2" class="col-lg-2 col-xs-2">WAIST :{{ $measurement->Waist}}
                                                        </td>


                                                    </tr>
                                                    <tr align="left">
                                                        <td class="col-lg-2 col-xs-2">SADA : {{ $measurement->Sada}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">ELBOW : {{ $measurement->ElbowSlvSada}}
                                                        </td>
                                                        <td  class="col-lg-2 col-xs-2">SLV SADA : {{$measurement->SlvSada}}
                                                        </td>
                                                        <td class="col-lg-2 col-xs-2">SIDE :{{ $measurement->SIDE}}</td>
                                                        <td class="col-lg-2 col-xs-2">BAR : {{ $measurement->Bar}} </td>
                                                        <td  class="col-lg-2 col-xs-2">FAKE BUTTON
                                                            :{{ $measurement->FakeButton}} </td>
                                                        <td  class="col-lg-2 col-xs-2">note
                                                            :{{ $measurement->note}} </td>
                                                    </tr>
                                                </table>
                                        </div><!-- col-lg-6 -->
                                    @endforeach
                                    </div>

                                </div><!-- row -->
                            </div><!-- panel body  -->
                        </div><!-- panel info  -->
                @endforeach
                <!-- order -->
                    <div class=" form-group row specific_details_item" style="margin-right: -1%;">
                        <div id='detail' class="panel-heading heading_order panel_detail_order">
                            <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>
                            &nbsp; رؤية الطلب
                        </div> <!-- panel heading  -->
                        <div class="specific_details_body" >
                        @include('layouts.helper.basic_order')<!-- basic information order -->
                        </div>
                    </div>
                    <div class="panel panel-info form-group row info_order ">
                        <div class="panel-heading"> حالة الطلب</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>معلق / في إنتظار تحديد القياسات</label>
                                </div><!--col-lg-6 -->
                            </div><!--row -->
                        </div><!--panel-body  -->
                    </div><!--panel info  -->
                    <button class="btn btn-success btn-block" type="button" onclick=printDiv("cn")>طباعة الفاتوره
                    </button>
                    <button class="btn btn-success btn-block" type="submit">إضافة القياسات</button>
                </form>
            </div><!-- specific_details_body-->
    </div>
    </div>
    </div>
    </div>
    <!-- TO PRINT BILL -->
    @include('layouts.helper.print_bill_customer')
    <script type="text/javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
        // $(".my_selection").change(function (e)  {
        //    e.preventDefault();
        // window.open( (this.children[this.selectedIndex].getAttribute('href')), "mywindow","location=1,status=1,scrollbars=1, width=600,height=600");
         var target = $('#target');
        $("input[type='radio']").on('change', function (event) {
            var value = $(this).val();
            var row = $(this).closest('#target')
           console.log(row)
            row.empty();
            event.preventDefault();
            row.load('../GetMeasurement/' +value);
        });
        // })


    </script>
    @include('layouts.javascript')
@endsection