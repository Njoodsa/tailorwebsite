@extends('layouts.app')
@section('content')
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">طلب مستلم  </h2>

                {{ csrf_field() }}
                </br>
                <!-- information user -->
            @include('layouts.helper.information_user')
            <!-- detail order -->
            <!-- order -->
                <div class=" form-group row specific_details_item  " style="margin-right: -1%">
                    <div id='detail' class="panel-heading heading_order panel_detail_order ">
                        <span class="glyphicon glyphicon-menu-down toggle_specific_details "></span>
                        &nbsp; رؤية الطلب
                    </div> <!-- panel heading  -->
                    <div class="specific_details_body read_only" hidden>
                    @include('layouts.helper.bascic_order_product') <!-- basic information order -->
                    </div>
                    <div class="panel panel-info form-group row info_order ">
                        <br/>
                        <div class="panel-heading"> حالة الطلب</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>تم تسليم الطلب</label>
                                </div><!--col-lg-6 -->
                            </div><!--row -->
                        </div><!--panel-body  -->
                    </div><!--panel info  -->
                </div><!-- specific_details_body-->
                <button class="btn btn-success " type="button" onclick=printDiv("cn")>طباعة الفاتوره
                </button>
            </div>
        </div>
    </div>

    @include('layouts.helper.print_bill_customer_product')
    @include('layouts.javascript')
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
@endsection