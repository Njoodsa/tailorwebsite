@extends('layouts.app')
@section('content')
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">طلب معلق بإنتظار المراجعة </h2>
                <form action="../updateOrder/{{$data->id}}" method="post">
                    {{ csrf_field() }}
                    </br>
                    <div>
                        @include('layouts.helper.information_user')
                    </div>
                    <!-- add btn -->
                    <div class="fabrics">
                        <button id="add-button" class="btn btn-info btn-xs add_specific_details_box"
                                type="button">
                            <span class="glyphicon glyphicon-plus"></span>
                        </button>
                        <div class="specific_details_block ">
                            @foreach($data->order_detail as $item)
                                <div class="panel  form-group row   panel_detail_order specific_details_item  ">
                                    <input type="hidden" value="{{$item->id}}" name="id_detailOrder[]"
                                           class="id_detailOrder">
                                    <div id='detail' class="panel-heading heading_order">

                                        <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>&nbsp;
                                        تفاصيل الطلب
                                        <input name='number_detail[]' class="input_number"
                                               value="{{$item->number_column}}">
                                    </div>
                                    <div class="panel-body specific_details_body">
                                        <div class="check">
                                            <div>
                                                <button
                                                        class=" deleter_box btn-danger btn-xs delete_specific_details_row"
                                                        type="button">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </button>
                                                <button class="btn btn-info btn-xs btn-print"
                                                        style="    margin-bottom: 6%;" type="button"
                                                        onclick="test('orders','div_measurement')">
                                                    طباعةالطلب
                                                </button>

                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2 col-xs-3">
                                                    <div id="titleInOrder"> Style</div>
                                                    <select name='id_design[]' class=" form-control style" required>
                                                        <option value=''>--select --</option>
                                                        @foreach($Designs as $design)
                                                            <option @if($item->id_design ==$design->id ) selected
                                                                    @endif value='{{$design->id}}'>{{$design->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-2 col-xs-3">
                                                    <div id="titleInOrder">Neck</div>
                                                    <select name='id_neck[]' class=" form-control" required>
                                                        @foreach($Necks as $neck)
                                                            <option @if($item->id_neck == $neck->id ) selected
                                                                    @endif  value=" {{$neck->id}}">{{$neck->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-2 col-xs-3">
                                                    <div id="titleInOrder"> Sleeves</div>
                                                    <select name="id_sleeve[]" class=" form-control" required>
                                                        @foreach($Sleeves as $Sleeve)
                                                            <option @if($item->id_sleeve == $Sleeve->id )selected
                                                                    @endif value="{{$Sleeve->id}}">{{$Sleeve->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-xs-3">
                                                    <div id="titleInOrder">button neck</div>
                                                    <select class=" form-control" required name="button_neck[]">
                                                        <option @if($item->button_neck == '2SP')selected @endif
                                                        value="2SP">2SP
                                                        </option>
                                                        <option @if($item->button_neck == '2SP w/button')selected @endif
                                                        value="2SP w/button">2SP w/button
                                                        </option>
                                                        <option @if($item->button_neck == 'button hole')selected @endif
                                                        value="button hole">button hole
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3  col-xs-3">
                                                    <div id="titleInOrder"> Filling collar</div>
                                                    <select name="collar_detail[] " class=" form-control" required>
                                                        <option value="">--chose --</option>
                                                        <option @if($item->collar_detail == 'Double')selected
                                                                @endif  value="Double">
                                                            Doubledd
                                                        </option>
                                                        <option @if($item->collar_detail =='medium')selected
                                                                @endif value="medium">Medium
                                                        </option>
                                                        <option @if($item->collar_detail =='Soft' )selected
                                                                @endif value="Soft">
                                                            Soft
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="page-header"></div>
                                            <div class=row>

                                                <div class="col-lg-2  col-xs-3">
                                                    <div id="titleInOrder"> Filling button</div>
                                                    <select name="button_detail[]" class=" form-control" required>
                                                        <option value="">--chose --</option>
                                                        <option @if($item->button_detail == 'Double')selected
                                                                @endif   value="Double">
                                                            Double
                                                        </option>
                                                        <option @if($item->button_detail =='Medium' ) selected
                                                                @endif  value="Medium">
                                                            Medium
                                                        </option>
                                                        <option @if($item->button_detail =='Soft')selected
                                                                @endif  value="Soft">
                                                            Soft
                                                        </option>
                                                        <option @if($item->button_detail == "Without" )selected
                                                                @endif  value="Without">
                                                            Without
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3  col-xs-3">
                                                    <div id="titleInOrder"> Inside pocket</div>
                                                    <select name="detail_pocket[]" class=" form-control" required>
                                                        <option value="">--chose --</option>
                                                        <option @if($item->detail_pocket =='Right pocket' )selected
                                                                @endif  value='Right pocket'>Right pocket
                                                        </option>
                                                        <option @if($item->detail_pocket =='Left pocket' )selected
                                                                @endif value='Left pocket'>Left pocket
                                                        </option>
                                                        <option @if($item->detail_pocket =='Left and right pocket' )selected
                                                                @endif value='Left and right pocket'>Left and right
                                                            pocket
                                                        </option>
                                                        <option @if($item->detail_pocket =='Without')selected
                                                                @endif value='Without'>Without
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="col-lg-2 h  col-xs-3">
                                                    <div id="titleInOrder">Bar</div>
                                                    <select name='id_button[]' class=" form-control" id="bar" required>
                                                        <option value="">-- chose--</option>
                                                        @foreach($Buttons as $Button)
                                                            <option

                                                                    @if($item->design->has_bar_pocket == 0)
                                                                    @if($item->id_button== $Button->id ) selected
                                                                    @endif @endif
                                                                    value="{{$Button->id}}">{{$Button->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-2 h  col-xs-3">
                                                    <div id="titleInOrder">Pocket</div>
                                                    <select name="id_pockets[]" class=" form-control" id="Pocket"
                                                            required>
                                                        <option value="">-- chose --</option>
                                                        @foreach($Pockets as $Pocket)
                                                            <option
                                                                    @if($item->design->has_bar_pocket == 0)
                                                                    @if($item->id_pockets== $Pocket->id ) selected

                                                                    @endif
                                                                    @endif
                                                                    value="{{$Pocket->id}}">{{$Pocket->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @if($item->id_fabric != null)
                                                    <div class="col-lg-3  col-xs-3">
                                                        <div id="titleInOrder"> Fabric</div>
                                                        <select data-live-search="true" class=" form-control"
                                                                data-show-subtext="true"
                                                                name="id_fabric_to_order[]">
                                                            @foreach($Fabrics as $Fabric)
                                                                @if($Fabric->current_balance < $Fabric->demand_limit)
                                                                    <option @if($item->id_fabric ==$Fabric->fabrics->id ) selected
                                                                            @endif
                                                                            value="{{$Fabric->fabrics_id}}"
                                                                            readonly>{{$Fabric->fabrics->name}}
                                                                        -نفذ من المخزون -
                                                                    </option>
                                                                @elseif($Fabric->current_balance ==  $Fabric->demand_limit)
                                                                    <option @if($item->id_fabric ==$Fabric->fabrics->id ) selected
                                                                            @endif
                                                                            value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}
                                                                        -{{$Fabric->current_balance}} ياردة متبقى فقط -
                                                                    </option>
                                                                @else
                                                                    <option @if($item->id_fabric ==$Fabric->fabrics->id ) selected
                                                                            @endif
                                                                            value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}</option>

                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                @else
                                                    <div class="col-lg-3  col-xs-3">
                                                        <div id="titleInOrder"> Fabric</div>
                                                        <select class=" form-control" name="id_fabric_to_order[]">
                                                            <option value="" selected>--chose --</option>

                                                            @foreach($Fabrics as $Fabric)
                                                                @if($Fabric->current_balance < $Fabric->demand_limit)
                                                                    <option value="{{$Fabric->fabrics_id}}"
                                                                            disabled>
                                                                        {{$Fabric->fabrics->name}}
                                                                        -نفذ من المخزون -
                                                                    </option>
                                                                @elseif($Fabric->current_balance == $Fabric->demand_limit)
                                                                    <option value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}
                                                                        -٥ ياردة متبقى فقط -
                                                                    </option>
                                                                @else
                                                                    <option value="{{$Fabric->fabrics_id}}">
                                                                        {{$Fabric->fabrics->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        </select>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="panel panel-info form-group row info_order ">
                        <br/>
                        <div class="panel-heading"> القياسات</div>
                        <div class="panel-body">
                            <div class="form-group row" id="div_measurement">
                                <p id="date_print"> Start Date : {{$data->bill->created_at}} | due Date
                                    : {{$data->bill->end_date}}</p>
                                @include('layouts.helper.measurement_toEdit_order')
                            </div>
                        </div><!--panel-body  -->
                    </div><!--panel info  -->
                    <!-- <div class="panel  form-group row specific_details_item">

                         <div id='detail' class="panel-heading heading_order">

                             <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>&nbsp;
                             تفاصيل الطلب
                         </div>
                         <div class="panel-body specific_details_body" hidden>
                     <div class="row">
                         <div id="titleInOrder">الموظفين :</div>

                         <div class="col-lg-6">
                             <label>الخياط </label>
                             <select name="id_tailor" class="selectpicker" data-show-subtext="true"
                                     data-live-search="true" required>
                                 <option value="">اختر</option>

                                     <optionselected
                                     value=""></option>

                             </select>
                         </div>
                         <div class="col-lg-6">
                             <label>اخذ المقاسات </label>
                             <select name="Taking_measurements" class="selectpicker" data-show-subtext="true"
                                     data-live-search="true" required>
                                 <option value="">اختر</option>

                                     <option selected
                                     value=""></option>

                             </select>
                         </div>
                     </div>

                         </div>
                     </div>-->

                    <!-- order -->
                    <div class=" form-group row  " style="margin-right: -1%;">
                        <div id='detail' class="panel-heading heading_order panel_detail_order">
                            <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>
                            &nbsp; رؤية الطلب
                            @if( $n = $discount->state == 1)/ نسبة الخصم المتاحة
                            {{$percentage = $discount->percentage}}%</p>
                            @endif
                        </div> <!-- panel heading  -->
                        <div class="specific_details_body">

                            <div class="panel panel-info form-group row panel_detail_order">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12 table-responsive">
                                            <button class="btn btn-success btn-xs add_new_fabric_row" id="add_new"
                                                    type="button"><span class="glyphicon glyphicon-plus"></span>
                                            </button>

                                            <table class="table">
                                                <thead>
                                                <th>*</th>
                                                <th> اسم القماش</th>
                                                <td>النوع</td>
                                                <th>إضافات</th>
                                                <th>عدد الياردات</th>
                                                <th>العدد</th>
                                                <th>سعر القطعة</th>
                                                <th>سعر الإضافة</th>
                                                <th>السعر</th>
                                                </thead>
                                                <tbody id="fabric_list">
                                                <!-- the default one -->
                                                <?php $count = 1 ?>
                                                @foreach($data->bill->order as $order)
                                                    <tr class="fabric_item">
                                                        <td style="width: 33px;"><input name='number_column[]'
                                                                                        value="{{$order->number_column}}"
                                                                                        class="number_column" readonly>
                                                        </td>

                                                        <td>
                                                            <select name="id_fabric[]" class="id_fabric">
                                                                @foreach($Fabrics as $Fabric)

                                                                    @if($Fabric->current_balance < $Fabric->demand_limit)
                                                                        <option @if($order->id_fabric ==$Fabric->fabrics->id ) selected
                                                                                @endif
                                                                                value="{{$Fabric->fabrics_id}}"
                                                                        >{{$Fabric->fabrics->name}}
                                                                            -نفذ من المخزون -
                                                                        </option>
                                                                    @elseif($Fabric->current_balance  ==  $Fabric->demand_limit)
                                                                        <option @if($order->id_fabric == $Fabric->fabrics->id ) selected
                                                                                @endif
                                                                                value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}
                                                                            -{{$Fabric->current_balance}} ياردة متبقى
                                                                            فقط -
                                                                        </option>
                                                                    @else
                                                                        <option @if($order->id_fabric ==$Fabric->fabrics->id ) selected
                                                                                @endif
                                                                                value="{{$Fabric->fabrics_id}}">{{$Fabric->fabrics->name}}</option>

                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select class="type_design" data-live-search="true"
                                                                    data-show-subtext="true"
                                                                    name="type_design[]" required>
                                                                <option value="" selected="selected">select</option>
                                                                <option @if($order->type == 'Thop') selected
                                                                        @endif value="Thop">Thop
                                                                </option>
                                                                <option @if($order->type == 'VEST') selected
                                                                        @endif value="VEST">VEST
                                                                </option>
                                                                <option @if($order->type == 'Pants') selected
                                                                        @endif value="Pants">Pants
                                                                </option>
                                                                <option @if($order->type == 'Sroal') selected
                                                                        @endif value="Sroal">Sroal
                                                                </option>

                                                            </select>
                                                        </td>
                                                        <td>

                                                            <select class="addition_id "
                                                                    name="{{$count++.'_addition_id[]'}}" multiple
                                                                    required>
                                                                <option value="0">select</option>
                                                                @foreach($Additions as $Addition)
                                                                    @foreach($order->additions as $order_addition)
                                                                        @if($Addition->id == $order_addition->id )
                                                                            <option selected class="here"
                                                                                    value="{{$Addition->id}}">{{$Addition->name}}</option>
                                                                            <?php continue 2; ?>
                                                                        @endif
                                                                    @endforeach
                                                                    <option @if( $order->type == 'Pants') disabled
                                                                            @endif
                                                                            class="here"
                                                                            value="{{$Addition->id}}">{{$Addition->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>

                                                            <input type="hidden" name="order_id[]" class="order_id"
                                                                   value="{{$order->id}}">
                                                            <input step="0.01" type='number' name='yards[]'
                                                                   value='{{$order->yards}}' id="yardh_number"
                                                                   class="form-control">
                                                        </td>
                                                        <td>
                                                            <input value='{{$order->number}}' name="number[]"
                                                                   type="text"
                                                                   class=" thop_count GivePrice form-control">
                                                        </td>
                                                        <td>
                                                            <input name="price[]" value='{{$order->price}}' id="price"
                                                                   type="text" class="thop_price form-control" readonly>
                                                        </td>
                                                        <td>
                                                            <input class=" form-control price_addition"
                                                                   name="price_addition[]" type="text"
                                                                   value="{{$order->additions->sum('price')}}" required
                                                                   disabled>
                                                        </td>
                                                        <td>
                                                            <input class=" form-control totalPriceThop"
                                                                   name='totalPriceThop[]'
                                                                   value='{{$order->price*$order->number + ($order->number *$order->additions->sum('price'))}}'
                                                                   type="text"
                                                                   readonly>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-danger btn-xs delete_fabric_row"
                                                                    type="button">
                                                                <span class="glyphicon glyphicon-trash"></span></button>
                                                        </td>
                                                    </tr>

                                                @endforeach

                                                </tbody>
                                            </table>

                                            <div class="col-lg-3 col-xs-3">
                                                العدد :
                                                <input class="total_number" id="total_thop_count" readonly>
                                            </div>
                                            <div class="col-lg-3 col-xs-3">
                                                الاجمالي: <input class="cost" name="total_thop_cost"
                                                                 value="{{$data->bill->price}}" id="total_thop_cost"
                                                                 readonly>
                                            </div>


                                            <div class="col-lg-3 col-xs-3">
                                                الضريبة : <input id="tax"
                                                                 value="{{ number_format((float)($data->bill->price *0.05), 2, '.', '')}}"
                                                                 readonly>

                                            </div>
                                            <div class="col-lg-3 col-xs-3">
                                                الخصم : <input @if($n == 0) readonly
                                                               @endif value='{{$data->bill->discount}}' name="discount"
                                                               id='discount'>
                                            </div>
                                            <div class="col-lg-3 col-xs-3">
                                                السعر : <input name="finally_price" value="" id="finally_price"
                                                               readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class=" col-lg-5">
                                            <label>الاجمالي يشمل ضريبة القيمة المضافة (5%) </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- BILL -->

                            <div class="panel panel-info form-group row panel_detail_order">
                                <div class="panel-heading"> الدفع</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-3 col-xs-6">
                                            <label> طريقة الدفع </label>
                                            <select name="payment_method_id" class="form-control payment_method"
                                                    required>
                                                @foreach($Payment_methods as $Payment_method)
                                                    <option @if($data->bill->payment_method_id ==$Payment_method->id)
                                                            selected
                                                            @endif value="{{$Payment_method->id}}">{{$Payment_method->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-xs-6">
                                            <label>* المدفوع : </label>
                                            <input type='number' step="0.01" name='paid_up' id="paid_up"
                                                   class='form-control '
                                                   value='{{$data->bill->paid_up}}'
                                                   required>
                                        </div>

                                        <div class="col-lg-3 col-xs-6 paid_up_card"
                                             @if($data->bill->payment_method_id != 3) hidden @endif>
                                            <div class="form-group">
                                                <label>* المدفوع / بطاقة : </label>
                                                <input type='number' step="0.01" id="paid_up_card" name='paid_up_card'
                                                       class='form-control'
                                                       value='{{$data->bill->paid_up_card}}'>
                                            </div>
                                        </div>


                                        <div class="col-lg-3 col-xs-6">
                                            <label>* المتبقي : </label>
                                            <input step="0.01" type='number' id="Residual" name='Residual'
                                                   class='form-control '
                                                   value='{{$data->bill->Residual}}'
                                                   required>
                                        </div>
                                        <div class="col-lg-3 col-xs-6">
                                            <label>تاريخ التسليم </label>
                                            <input type='date' value="{{$data->bill->end_date}}" required
                                                   class="form-control"
                                                   name="end_date">
                                        </div>

                                    </div>
                                    <input type="hidden" name='bill_id' value="{{$data->bill->id }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info form-group row info_order ">
                        <br/>
                        <div class="panel-heading"> حالة الطلب</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>معلق / في إنتظار المراجعة </label>
                                </div><!--col-lg-6 -->
                            </div><!--row -->
                        </div><!--panel-body  -->
                    </div><!--panel info  -->
                    <button class="btn btn-success" name='done_review' value='done_review' type="submit">تاكيد الطلب
                    </button>
                    <button class="btn btn-danger " name='cancel' value="cancel" type="submit">حذف الطلب</button>
                    <button class="btn btn-info " name='edit' value="edit" type="submit">تعديل</button>
                    <button class="btn btn-success " type="button" onclick=printDiv("cn")>طباعة الفاتوره
                    </button>
                </form>
            </div><!-- specific_details_body-->
        </div>
    </div>
    <input name="state_percentage" value="{{$n}}" id="state_percentage" type="hidden">
    @if($n != 0 )
        <input type="hidden" value="{{$percentage}}" id="percentage" name="percentage">
    @endif


    <!-- TO PRINT BILL -->
    @include('layouts.helper.measurement_toEdie')
    @include('layouts.helper.print_bill_customer')

    <script>
        function test(divName, n) {

            var div_1 = document.getElementById('div_1');
            var div_2 = document.getElementById('div_2');
            var div_3 = document.getElementById(n);
            var div_4 = document.getElementById(divName);
            var combined = document.createElement('div');
            var printContents = combined.innerHTML = div_1.innerHTML + " " + div_2.innerHTML + " " +
                div_3.innerHTML + " " + div_4.innerHTML; // a little spacing
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }


        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }


        // whenever price changed
        $('.price').on('blur', function () {
            var fabric_number = $(this).parent().parent('.id_fabric').val();
            var price = $(this);
            $('#Residual').val('');
            checkFabricPrice(fabric_number, price)
        });
        //FUNCTION TO COUNT NUMBER OF THOP

        function totalThop() {
            var total = 0;
            $('input[name="number[]"]').each(function () {
                total += parseInt($(this).val());
                console.log(total);
            });
            $('.total_number').val(total);
            console.log(total);
            totalPriceThop();
        }

        //FUNCTION WILL WORKE AFTER LOAD PAGE

        $(window).bind("load", function () {
            totalThop();
        });


        //FUNCTION TO CALCULATE THOP PRICE


        // check if the cudsr right

        $('#Residual').on('change', function () {
            var result = Number($('#Residual').val()) + Number($('#paid_up').val());
            if (result != $('#finally_price').val()) {
                alert('يجب ان يكون مجموع المدفوع و المتبقي يساوي السعر ')
                $('#Residual').val('');
            }
        });


        //FUNCTION WILL WORK AFERT PAGE LOAD
        $(window).bind("load", function () {
            var row;
            var style;
            $(".specific_details_block").find(".check").each(function () {
                row = $(this).closest(".check").children();
                console.log(row)
                style = $(row[1]).find(".style").val();
                test_style(style, row);
                console.log(row)
            })
        });

        //GIVE A COST AFTER DISCOUNT

        //GIVE A COST AFTER CHINGE DISCOUNT


    </script>
    @include('layouts.javascript')
@endsection
