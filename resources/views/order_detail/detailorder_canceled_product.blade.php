@extends('layouts.app')
@section('content')
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title">طلب ملغي </h2>
                <form action="../AddMeasurement/{{$data->id}}" method="post">
                    {{ csrf_field() }}
                    </br>
                    <!-- information user -->
                @include('layouts.helper.information_user')
                <!-- detail order -->

                <!-- order -->
                    <div class=" form-group row specific_details_item " style="margin-right: -1%;">
                        <div id='detail' class="panel-heading heading_order panel_detail_order">
                            <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>
                            &nbsp; رؤية الطلب
                        </div> <!-- panel heading  -->
                        <div class="specific_details_body" hidden>
                            <div class="read_only">
                            @include('layouts.helper.bascic_order_product') <!-- basic information order -->
                            </div>
                            <div class="panel panel-info form-group row info_order ">
                                <div class="panel-heading"> حالة الطلب</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label>طلب ملغي</label>
                                        </div><!--col-lg-6 -->
                                    </div><!--row -->
                                </div><!--panel-body  -->
                            </div><!--panel info  -->
                        </div><!-- specific_details_body-->
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection