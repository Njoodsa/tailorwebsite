@extends('layouts.app')
@extends('layouts.app')
@section('content')
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title"> إضافة طلب  </h2>
                <form action="inser_product" method="post">
                    {{ csrf_field() }}
                    </br>
                    <div >
                        @include('layouts.helper.information_user')
                    </div>
                    <!-- order -->
                    <div class=" form-group row specific_details_item " style="margin-right: -1%;">
                        <div id='detail' class="panel-heading heading_order panel_detail_order">
                            <span class="glyphicon glyphicon-menu-down toggle_specific_details"></span>
                            &nbsp; رؤية الطلب
                        </div> <!-- panel heading  -->
                        <div class="specific_details_body" >
                            <div class="panel panel-info form-group row ">
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-lg-12 table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr >
                                                    <th> اسم المنتج </th>
                                                    <th>العدد </th>
                                                    <th> سعر المنتج</th>
                                                    <th>الإجمالي</th>
                                                </tr>
                                                </thead>
                                                <tbody id="fabric_list">
                                                <!-- the default one -->

                                                @foreach($data->bill->order as $order)
                                                    <input type="hidden" name="order_id[]" value="{{$order->id}}">
                                                    <tr class="fabric_item">
                                                        <td>
                                                            <select name="id_fabric[]" class="id_fabric_product">
                                                                @foreach($Fabrics as $Fabric)
                                                                    @if($Fabric->fabrics->type == 'product')
                                                                        <option @if($order->id_fabric ==$Fabric->fabrics->id ) selected
                                                                                @endif
                                                                                value="{{$Fabric->fabrics->id}}">{{$Fabric->fabrics->name}}
                                                                        </option>
                                                                    @endif

                                                                @endforeach
                                                            </select>
                                                        </td>

                                                        <td>
                                                            <input value='{{$order->number}}' name="number[]"
                                                                   type="text" class=" product_count GivePrice form-control">
                                                        </td>
                                                        <td>
                                                            <input name="price[]" value='{{$order->price}}' id="price"
                                                                   type="text" class="product_price form-control">
                                                        </td>
                                                        <td><input class=" form-control totalPriceProduct" name='totalPriceThop[]'
                                                                   value='{{$order->price*$order->number}}' type="text"
                                                                   readonly></td>
                                                    </tr>

                                                @endforeach

                                                </tbody>
                                            </table>
                                            <div class="col-lg-3">
                                                العدد :
                                                <input class="total_number" id="total_product_count" readonly>
                                            </div>
                                            <div class="col-lg-3">
                                                الاجمالي : <input class="cost" name="total_thop_cost" value="{{$data->bill->price}}" id="total_product_cost" readonly>
                                            </div>
                                            <div class="col-lg-3">
                                                الضريبة  :   <input class="form-control" id="tax" value="{{floor($data->bill->price *0.05)}}" readonly>

                                            </div>
                                            <div class="col-lg-3">
                                                السعر : <input name="finally_price" value="" id="finally_price" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class=" col-lg-5">
                                            <label>الاجمالي يشمل ضريبة القيمة المضافة (5%) </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- BILL -->

                            <div class="panel panel-info form-group row" style="width: 750px;margin-right: -92px;">
                                <div class="panel-heading"> الدفع</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label> طريقة الدفع </label>
                                            <select name="payment_method_id" class="selectpicker" data-show-subtext="true"
                                                    data-live-search="true" required>
                                                @foreach($Payment_methods as $Payment_method)
                                                    <option @if($data->bill->payment_method_id ==$Payment_method->id)
                                                            selected
                                                            @endif value="{{$Payment_method->id}}">{{$Payment_method->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>* المدفوع : </label>
                                            <input type='number' name='paid_up' id="paid_up" class='form-control '
                                                   value='{{$data->bill->paid_up}}'
                                                   required>
                                        </div>

                                        <div class="col-lg-4">
                                            <label>* المتبقي : </label>
                                            <input type='number' id="Residual" name='Residual' class='form-control '
                                                   value='{{$data->bill->Residual}}'
                                                   required>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>تاريخ التسليم </label>
                                            <input type='date' value="{{$data->bill->end_date}}" required
                                                   class="form-control"
                                                   name="end_date">
                                        </div>
                                    </div>
                                    <input type="hidden" name='bill_id' value="{{$data->bill->id }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info form-group row info_order ">
                        <br/>
                        <div class="panel-heading"> حالة الطلب</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>غير مكتمل  </label>
                                </div><!--col-lg-6 -->
                            </div><!--row -->
                        </div><!--panel-body  -->
                    </div><!--panel info  -->
                    <button class="btn btn-success" name ='type' value='done_review' type="submit">تعديل </button>
                    <button class="btn btn-danger " name='type' value="cancel" type="submit">حذف الطلب</button>
                    <button class="btn btn-success " type="button" onclick=printDiv("cn")>طباعة الفاتوره

                </form>

            </div><!-- specific_details_body-->
        </div>
    </div>
    @include('layouts.helper.print_bill_customer_product')

    <!-- TO PRINT BILL -->


    <script>
        function test(divName ,n) {

            var div_1 = document.getElementById('div_1');
            var div_2 = document.getElementById('div_2');
            var div_3 = document.getElementById(n);
            var div_4 = document.getElementById(divName);
            var combined = document.createElement('div');
            var printContents = combined.innerHTML = div_1.innerHTML + " " + div_2.innerHTML + " " +
                div_3.innerHTML + " " + div_4.innerHTML; // a little spacing
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }



        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }



    </script>
    @include('layouts.javascript')
@endsection
