@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title">الارادات لسنة :  {{$year}}  </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                            </div>
                            <div class="pull-left">
                                <button class="btn btn-success btn-xs "> مجموع الارادات :
                                    {{$total}}
                                </button>
                            </div>
                        </div><br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder=المشترى  disabled></th>
                                <th><input type="text" class="form-control" placeholder=" التاريخ " disabled></th>
                                <th><input type="text" class="form-control" placeholder=" المبلغ  " disabled></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>{{$item->supplier->name}}</td>
                                    <td>{{$item->date}}</td>
                                    <td>{{$item->price }}</td>
                                    <td>
                                        <a class="btn btn-info btn-xs"
                                           href="../detailInvoice/{{$item->id}}"><span
                                                    class=" glyphicon glyphicon-edit "></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection

