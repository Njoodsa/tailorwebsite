<link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css">
@extends('layouts.app')
@section('content')
    <title>ماوان</title>

        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <section style="margin-top:30px;">
                    <img class="img-responsive" src="{{asset("images/nm.png")}}">
                    <div class="wrap"></div>
                </section>
            </div>
            <div class=" col-lg-12 col-sm-12">
                <article>
                    <div class="row" id="row-section">
                        <div class="col-lg-3 col-sm-3">
                            <img src="{{asset("images/phone.png")}} "
                                 width="30%">
                            <br/>
                            <p><strong id="contact-text"> هاتف  </strong></p>
                            <p>  056720011 966+  </p>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <img src="{{asset("images/earth.png")}} "
                                 width="30%" >
                            <p><strong id="contact-text">الموقع </strong></p>
                            <p>شارع التخصصي، النخيل، الرياض 12381</p>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <img src="{{asset("images/time.png")}} " width="30%" >
                            <p><strong id="contact-text">وقت الدوام</strong></p>
                            <p>٩ص–١٢م٤–١١م</p>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <img src="{{asset("images/day.png")}} " width="30%" >
                            <p><strong id="contact-text"> ايام الدوام </strong></p>
                            <p> من السبت الى الخميس</p>
                        </div>
                    </div>
                </article>
                <article style=" padding-top: 80px; padding-bottom:80px; width:80%;margin-right:10%">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14493.824399189632!2d46.643215
                    5!3d24.7455464!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x13a436f24da78250!2z2YXYp9mI2KfZhiDZhNmE2K7
                    Zitin2LfYqSDYp9mE2LHYrNin2YTZitip!5e0!3m2!1sar!2ssa!4v1495445630537" width="100%"
                                height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                </article>
            </div>
        </div>
   
@endsection
