@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">إضافة منتج </h2>
                <form action="insertProduct" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * اسم المنتج  : </label>
                                <input type='text' name='name' class='form-control' value='{{old('name') }}' required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * القسم  : </label>
                                <select name='section_id' class='form-control' required>
                                    <option value="">--اختر القسم --</option>
                                    @foreach($Sections as $item)
                                        <option  value='{{$item->id}}'>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> *  السعر  : </label>
                                <input type='number'  step="0.01" name='price' class='form-control'
                                       value="{{old('lowest_price')}}" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> *  الوحدة: </label>
                                <input type='text'  step="0.01" name='unit' class='form-control'
                                       value="{{old('unit')}}" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label> رقم الباركود: </label>
                            <input type='number' name='barcode_number' class='form-control'
                                   value="{{ old('barcode_number') }}">
                        </div>
                        <div class="col-lg-6">
                            <label> الملاحظات : </label>
                            <input type='text' name='note' class='form-control' value='{{old('note') }}'>
                        </div>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>

@endsection

