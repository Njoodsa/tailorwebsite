@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">تعديل قماش</h2>
                <form action="../updateProduct/{{$data->id}}" method="post" class="contentForm">
                    <input type="hidden" name="id" value="{{$data->id}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * اسم المنتج  : </label>

                                <input type='text' name='name' class='form-control' value='{{$data->name}}' required>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * القسم  : </label>
                                <select name='section_id' class='form-control' required>
                                    @foreach($Sections as $item)
                                        <option @if($data->section_id == $item->id){ selected }
                                                @endif value='{{$item->id}}'>
                                            {{$item->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label> * السعر  : </label>
                            <input type='number' name='price' class='form-control'
                                   value='{{$data->price}}' required>
                        </div>
                        <div class="col-lg-6">
                            <label> * الوحدة   : </label>
                            <input type='text' name='unit' class='form-control'
                                   value='{{$data->unit}}' required>
                        </div>
                        <div class="col-lg-6">
                            <label> رقم الباركود: </label>
                            <input type='number' name='barcode_number' class='form-control'
                                   value='{{$data->barcode_number }}'>
                        </div>
                        <div class="col-lg-6">
                            <label> الملاحظات : </label>
                            <input type='text' name='note' class='form-control' value='{{$data->note }}'>
                        </div>

                    </div>
                    <br/>
                    <button class="btn btn-block  btn-info">تعديل</button>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')

@endsection