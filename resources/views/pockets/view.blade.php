@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title">تصميم الجيوب  </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                                <a href="createPocket" class="btn btn-success btn-xs "><span
                                            class="glyphicon glyphicon-plus-sign"></span> إضافه</a>
                            </div>
                        </div><br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder=الاسم disabled></th>
                                <th><input type="text" class="form-control" placeholder=" الملاحظات" disabled></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $pocket)
                                <tr>
                                    <td>{{$pocket->name}}</td>
                                    <td>{{$pocket->note}}</td>

                                    <td>
                                        <a class="btn btn-info btn-xs" href="editPocket/{{$pocket->id}}"><span
                                                    class=" glyphicon glyphicon-edit "></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection

