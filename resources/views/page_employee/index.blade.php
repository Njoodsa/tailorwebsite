@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6" style="margin-left :-10% ">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">بياناتي</h2>
                <form action="update_employeeInformation/{{Auth::user()->id}}" method="post">
                    {{ csrf_field() }}
                    <center style="margin: 4%"> <!-- make  form center  -->
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                <input type="hidden" name="id_number" value="{{Auth::user()->employee_datas->id}}">
                                <label >الاسم</label>
                                <input type="text" class="form-control" value="{{Auth::user()->employee_datas->name}}"
                                       name="name">
                            </div>
                            <div class="form-group">
                                <label >رقم الجوال</label>
                                <input type="number" class="form-control" value="0{{ Auth::user()->employee_datas->phone}}"
                                       name="phone">
                            </div>
                            <div class="form-group">
                                <label for="usr">الايميل </label>

                                <input type="email" name="email" value="{{Auth::user()->email}}" class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="usr">العنوان </label>

                                <input type="text" name="address" value="{{Auth::user()->employee_datas->address}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="pwd">كلمة المرور</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                            <button type="submit" name=" update" class="btn btn-info  btn-md">تعديل</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
@endsection