@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6" style="margin-left :-20% ">
            </div>
            <div class="col-md-6">
                @include('layouts.error.error')
                <h2 class="main-title">تم منعك من صلاحية الوصول الى النظام يرجى مراسلة الاداره لمعرفة السبب</h2>
            </div>
        </div>
    </div>
@endsection