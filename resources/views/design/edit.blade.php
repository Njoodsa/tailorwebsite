@extends('layouts.app')
@section('content')
    <Style>
        .margin-SidBar{
            margin-left :-12% !important;
        }
    </Style>
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.flash')
                <h2 class="main-title">تعديل تفصيل</h2>
                <form action="../updateDesign/{{$data->id}}" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <label> الاسم : </label>
                            <input type='hidden' name='id' value='{{$data->id}}'>
                            <input type='text' name='name' class='form-control' value='{{$data->name}}' required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        * <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <label>ملاحظات : </label>
                            <input type='text' name='note' class='form-control' value='{{$data->note}}'>
                            @if ($errors->has('note'))
                                <span class="help-block">
                                        * <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group" style="float: left">
                            <label>   Do not has pocket and bar <input @if($data->has_bar_pocket == 1) checked @endif type='checkbox' name='has_bar_pocket'  >   </label>

                        </div>
                    </div>

                    <br/>
                    <button class="btn btn-block  btn-info">تعديل</button>
                </form>
            </div>
        </div>
    </div>
@endsection