@component('layouts.include.views')
   @slot('title')
    'التفصيل
    @endslot

    @slot('linkAdd')
        <a href="createDesign" class="btn btn-success btn-xs ">
            <span class="glyphicon glyphicon-plus-sign"></span> إضافه
        </a>
    @endslot

    @slot('thead')
        <th></th>
        <th><input type="text" class="form-control" placeholder=الاسم disabled></th>
        <th><input type="text" class="form-control" placeholder=" الملاحظات" disabled></th>
        <th></th>
    @endslot

    @slot('tbody')
        @foreach($data as $design)
            <tr>
                <td> </td>
                <td>{{$design->name}}</td>
                <td>{{$design->note}}</td>
                <td>
                    <a class="btn btn-info btn-xs" href="editDesign/{{$design->id}}"><span
                                class=" glyphicon glyphicon-edit "></span></a>
                </td>
            </tr>
        @endforeach
    @endslot
@endcomponent
