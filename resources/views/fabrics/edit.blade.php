@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">تعديل قماش</h2>
                <form action="../updateFabric/{{$data->id}}" method="post" class="contentForm">
                    <input type="hidden" name="id" value="{{$data->id}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label> * اسم القماش : </label>
                                <input type='text' name='name' class='form-control' value="{{$data->name}}" required>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * الصناعه : </label>
                                <select name='industries_id' required>
                                    @foreach($industries as $industry)

                                        <option @if($data->industries_id == $industry->id){ selected }
                                                @endif value='{{$industry->id}}'>
                                            {{$industry->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>* عرض القماش : </label>
                                <select name='fabric_width' required>
                                    <option @if($data->fabric_width == 'عرض واحد'){ selected } @endif value='عرض واحد'>
                                        عرض واحد
                                    </option>
                                    <option @if($data->fabric_width == 'عرضين'){ selected } @endif value='عرضين'>عرضين
                                    </option>
                                </select>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <table class="table">
                                <thead>
                                <th>*</th>
                                <th>الوحدة</th>
                                <th>الكمية المعادلة</th>
                                <th>سعر البيع</th>
                                </thead>
                                <tbody>
                                @foreach($Fabrics_units as $item)
                                    <tr>
                                        @if($item->Equation == 1)
                                            <td style="width: 100px">وحدة التجزئة :</td>
                                        @else
                                            <td>وحدة الجملة :</td>
                                        @endif
                                        <td>
                                            <select name='unit_id[]' required>
                                                    <option  value='{{$item->unit_id}}'> {{$item->units->name}} </option>
                                            </select>
                                        </td>

                                        <td><input step="0.01" type='number' name='Equation[]'
                                                  value='{{$item->Equation}}'
                                                   @if($item->Equation == 1)  readonly @endif
                                                   @if($item->units->id == 1)
                                                   class='form-control Equation_taga'
                                                   @elseif($item->units->id != 1 && $item->units->id != 2 )
                                                   class='form-control Equation2'
                                                   @endif
                                                   required></td>

                                        <td><input step="0.01" type='number' name='price[]'
                                                   value='{{$item->price}}' required
                                                   @if($item->units->id == 2)
                                                   class='form-control price_yearda'
                                                   @elseif($item->units->id == 1)
                                                   class='form-control price_taga'
                                                   @else
                                                   class='form-control price2'
                                                   @endif

                                            ></td>
                                        <input type="hidden" name='u_id[]' value="{{$item->id}}">
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-12 col-md-6">
                            <label> * ادنى سعر للبيع : </label>
                            <input type='number' name='lowest_price'  step="0.01" class='form-control'
                                   value='{{$data->lowest_price}}' required>
                        </div>
                        <div class="col-lg-12 col-md-6">
                            <label> رقم الباركود: </label>
                            <input type='number' name='barcode_number' class='form-control'
                                   value='{{$data->barcode_number }}'>
                        </div>
                        <div class="col-lg-12 col-md-6">
                            <label> الملاحظات : </label>
                            <input type='text' name='note' class='form-control' value='{{$data->note }}'>
                        </div>
                        <div class="col-lg-12 col-md-6">
                            <label><input  @if($data->disable == 1) checked @endif
                                type="checkbox" value="1" name="disable">&nbsp;  تعطيل    </label>
                        </div>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-info">تعديل</button>
                </form>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
    <script>
        $('document').ready(function () {

            function price_taga() {
                $('.price_taga').val(
                    $('.Equation_taga').val() * $('.price_yearda').val()
                );

            }

            //
            function price2() {

                $('.price2').val(
                    $('.Equation2').val() * $('.price_yearda').val()
                );
            }

            $('.Equation_taga').on('input', function () {
                price_taga()
            });

            $('.Equation2').on('input', function () {
                price2();
            });

            $('.price_yearda').on('input', function () {
                price2();
                price_taga();
            });
        });


    </script>

@endsection
