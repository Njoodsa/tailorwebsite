@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">إضافة قماش</h2>
                <form action="insertFabric" method="post" class="contentForm">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label> * اسم القماش : </label>
                                <input type='text' name='name' class='form-control' value='{{old('name') }}' required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label> * الصناعه : </label>
                                <select name='industries_id' required>
                                    <option value="">--اختر الصناعه --</option>
                                    @foreach($industries as $industry)
                                        <option value='{{$industry->id}}'>{{$industry->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>* عرض القماش : </label>
                                <select name='fabric_width' required>
                                    <option value="">--اختر --</option>
                                    <option value='عرض واحد'>عرض واحد</option>
                                    <option value='عرضين'>عرضين</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            

                            <table class="table">
                                <thead>
                                <th>*</th>
                                <th>الوحدة</th>
                                <th>الكمية المعادلة</th>
                                <th>سعر البيع</th>
                                </thead>
                                <tbody id="new_list">
                                <tr>
                                    <td style="width: 100px">وحدة التجزئة :</td>
                                    <td>
                                        <select name='unit_id[]' required >
                                            <option value='2'> يــــــاردة</option>
                                        </select>
                                    </td>
                                    <td><input type='number' name='Equation[]' class='form-control' value='1' readonly
                                               required></td>
                                    <td><input type='number'   step="0.01" name='price[]' class='form-control price_yearda'
                                               required></td>
                                </tr>
                                <tr>
                                    <td>وحدة الجملة 1 :</td>
                                    <td>
                                        <select name='unit_id[]' >
                                            <option value='1'> طـــاقــــة</option>
                                        </select>
                                    </td>
                                    <td><input type='number'  step="0.01"  name='Equation[]' class='form-control'
                                                 id ='Equation_taga' required></td>
                                    <td><input type='number'  step="0.01" name='price[]' class='form-control price_taga'
                                               required readonly></td>
                                </tr>
                                <tr>
                                    <td>وحدة الجملة 2 :</td>
                                    <td>
                                        <select name='unit_id[]' required>
                                            @foreach($units as $unit)
                                                <option value='{{$unit->id}}'> {{$unit->name}}  </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type='number'  step="0.01" name='Equation[]' class='form-control Equation2'
                                              ></td>
                                    <td><input type='number'  step="0.01" name='price[]' class='form-control price2'
                                              readonly></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-12 col-md-6 ">
                            <label> * ادنى سعر للبيع : </label>
                            <input type='number'  step="0.01" name='lowest_price' class='form-control'
                                   value="{{old('lowest_price')}}" required>
                        </div>
                        <div class="col-lg-12 col-md-6">
                            <label> رقم الباركود: </label>
                            <input type='number' name='barcode_number' class='form-control'
                                   value="{{ old('barcode_number') }}">
                        </div>
                        <div class="col-lg-12 col-md-6">
                            <label> الملاحظات : </label>
                            <input type='text' name='note' class='form-control' value='{{old('note') }}'>
                        </div>
                        <div class="col-lg-12 col-md-6">
                             <label><input value='1' type="checkbox" name="disable">&nbsp;  تعطيل    </label>
                        </div>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>
    
<script>
    $('document').ready(function () {

        function price_taga() {

            $('.price_taga').val(
                $('#Equation_taga').val() * $('.price_yearda').val()
            );

        }

        //
        function price2() {

            $('.price2').val(
                $('.Equation2').val() * $('.price_yearda').val()
            );
        }

        $('#Equation_taga').on('input', function () {
            price_taga()
        });

        $('.Equation2').on('input', function () {
            price2();
        });

        $('.price_yearda').on('input', function () {
            price2();
            price_taga();
        });
    });


</script>
    @include('layouts.javascript')
@endsection

