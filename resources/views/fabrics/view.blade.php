@component('layouts.include.views')
    @slot('title')
        الاقمشه
    @endslot

    @slot('linkAdd')
        <a href="createFabric" class="btn btn-success btn-xs "><span
                    class="glyphicon glyphicon-plus-sign"></span> إضافه
        </a>
    @endslot

    @slot('thead')
        <th><input type="text" class="form-control" placeholder=الاسم disabled></th>
        <th><input type="text" class="form-control" placeholder=" الصناعه" disabled></th>
        <th><input type="text" class="form-control" placeholder=" ادنى سعر للبيع " disabled></th>
        <th></th>
    @endslot

    @slot('tbody')
        @foreach($data as $fabric)
            <tr>
                <td>{{$fabric->name}}</td>
                <td>{{$fabric->industries->name}}</td>
                <td>{{$fabric->lowest_price}}</td>
                <td>
                    <a class="btn btn-info btn-xs" href="editFabric/{{$fabric->id}}"><span
                                class=" glyphicon glyphicon-fullscreen "></span></a>
                </td>
            </tr>
        @endforeach
    @endslot
@endcomponent
