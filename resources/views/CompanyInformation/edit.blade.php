@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar" >
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.error')
                <h2 class="main-title">بيانات الشركه </h2>
                <form action="{{route('companyInformation.update',$data->id)}}" method="post"  class="contentForm" >
                    {{ csrf_field() }}
                    {{ @method_field('PATCH') }}
                    <div class="form-group">
                        <label>هاتف</label>
                        <input type='number' name='phone' class='form-control' value='{{$data->phone}}'>
                    </div>
                    <div class="form-group">
                        <label>جوال</label>
                        <input type='number' name='cell_phone' class='form-control' value='{{$data->cell_phone}}'>
                    </div>
                    <div class="form-group">
                        <label>فاكس : </label>
                        <input type='text' name='fax' class='form-control' value='{{$data->fax}}'>
                    </div>
                    <div class="form-group">
                        <label>البريد الالكتروني : </label>
                        <input type='email' name="email" class='form-control' value='{{$data->email}}'>
                    </div>
                    <div class="form-group">
                        <label>اوقات الدوام : </label>
                        <input type='text' name="work_hours" class='form-control' value='{{$data->work_hours}}'>
                    </div>
                    <div class="form-group">
                        <label>ايام الدوام :</label>
                        <input type='text' name='work_days' class='form-control' value='{{$data->work_days}}'>
                    </div>
                    <div class="form-group">
                        <label>الموقع :</label>
                        <input type='text' class='form-control' name="location" value='{{$data->location }}'>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-danger">تعديل</button>
                </form>
            </div>
        </div>
    </div>
@endsection