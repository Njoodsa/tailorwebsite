@extends('layouts.app')
@section('content')
<style>

.row i {
  color: #586165;
}
</style>
    <div class="container background-container ">
        <div class="row">
            <div class="col-md-6 margin-SidBar">@include('layouts.SidBar')</div>
            <div class="col-md-6">
                <h2 class="main-title">حجز بإسم {{$reservation->name}} </h2>
                </br>
                <div class="panel  form-group row   panel_detail_order specific_details_item  ">
                    <div id='detail' class="panel-heading heading_order">تفاصيل الحجز  رقم &nbsp({{$reservation->id}})</div>
                    <div class="panel-body specific_details_body" >
                      <div class="row">
                          <div class="col-md-3 col-lg-3 col-sm-3">
                             <label> الاسم : </labale><i>{{$reservation->name}}</i>
                          </div>
                          <div class="col-md-3 col-lg-3 col-sm-3">
                             <label> رقم الجوال : </labale><i>{{$reservation->phone}}</i>
                          </div>
                          <div class="col-md-3 col-lg-3 col-sm-3">
                             <label>التاريخ  : </labale><i>{{$reservation->date_time->format('Y-m-d')}}</i>
                          </div>
                          <div class="col-md-3 col-lg-3 col-sm-3">
                             <label>  الوقت  : </labale><i>{{$reservation->date_time->format('H:i')}}</i>
                          </div>
                          <div class="col-md-3 col-lg-3 col-sm-3">
                             <label>  الموقع   : </labale>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div id='map_canvas' style="width:100%;height:300px">
                               <input name='coords' id="coords"  size="100" value="{{$reservation->lan}}"  >
                               <input name='map_url' id="map_url" size="100" value="{{$reservation->lat}}"  >
                            </div>
                      </div>
                    </div><!-- panel-body -->
                    <div class="row" style="margin-top:30px">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                             <label>  الحالة    : </labale><i>{{$reservation->state}}</i>
                        </div>
                        @if($reservation->state == 'مؤكد')
                        <div class="col-md-6 col-lg-6 col-sm-6">
                             <label>  تحديث الى     : </labale><a href="{{url('visited/'.$reservation->id)}}">تمت الزيارة </a>
                        </div>
                        @endif
                        <div class="col-md-6 col-lg-6 col-sm-6">
                           <label>  إنشاء طلب للعميل     : </labale>
                          @foreach($reservation->mobileNumbers->customer as $item )
                            &nbsp |  <a href="{{url('createOrder/'.$item->id)}}">{{$item->name}}</a>
                          @endforeach
                        </div>
                      </div>
               </div><!-- specific_details_body-->
          </div><!-- div-md-6 -->
      </div><!-- row -->
  </div><!-- container -->
@include('layouts.map')
@endsection
