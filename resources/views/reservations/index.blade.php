@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6 ">
                <h2 class="main-title"> الحجوزات  </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>

                            </div>
                        </div>
                        <br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th><input type="text" class="form-control" placeholder='رقم الحجز' disabled></th>
                                <th><input type="text" class="form-control" placeholder='رقم الجوال' disabled></th>
                                <th><input type="text" class="form-control" placeholder='تاريخ الحجز  ' disabled></th>
                                <th><input type="text" class="form-control" placeholder= "حالة الحجز"  disabled></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($reservations as $reservation)
                                <tr>
                                    <td>{{$reservation->id}}</td>
                                    <td>{{ $reservation->phone}}</td>
                                    <td>{{$reservation->date_time->format('Y-m-d')}}</td>
                                    <td>{{$reservation->state}}</td>
                                    <td>
                                        <a class="btn btn-info btn-xs" href="reservation/{{$reservation->id}}"><span
                                                    class=" glyphicon glyphicon-fullscreen "></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{$reservations->links()}}

    </div>

    @include('layouts.javascript')
@endsection
