@extends('layouts.app')
@section('content')
    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar" >
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                @include('layouts.flash')
                <h2 class="main-title">إضافة قسم  </h2>
                <form action="insertSection" method="post" class="contentForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <label> الاسم : </label>
                            <input type='text' name='name' class='form-control' value='{{old('name') }}' required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        * <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <br/>
                    <button class="btn btn-block  btn-success">إضافة</button>
                </form>
            </div>
        </div>
    </div>
@endsection