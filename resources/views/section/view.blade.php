@extends('layouts.app')
@section('content')

    <div class="container background-container">
        <div class="row">
            <div class="col-md-6 margin-SidBar">
                @include('layouts.SidBar')
            </div>
            <div class="col-md-6">
                <h2 class="main-title">الاقسام   </h2>
                <div class="row">
                    <div class=" filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                                <button class="btn btn-info btn-xs btn-filter"><span
                                            class="glyphicon glyphicon-filter"></span> بحث
                                </button>
                                <a href="createSection" class="btn btn-success btn-xs "><span
                                            class="glyphicon glyphicon-plus-sign"></span>  إضافة قسم </a>
                            </div>
                        </div><br/>
                        <table class="table">
                            <thead>
                            <tr class="filters">
                                <th> * </th>
                                <th><input type="text" class="form-control" placeholder=' القسم ' disabled></th>
                                <th><input type="text" class="form-control" placeholder=' عدد المنتجات ' disabled></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1;?>
                            @foreach($data as $item)
                                <tr>
                                    <td><?php echo $count++ ;?></td>
                                    <td>{{$item->name}}</td>
                                    <td >{{$item->products->count()}}</td>

                                    <td>
                                        <a class="btn btn-info btn-xs" href="editSection/{{$item->id}}"><span
                                                    class=" glyphicon glyphicon-edit "></span></a>
                                    </td>
                                    <td>
                                        <a class="btn btn-info btn-xs" href="ViewSection/{{$item->id}}/{{$item->name}}"><span
                                                    class=" glyphicon glyphicon-open "></span></a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.javascript')
@endsection

