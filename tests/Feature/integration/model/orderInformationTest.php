<?php

//namespace Tests\Feature;
use App\Order_Information;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Order_detail;
class orderInformationTest extends TestCase
{
   use DatabaseTransactions;
    /**
     * A basic test example.
     * @return void
     */
    /** @test*/
     function it_fetches_order()
    {
        //*Given
        factory(Order_Information::class,2)->create();
        $product = factory(Order_Information::class)->create(['state'=>2,'type'=> 'fabirc']);
        factory(Order_Information::class)->create(['state'=>2]);
        factory(Order_Information::class)->create(['state'=>3]);
        //When
        $order = Order_Information::findOrderTest(2,'fabirc');
        //Then
        $this->assertEquals($product->id,$order->id);
        //dd($order);
    }
    /** @test*/
    function add_order_detail()
    {
        $order = factory(Order_Information::class)->create();
        $detail =factory(Order_detail::class)->create();
        $this->expectException('Exception');
        $detail2 =factory(Order_detail::class)->create();
        $order->addOrder($detail);
        $order->addOrder($detail2);
        //$detailMany =factory(Order_detail::class,2)->create();
        //$order->addOrder($detailMany);
        $this->assertEquals(2,$order->count());

    }

    /** @test*/
    function order_has_maximum_detail()
    {
        $order = factory(Order_Information::class)->create(['detail_number'=>4]);
        $detail =factory(Order_detail::class)->create();
        $detailTow =factory(Order_detail::class)->create();
        $order->addOrder($detail);
        $order->addOrder($detailTow);
        $this->expectException('Exception');
        $detailThree = factory(Order_detail::class)->create();
        $order->addOrder($detailThree);

    }

    /** @test*/
    function order_can_add_detail_at_once()
    {
        $order = factory(Order_Information::class)->create(['detail_number'=>1]);
        $detail2 =factory(Order_detail::class,4)->create();
        $this->expectException('Exception');
        $order->addOrder($detail2);
        $this->assertEquals(1,$order->count());

    }


    /** @test*/

    function remove_detail_order()
    {
        $order = factory(Order_Information::class)->create(['detail_number'=>2]);
        $detail =factory(Order_detail::class)->create();
        $order->addOrder($detail);
        $order->removeDetail($detail);
        $this->assertEquals(0,$order->count());
    }
    /** @test*/

    function remove_detail_many_detail_at_once()
    {
        $order = factory(Order_Information::class)->create(['detail_number'=>2]);
        $detail =factory(Order_detail::class,4)->create();
        $order->addOrder($detail);
        $order->restartDetail($detail);
        $this->assertEquals(0,$order->count());
    }
    /** @test*/

    function remove_detail_more_one_detail_at_once()
    {
        $order = factory(Order_Information::class)->create(['detail_number'=>4]);
        $detail =factory(Order_detail::class,5)->create();
        $order->addOrder($detail);
        $order->removeDetail($detail->slice(0,2));
        $this->assertEquals(2,$order->count());
    }
}
