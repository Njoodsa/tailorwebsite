<?php

namespace Tests\Feature;

use App\Date;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
class DateTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function transform_date_to_timestamp()
    {
      $date = '2019-12-12';
      $time = '2:00';
      $date = Date::transformDate($date,$time);
      factory(Date::class)->create(['date'=>$date,'type'=>0]);
      $this->assertDatabaseHas('dates',[
          'date'=>$date,
          'type'=>0
              ]);
      //$this->assertEquals($date, '2019-12-12 02:00:00');
    }
}
