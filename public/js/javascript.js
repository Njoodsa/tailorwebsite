
/*
 Please consider that the JS part isn't production ready at all, I just code it to show the concept of merging filters and titles together !
 */
$(document).ready(function(){
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
            $filters = $panel.find('.filters input'),
            $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
            inputContent = $input.val().toLowerCase(),
            $panel = $input.parents('.filterable'),
            column = $panel.find('.filters th').index($input.parents('th')),
            $table = $panel.find('.table'),
            $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
});




/// conver number from arabic to english


String.prototype.toEnglishDigits = function () {
    var persian = {
        '۰': '0',
        '۱': '1',
        '۲': '2',
        '۳': '3',
        '۴': '4',
        '۵': '5',
        '۶': '6',
        '۷': '7',
        '۸': '8',
        '۹': '9'
    };
    var arabic = {
        '٠': '0',
        '١': '1',
        '٢': '2',
        '٣': '3',
        '٤': '4',
        '٥': '5',
        '٦': '6',
        '٧': '7',
        '٨': '8',
        '٩': '9'
    };
    return this.replace(/[^0-9.]/g, function (w) {
        return persian[w] || arabic[w] || w;
    });
};



$(window).bind("load", function () {
    var total2 = 0;
    $('input[name="totalPriceThop[]"]').each(function () {
        total2 += parseInt($(this).val());
    });
    $('.cost').val(total2);
    $('#finally_price').val($('.cost').val() - $('#discount').val());
});

$(".toggle_specific_details").on("click", function () {
    $(this).closest('.specific_details_item').find('.specific_details_body').fadeToggle()
});
$(".my_selection").change(function () {
    window.location.href = this.children[this.selectedIndex].getAttribute('href');
})


    // select2
    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span><img src="' + $(state.element).data('imagesrc') + '" width="24px"  /> ' + state.text + '</span>'
        );
        return $state;
    };
    $('.select2').select2({templateResult: formatState, language: "ar"});

    /*
     * Specific Details Block
     */

    // add item

    // add item
    $(".add_specific_details_box").click(function(){

        $(".specific_details_item").find("select").select2("destroy").end();
        $('.specific_details_block').append(
            $(".specific_details_item").first().clone()
        );

        $(".specific_details_item").find("select").select2({templateResult: formatState, language: "ar"});
    });

    $(".delete_specific_details_row").on("click", function () {
        var item = $(this).closest(".specific_details_item")
        // if this is the last row then just empty its fields
        if ($('.specific_details_item').length === 1) {
            item.find("select").select2("destroy").end();
        } else {
            item.remove();
        }
    });


    // get lawest price

    // to display phone if the customer currently

    function checkIfOld() {
        $('#name').attr("disabled", true);
        $('#name').val('');
        $('#phone').val('');
        $('input[name="phone"]').on('blur', function () {
            var phone = $('input[name="phone"]').val();
            $.ajax({
                url: 'GetName/' + phone,
                type: 'Get',
                dataType: "json",
                success: function (data) {
                    console.log(data['name']);
                    $('#name').val(data['name']);
                },
                error: function () {

                    $('#name').val('الرقم غير موجود');
                }
            })
        });
    }

    function checkIfNew() {
        $('#name').attr("disabled", false);
        $('#name').val('');
        $('#phone').val('');
        $('input[name="phone"]').on('blur', function () {
            var phone = $('input[name="phone"]').val().toEnglishDigits();
            $.ajax({
                url: 'Check_number/' + phone,
                type: 'Get',
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (data['phone'] = $('#phone').val()) {
                        alert('الرقم موجود سابقاً');
                        $('#name').val('');
                        $('#phone').val('');
                    }
                },
                error: function () {
                    $('#name').val('');
                }

            });
        });
    }

    $('.test-select').click(function () {
        if ($('input[name="select"]:checked').val() == 'old') {
            checkIfOld();
        } else if ($('input[name="select"]:checked').val() == 'new') {
            checkIfNew();
        }
    });

    // /// /// /// ///// // /// // / / / / // / / / / /
    //to give price of fabric

    /////////////////////////////////////////////////////


    // ddSlick Plugin for using thumb images inside of <option> of select
    // http://designwithpc.com/plugins/ddslick


    /*
     * Fabrics Block
     */
    // Add new fabric row
    $(".add_new_fabric_row").on("click", function () {
        $(".fabric_item").first()
            .clone(true)
            .find("input:text").val("").end()
            .appendTo("#fabric_list")
    })

    // Delete fabric row
    $(".delete_fabric_row").on("click", function () {
        var tr = $(this).closest("tr")

        // if this is the last row then just empty its fields
        if ($('#fabric_list tr').length === 1) {
            tr.find("input:text").val("").end()
        } else {
            tr.remove()
        }

        updateNumbers()

    });

    function updatedRowPrice(columns, callback) {
        var count = Number($(columns[2]).find(".thop_count").val().toEnglishDigits())
        var price = Number($(columns[3]).find(".thop_price").val().toEnglishDigits())
        return callback($(columns[4]).find(".totalPriceThop"), count * price)
    }

    $(".thop_count, .thop_price").on("input", function () {
        var row = $(this).closest("tr").children()
        updatedRowPrice(row, function (input, cost) {
            input.val(cost)
            updateNumbers()
        })
    })

    $(".thop_price").on("change", function () {
        var row = $(this).closest("tr").children()
        updatedRowPrice(row, function (input, cost) {
            input.val(cost)
            updateNumbers()
        })

        var id_fabric = $(row[0]).find(".id_fabric").val();
        var thopprice = Number($(row[3]).find(".thop_price").val().toEnglishDigits());
        console.log(id_fabric);

        FAKEcheckFabricPrice(id_fabric, thopprice, row)
        // checkFabricPrice(id_fabric, thopprice, row)

    })

    $('.id_fabric').on('change', function () {

        var row = $(this).closest("tr").children()
        updatedRowPrice(row, function (input, cost) {
            input.val(cost)
            updateNumbers()
        })

        var id_fabric = $(row[0]).find(".id_fabric").val();
        var thopprice = Number($(row[3]).find(".thop_price").val().toEnglishDigits());
        if (thopprice != 0) {
            console.log(id_fabric);

            FAKEcheckFabricPrice(id_fabric, thopprice, row)
        }
    });


    function FAKEcheckFabricPrice(fabric_number, selected_price, columns) {
        $.ajax({
            url: 'GetLowestPrice/' + fabric_number,
            Type: 'GET',
            dataType: 'json',
            success: function (data) {
                var lowest_price = data['lowest_price']
                if (selected_price < lowest_price) {
                    alert('ادنى سعر للبيع هو ' + lowest_price)
                    $(columns[3]).find(".thop_price").val(lowest_price)
                    updatedRowPrice(columns, function (cost) {
                        $(columns[4]).find(".totalPriceThop").val('')
                        updateNumbers()
                    })
                }
            }
        })
    }

    $("#discount").on("input", function () {
        $("#finally_price").val(Number($("#total_thop_cost").val()) - Number($("#discount").val().toEnglishDigits()))
    });

    function updateNumbers() {
        // updating thop total count
        var i = 0
        $("#fabric_list").find(".thop_count").each(function () {
            i += Number($(this).val().toEnglishDigits())
        })
        $("#total_thop_count").val(i)

        // updating grand total price with discount
        i = 0
        $("#fabric_list").find(".totalPriceThop").each(function () {
            i += Number($(this).val())
        })
        $("#total_thop_cost").val(i)
        $("#finally_price").val(i - Number($("#discount").val().toEnglishDigits()))

    }

    $('#Residual').on('change', function () {
        var result = Number($('#Residual').val()) + Number($('#paid_up').val());
        if (result != $('#finally_price').val()) {
            alert('يجب ان يكون مجموع المدفوع و المتبقي يساوي السعر ')
            $('#Residual').val('');
        }
        console.log(result);

    });

    $('#bill_state').on('change', function () {
        var result = $('#paid_up').val();
        if (result != $('#finally_price').val() && $('#bill_state').val() == 1) {
            alert(' لم يتم دفع كافة المبلغ لتاكيد الفاتورة ')
            $('#bill_state').val('');
        }




});









/////




/* end */