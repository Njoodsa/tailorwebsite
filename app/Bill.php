<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use App\Http\Helper;
class Bill extends Model
{
   protected $fillable = [ 'paid_up' , 'Residual' ,'discount' ,'payment_method_id','end_date','price','state','paid_card_later',
                            'paid_later','paid_up_card','other_payment_method'];

   public function order(){
       return $this->hasMany(Order::class,'Bill_id', 'id');
   }
    public function order_information(){
        return $this->hasOne(Order_Information::class,'id_bill' );
    }

    public function payment_method(){
        return $this->belongsTo(Payment_method::class,'payment_method_id');
    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d ');
    }
    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');

    }
    public function addBill($bill)
    {
        return $this->create($bill);
    }
    public function addOrder($p_order)
    {
       return $this->order()->create($p_order);
    }
    public function information_order($order)
    {
        return $this->order_information()->create($order);

    }
    public function updateBill($bill)
    {
        $this->update($bill);
    }
    public function updateOrder($order)
    {
        return $this->order()->update($order);
    }

}
