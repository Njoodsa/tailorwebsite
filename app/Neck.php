<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
class Neck extends Model
{
    //
    protected $fillable = ['name','note'];

    public function order_details(){
        return $this->hasMany(Order_detail::class);
    }

    public static function cache(){
        return Cache::rememberForever('Neck', function() {
            return static::get();
        });
    }
}
