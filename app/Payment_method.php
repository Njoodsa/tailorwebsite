<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
class Payment_method extends Model
{
    //
    protected $fillable = ['name'];

    public function bill(){
        return $this->hasMany(Bill::class);
    }

    public function supplier(){
        return $this->hasMany(Bill::class);
    }

    public static function cache(){
        return Cache::rememberForever('Payment_method', function() {
            return static::get();
        });
    }
}
