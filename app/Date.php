<?php

namespace App;
use App\Http\Helper;
use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $fillable = ['date','type'];
    protected $dates = ['date'];
    protected $hidden   =['created_at','updated_at'];
    public static function transformDate(){
        // if type = 1 that mean all day blocked, else we transform date to timestamp to blocked hour
        return (request('type') != 0)?request('date') :  Helper::transformDateToTimestamp(request('date'),request('hours')) ;
    }
}
