<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fabrics_unit extends Model
{
    // Equation to product mean quantity
    protected $fillable = [ 'id ', 'unit_id' , 'Equation' , 'price' , 'fabrics_id' ];


    public function Fabrics(){

        return $this->belongsTo(Fabrics::class,'fabrics_id');
    }
    public function units(){

        return $this->belongsTo(Unit::class,'unit_id');
    }

    public static function findBy($conditions=[])
    {
        return Fabrics_unit::where($conditions);
    }
}

