<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['name' ,'address', 'phone' , 'cell_phone' ,'fax' , 'mailbox' , 'warehouses_id'] ;

    public function Warehouses()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function boxes()
    {
        return $this->hasMany(Box::class,'branches_id');
    }
    public function users()
    {
        return $this->hasMany(User::class);
    }

}
