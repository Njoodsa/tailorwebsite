<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helper;
class Order extends Model
{
    protected $fillable = ['id_fabric', 'yards', 'price', 'number', 'Bill_id','addition_id','type','number_column'];


    public function bill()
    {
        return $this->belongsTo(Bill::class, 'Bill_id', 'id');
    }
    public function fabrics()
    {
        return $this->belongsTo(Fabrics::class, 'id_fabric','id');
    }
    public function additions1()
    {
        return $this->belongsTo(Addition::class, 'addition_id');
    }
    public function additions(){

        return $this->belongsToMany(Addition::class ,'orders_additions' ,'order_id','addition_id');
    }
    public function products()
    {
        return $this->belongsTo(Product::class,'id_fabric');
    }
    public function updateOrder($order)
    {
        Helper::check($this->update($order));
    }

}
