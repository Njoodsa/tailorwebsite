<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'price', 'note' , 'unit', 'section_id','barcode_number'];
    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function invoice_items(){
        return $this->hasMany(Order::class);
    }
    public function order(){
        return $this->hasMany(Order::class);
    }
}
