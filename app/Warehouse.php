<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{

    protected $fillable = ['name', 'cell_phone', 'phone', 'fax', 'mailbox', 'note', 'address'];

    public function branch()
    {
        return $this->hasMany(Branch::class,'warehouses_id');
    }

    public function fabrics(){

        return $this->hasMany(Fabrics::class , 'warehouses_fabrics','warehouses_id' ,'fabrics_id' );
    }

    public function warehouses_fabric(){
        return $this->hasMany(warehouses_fabric::class);
    }

    public function invoice_items(){
        return $this->hasMany(Invoice_items::class);
    }


}
