<?php

namespace App\Policies;

use App\User;
use App\Order_Information;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the order_ information.
     *
     * @param  \App\User  $user
     * @param  \App\Order_Information  $orderInformation
     * @return mixed
     */
    public function view(User $user, Order_Information $orderInformation)
    {
        //
    }

    /**
     * Determine whether the user can create order_ informations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the order_ information.
     *
     * @param  \App\User  $user
     * @param  \App\Order_Information  $orderInformation
     * @return mixed
     */
    public function update(User $user, Order_Information $orderInformation)
    {
        //
    }

    /**
     * Determine whether the user can delete the order_ information.
     *
     * @param  \App\User  $user
     * @param  \App\Order_Information  $orderInformation
     * @return mixed
     */
    public function delete(User $user, Order_Information $orderInformation)
    {
        //
    }

    /**
     * Determine whether the user can restore the order_ information.
     *
     * @param  \App\User  $user
     * @param  \App\Order_Information  $orderInformation
     * @return mixed
     */
    public function restore(User $user, Order_Information $orderInformation)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the order_ information.
     *
     * @param  \App\User  $user
     * @param  \App\Order_Information  $orderInformation
     * @return mixed
     */
    public function forceDelete(User $user, Order_Information $orderInformation)
    {
        //
    }
}
