<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Employee extends Model
{


    public function order()
    {
        return $this->hasMany(Order::class);
    }
    public function user()
    {
        return $this->hasOne(User::class,'employee_id');
    }

    public static function findBy($conditions=[])
    {
        return Employee::where($conditions);
    }

    public static function modify($conditions=[],$args=[]){
        return Employee::where($conditions)->update($args);
    }
    public static function add($args=[])
    {
        return Employee::create($args);
    }
    protected $fillable =['phone_number','name','level'];



}
