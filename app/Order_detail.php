<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    protected $fillable = ['id_design', 'button_neck', 'order_Information_id', 'id_sleeve', 'id_button', 'id_neck', 'id_pockets',
        'collar_detail', 'detail_pocket', 'button_detail', 'state', 'id_fabric', 'measurement_id', 'number_column'];
    //  -- STATE --  //

    // 1 the order pending
    // 2 mean the order have measurement but need review
    // 5  mean the order cancel
    // 3 order in work
    // 4 mean the order is done and waite customer to receive it
    // 6 mean the order is deliver
    // 7  product  incomplete

    public function design()
    {
        return $this->belongsTo(Design::class, 'id_design')->withDefault(function () {
            return new design();
        });;
    }

    public function order_Information()
    {
        return $this->belongsTo(Order_Information::class, 'order_Information_id', 'id');
    }

    public function neck()
    {
        return $this->belongsTo(Neck::class, 'id_neck')->withDefault(function () {
            return new neck();
        });;
    }


    public function fabric()
    {
        return $this->belongsTo(Fabrics::class, 'id_fabric')->withDefault(function () {
            return new Fabrics();
        });
    }


    public function pocket()
    {
        return $this->belongsTo(Pocket::class, 'id_pockets')->withDefault(function () {
            return new pocket();
        });
    }

    public function sleeve()
    {
        return $this->belongsTo(Sleeve::class, 'id_sleeve')->withDefault(function () {
            return new sleeve();
        });
    }

    public function button()
    {
        return $this->belongsTo(Button::class, 'id_button')->withDefault(function () {
            return new button();
        });
    }


    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function measurement()
    {
        return $this->belongsTo(Measurement_order::class, 'measurement_id');
    }


}
