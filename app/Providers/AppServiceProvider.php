<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; //Import Schema
use Illuminate\Support\Facades\Input;
use App\MobileNumbers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); //Solved by increasing StringLength
        \View::share('urlAWS', 'https://mawan.s3.eu-west-2.amazonaws.com');
        \Validator::extend('arabic_numbers', function ($attributes, $value, $parameters, $validation)  {
            $arabic_eastern = array('٠','١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
            $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
            $test =  str_replace($arabic_eastern, $arabic_western, $value);
                    if(is_array($test)) {
                        foreach ($test as $number) {
                            if (!is_numeric($number)) {
                                return false;
                            }
                        }
                    }elseif(!is_numeric($test))
                    {
                        return false;
                    }

            return $test;
        });
        Auth::viaRequest('custom-token', function ($request) {
      return MobileNumbers::where('api_token', $request->api_token)->first();
  });
    }

    public function test(){
        $data = warehouses_fabric::get();
        return view('layouts.SidBar', compact('data'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
