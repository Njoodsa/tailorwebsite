<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable =['name','lan','lat','phone','date_time','state','id_customer'];
    protected $dates = ['date_time'];
    public function mobileNumbers()
    {
        return $this->belongsTo(MobileNumbers::class,'id_customer');
    }

}
