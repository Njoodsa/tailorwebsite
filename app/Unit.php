<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    //
    protected $fillable = ['name'];

    public function fabrics(){


        return $this->belongsToMany(Fabrics::class , 'fabrics_units'  , 'fabrics_id', 'unit_id' );
    }

    public function Fabrics_units(){

        return $this->belongsToMany(Fabrics_unit::class , 'Fabrics_units' , 'fabrics_id' );
    }


}
