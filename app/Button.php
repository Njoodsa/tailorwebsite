<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
class Button extends Model
{
    //

    protected $fillable = ['name','note'];

    public static function cache(){
        return Cache::rememberForever('Button', function() {
            return static::get();
        });
    }
}
