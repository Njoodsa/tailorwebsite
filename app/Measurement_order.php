<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use App\Http\Helper;

class Measurement_order extends Model
{
    protected $fillable = ['Length', 'ShlR', 'ShlL', 'Slv_w', 'Chest', 'Waist', 'Bottom', 'CollarGalab', 'FrontLonger',
        'FakeButton', 'ElbowCuffs', 'Cuffs', 'ElbowSlvSada', 'Sada', 'Bar', 'SIDE', 'MIDDLE', 'Back',
        'CollarSada', 'Shl', 'SnpIN', 'SlvSada', 'note', 'parent_id',
        'HIPS', 'Armhole', 'FrontChest', 'id_cutter','Shafa121','Elpw','Cuff57'];

    public function order_detail()
    {
        return $this->hasMany(Order_detail::class, 'measurement_id');
    }

    public function cutters()
    {
        return $this->belongsTo(Employee_data::class, 'id_cutter');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d ');

    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d ');
    }

    public function updateMeasurement_order()
    {
        return $this->update(['Length' => request('Length'), 'ShlR' => request('ShlR')
            , 'ShlL' => request('ShlL'), 'Slv_w' => request('Slv_w'), 'Chest' => request('Chest')
            , 'Waist' => request('Waist'), 'Bottom' => request('Bottom'), 'CollarGalab' => request('CollarGalab'),
            'FrontLonger' => request('FrontLonger'), 'FakeButton' => request('FakeButton'), 'Cuffs' => request('Cuffs'),
            'HIPS' => request('HIPS'),
            'ElbowSlvSada' => request('ElbowSlvSada'), 'Sada' => request('Sada'), 'Bar' => request('Bar')
            , 'SIDE' => request('SIDE'), 'MIDDLE' => request('MIDDLE'), 'Back' => request('Back'),
            'CollarSada' => request('CollarSada')
            , 'CollarSada' => request('CollarSada'), 'Shl' => request('Shl'), 'SnpIN' => request('SnpIN'),
            'note' => request('note'), 'SlvSada' => request('SlvSada'), 'Armhole' => request('Armhole'),
            'FrontChest' => request('FrontChest'),'Shafa121'=> request('Shafa121'),'Elpw' => request('Elpw')
            ,'Cuff57' => request('Cuff57')
        ]);

    }



}
