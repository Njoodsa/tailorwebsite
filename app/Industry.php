<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
  
    protected $fillable = ['name','note'];

    public function fabrics(){
        return $this->hasMany(Fabrics::class ,'industries_id');
    }
}
