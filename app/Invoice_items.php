<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice_items extends Model
{
    protected $fillable = ['id_fabric' ,'number','price','supplier_invoices_id','warehouses_id','number'];

    public function fabrics()
    {
        return $this->belongsTo(Fabrics::class, 'id_fabric');
    }
    public function product()
    {
        return $this->belongsTo(Product::class, 'id_fabric');
    }
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouses_id');
    }
    public function supplier_invoices()
    {
        return $this->belongsTo(supplier_invoice::class,'supplier_invoices_id');
    }


}


