<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
class Order_Information extends Model
{
    protected $fillable = [ 'id_employees' , 'type','id_tailor' ,'id_customer' ,'state','Taking_measurements','id_bill'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            $query->id_employees = auth()->id();
        });
    }
    public static function stateOrder($state ,$type){
          return static::where([['state',$state],['type',$type]])->with(['customer.mobile','bill.order','bill.order.fabrics'])->latest()->paginate(100);
      }
    public static function findOrder($state ,$type,$id){
        return static::where([['state',$state],['type',$type]])->findOrFail($id);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'id_customer');
    }
    public function tailor()
    {
        return $this->belongsTo(Employee_data::class,'id_tailor');
    }
    public function employee()
    {
        return $this->belongsTo(User::class,'id_employees');
    }
    public function take_measurements()
    {
        return $this->belongsTo(Employee_data::class,'Taking_measurements');
    }

    public function bill()
    {
        return $this->belongsTo(Bill::class, 'id_bill');
    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d ');
    }
    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');

    }
    public function order_detail(){
        return $this->hasMany(Order_detail::class,'order_Information_id' );
    }

    public function updateOrderInformation($order)
    {
         $this->update($order);
    }
    public function updateDetail_order($i)
    {
           $this->order_detail()->update(['id_design' => request('id_design')[$i], 'id_sleeve' => request('id_sleeve')[$i],
            'id_button' => request('id_button')[$i], 'id_neck' => request('id_neck')[$i], 'id_pockets' => request('id_pockets')[$i],
            'collar_detail' => request('collar_detail')[$i], 'detail_pocket' => request('detail_pocket')[$i],
            'button_detail' => request('button_detail')[$i],
            'id_fabric' => request('id_fabric_to_order')[$i], 'number_column' => request('number_detail')[$i],
            'button_neck' => request('button_neck')[$i]]);
    }
    public function createDetail_order($i,$id_measurement)
    {
        return $this->order_detail()->create(['id_design' => request('id_design')[$i], 'id_sleeve' => request('id_sleeve')[$i],
            'id_button' => request('id_button')[$i], 'id_neck' => request('id_neck')[$i], 'id_pockets' => request('id_pockets')[$i],
            'collar_detail' => request('collar_detail')[$i], 'detail_pocket' => request('detail_pocket')[$i],
            'button_detail' => request('button_detail')[$i], 'id_fabric' => request('id_fabric_to_order')[$i]
            , 'number_column' => request('number_detail')[$i], 'button_neck' => request('button_neck')[$i],'measurement_id'=>$id_measurement]);
    }

    public function updateDetail_order_with_measurement($i,$id_measurement)
    {
        $this->order_detail()->update(['id_design' => request('id_design')[$i], 'id_sleeve' => request('id_sleeve')[$i],
            'id_button' => request('id_button')[$i], 'id_neck' => request('id_neck')[$i], 'id_pockets' => request('id_pockets')[$i],
            'collar_detail' => request('collar_detail')[$i], 'detail_pocket' => request('detail_pocket')[$i],
            'button_detail' => request('button_detail')[$i],
            'id_fabric' => request('id_fabric_to_order')[$i], 'number_column' => request('number_detail')[$i],
            'button_neck' => request('button_neck')[$i],'measurement_id'=>$id_measurement]);
    }


}
