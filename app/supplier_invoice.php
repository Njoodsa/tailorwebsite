<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class supplier_invoice extends Model
{
    protected $fillable = ['supplier_id', 'price', 'state', 'discount', 'price', 'payment_method_id', 'paid_up'
        , 'Residual','date','type'];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
    public function payment_method()
    {
        return $this->belongsTo(Payment_method::class, 'payment_method_id');
    }

    public function invoice_items(){
        return $this->hasMany(Invoice_items::class ,'supplier_invoices_id');
    }
}
