<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
class Design extends Model
{


    protected $fillable = ['name','note' ,'has_bar_pocket'];

    public function order_details(){
        return $this->hasMany(Order_detail::class);
    }
    public static function cache(){
    return Cache::rememberForever('Design', function() {
        return static::get();
    });
}
}
