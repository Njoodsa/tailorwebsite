<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
class Supplier extends Model
{
    protected $fillable = ['name', 'phone', 'email', 'address', ' note', 'date'];

    public function purchase()
    {
        return $this->hasMany(Purchase::class);
    }

    public function supplier_invoice()
    {
        return $this->hasMany(supplier_invoice::class);
    }
    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');

    }
}
