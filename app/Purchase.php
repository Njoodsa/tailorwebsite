<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = ['supplier_id', 'price', 'qty', 'discount', 'finally_price','warehouses_fabrics_id'];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class,'supplier_id');
    }

    public function warehouses_fabric()
    {
        return $this->belongsTo(warehouses_fabric::class,'warehouses_fabrics_id');
    }

}
