<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
class Measurement extends Model
{
    protected $fillable = ['Length', 'ShlR', 'ShlL', 'Slv_w', 'Chest', 'Waist', 'Bottom', 'CollarGalab', 'FrontLonger',
        'FakeButton', 'ElbowCuffs', 'Cuffs','ElbowSlvSada', 'Sada', 'Bar', 'SIDE', 'MIDDLE', 'Back',
        'CollarSada', 'Shl', 'SnpIN', 'SlvSada','note','customer_id' ,'parent_id' ,
        'HIPS','Armhole' ,'FrontChest','id_cutter'];

    public function customers()
    {
        return $this->belongsTo(Customer::class,'customer_id','id');
    }

    public function cutters()
    {
        return $this->belongsTo(Employee_data::class,'id_cutter','id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d ');

    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d ');
    }

}