<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $fillable = ['name', 'branches_id'];

    public function branches()
    {
        return $this->belongsTo(Branch::class);
    }

}
