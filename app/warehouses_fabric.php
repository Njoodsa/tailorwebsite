<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use App\Services\PayUService\Exception;

class warehouses_fabric extends Model
{

    protected $fillable = ['warehouses_id', 'fabrics_id', 'demand_limit', 'current_balance', 'current_price', 'type'];

    public function __construct()
    {
    return session()->put('userWarehouses',  \Auth::user()->branch->Warehouses->id);
    }

    public function fabrics()
    {
        return $this->belongsTo(Fabrics::class, 'fabrics_id');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'fabrics_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouses_id');
    }

    public function purchase()
    {
        return $this->hasMany(Purchase::class);
    }

    public static function cache()
    {
        return Cache::rememberForever('warehouses_fabric', function () {
            if (\Auth::user()->id == 1 && \Auth::user()->branches_id == null) {
                return 'يجب اختيار الفرع ';
            } else {
                return static::select('fabrics_id', 'current_balance')
                    ->where('warehouses_id', session()->get('userWarehouses'))->get();
            }
        });
    }

    public static function warehousesToChose($p_type ,$p_model)
    {
        return static::select('fabrics_id', 'current_balance', 'demand_limit')->whereHas($p_model , function($q){
            $q->where('disable','<>',1);
    })->with($p_model)->where([['warehouses_id', session()->get('userWarehouses')], ['type', $p_type]])->get();
    }


    public static function checkWarehouses($id_fabric, $oldFabric, $number, $oldQuantity, $type)
    {

        if ($id_fabric != $oldFabric) {
            self::incrementWarehouses($oldFabric, $type,$oldQuantity);
            return self::decrementWarehouses($id_fabric,$type, $number); // use return to check if decrementWarehouses
        }                                                                //   return error
        $checkChange = $number - $oldQuantity;
         if($checkChange != 0){// not change

             return self::decrementWarehouses($id_fabric, $type, $checkChange);
         }
    }                                                                                                    //Quantity

    public static function incrementWarehouses($id_fabric, $type, $number)
    {
        $add_current_balance = static::where([['fabrics_id', $id_fabric], ['warehouses_id', session()->get('userWarehouses')], ['type', $type]])->first();
        $add_current_balance->increment('current_balance', $number);
    }


    public static function decrementWarehouses($id_fabric, $type, $checkChange)
    {
        $item = warehouses_fabric::where([['fabrics_id', $id_fabric], ['warehouses_id', session()->get('userWarehouses')]
            , ['type', $type]])->first();
        return ($item->current_balance < $checkChange) ? 'error' : $item->decrement('current_balance', $checkChange);
    }

    public static function isEmpty($number, $yeardh,$id_fabric){
        if(!empty($yeardh)){
            $quantity  = $number * $yeardh;
            warehouses_fabric::incrementWarehouses($id_fabric,'fabirc',$quantity);
        }
    }
}
