<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MobileNumbers extends Authenticatable
{
    use Notifiable;
    //
    protected $fillable = ['id' , 'phone','api_token','password'];

    protected $hidden = ['password'];

    public function customer()
    {
        return $this->hasMany(Customer::class,'MobileNumbers_id');
    }
    public function reservations()
    {
        return $this->hasMany(Reservation::class,'id_customer');
    }

    public function addCustomer($data)
    {
        return $this->customer()->create($data);
    }

    //---------- METHOD TO AUTH API ------//
    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();
        return $this->api_token;
    }
}
