<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobile extends Model
{
    //
    public function measurements(){
        return $this->hasOne(Measurements::class,'id_mobile');
    }

    protected $fillable = ['number'];
 //   public static function add($args=[])
   // {
     //   return Mobile::create($args);
 //   }

    public static function findBy($conditions=[])
    {
        return Mobile::where($conditions);
    }
}
