<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addition extends Model
{
    protected $fillable = ['name','price'];

    public function order(){
        return $this->hasMany(Order::class);
    }

    public function orders(){

        return $this->hasMany(Order::class , 'orders_additions' ,'order_id', 'addition_id');
    }

}
