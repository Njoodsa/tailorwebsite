<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
class Employee_data extends Model
{

    use SoftDeletes;
    protected $fillable = ['name', 'phone','address', 'note', 'employee_type_id'];

    public function employee_type()
    {
        return $this->belongsTo(Employee_type::class);
    }
    public function measurement(){
        return $this->hasMany(Measurement::class,'id_cutter');
    }

    public function users(){
        return $this->belongsTo(User::class);
    }
    public static function cache_tailor(){
        return Cache::rememberForever('cache_tailors', function() {
            return static::where('employee_type_id', 1)->get();
        });
    }
    public static function cache_design(){
        return Cache::rememberForever('cache_designs', function() {
            return static::where('employee_type_id',4)->get();
        });
    }
    public static function cache_cutter(){
        return Cache::rememberForever('cache_cutters', function() {
            return static::where('employee_type_id', 2)->get();
        });
    }


}
