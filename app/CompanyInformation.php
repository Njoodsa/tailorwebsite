<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class companyInformation extends Model
{
    protected $fillable = ['location', 'work_days' , 'work_hours', 'phone' , 'fax' , 'email' , 'cell_phone'];
}
