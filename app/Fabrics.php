<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
class Fabrics extends Model
{
    //this table to product and fabrics
    //type if product or fabrics
    // product do not has fabric_width

    protected $fillable = ['name','note', 'industries_id','lowest_price'  ,  'barcode_number','fabric_width','disable'];

    public static function cache()
    {
        return Cache::rememberForever('Fabric', function () {
            return static::get();
        });
    }
    public function industries(){
        return $this->belongsTo(Industry::class);
    }
    public function order_details(){
        return $this->hasMany(Order_detail::class);
    }

    public function units(){

        return $this->belongsToMany(Unit::class , 'fabrics_units' , 'fabrics_id','unit_id' );
    }
    public function warehouses(){

        return $this->hasMany(Warehouse::class , 'warehouses_fabrics' ,'warehouses_id', 'fabrics_id');
    }

    public function Fabrics_units(){

        return $this->belongsToMany(Fabrics_unit::class , 'Fabrics_units' , 'fabrics_id' );
    }

    public function Fabrics_unit(){

        return $this->hasMany(Fabrics_unit::class  );
    }

    public function warehouses_fabric(){
        return $this->hasMany(warehouses_fabric::class);
    }
    public function order(){
        return $this->hasMany(Order::class,'id_fabric');
    }

    public function invoice_items(){
        return $this->hasMany(Order::class);
    }



}
