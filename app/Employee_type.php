<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_type extends Model
{
    protected $fillable = ['name' , 'id'];

    public function employee_data()
    {
        return $this->hasMany(Employee_data::class ,'employee_type_id');
    }
}
