<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
class Customer extends Model
{


    protected $fillable = ['name' , 'phone','MobileNumbers_id' ];

    public function measurement(){

        return $this->hasMany(Measurement::class);

    }
    public function measurement_order(){
        return $this->hasMany(Measurement_order::class);
    }

    public function orders(){
        return $this->hasMany(Order_Information::class,'id_customer');
    }
    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');

    }

    public function mobile(){
        return $this->belongsTo(MobileNumbers::class , 'MobileNumbers_id');
    }
    public function insertMeasurement(Customer $id , $order){
        $this->measurement()->create($order);
    }


}
