<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\HasApiTokens;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password' ,'employee_datas_id','branches_id','access'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function branch(){

      return $this->belongsTo(Branch::class ,'branches_id');

    }


    public function employee_datas(){
        return $this->belongsTo(Employee_data::class);
    }
    public function order_detail(){
        return $this->hasMany(Order_detail::class);
    }

   

    public function setPasswordAttribute($password) // B C R Y P T  Password
    {
        $this->attributes['password'] = bcrypt($password);
    }

    //---------- METHOD TO AUTH API ------//
    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }

}
