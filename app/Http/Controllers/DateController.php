<?php

namespace App\Http\Controllers;

use App\Date;
use App\Http\Helper;
use Illuminate\Http\Request;
use App\Hour;
use Carbon\Carbon;
use App\Http\Requests\DateRequest;
use App\Reservation;
class DateController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index()
    {
        $dates = Date::all();
        return view('date.index', compact('dates'));
    }

    /**
    * @return array
    * get the times blocked or reserved to disable it.
    *
    **/
    public function AjaxGetHour($date)
    {
        $hourBlocked  =  Date::whereDate('date',$date)->pluck('date')
                          ->map(function ($date) {
                          return $date->format('g:i');
          });
        $hourReserved =  Reservation::whereDate('date_time',$date)->pluck('date_time')
                          ->map(function ($date) {
                          return $date->format('g:i');
         });

        return $hourBlocked->merge($hourReserved);

    }

    /**
    *
    * disable time or date .
    *
    **/
    public function create()
    {
        $hours =  Hour::all()->pluck('hour');
        $date  =  Date::where('type',1)->pluck('date');
        return view('date.createe', compact('hours','date'));
    }
    /**
    *
    * disable day or time.
    *
    **/
    public function insert(DateRequest $request)
    {
         $date = Date::transformDate();
         Helper::check(Date::create(request(['type'])+['date'=>$date]));
         return back();
    }

    /**
    *
    * Availability of a day or time.
    *
    **/
    public function destroy(Date $Date)
    {
        $Date->delete();
        return back();
    }

}
