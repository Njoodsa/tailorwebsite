<?php

namespace App\Http\Controllers;
use App\CompanyInformation;
use App\Http\Requests\companyInformationRequest;

class CompanyInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = CompanyInformation::first();
        return view('welcome', compact('data'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\companyInformation  $companyInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyInformation $companyInformation)
    {
        return view('CompanyInformation.edit', ['data' =>$companyInformation ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\companyInformation  $companyInformation
     * @return \Illuminate\Http\Response
     */
    public function update(companyInformationRequest $request, CompanyInformation $companyInformation)
    {
        flash_if_success_or_fail($companyInformation->update($request->all()));
        return back();
    }

}
