<?php

namespace App\Http\Controllers;

use App\Http\Helper;
use App\Industry;
use Illuminate\Http\Request;
use App\Http\Requests\IndustriesRequest;

class IndustriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = Industry::get();
        return view('industries.view', compact('data'));
    }

    public function create()
    {
        return view('industries.create');
    }

    public function insert(IndustriesRequest $request)
    {
        $data = Industry::create($request->all());
        if ($data == true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }

    public function edit($id)
    {
        $data = Industry::findOrFail($id);
        return view('industries.edit', compact('data'));
    }

    public function update(IndustriesRequest $request, $id)
    {
        $data = Industry::findOrFail($id);
        if ($data->update($request->all())) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }

}
