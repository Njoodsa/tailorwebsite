<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Inventory_balance;
use App\Fabrics;
use App\Warehouse;
use App\Fabrics_unit;
use App\Unit;
use App\Http\Requests\inventory_balancesRequest;
use App\warehouses_fabric;
use App\Http\Helper;
use App\supplier_invoice;
use DB;
use App\Invoice_items;
use Carbon\Carbon;

class Warehouses_fabricsController extends Controller
{

    public function index()
    {
        $data = warehouses_fabric::whereType('fabirc')->whereHas('fabrics' , function($q){
            $q->where('disable','<>',1);
        })->with('fabrics','warehouse')->get();
        return view('inventory_balance.view', compact('data'));
    }
    public function index_product()
    {
        $data = warehouses_fabric::whereType('product')->get();
        return view('inventory_balance_product.view', compact('data'));
    }

    public function GetFabrics_create($fabrics)
    {
        $FinedTagah   = Fabrics_unit::select('Equation')->where([['fabrics_id', $fabrics], ['unit_id', 1]])->first();
        $FinedYardah  = Fabrics_unit::where([['fabrics_id', $fabrics], ['unit_id', 2]])->first();
        if ($FinedTagah == null || $FinedYardah == null) {
             return $multible = [""];
        } else {
            $multible = $FinedTagah->Equation;
        }

        return array('Equation' => $multible, 'price' => $FinedYardah->price);
    }


    public function edit($id)
    {
        $data = warehouses_fabric::findOrFail($id);
        $id_fabric= $data->fabrics_id;

        // get all Purchase where type is fabirc and it Confirmed
        $Purchase = Invoice_items::select('number','created_at','supplier_invoices_id')
            ->where('id_fabric',$id_fabric)->whereHas('supplier_invoices', function($q)
        { $q->where([['type', 'fabirc'],['state',1]]);} )->get();

        //get all sales
        $bills = collect(Order::select('id','created_at','Bill_id','yards','number')->where('id_fabric' ,$id_fabric)
            ->whereHas('bill.order_information', function($q)
        { $q->where([['type', 'fabirc']]); } )->whereHas('bill', function($q)
            { $q->whereIn('state',[1,0]); } )->with('bill.order_information')->get());
        //merge sales and Purchase to order by asc
        $sort = ["created_at" => "asc"];
        $result = $bills->merge($Purchase)->sortBy($sort)->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('Y');
        });

       $total_sales_yeardh= $bills->sum(function($t){
            return $t->number * $t->yards;
        });

        return view('inventory_balance.edit', compact('bills','data','Purchase','result','total_sales_yeardh'));
    }
    public function edit_product($id)
    {

        $data = warehouses_fabric::findOrFail($id);
        $id_fabric= $data->fabrics_id;
        $Purchase = Invoice_items::select('number','created_at','supplier_invoices_id')->where('id_fabric',$data->fabrics_id)->whereHas('supplier_invoices', function($q)
        { $q->where([['type', 'product'],['state',1]]); } )->get();

        $bills = collect(Order::select('id','created_at','Bill_id','number')->where('id_fabric' ,$id_fabric)
            ->whereHas('bill.order_information', function($q)
            { $q->where([['type', 'product']]); } )->whereHas('bill', function($q)
            { $q->whereIn('state',[1,0]); } )->with('bill.order_information')->get());

        $sort = ["created_at" => "asc"];
        $result = $bills->merge($Purchase)->sortBy($sort)->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('Y');
        });

        return view('inventory_balance_product.edit', compact('data','result'));

    }

    public function update(inventory_balancesRequest $request, $id)
    {
        $data = warehouses_fabric::findOrFail($id);

        $data->demand_limit = $request->input('demand_limit');
        if ($data->save() == true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();

    }

}
