<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use App\User;
use App\Http\Helper;
use App\Branch;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    //

    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index()
    {
        $Branches = Branch::get();
        return view(' Admin.index', compact('Branches'));
        return back();


    }

    public function update(AdminRequest $request, $id)
    {
        $user = User::findOrFail($id);
        if (empty($request->input('password'))) {
            $new_email = $request->input('email');
            $user->email = $new_email;
            $user->branches_id = $request->input('branches_id');

            $user->save();
        } else {

            $user->update($request->all());
        }
        if ($user) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }
}
