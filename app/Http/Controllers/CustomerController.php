<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Helper;
use App\Measurement;
use App\Measurement_order;
use DB;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\MeasurementsRequest;
use App\MobileNumbers;
use Illuminate\Http\Request;
use App\Employee_data;
use Excel;
use App\Order_detail;

use App\Order_Information;
class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function downloadExcel($type)
    {
        $data = Measurement::get();
        $array = array();
        foreach ($data as $item) {
            $array[] = [
                'id' => $item->customers->id,
                'Name' => $item->customers->name,
                'Mobile number' => $item->customers->mobile->phone,
                'Note' => $item->customers->note,
                'FORNT LONGER ' => $item->FrontLonger,
                'LENGTH' => $item->Length,
                'SNP IN ' => $item->SnpIN,
                'FAKE BUTTON' => $item->FakeButton,
                'SHL' => $item->Shl,
                'L' => $item->ShlL,
                'R' => $item->ShlR,
                'SLV W /CUFFS' => $item->Slv_w,
                'CUFFS' => $item->Cuffs,
                'ELBOW' => $item->ElbowCuffs,
                'SLV SADA' => $item->SlvSada,
                'SADA' => $item->Sada,
                'ELBOW' => $item->ElbowSlvSada,
                'Armhole' => $item->Armhole,
                'Front chest' => $item->FrontChest,
                'CHEST' => $item->Chest,
                'BOTTOM' => $item->Bottom,
                'BAR' => $item->Bar,
                'WAIST' => $item->Waist,
                'BACK' => $item->Back,
                'MIDDLE' => $item->MIDDLE,
                'SIDE' => $item->SIDE,
                'HIPS' => $item->HIPS,
                'COLLAR SADA' => $item->CollarSada,
                'COLLAR GALAB' => $item->CollarGalab,
                'Take measurements' => $item->cutters->name,
            ];
        }

        return Excel::create('Customer', function ($excel) use ($array) {
            $excel->sheet('mySheet', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->download($type);

        return back();
    }

    public function index()
    {

        $data = Customer::select(['name','id','created_at','MobileNumbers_id'])->with('mobile')->paginate(100);

        return view('customer.view', compact('data'));
    }

    public function create()
    {
        $cutters = Employee_data::cache_cutter();
        return view('customer.create', compact('data', 'cutters'));
    }

    public function insert(CustomerRequest $request)
    {
        return DB::transaction(function () use ($request) {

            $phone = $request->input('phone');
            $name = $request->input('name');
            if ($request->input('select') == 'old') {// if the number is exist
                $number = MobileNumbers::select('id')->where('phone', $phone)->first();
                if ($number) {
                    $number_id = $number->id;
                    $find_customer = Customer::where([['MobileNumbers_id', $number_id], ['name', $name]])->first();
                    if ($find_customer) {
                        return back()->withErrors(['العميل موجود مسبقاً ']);
                    }
                } else { // if the number of customer does not exist
                    return back()->withErrors(['الرقم غير موجود مسبقاً ']);
                }
            } elseif ($request->input('select') == 'new') { // if customer is new
                $this->validate($request, [
                    'phone' => 'unique:mobile_numbers,phone'], ['unique' => 'الرقم موجود مسبقاً'
                ]);
                $create_number = MobileNumbers::create([
                    'phone' => $phone,
                ]);
                $number_id = $create_number->id;
            }
            $data_customers = [
                'name' => $name,
                'MobileNumbers_id' => $number_id,
                'note' => $request->input('note'),
            ];
            $id_customer = Customer::create($data_customers);
            //$this->addMeasurment($myRequest, $id_customer->id);

            if ($id_customer) {
                Helper::true();
            } else {
                Helper::false();
            }
            return back();
            // return redirect('newMeasurement/'.$id_customer->id);
        });

    }

  /*  public function addMeasurment($myRequest, $id)
    {

        $data_measurement = [
            'Length' => $myRequest->input('Length'),//
            'ShlR' => $myRequest->input('ShlR'),//
            'ShlL' => $myRequest->input('ShlL'),//
            'Slv_w' => $myRequest->input('Slv_w'),//
            'Chest' => $myRequest->input('Chest'),//
            'Waist' => $myRequest->input('Waist'),//
            'Bottom' => $myRequest->input('Bottom'),//
            'CollarGalab' => $myRequest->input('CollarGalab'),//
            'FrontLonger' => $myRequest->input('FrontLonger'),//
            'FakeButton' => $myRequest->input('FakeButton'),//
            'ElbowCuffs' => 1,//
            'Cuffs' => $myRequest->input('Cuffs'),//
            'ElbowSlvSada' => $myRequest->input('ElbowSlvSada'),//
            'Sada' => $myRequest->input('Sada'),//
            'Bar' => $myRequest->input('Bar'),//
            'SIDE' => $myRequest->input('SIDE'),//
            'MIDDLE' => $myRequest->input('MIDDLE'),//
            'Back' => $myRequest->input('Back'),//
            'CollarSada' => $myRequest->input('CollarSada'),//
            'Shl' => $myRequest->input('Shl'),//
            'SnpIN' => $myRequest->input('SnpIN'),//
            'HIPS' => $myRequest->input('HIPS'),//
            'note' => $myRequest->input('note'),//
            'SlvSada' => $myRequest->input('SlvSada'),//
            'customer_id' => $id,
            'Armhole' => $myRequest->input('Armhole'),
            'FrontChest' => $myRequest->input('FrontChest'),
            'id_cutter' => $myRequest->input('id_cutter')
        ];
        Measurement::create($data_measurement);
    }
*/

    public function edit($id)
    {
        // get id last measurement from order to view it
        $measurement_id = Order_detail::select('measurement_id')->whereHas('Order_Information', function ($q) use ($id) {
            return $q->whereId_customer($id);
        })->latest()->first();
        $measurement = null; // when customer do not have order yet
        if ($measurement_id)
            $measurement = Measurement_order::whereId($measurement_id->measurement_id)->first();
        $data = Customer::FindOrFail($id);

        $cutters = Employee_data::cache_cutter();
        return view('customer.edit', compact('data', 'cutters', 'measurement'));
    }

    public function update_information(CustomerRequest $request, $id)
    {
      $data = Customer::FindOrFail($id);
      if (request('select') == 'old') { // if the number is exist
          $phone = request('phone');
          $number = MobileNumbers::select('id')->where('phone', $phone)->first();
          if ($number) {
              $data->MobileNumbers_id = $number->id;
          }else {
              return back()->withErrors(['رقم الجوال غي موجود']);
            }
       }else{
        $phone  = $data->MobileNumbers_id;
        $number = MobileNumbers::FindOrFail($phone);
        $this->validate($request, [
            'phone' => 'unique:mobile_numbers,phone,' . $phone,
        ]);
        $number->phone = $request->input('phone');
        $number->save();
      }
      $data->name = $request->input('name');
      $data->note = $request->input('note');
      if ($data->save()) {
        Helper::true();
      }else {
        Helper::false();
      }
      return back();
    }

   /* public function update_measurement(MeasurementsRequest $request, $id)
    {

        // ID measurement = id_meas
        $measurement_update = Measurement::findOrfail($id);
        $measurement_customer = [

            'Length' => $request->input('Length'),//
            'ShlR' => $request->input('ShlR'),//
            'ShlL' => $request->input('ShlL'),//
            'Slv_w' => $request->input('Slv_w'),//
            'Chest' => $request->input('Chest'),//
            'Waist' => $request->input('Waist'),//
            'Bottom' => $request->input('Bottom'),//
            'HIPS' => $request->input('HIPS'),//
            'CollarGalab' => $request->input('CollarGalab'),//
            'FrontLonger' => $request->input('FrontLonger'),//
            'FakeButton' => $request->input('FakeButton'),//
            'ElbowCuffs' => 1,//
            'Cuffs' => $request->input('Cuffs'),//
            'ElbowSlvSada' => $request->input('ElbowSlvSada'),//
            'Sada' => $request->input('Sada'),//
            'Bar' => $request->input('Bar'),//
            'SIDE' => $request->input('SIDE'),//
            'MIDDLE' => $request->input('MIDDLE'),//
            'Back' => $request->input('Back'),//
            'CollarSada' => $request->input('CollarSada'),//
            'Shl' => $request->input('Shl'),//
            'SnpIN' => $request->input('SnpIN'),//
            'SlvSada' => $request->input('SlvSada'),
            'note' => $request->input('note'),//
            'Armhole' => $request->input('Armhole'),
            'FrontChest' => $request->input('FrontChest'),
            'id_cutter' => $request->input('id_cutter')

        ];

        $true = $measurement_update->update($measurement_customer);
        if ($true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }
*/
    public function search_name(Request $request)
    {
        $this->validate($request, [
            'search' => 'string|required'], ['string' => 'ادخل الاسم بشكل الصحيح '
        ]);
        $data = Customer::where('name', 'like', '%' . $request->input('search') . '%')->get();
        return view('Customer.search_name', compact('data'));
        // return view('Customer.view' ,compact('data'));

    }

    public function autocomplete(Request $request)
    {
        $data = Customer::where('name', 'like', '%' . $request->input('query') . '%')->get();
        return $data;
    }

}
