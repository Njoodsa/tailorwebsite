<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fabrics;
use App\Warehouse;
use App\supplier_invoice;
use DB;
class PurchasesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $data = DB::table('supplier_invoices')
            ->select(DB::raw('YEAR(date) as year'), DB::raw('sum(price) as price'))
            ->where('state', 1)
            ->groupBy(DB::raw('YEAR(date)'))
            ->get();
        $product = DB::table('supplier_invoices')
            ->select( DB::raw('sum(price) as product'))
            ->where([['state', 1],['type','product']])
            ->first();
        $fabirc = DB::table('supplier_invoices')
            ->select( DB::raw('sum(price) as fabirc'))
            ->where([['state', 1],['type','fabirc']])
            ->first();

        $total = DB::table('supplier_invoices')
            ->select( DB::raw('sum(price) as TotalPrice'))
            ->where('state', 1)
            ->first();


        return view('Purchases.view', compact('data' ,'total','product','fabirc'));
    }

    public function purchases($year){
        $data  = supplier_invoice::with('supplier')->whereYear('date', $year)->get();
        $total =$data->sum('price');
        return view('Purchases.AllPurchases', compact('data','year','total'));
    }

}
