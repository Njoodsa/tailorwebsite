<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Addition;
use App\Http\Requests\AdditionRequest;
use App\Http\Helper;
class AdditionController extends Controller
{
    public function index()
    {
        $data =  Addition::get();
        return view('addition.view',compact('data'));
    }
    public function create()
    {
        return view('addition.create');
    }

    public function insert(AdditionRequest $request)
    {
        Addition::create($request->all()) ?  Helper::true() :  Helper::false() ;
        return back();
    }

    public  function edit(Addition $data)
    {
        return view('addition.edit', compact('data'));
    }

    public function update(Addition $addition, AdditionRequest $request)
    {
        $addition->update($request->all()) ?   Helper::true() : Helper::false() ;
        return back();
    }
}
