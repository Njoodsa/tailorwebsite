<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MobileNumbers;
class MobileNumbersController extends Controller
{
    public function search(Request $request){
        $this->validate($request,[
            'search'=>'numeric|digits:10 |required'],['numeric'=>'ادخل الرقم بشكل الصحيح'
            ,'digits'=>'يجب ان يتكون الرقم من ١٠ ارقام '
        ]);
        $data = MobileNumbers::where('phone',$request->search)->first();
        if($data == null)
        {
            return back()->withErrors('هذا الرقم غير موجود ');
        }
        return view('Customer.search_customer' ,compact('data'));
    }
}
