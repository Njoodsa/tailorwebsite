<?php

namespace App\Http\Controllers;

use App\Http\Helper;
use Illuminate\Http\Request;
use App\Design;
use App\Http\Requests\DesignRequest;
use Illuminate\Support\Facades\Cache;
class designController extends Controller
{
    public function index()
    {
        $data =  Design::cache() ;
        return view('design.view', compact('data') );
    }

    public function create()
    {
        return view('design.create');
    }

    public function insert(DesignRequest $request)
    {
        request('has_bar_pocket') == 'on' ? $has_bar_pocket = 1 : $has_bar_pocket = 0 ;

        $data = new Design() ;
        $data->name = request('name');
        $data->note = $request->input('note');
        $data->has_bar_pocket = $has_bar_pocket;
        $data->save() ?  Helper::true() : Helper::false();
        Cache::forget('Design');
        Design::cache();
        return back();
    }

    public function edit(Design $data)
    {
        return view('design.edit', compact('data'));
    }

    public function update(DesignRequest $request, Design $design)
    {
        $request->input('has_bar_pocket') == 'on'  ?  $has_bar_pocket = 1 : $has_bar_pocket = 0 ;
        $design->name = $request->input('name');
        $design->note = $request->input('note');
        $design->has_bar_pocket = $has_bar_pocket;
        $design->save() ?  Helper::true() :  Helper::false() ;
        Cache::forget('Design');
        Design::cache();
        return back();
    }
}
