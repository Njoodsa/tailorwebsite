<?php

namespace App\Http\Controllers;
use App\Supplier;
use App\Http\Requests\SupplierRequest;
use App\Http\Helper;
class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Supplier::get();
        return view('supplier.view', compact('data'));
    }

    public function create()
    {
        return view('supplier.create');
    }

    public function insert(SupplierRequest $request)
    {
        $data = $request->all();
        if (Supplier::create($data)) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }

    public function edit($id)
    {
        $data = Supplier::FindOrFail($id);
        return view('supplier.edit', compact('data'));
    }

    public function update(SupplierRequest $request, $id)
    {
        $data = Supplier::FindOrFail($id);
        $update = [
            'name'=>$request->input('name'),
            'date'=>$request->input('date'),
            'note'=>$request->input('note'),
            'phone'=>$request->input('phone'),
            'email'=>$request->input('email'),
            'address'=>$request->input('address'),
        ];
        if ($data->update($update)) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }

}
