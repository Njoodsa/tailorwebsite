<?php
namespace App\Http\Controllers;

use App\Http\Helper;
use App\Measurement;
use App\Measurement_order;
use App\Order;
use App\Product;
use App\warehouses_fabric;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\ProductToOrderRequest;
use App\Http\Requests\MeasurementsRequest;
use App\Http\Requests\editOrderRequest;
use App\Customer;
use App\Button;
use App\Design;
use App\Discount;
use App\Neck;
use App\Pocket;
use App\Sleeve;
use App\Fabrics;
use App\Order_detail;
use App\Payment_method;
use App\Bill;
use DB;
use Illuminate\Http\Request;
use App\Order_Information;
use App\Http\Requests\AddMeasurmentRequest;
use App\Http\Requests\Measurement_orderRequest;
use Illuminate\Support\Facades\Cache;
use Spatie\ArrayToXml\ArrayToXml;
use App\Employee_data;
use View;
use Response;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Addition;
use DateTime;
use Barryvdh\Debugbar\Facade as Debugbar;
class Order_detailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
        $date = Carbon::now();
        $date_deliver = $date->addDays(14)->toDateString();
        $user = \Auth::user();
        $designs = Design::cache();
        $necks = Neck::cache();
        $pockets = Pocket::cache();
        $sleeves = Sleeve::cache();
        $discount = Discount::first();
        $buttons = Button::cache();
        $fabrics = warehouses_fabric::warehousesToChose('fabirc','fabrics');

        $additions = Addition::get();
        $payment_methods = Payment_method::cache();
        $data = Customer::with('measurement.cutters')->findOrFail($id);
        return view('order_detail.createe', compact('user','discount','designs','necks','pockets','sleeves','buttons', 'fabrics',
            'payment_methods','data','additions','date_deliver'));
    }

    public function checkStyle(Design $design)
    {
        return $design->has_bar_pocket;
    }

    public function getLowestPrice(Fabrics $fabrics) // when he enter the price of fabrics Then we check his lower price
    {
        return $fabrics->lowest_price;
    }

    public function getPriceAddition( $addition) // when he enter the price of fabrics Then we check his lower price
    {
        $addition_array= array_map('intval', explode(',', $addition)); //Convert the numbers into array
        return Addition::whereIn('id',$addition_array)->sum('price');
    }

    public function getProductPrice(Product $product)
    {
        return $product->price;
    }
    public function insert(OrderRequest $request, $id, Bill $bill)
    {
        if (request('type') == 'product') {
            $value = $this->insertProduct($id, $bill);
            if ($value == 'error') {
              return back()->withErrors('المنتج نفذ من المخزون ');
            }
            return redirect('view_product_unCompleted');
        } else {
            $id_order = $this->insertFabrics($bill, $id);
            return redirect('editOrder/' . $id_order);
        }
    }
    public function GetProductAJax()
    {
        $product = warehouses_fabric::warehousesToChose('product','products');
        return $product;
    }
    public function insertProduct($p_id, $p_bill)
    {
        return DB::transaction(function () use ($p_id, $p_bill)
        {

            $state = Helper::stateOrder(); // get state bill and order
            $p_bill = $p_bill->addBill(request(['payment_method_id','discount', 'paid_up', 'Residual', 'end_date','paid_up_card']) + ['state'=> $state['state_bill'], 'price' => request('total_thop_cost')]); // insert bill

            for ($i = 0; $i < count(request('id_fabric')); $i++) {
                $id_fabric = request('id_fabric')[$i];
                $number = request('number')[$i];
                $p_bill->addOrder(Helper::arabic_w2e(['id_fabric' => $id_fabric, 'number' => $number,
                    'number_column' => request('number_column')[$i], 'price' => request('price')[$i], 'type' => 'product']));
                // after insert order we must deduct the amount of fabric to di
                $result = warehouses_fabric::decrementWarehouses($id_fabric, request('type'), $number);
                if ($result == 'error') {
                  return 'error';
                }
            }
            $p_bill->information_order((['id_customer' => $p_id, 'state' => $state['state_order'], 'type' => 'product']));
        });
    }
    public function productUnCompleted()
    {
        $Order_details = Order_Information::stateOrder(7, 'product'); // 7 mean is product un completed
        return view('order_detail.view_product_unCompleted', compact('Order_details'));
    }
    public function editOrderProduct($id)
    {
        $data = Order_Information::findOrder(7, 'product', $id);
        $products = warehouses_fabric::warehousesToChose('product' ,'products');
        $Payment_methods = Payment_method::cache();
        $discount = Discount::first();
        return view('order_detail.editOrderProduct', compact('data','discount', 'products', 'Payment_methods'));
    }
    public function updateProductUncompleted(Order_Information $id, ProductToOrderRequest $request)
    {
        if (request('type') == 'done_review') {
            $value = $this->updateProduct($id);
            if ($value == 'error') {
                return back()->withErrors('المنتج نفذ من المخزون ');
            }
            return redirect('view_product_unCompleted');
        } else {
            $this->cancelProduct($id);
            return redirect('view_product_unCompleted');
        }

    }
    public function updateProduct($id)
    {
        if (request('state_percentage') == 1) {
            //check if discount greater than basic percentage
            if ((request('percentage')/ 100)
                *
                (request('finally_price') + request('discount'))
                <
                request('discount')
            ) {
                return back()->withErrors([request('percentage') . 'نسبة الخصم تجاوزة القيمة المتاحة  ']);
            }
        }
        return DB::transaction(function () use ($id) {
            $state = Helper::stateOrder();
            $id->updateOrderInformation(['state' => $state['state_order']]);
            $bill = Bill::findOrFail(request('bill_id'));
            $bill->updateBill(request(['payment_method_id', 'paid_up','discount', 'Residual', 'end_date','paid_up_card']) + ['price' => request('total_thop_cost'), 'state' => $state['state_bill']]);
            for ($i = 0; $i < count(request('order_id')); $i++) {
                $orders = Order::findOrFail(request('order_id'))[$i];
                $number = Helper::arabic_w2e(request('number'))[$i];
                $id_fabric = request('id_fabric')[$i];
                $oldFabric = $orders->id_fabric;
                $oldQuantity = $orders->number;
                $if_error = warehouses_fabric::checkWarehouses($id_fabric, $oldFabric, $number, $oldQuantity, 'product');
                if ($if_error == 'error') {return 'error';}
                $orders->updateOrder(Helper::arabic_w2e(['id_fabric' => $id_fabric, 'number' => $number, 'price' => request('price')[$i]]));
            }
        });
    }
    public function cancelProduct(Order_Information $p_id)
    {
        return DB::transaction(function () use ($p_id) {
            $p_id->updateOrderInformation(['state' => 5]);
            Bill::findOrFail(request('bill_id'))->update((['state' => 2]));
            for ($i = 0; $i < count(request('order_id')); $i++) {
                $orders = Order::findOrFail(request('order_id'))[$i];
                Helper::check(warehouses_fabric::incrementWarehouses($orders->id_fabric, 'product', $orders->number));
            }
        });
    }

    public function insertFabrics($p_bill, $p_id)
    {
        return DB::transaction(function () use ($p_bill, &$id_order, $p_id) {
            try {


                $measurement = Measurement_order::whereHas('order_detail.Order_Information', function ($q) use ($p_id) {
                    return $q->whereId_customer($p_id);
                })->latest()->first();
                if ($measurement) {
                    $measurement = $measurement->replicate();
                    $measurement->save();
                } else {
                    $measurement = Measurement_order::create();
                }

                    $bill = $p_bill->addBill(request(['payment_method_id','discount','paid_up', 'Residual', 'end_date', 'paid_up_card']) +
                        ['price' => request('total_thop_cost'), 'state' => 0]);

                    $ids_addition = 1;
                for ($i = 0; $i < count(request('id_fabric')); $i++) {
                    $order = $bill->addOrder(['id_fabric' => request('id_fabric')[$i], 'number' => request('number')[$i],
                        'number_column' => request('number_column')[$i], 'price' => request('price')[$i],
                        'type' => request('type_design')[$i], 'addition_id' => request('addition_id')[$i],
                        'yards' => request('yards')[$i]]);

                    $order->additions()->attach(request($ids_addition++ . '_addition_id')); // add addition ad id order
                }
                $order = $bill->information_order((['id_customer' => $p_id, 'state' => 2]));
                for ($i = 0; $i < count(request('id_design')); $i++) {
                    $order->createDetail_order($i, $measurement->id);
                }
            } catch (\Exception $e)
            {
                $this->rollBack();

                throw new \Exception('order not created ');
            }
            return $order->id;
        });
    }

    public function GetMeasurement($id)
    {
        $measurement = Measurement::findOrFail($id);
        $cutters = Employee_data::cache_cutter();
        return view('layouts.helper.measurement_toEdit_order', compact('measurement', 'cutters'));
    }
    //state 2 the order is pending need review
    public function viewReviewMeasurement()
    {
        $Order_details = Order_Information::stateOrder(2, 'fabirc');
        return view('order_detail.view_Review_measurement', compact('Order_details'));
    }

    public function edit($id)
    {
        $data = Order_Information::findOrder(2, 'fabirc', $id) ;
        $cutters = Employee_data::cache_cutter();
        $Designs = Design::cache();
        $Necks = Neck::cache();
        $Pockets = Pocket::cache();
        $Sleeves = Sleeve::cache();
        $Buttons = Button::cache();
        $Additions = Addition::get();
        $discount = Discount::first();
        $Fabrics = warehouses_fabric::warehousesToChose('fabirc','fabrics');
        $Payment_methods = Payment_method::cache();
        // get id last measurement from order to view it
        return view('order_detail.edit', compact('data', 'discount','Payment_methods', 'Designs', 'Necks', 'Pockets'
            , 'Sleeves', 'Buttons', 'Fabrics', 'tailors', 'designs', 'cutters', 'Additions','measurement'));
    }

    public function review(Order_Information $id, editOrderRequest $request,MeasurementsRequest $measurementsRequest)
    {    $value = null ;
        if (Input::get('cancel')) {
            $this->canceled_order($id);
        }
        if (Input::get('edit')) {
            $value= $this->edit_order($id,$request);
        }
        if (Input::get('done_review')) {
            $value = $this->done_review($id, $request);
        }
        if ($value == 'error')
            return back()->withErrors('القماش نفذ من المخزون');

        return redirect('orders');

    }

    public function canceled_order($id)
    {
        return DB::transaction(function () use ($id) {
            $id->updateOrderInformation(['state' => 5]);
            Bill::findOrFail(request('bill_id'))->update((['state' => 2]));
            for ($i = 0; $i < count(request('order_id')); $i++) {
                $orders = Order::findOrFail(request('order_id'))[$i];
                warehouses_fabric::isEmpty($orders->number, $orders->yards, $orders->id_fabric);

            }
        });
    }

    public function orders_canceled()
    {
        $Order_details = Order_Information::stateOrder(5, 'fabirc');
        return view('order_detail.view_order_canceled', compact('Order_details'));
    }

    public function viewProduct_cancel()
    {
        $Order_details = Order_Information::stateOrder(5, 'product');
        return view('order_detail.viewProduct_cancel', compact('Order_details'));
    }

    public function order_canceled($id)
    {
        $data = Order_Information::findOrder(5, 'fabirc', $id);
        $payment_methods = Payment_method::cache();
        return view('order_detail.order_canceled', compact('data','payment_methods'));
    }

    public function detailorder_canceled_product($id)
    {
        $data = Order_Information::findOrder(5, 'product', $id) ;
        $payment_methods = Payment_method::cache();
        return view('order_detail.detailorder_canceled_product', compact('data','payment_methods'));

    }

    public function edit_order($id,$request){
        if (request('state_percentage') == 1) {
            //check if discount greater than basic percentage
            if ((request('percentage')/ 100)
                *
                (request('finally_price') + request('discount'))
                <
                request('discount')
            ) {
                return back()->withErrors([request('percentage') . 'نسبة الخصم تجاوزة القيمة المتاحة  ']);
            }
        }
        return DB::transaction(function () use ($id) {
            //  update measurement
            $update_measurement = Measurement_order::findOrFail(request('measurement_id'));
            $update_measurement->updateMeasurement_order();
            $Bill = Bill::findOrFail(request('bill_id'));
            $get_addition_number = $Bill->order->count();

            $Bill->updateBill(request(['payment_method_id','discount', 'paid_up', 'Residual', 'end_date','paid_up_card']) + ['price' => request('total_thop_cost')]);
            // if they want delete item
            $itemToDelete = array_values(array_diff($Bill->order->pluck('id')->toArray(), request('order_id')));
             for ($i = 0; $i < count($itemToDelete); $i++) {
                $order = Order::findOrFail($itemToDelete[$i]);
                warehouses_fabric::isEmpty($order->number, $order->yards, $order->id_fabric);
                $order->delete();
                $order->additions()->detach();
            }
            $get_old_addition_number = 1 ;
            for ($i = 0; $i < count(request('order_id')); $i++) {
                if (request('order_id')[$i] == 0) // create new order
                {
                    $new_order =  $Bill->addOrder(['id_fabric'=>request('id_fabric')[$i],'number'=>request('number')[$i],
                        'number_column'=>request('number_column')[$i] ,'price'=>request('price')[$i],
                        'type' => request('type_design')[$i] ,
                        ]);
                    $new_order->additions()->attach(request(++$get_addition_number.'_addition_id')); // add addition ad id order
                } else {
                    $order = Order::findOrFail(request('order_id')[$i]);
                    $update_order = ['id_fabric' => request('id_fabric')[$i], 'number' => request('number')[$i],
                        'number_column' => request('number_column')[$i], 'price' => request('price')[$i],
                        'type' => request('type_design')[$i]];

                    $order->update($update_order);
                    $old_addition_number = $order->additions()->get()->pluck('id')->toArray(); // get old addition in order
                    $new_addition_number = request($get_old_addition_number++ . '_addition_id'); // get  update addition
                    if ($old_addition_number != $new_addition_number) {
                        $get_addition_deleted = array_diff($old_addition_number, $new_addition_number);
                        $order->additions()->detach($get_addition_deleted); // delete addition
                        $get_addition_add = array_diff($new_addition_number, $old_addition_number);
                        $order->additions()->attach($get_addition_add); // add new addition
                    }
                }
            }

            $detailOrderDelete = array_values(array_diff($id->order_detail->pluck('id')->toArray(), request('id_detailOrder')));
            if($detailOrderDelete != null )
            {
                for($i=0;$i<count($detailOrderDelete);$i++){
                    $Order_detail = Order_detail::findOrFail($detailOrderDelete[$i]);
                    $Order_detail->delete();
                }
            }
            for ($i = 0; $i < count(request('id_detailOrder')); $i++) {
                if (request('id_detailOrder')[$i] == 0) {
                    $id->createDetail_order($i, $update_measurement->id);
                } else {
                    $id->updateDetail_order($i);
                }
            }
        });
    }
    public function done_review($id, $request)
    {
        $this->validate($request, [   //add number of yeardh
            "yards" => ' required |arabic_numbers ',
        ], [
            'yards.required' => ' ادخل  عدد الياردات ',
            'yards.arabic_numbers' => ' ادخل  عدد الياردات ',
        ]);
        return DB::transaction(function () use ($id) {
            //  update measurement
            $update_measurement = Measurement_order::findOrFail(request('measurement_id'));
            $update_measurement->updateMeasurement_order();
            $Bill = Bill::findOrFail(request('bill_id'));
            $get_addition_number = $Bill->order->count();
            $Bill->updateBill(request(['payment_method_id','discount','paid_up', 'Residual', 'end_date','paid_up_card']) + ['price' => request('total_thop_cost')]);
            // if they want delete item
            $itemToDelete = array_values(array_diff($Bill->order->pluck('id')->toArray(), request('order_id')));
            for ($i = 0; $i < count($itemToDelete); $i++) {
                $order = Order::findOrFail($itemToDelete[$i]);
                warehouses_fabric::isEmpty($order->number, $order->yards, $order->id_fabric);
                $order->delete();
                $order->additions()->detach();
            }
            $get_old_addition_number = 1 ;
            for ($i = 0; $i < count(request('order_id')); $i++) {
                $yards = Helper::arabic_w2e(request('yards'))[$i];
                $number = Helper::arabic_w2e(request('number'))[$i];
                $id_fabric = request('id_fabric')[$i];

                if (request('order_id')[$i] == 0) // create new order
                {
                    $new_order =  $Bill->addOrder(['id_fabric'=>request('id_fabric')[$i],'number'=>request('number')[$i],
                        'number_column'=>request('number_column')[$i] ,'price'=>request('price')[$i],
                        'type' => request('type_design')[$i] ,'yards'=>$yards
                    ]);
                    $new_order->additions()->attach(request(++$get_addition_number.'_addition_id')); // add addition ad id order
                } else {
                   $order = Order::findOrFail(request('order_id')[$i]);
                   $update_order= ['id_fabric' => request('id_fabric')[$i], 'number' => request('number')[$i],
                        'number_column' => request('number_column')[$i], 'price' => request('price')[$i],
                        'type' => request('type_design')[$i],'yards'=>$yards];

                    $old_addition_number = $order->additions()->get()->pluck('id')->toArray(); // get old addition in order
                    $new_addition_number = request($get_old_addition_number++ . '_addition_id'); // get  update addition
                    if ($old_addition_number != $new_addition_number) {
                        $get_addition_deleted = array_diff($old_addition_number, $new_addition_number);
                        $order->additions()->detach($get_addition_deleted); // delete addition
                        $get_addition_add = array_diff($new_addition_number, $old_addition_number);
                        $order->additions()->attach($get_addition_add); // add new addition
                    }
                }
                // if user do not add yeardh
                $if_error = null ;
                if ($order->yards == null || request('order_id')[$i] == 0) {
                    $result = $yards * $number;
                    $if_error = warehouses_fabric::decrementWarehouses($id_fabric, 'fabirc', $result);
                } elseif ($order->yards != null and request('order_id')[$i] != 0) {
                    $oldQuantity = $order->number * $order->yards; // check if change yeard
                    $Quantity = $yards * $number;
                    $if_error = warehouses_fabric::checkWarehouses($id_fabric, $order->id_fabric, $Quantity, $oldQuantity, 'fabirc');
                }
                if($if_error == 'error'){return 'error' ;}
                $order->update($update_order);
            }
            $detailOrderDelete = array_values(array_diff($id->order_detail->pluck('id')->toArray(), request('id_detailOrder')));
            if($detailOrderDelete != null )
            {
                for($i=0;$i<count($detailOrderDelete);$i++){
                    $Order_detail = Order_detail::findOrFail($detailOrderDelete[$i]);
                    $Order_detail->delete();
                }
            }
            for ($i = 0; $i < count(request('id_detailOrder')); $i++) {
                if (request('id_detailOrder')[$i] == 0) {
                    $id->createDetail_order($i, $update_measurement->id);
                } else {
                    $id->updateDetail_order($i);
                }
            }
            $id->updateOrderInformation(['state'=>3]);
        });


    }
    // number 3 order in work
    public function index()
    {
        $Order_details = Order_Information::stateOrder(3, 'fabirc');
        return view('order_detail.view', compact('Order_details'));
    }

    public function DetailWorked_order($id)
    {
        $payment_methods = Payment_method::cache();
        $data = Order_Information::where('state', 3)->findOrFail($id);
        return view('order_detail.detail_worked', compact('data','payment_methods'));
    }

    public function completed_or_review(Order_Information $id) //

    {
        if (Input::get('complete_order')) {
            $this->it_is_completed($id);
            return redirect('orders_completed');
        } elseif (Input::get('review_order')) {
            $this->returnReview($id);
            return redirect('editOrder/' . $id->id);
        }

    }

    public function returnReview($id)
    {
        $id->updateOrderInformation(['state' => 2]);
    }

    public function it_is_completed(Order_Information $id)
    {
        return DB::transaction(function () use ($id) {
            $phone = $id->customer->mobile->phone;
            $name = $id->customer->name;
            $msg = urlencode("السلام عليكم.
عميلنا:$name
نود تذكيرك بإستلام طلبك ودمتم بخير.
خياط ماوان ");
            file_get_contents("http://www.mobily.ws/api/msgSend.php?mobile=966596591130&password=77885544NJODsa&numbers=$phone&sender=Mawan&msg=".$msg."&applicationType=68&68&lang=3");
            //$homepage = file_get_contents("http://aramob.com/sendsms.php?user=mawan-sms&password=123456Aa&numbers=0$phone&sender=Mawan&lang=ar&message=" . $msg);;
            $id->updateOrderInformation(['state' => 4]);
        });

    }

    public function orders_completed()
    {
        $payment_methods = Payment_method::cache();
        $Order_details = Order_Information::stateOrder(4, 'fabirc');
        return view('order_detail.viewCompleteOrders', compact('Order_details','payment_methods'));
    }

    public function detail_orders_completed($id)
    {
        $payment_methods = Payment_method::cache();
        $data = Order_Information::where('state', 4)->findOrFail($id);

        return view('order_detail.detailCompleteOrder', compact('data','payment_methods'));
    }

    public function it_is_deliver(Order_Information $id)
    {

        if((request('paid_later') + request('paid_card_later') + request('other_payment_method'))
            != $id->bill->Residual)
            {
            return back()->withErrors([ 'مجموع المدفوع لا يساوي مجموع المتبقى ']);
        }

        DB::transaction(function () use ($id) {
            $id->updateOrderInformation(['state' => 6]);
            $id_bill = $id->bill->id;

            Bill::findOrFail($id_bill)->updateBill(request(['payment_method_id', 'paid_up', 'end_date','paid_up_card','paid_later'
                 ,'paid_card_later','other_payment_method']) + [ 'state' => 1]);

        });

        return redirect('billCompleted/' . $id->id);

    }

    public function order_delivered()
    {
        $Order_details = Order_Information::stateOrder(6, 'fabirc');
        return view('order_detail.viewOrder_delivered', compact('Order_details'));
    }

    public function product_delivered()
    {
        $Order_details = Order_Information::stateOrder(6, 'product');
        return view('order_detail.viewProduct_delivered', compact('Order_details'));
    }

    public function detailProduct_delivered($id)
    {
        $data = Order_Information::where([['state', 6], ['type', 'product']])->findOrFail($id);
        return view('order_detail.detailProduct_delivered', compact('data'));

    }

    public function detailDelivered($id)
    {
        $payment_methods = Payment_method::cache();
        return Cache::rememberForever('detailDelivered' . $id, function () use ($id,$payment_methods) {
            $data = Order_Information::where('state', 6)->findOrFail($id);
            return view('order_detail.detail_deliver', compact('data','payment_methods'))
                ->render();
        });
    }

    public function viewBillCompleted($id)
    {
        $data = Order_Information::where('state', 6)->findOrFail($id);
        return view('bill.bill_completed', compact('data'));

    }

    public function search(Request $request)
    {
        $item = $request->input('search');
        $state = $request->input('state');
        switch ([$item, $state]) {
            case is_numeric($item) && strlen((string)$item) == 10 || strlen((string)$item) == 9  :
                // Find the customer with the mobile number
                $Order_details = Order_Information::where([['state', $state], ['type', 'fabirc']])
                    ->whereHas('customer.mobile', function ($q) use ($item) {
                        $q->where('phone', $item);
                    })->paginate(100);

                return view('order_detail.viewOrder_delivered', compact('Order_details'));
                break;
            case  is_numeric($item) && strlen((string)$item) < 10 :
                // Find the customer with the  id
                $Order_details = Order_Information::
                where([['state', $state], ['type', 'fabirc'], ['id', $item]])->latest()->paginate(100);
                return view('order_detail.viewOrder_delivered', compact('Order_details'));
                break;
            case !is_numeric($item) && !DateTime::createFromFormat('Y-m-d', $item) && is_string($item):
                // Find the customer with the   name
                $Order_details = Order_Information::where([['state', $state], ['type', 'fabirc']])
                    ->whereHas('customer', function ($q) use ($item) {
                        $q->where('name', 'like', '%' . $item . '%');
                    })->latest()->paginate(100);
                return view('order_detail.viewOrder_delivered', compact('Order_details'));
                break;

            case (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $item) ? true : false):
                // tFind the customer with the  created at
                $Order_details = Order_Information::
                where([['state', $state], ['type', 'fabirc']])->whereDate('created_at', $item)->latest()->paginate(100);
                return view('order_detail.viewOrder_delivered', compact('Order_details'));
                break;
            default:
                session()->flash('messageF', 'تاكد من البيانات المدخلة ، وعدم وجود مسافات  ');
                return back();
        }

    }

}
