<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Sleeve;
use App\Http\Requests\SleevesRequest;
use App\Http\Helper;
use Illuminate\Support\Facades\Cache;
class SleevesController extends Controller
{
    public function index()
    {
        $data =  Sleeve::get();
        return view('sleeve.view', compact('data') );
    }

    public function create()
    {
        return view('sleeve.create');
    }

    public function insert(SleevesRequest $request)
    {
        Sleeve::create($request->all()) ?  Helper::true() : Helper::false();
        Cache::forget('Sleeve');
        Sleeve::cache();
        return back();
    }

    public function edit(Sleeve $sleeve)
    {
        return view('sleeve.edit', ['data' => $sleeve ]);
    }

    public function update(SleevesRequest $request, Sleeve $sleeve)
    {
       $sleeve->update($request->all()) ?   Helper::true() :  Helper::false();
        Cache::forget('Sleeve');
        Sleeve::cache();
        return back();
    }

}
