<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Pocket;
use App\Http\Requests\PocketsRequest;
use App\Http\Helper;
use Illuminate\Support\Facades\Cache;
class PocketController extends Controller
{
    public function index()
    {
        $data  = Pocket::get();
        return view('pockets.view',compact('data') );
    }

    public function create()
    {
        return view('pockets.create');
    }

    public function insert(PocketsRequest $request)
    {
        Pocket::create($request->all()) ?   Helper::true() :  Helper::false();
        Cache::forget('Pocket');
        Pocket::cache();
        return back();
    }

    public function edit( Pocket $data)
    {
        return view('pockets.edit', compact('data'));
    }

    public function update(PocketsRequest $request,Pocket $pocket)
    {
        $pocket->update($request->all()) ?   Helper::true() :  Helper::false();
        Cache::forget('Pocket');
        Pocket::cache();
        return back();
    }

}
