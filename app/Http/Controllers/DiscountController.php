<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discount ;
use App\Http\Helper;
class DiscountController extends Controller
{
    public function edit()
    {
        return view('Discount.edit', ['data' =>  Discount::first() ] );
    }
    public function update(Request $request ,Discount $id)
    {
        $this->validate($request, [
            'state'=>' max:5 |string',
            'percentage'=>'numeric|required|max:100'],['required'=>'ادخل جميع القيمً'
        ]);

        $request->input('state') == 'on' ? $data['state'] = 1 : $data['state'] = 0;
        $data['percentage'] =$request->input('percentage');
        $data->save() ?  Helper::true() : Helper::false();
        return back();
    }
}
