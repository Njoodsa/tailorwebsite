<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bill;
use App\Invoice_items;
use DB;

class RevenueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $data = DB::table('bills')
            ->select(DB::raw('YEAR(created_at) as year'), DB::raw('sum(price)  as price'))
            ->where('state', 1)
            ->groupBy(DB::raw('YEAR(created_at)'))
            ->get();

        $product = DB::table('bills')->join('order__informations', 'bills.id', '=', 'order__informations.id_bill')
            ->select( DB::raw('sum(price)  as price'))
            ->where([['bills.state', 1],['order__informations.type','product']])
            ->groupBy(DB::raw('YEAR(bills.created_at)'))
            ->first();

        $fabirc = DB::table('bills')->join('order__informations', 'bills.id', '=', 'order__informations.id_bill')
            ->select( DB::raw('sum(price)  as price'))
            ->where([['bills.state', 1],['order__informations.type','fabirc']])
            ->groupBy(DB::raw('YEAR(bills.created_at)'))
            ->first();


        $total = DB::table('bills')
            ->select( DB::raw('sum(price) as TotalPrice'))
            ->where('state', 1)
            ->first();

        return view('revenue.view', compact('data' ,'total','product','fabirc'));
    }

    public function bills($year){
        $data  = Bill::with('order_information.customer')->where('state',1)->whereYear('created_at', $year)->get();
        $total =$data->sum('price') - $data->sum('discount');
      return view('revenue.AllBill', compact('data','year','total'));
    }

}
