<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Helper;
use App\Fabrics;
use App\Http\Requests\ProductRequest;
use App\Industry;
use App\Section;
use DB;
class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Product::get();
        return view('product.view', compact('data'));
    }
    public function create()
    {
        $Sections = Section::get();
        return view('product.create', compact('Sections'));
    }
    public function insert(ProductRequest $request)
    {
        return DB::transaction(function () use ($request) {
            $data = new Product();
            $data->name = $request->Input('name') ;
            $data->section_id = $request->Input('section_id') ;
            $data->price = $request->Input('price') ;
            $data->barcode_number = $request->Input('barcode_number') ;
            $data->note = $request->Input('note') ;
            $data->unit = $request->Input('unit') ;
            if ( $data->save() ) {
                Helper::true();
            } else {
                Helper::false();
            }
            return back();
        });

    }
    public function edit($id)
    {

        $data = Product::findOrFail($id);
        $Sections = Section::get();
        return view('product.edit', compact('data', 'Sections'));

    }
    public function update(ProductRequest $request, $id)
    {
        DB::transaction(function () use ($request, $id) {

            $fabrics = Product::findOrFail($id);

            $update = [
                'name' => $request->input('name'),
                'section_id' => $request->input('section_id'),
                'price' => $request->input('price'),
                'barcode_number' => $request->input('barcode_number'),
                'note' => $request->input('note'),
                'unit' => $request->input('unit'),
            ];


            if ($fabrics->update($update)) {
                Helper::true();
            } else {
                Helper::false();
            }

        });
        return back();
    }

    public function viewSection($id ,$name)  // view product in one section
    {
        $data = Product::where('section_id',$id)->get();
        return view('section.viewSection', compact('data' ,'name'));

    }


}
