<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bill;
use App\Order_Information;
use Illuminate\Support\Collection;
use Barryvdh\Debugbar\Facade as Debugbar;
use App\Invoice_items;
use App\Order;
use App\Fabrics_unit;

class ReportController extends Controller
{
    public function search_fabric()
    {
        return view('Report.search_fabric');
    }

    public function result_search_fabric(Request $request)
    {
        $this->validate($request, [
            'state' => 'required|numeric',
            'first_date' => 'date|required',
            'second_date' => 'date|required',
        ], ['required' => 'ادخل جميع البيانات  ',
            'date' => 'ادخل التاريخ بشكل الصحيح'
        ]);
        $state = $request->input('state');
        $first_date = $request->input('first_date');
        $second_date = $request->input('second_date');
        if ($state == 0) {
            $data = Order_Information::with(['bill.order', 'bill.payment_method', 'customer'])
                ->whereIn('state', [6, 3, 4])
                ->where('type', 'fabirc')
                ->whereBetween('created_at', array($first_date, $second_date . ' 23:59:59'))
                ->orderBy('id', 'asc')->get();

        } else {
            $data = Order_Information::where('state', $state)
                ->where('type', 'fabirc')
                ->whereBetween('updated_at', array($first_date, $second_date . ' 23:59:59'))->orderBy('id', 'asc')
                ->get();
        }

        return view('Report.detail_bill_fabric', compact('data', 'first_date', 'second_date'));
    }

    public function search_product()
    {
        return view('Report.search_product');
    }


    public function result_search_product(Request $request)
    {
        $this->validate($request, [
            'state' => 'required|numeric',
            'first_date' => 'date|required',
            'second_date' => 'date|required',
        ], ['required' => 'ادخل جميع البيانات  ',
            'date' => 'ادخل التاريخ بشكل الصحيح'
        ]);
        $state = $request->input('state');
        $first_date = $request->input('first_date');
        $second_date = $request->input('second_date');
        if ($state == 0) {
            $data = Order_Information::with(['bill.order', 'bill.payment_method', 'customer'])
                ->whereIn('state', [6, 7])
                ->where('type', 'product')
                ->whereBetween('updated_at', array($first_date, $second_date . ' 23:59:59'))
                ->orderBy('id', 'asc')->get();

        } else {
            $data = Order_Information::where('state', $state)
                ->where('type', 'product')
                ->whereBetween('updated_at', array($first_date, $second_date . ' 23:59:59'))->orderBy('id', 'asc')
                ->get();
        }

        return view('Report.detail_bill_product', compact('data', 'first_date', 'second_date'));
    }


    public function warehouses_fabric()
    {
        // get total of Purchase for every fabric
        $purchases = Invoice_items::select('number as cost', 'id_fabric', 'supplier_invoices_id')
            ->with('fabrics')
            ->whereHas('supplier_invoices', function ($q) {
                $q->where([['type', 'fabirc'], ['state', 1]]);
            })->get();

        // get unit of  fabric
        $units = Fabrics_unit::select('fabrics_id', 'Equation')->where('unit_id', 1)->get();

        // turn taqah to yeardh by multi
        $purchase['total_cost'] = 0;
        foreach ($purchases as $key=> $purchase) {
            foreach ($units as $unit) {
                if ($unit->fabrics_id == $purchase->id_fabric) {
                    $purchase['total_cost'] = $unit->Equation * $purchase->cost;
                    break;
                }
            }
            // get name of fabric
            $purchase['id_fabric'] = $purchase->fabrics->name;
        }

        // group fabric to sum cost
        $purchases = $purchases->groupBy('id_fabric');

        // get bill to count sales
        $bills = Order::select('id', 'Bill_id', 'yards', 'number', 'id_fabric')
            ->with('fabrics')
            ->whereHas('bill.order_information', function ($q) {
                $q->where([['type', 'fabirc']]);
            })->whereHas('bill', function ($q) {
                $q->whereIn('state', [1, 0]);
            })->get();

        // yeardh * thop to get number of yeardh
        $bill['total_yeardh'] = 0;
        foreach ($bills as $bill) {
            $bill['total_yeardh'] = $bill->yards * $bill->number;
            $bill['id_fabric'] = $bill->fabrics->name; // get naame of fabric
        }

        $bills = $bills->groupBy('id_fabric');

        // merge sales with purchases  for every fabric
        $result = array_merge_recursive($bills->toArray(), $purchases->toArray()) ;

        return view('Report.warehouses_fabric', compact('result'));

    }

    public function warehouses_product()
    {

        $purchases = Invoice_items::select('number as cost', 'id_fabric', 'supplier_invoices_id')
            ->with('product')
            ->whereHas('supplier_invoices', function ($q) {
                $q->where([['type', 'product'], ['state', 1]]);
            })->get();

        foreach ($purchases as  $purchase) {
            // get name of product

            $purchase['id_fabric'] = $purchase->product->name  ;
        }

        $purchases = $purchases->groupBy('id_fabric');


        // get bill to count sales
        $bills = Order::select('id', 'Bill_id', 'number', 'id_fabric')
            ->with('products')
            ->whereHas('bill.order_information', function ($q) {
                $q->where([['type', 'product']]);
            })->whereHas('bill', function ($q) {
                $q->whereIn('state', [1, 0]);
            })->get();

        foreach ($bills as $bill) {
            $bill['id_fabric'] = $bill->products->name; // get naame of fabric
        }

        $bills = $bills->groupBy('id_fabric');
        $result = array_merge_recursive($bills->toArray(), $purchases->toArray()) ;
        return view('Report.warehouses_product', compact('result'));

    }
}
