<?php

namespace App\Http\Controllers;

use App\Button;
use App\Http\Requests\ButtonRequest;
use App\Http\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
class ButtonController extends Controller
{
    public function index()
    {
        $data =  Button::get();
        return view('buttons.view', compact('data'));
    }

    public function create()
    {
        return view('buttons.create');
    }

    public function insert(ButtonRequest $request)
    {
        Button::create( $request->all()) ? Helper::true() : Helper::false();
        Cache::forget('Button');
        Button::cache();
        return back();
    }

    public function edit(Button $data)
    {
        return view('buttons.edit', compact('data'));
    }

    public function update(ButtonRequest $request,Button $button)
    {
        $button->update($request->all()) ?  Helper::true() : Helper::false() ;
        Cache::forget('Button');
        Button::cache();
        return back();
    }

}
