<?php

namespace App\Http\Controllers;

use App\Http\Helper;
use App\Employee_type;
use App\Http\Requests\employee_typeRequest;
use App\Http\Requests\employee_typeUpdateRequest;
class Employee_typeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function view()
    {
        $data = Employee_type::get();
        return view('employee_type.view', compact('data'));
    }
    public function create()
    {
        return view('employee_type.create');
    }

    public function insert(employee_typeRequest $request)
    {
        $data = Employee_type::create($request->all());
        if ($data) {
            Helper::true();
        }else {
            Helper::false();
        }
        return back();
    }


    public function edit($id)
    {
        $data = Employee_type::findOrFail($id);
        return view('employee_type.edit', compact('data'));
    }

    public function update(employee_typeRequest $request, $id)
    {
        $data = Employee_type::findOrFail($id);
        $update = $data->update($request->all());
        if ($update == true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }
}
