<?php

namespace App\Http\Controllers;

use App\Unit;
use App\Http\Helper;
use App\Http\Requests\UnitRequest;

class UnitController extends Controller
{
    public function index()
    {
        $data = Unit::get();
        return view('units.view',compact('data') );
    }

    public function create()
    {
        return view('units.create');
    }

    public function insert(UnitRequest $request)
    {
        Unit::create($request->all()) ? Helper::true() : Helper::false();
        return back();
    }

    public function edit(Unit $data)
    {
        return view('units.edit', compact('data'));
    }

    public function update(UnitRequest $request, Unit $unit)
    {
        $unit->update($request->all()) ? Helper::true() : Helper::false();
        return back();
    }

}
