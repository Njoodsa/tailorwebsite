<?php

namespace App\Http\Controllers;

use App\Fabrics_unit;
use App\Http\Helper;
use App\Fabrics;
use App\Http\Requests\FabricsRequest;
use App\Industry;
use App\Unit;
use DB;
use App\Fabrics_units;

class FabricsController extends Controller
{
    public function index()
    {
        $data = Fabrics::get() ;
        return view('fabrics.view', compact('data') );
    }

    public function create()
    {
        $industries = Industry::get();
        $units = Unit::get()->except([1,2]);
        return view('fabrics.create', compact('industries', 'units'));
    }

    public function insert(FabricsRequest $request)
    {
        if($request->Input('disable') != 1)
            $request['disable'] = 0 ;

        DB::transaction(function () use ($request) {
            $data = Fabrics::create($request->all());
            $id_fabrics = $data->id;
            for($i=0; $i<count($request->Input('unit_id'));$i++){
                $items = [
                    'unit_id'=>$request->Input('unit_id')[$i],
                    'Equation' =>$request->Input('Equation')[$i],
                    'price' =>$request->Input('price')[$i],
                    'fabrics_id' => $id_fabrics,
                    ];
                Fabrics_unit::insert($items) ?  Helper::true() :  Helper::false() ;
            }
        });
        return back();
    }

    public function edit(Fabrics $data)
    {
        $Fabrics_units = Fabrics_unit::where('fabrics_id', '=', $data->id)->get();
        $industries = Industry::get();
        $units = Unit::get();
        return view('fabrics.edit', compact('data', 'industries', 'units', 'Fabrics_units'));
    }

    public function update(FabricsRequest $request, Fabrics $fabrics)
    {
        DB::transaction(function () use ($request, $fabrics) {
            if($request->Input('disable') != 1)
                $request['disable'] = 0 ;
            $update = [
                'name' => $request->input('name'),
                'industries_id' => $request->input('industries_id'),
                'fabric_width' => $request->input('fabric_width'),
                'lowest_price' => $request->input('lowest_price'),
                'barcode_number' => $request->input('barcode_number'),
                'note' => $request->input('note'),
                'disable'=>$request->input('disable')
            ];


            if ($fabrics->update($update)) {
                for ($i = 0; $i < count($request->input('unit_id')); $i++) {
                    $item = Fabrics_unit::where('id', $request->input('u_id')[$i])->first();
                    $item->Equation = $request->input('Equation')[$i];
                    $item->price = $request->input('price')[$i];
                    $item->unit_id = $request->input('unit_id')[$i];
                    $item->save();
                }
                Helper::true();
            } else {
                Helper::false();
            }

        });
        return back();
    }
}
