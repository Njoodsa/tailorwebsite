<?php

namespace App\Http\Controllers;
use App\Branch;
use App\Http\Helper;
use App\Warehouse;
use App\Http\Requests\BranchRequest;
class BranchController extends Controller
{

    public function index()
    {
        $data = Branch::get() ;
        return view('branches.view',compact('data'));
    }


    public function create()
    {
        return view('branches.create', ['warehouses' => Warehouse::get()]);
    }


    public function store(BranchRequest $request)
    {
        flash_if_success_or_fail(Branch::create($request->all()));
        return back();
    }


    public function edit(Branch $Branch)
    {
        return view('branches.edit', ['branch' => $Branch, 'warehouses' => Warehouse::get()]);
    }

    public function update(BranchRequest $request,Branch $Branch)
    {

        flash_if_success_or_fail($Branch->update($request->all()));
        return back();
    }

}
