<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fabrics;
use App\Payment_method;
use App\Http\Helper;
use App\Warehouse;
use App\Supplier;
use App\supplier_invoice;
use App\Invoice_items;
use App\Http\Requests\supplier_invoiceRequest;
use DB;
use App\warehouses_fabric;
use App\Fabrics_unit;
use Illuminate\Support\Facades\Cache;
use App\Product;
class Supplier_invoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $supplier_invoices = supplier_invoice::where('state', 1)->get();
        return view('supplier_invoice.viewCompleted', compact('supplier_invoices'));
    }

    public function create()
    {
        $Fabrics = Fabrics::get();
        $Payment_methods = Payment_method::get();
        $Warehouses = Warehouse::get();
        $suppliers = Supplier::get();
        return view('supplier_invoice.create', compact('Fabrics', 'Payment_methods', 'Warehouses', 'suppliers'));
    }
    public function GetProductAJax()
    {
        $product = Product::get();

        return $product;
    }
    public function GetFabricsAJax()
    {
        $Fabrics = Fabrics::where('disable','<>',1)->get();
        return $Fabrics;
    }

    public function insert(supplier_invoiceRequest $request)
    {

        DB::transaction(function () use ($request) {
            $data_supplier_invoice = [
                'supplier_id' => $request->input('supplier_id')
                , 'price' => $request->input('supplier_id')
                , 'state' => $request->input('state')
                , 'discount' => $request->input('discount')
                , 'price' => $request->input('finally_price')
                , 'payment_method_id' => $request->input('payment_method_id')
                , "paid_up" => $request->input('paid_up')
                , "Residual" => $request->input('Residual')
                , "date" => $request->input('date'),
            ];
            $type = $request->input('type');
            if($type == 'product')
            {
                $data_supplier_invoice['type'] ='product';
            }

            $insert_invoice = supplier_invoice::create($data_supplier_invoice);
            for ($i = 0; $i < count($request->input('id_item')); $i++) {
                $id_fabric = $request->input('id_item')[$i];
                $warehouses_id = $request->input('warehouses_id')[$i];
                $number = $request->input('number')[$i];
                $data_items = [
                    'id_fabric' => $id_fabric
                    , 'price' => $request->input('price')[$i]
                    , 'supplier_invoices_id' => $insert_invoice->id
                    , 'warehouses_id' => $request->input('warehouses_id')[$i]
                    , 'number' => $number
                ];
                $insert_invoice_item = Invoice_items::create($data_items);
                if ($request->input('state') == 1) {
                    if( $type == 'product'){
                        $add_current_balance = warehouses_fabric::where([['fabrics_id', $id_fabric], ['warehouses_id', $warehouses_id] ,['type','product']])->first();
                        if(!$add_current_balance == null)
                        {
                            $add_current_balance->increment('current_balance', $number);
                        }else{
                          $warehouses_fabric = new warehouses_fabric;
                          $warehouses_fabric->fabrics_id =  $id_fabric;
                          $warehouses_fabric->warehouses_id =  $warehouses_id;
                          $warehouses_fabric->current_balance= $number;
                          $warehouses_fabric->demand_limit = 5;
                          $warehouses_fabric->type = 'product';
                          $warehouses_fabric->save();
                        }
                    }else{
                        $Equation = Fabrics_unit::select('Equation')->where([['fabrics_id', $id_fabric], ['unit_id', 1]])->first();
                        $result = $number * $Equation->Equation;
                        // تحديد او اضافة رصيد المخزن
                        $add_current_balance = warehouses_fabric::where([['fabrics_id', $id_fabric], ['warehouses_id', $warehouses_id],['type','fabirc']])->first();
                        if ($add_current_balance) {
                            $add_current_balance->increment('current_balance', $result);
                        } else {
                            $warehouses_fabric = new warehouses_fabric;
                            $warehouses_fabric->fabrics_id =  $id_fabric;
                            $warehouses_fabric->warehouses_id =  $warehouses_id;
                            $warehouses_fabric->current_balance= $result;
                            $warehouses_fabric->demand_limit = 1;
                            $warehouses_fabric->save();
                        }
                    }
                }

            }
            if($insert_invoice_item) {
                Helper::true();
            }else {
                Helper::false();
            }
        });

        return back();
    }

    public function detailCompleted($id)
    {
        $data = supplier_invoice::FindorFail($id);
        return view('supplier_invoice.detailCompleted', compact('data'));
    }
    public function detailCompleted_product($id)
    {
        $data = supplier_invoice::FindorFail($id);
        return view('supplier_invoice.detailCompleted_product', compact('data'));
    }


    public function indexUncompleted()
    {
        $supplier_invoices = supplier_invoice::where('state', 0)->get();
        return view('supplier_invoice.viewUncompleted', compact('supplier_invoices'));
    }

    public function detailUnCompleted($id)
    {
        $data = supplier_invoice::FindOrFail($id);
        $Fabrics = Fabrics::get();
        $Payment_methods = Payment_method::get();
        $Warehouses = Warehouse::get();
        $suppliers = Supplier::get();
        return view('supplier_invoice.detailUncompleted', compact('data', 'Fabrics', 'Payment_methods', 'Warehouses', 'suppliers'));

    }
    public function detailUnCompleted_product($id)
    {
        $data = supplier_invoice::FindOrFail($id);
        $products = Product::get();
        $Payment_methods = Payment_method::get();
        $Warehouses = Warehouse::get();
        $suppliers = Supplier::get();
        return view('supplier_invoice.detailUncompleted_product', compact('data', 'products', 'Payment_methods', 'Warehouses', 'suppliers'));

    }

    public function update(supplier_invoiceRequest $request, $id)
    {

        DB::transaction(function () use ($request, $id) {

            $update_invoice = supplier_invoice::findOrFail($id);
            $data_supplier_invoice = [
                'supplier_id' => $request->input('supplier_id')
                , 'price' => $request->input('supplier_id')
                , 'state' => $request->input('state')
                , 'discount' => $request->input('discount')
                , 'price' => $request->input('finally_price')
                , 'payment_method_id' => $request->input('payment_method_id')
                , "paid_up" => $request->input('paid_up')
                , "Residual" => $request->input('Residual')
                , "date" => $request->input('date'),
            ];

            $update_invoice->update($data_supplier_invoice);

            for ($i = 0; $i < count($request->input('id_item')); $i++) {
                $id_fabric = $request->input('id_fabric')[$i];
                $warehouses_id = $request->input('warehouses_id')[$i];
                $number = $request->input('number')[$i];
                $update_item = Invoice_items::findOrFail($request->input('id_item'))[$i];
                $data_items = [
                     'id_fabric' => $id_fabric
                    , 'price' => $request->input('price')[$i]
                    , 'warehouses_id' => $warehouses_id
                    , 'number' => $number
                ];
                $update_item->update($data_items);

                if ($request->input('state') == 1) {
                    if($update_invoice->type == 'product'){
                        $add_current_balance = warehouses_fabric::where([['fabrics_id', $id_fabric], ['warehouses_id', $warehouses_id],['type','product']])->first();
                        if(!$add_current_balance == null)
                        {
                            $add_current_balance->increment('current_balance', $number);
                        }else{
                            $insert_wherhouse = new warehouses_fabric;
                            $insert_wherhouse->fabrics_id = $id_fabric;
                            $insert_wherhouse->warehouses_id = $warehouses_id;
                            $insert_wherhouse->current_balance = $number;
                            $insert_wherhouse->demand_limit = 5;
                            $insert_wherhouse->type = 'product';
                            $insert_wherhouse->save();

                        }

                    }else{
                        $Equation = Fabrics_unit::select('Equation')->where([['fabrics_id', $id_fabric], ['unit_id', 1]])->first();
                        $result = $number * $Equation->Equation;
                        // تحديد او اضافة رصيد المخزن
                        $add_current_balance = warehouses_fabric::where([['fabrics_id', $id_fabric], ['warehouses_id', $warehouses_id],['type','fabirc']])->first();
                        if ($add_current_balance) {

                            $add_current_balance->increment('current_balance', $result);

                        } else {
                            $warehouses_fabric = new warehouses_fabric;
                            $warehouses_fabric->fabrics_id =  $id_fabric;
                            $warehouses_fabric->warehouses_id =  $warehouses_id;
                            $warehouses_fabric->current_balance= $result;
                            $warehouses_fabric->demand_limit = 1;
                            $warehouses_fabric->save();
                        }
                    }

                }
            }
        });
        Helper::true();
        return redirect('invoice');
    }

}
