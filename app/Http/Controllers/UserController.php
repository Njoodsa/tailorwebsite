<?php

namespace App\Http\Controllers;
use App\User;
use App\Employee_data;
use App\Branch;
use App\Http\Requests\UserRequest;
use App\Http\Helper;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $users= User::where('id', '!=', 1)->get();
        return view('users.view',compact('users'));
    }

    public function create(){

        $Employee_data = Employee_data::get();
        $Branches = Branch::get();
        return view('users.create',compact('Employee_data','Branches'));
    }

    public function insert(UserRequest $request){
        $data =$request->all();
        if(User::create($data)){
            Helper::true();
        }else{
            Helper::false();
        }
        return back();
    }

    public function access0( $id){
        $data = User::FindOrFail($id);
        if($data->update(['access'=>0])){ // A user whose permissions have been denied
            Helper::true();
        }else{
            Helper::false();
        }
        return back();
    }
    public function access2( $id){

        $data = User::FindOrFail($id);
        if($data->update(['access'=>2])){ // employee
            Helper::true();
        }else{
            Helper::false();
        }
        return back();
    }

}
