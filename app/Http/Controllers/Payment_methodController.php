<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment_method;
use App\Http\Helper;
use App\Http\Requests\Payment_methodRequest;
use Illuminate\Support\Facades\Cache;
class Payment_methodController extends Controller
{

    public function index(){
        $data =  Payment_method::get();
        return view('payment_method.view', compact('data') );
    }



    public function create(){
        return view('payment_method.create');
    }


    public function insert(Payment_methodRequest $request)
    {
        Payment_method::create($request->all()) ? Helper::true() :  Helper::false();
        Cache::forget('Payment_method');
        Payment_method::cache();
        return back();
    }



    public function edit($id)
    {
        return view('payment_method.edit', ['data' => Payment_method::findOrFail($id)]);
    }


    public function update(Payment_methodRequest $request, Payment_method $payment_method)
    {
        $payment_method->update($request->all())?    Helper::true() : Helper::false();
        Cache::forget('Payment_method');
        Payment_method::cache();
        return back();
    }

}
