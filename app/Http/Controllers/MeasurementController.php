<?php

namespace App\Http\Controllers;

use App\Measurement;
use DB;
use App\Customer;
use App\Http\Helper;
use App\Http\Requests\MeasurementsRequest;
use App\Employee_data;

class MeasurementController extends Controller
{
    //

    public function NewMeasurement($id)
    {
        $data = Customer::findOrFail($id);
        $cutters = Employee_data::cache_cutter();
        return view('customer.addMeasurement', compact('data','cutters'));
    }

    public function insertNewMeasurement(MeasurementsRequest $request, $id)
    {

        $check_measuremet = Measurement::where('customer_id' ,$id)->get();

        if(count($check_measuremet) < 2) {
            return DB::transaction(function () use ($request, $id) {
                $data = Customer::FindOrFail($id);
                $data_measurement = [
                    'customer_id'=>$id,
                    'Length' => Helper::arabic_w2e($request->input('Length')),//
                    'ShlR' => Helper::arabic_w2e($request->input('ShlR')),//
                    'ShlL' => Helper::arabic_w2e($request->input('ShlL')),//
                    'Slv_w' => Helper::arabic_w2e($request->input('Slv_w')),//
                    'Chest' => Helper::arabic_w2e($request->input('Chest')),//
                    'Waist' => Helper::arabic_w2e($request->input('Waist')),//
                    'Bottom' => Helper::arabic_w2e($request->input('Bottom')),//
                    'CollarGalab' => Helper::arabic_w2e($request->input('CollarGalab')),//
                    'FrontLonger' => Helper::arabic_w2e($request->input('FrontLonger')),//
                    'FakeButton' => Helper::arabic_w2e($request->input('FakeButton')),//
                    'ElbowCuffs' =>1,//
                    'Cuffs' => Helper::arabic_w2e($request->input('Cuffs')),//
                    'HIPS' => Helper::arabic_w2e($request->input('HIPS')),//
                    'ElbowSlvSada' => Helper::arabic_w2e($request->input('ElbowSlvSada')),//
                    'Sada' => Helper::arabic_w2e($request->input('Sada')),//
                    'Bar' => Helper::arabic_w2e($request->input('Bar')),//
                    'SIDE' => Helper::arabic_w2e($request->input('SIDE')),//
                    'MIDDLE' => Helper::arabic_w2e($request->input('MIDDLE')),//
                    'Back' => Helper::arabic_w2e($request->input('Back')),//
                    'CollarSada' => Helper::arabic_w2e($request->input('CollarSada')),//
                    'Shl' => Helper::arabic_w2e($request->input('Shl')),//
                    'SnpIN' => Helper::arabic_w2e($request->input('SnpIN')),//
                    'note' => Helper::arabic_w2e($request->input('note')),//
                    'SlvSada' => Helper::arabic_w2e($request->input('SlvSada')),//
                    'Armhole' => Helper::arabic_w2e($request->input('Armhole')),
                    'FrontChest' => Helper::arabic_w2e($request->input('FrontChest')),
                    'id_cutter' => Helper::arabic_w2e($request->input('id_cutter'))


                ];

                if (Measurement::create($data_measurement)) {
                    Helper::true();
                } else {
                    Helper::false();
                }
                return back();
            });
        }else{
            return back()->withErrors('لا يمكنك إضافة اكثر من قياسين للعميل ');
        }
    }

    public function viewOneMeasurement($id){
        $item = Measurement::FindOrFail($id);
        return view('Customer.viewDetail',compact('item'));
    }


}
