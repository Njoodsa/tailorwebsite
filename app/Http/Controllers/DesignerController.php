<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Http\Requests\TailorDesigner;
use App\Http\Requests\UpdateDesignerTailor;

class DesignerController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    // view all Designer //
    public function view()
    {
      
        $this->data['Designers'] = Employee::findBy(['level' => 2])->get();

        return view('Designer.Designer', $this->data);
    }

    // view  form add new  designer //

    public function AddNewDesigner()
    {
        return view('Designer.AddNewDesigner');
    }

    // insert  new designer //

    public function insertNewDesigner(TailorDesigner $request)
    {

        $data = $request->all();
        $data['level']= 2 ; //2 mean he is designer
        Employee::create($data);
        session()->flash('messageT', 'تمة الاضافه بنجاح  ');
        return back();
    }

    // view information tailor to edit  //

    public function editDesigner($id)
    {

        $Designer = Employee::findBy(['id' => $id])->first();

        return view('Designer.UpdateDesigner', compact('Designer'));
    }

    // view information Designer to update  //

    public function UpdateDesigner(UpdateDesignerTailor $request, $id)
    {
       
        $data = Employee::findOrFail($id);
        $data->update($request->all());
        session()->flash('messageT', 'تم التعديل بنجاح ');
        return back();
    }


   /* public function destroy(Request $request)
    {
        $ids = $request->input('delete_Designer');
        if (empty($ids)) {    // if no select tailor
            session()->flash('messageF', 'يجب اختيار مصمم ');
            return back();

        } else {
            foreach ($ids as $id) {  // tailor Directed by id

                Employee::destroy($id);
            }
            session()->flash('messageT', 'تم حذف المصمم بنجاح ');
            return back();

        }
    }*/

}
