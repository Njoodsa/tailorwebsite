<?php

namespace App\Http\Controllers;

use App\Http\Helper;
use Illuminate\Http\Request;
use App\Employee_data;
use App\Http\Requests\EmployeeProfileRequest;
use App\User;
class EmployeeProfileController extends Controller
{
    //

    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index()
    {

        return view(' page_employee.index');
    }

    public function update(EmployeeProfileRequest $request, $id)
    {
        $user = User::findOrFail(\Auth::user()->id);
        if (empty($request->input('password'))) {
            $user->update(array('email' => $request->Input('email')));
        } else {
            $user->update($request->all());
        }
        $id_employee = $user->employee_datas->id;
        $user_date = Employee_data::findOrFail($id_employee);
        $user_date->update($request->all());
        if ($user_date->update($request->all())) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }
    public function noAccess( )
    {
        return view(' page_employee.EmployeeNoAccess');
    }

}
