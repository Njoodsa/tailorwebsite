<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\Http\Helper;
class ReservationController extends Controller
{


  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Reservation::latest()->paginate(100);
        return view('reservations.index',compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        return view('reservations.show',['reservation' => $reservation]);
    }

    /**
     * update state to تمت الزيارة .
     */
    public function visited(Reservation $reservation)
    {
          Helper::check(  $reservation->update(['state'=>'تمت الزيارة']));
          return back();
    }



}
