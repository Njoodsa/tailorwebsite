<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Neck;
use App\Http\Requests\NeckRequest;
use App\Http\Helper;
use Illuminate\Support\Facades\Cache;
class NeckController extends Controller
{
    public function index()
    {
        $data =  Neck::get() ;
        return view('necks.view', compact('data') );
    }

    public function create()
    {
        return view('necks.create');
    }

    public function insert(NeckRequest $request)
    {
        Neck::create( $request->all()) ?   Helper::true() :  Helper::false();
        Cache::forget('Neck');
        Neck::cache();
        return back();
    }

    public function edit(Neck $data)
    {
        return view('necks.edit', compact('data'));
    }

    public function update(NeckRequest $request, Neck $neck)
    {
        $neck->update($request->all()) ?  Helper::true() :  Helper::false();
        Cache::forget('Neck');
        Neck::cache();
        return back();
    }
}
