<?php

namespace App\Http\Controllers;
use App\Section;
use App\Http\Helper;
use App\Http\Requests\SectionRequest;
class SectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = Section::get();
        return view ('section.view',compact('data'));
    }

    public function create()
    {
        return view ('section.create');
    }
    public function insert(SectionRequest $request)
    {
      $data = $request->all();
      if(Section::create($data))
      {
          Helper::true();
      }else{
          Helper::false();
      }
      return back();
    }
    public function edit($id)
    {
        $data = Section::findOrfail($id);
        return view('section.edit',compact('data'));
    }
    public function update(SectionRequest $request , $id)
    {
        $data = $request->all();
        $item = Section::findOrfail($id);
        if( $item->update($data) )
        {
            Helper::true();
        }else{
            Helper::false();
        }
        return back();
    }
}
