<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order_detail;
use App\Payment_method;
use App\Bill;
class BillController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $bills = Bill::where('state', 0)->get();
        return view('bill.view', compact('bills'));

    }
}
