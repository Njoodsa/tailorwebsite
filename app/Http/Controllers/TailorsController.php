<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\TailorDesigner;
use App\Http\Requests\UpdateDesignerTailor;
use App\Employee;


class tailorsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    // view all tailor //
    public function view()
    {

        $this->data['Tailors'] = Employee::findBy(['level' => 3])->get();
        return view('Tailors.tailors', $this->data);
    }

    // view  form add new  tailor //

    public function AddNewTailor()
    {

        return view('Tailors.AddNewTailor');
    }

    // insert  new tailor //

    public function insertNewTailor(TailorDesigner $request)
    {
        $data = $request->all();
        $data['level']= 3 ;
        Employee::create($data);  // insert number
        session()->flash('messageT', 'تمة الاضافه بنجاح  ');
        return back();
    }

    // view information tailor to edit  //

    public function editTailor($id)
    {

        $Tailor = Employee::findBy(['id' =>$id])->first();

        return view('Tailors.UpdateTailor',compact('Tailor'));
    }

    // view information tailor to update  //

    public function UpdateTailor(UpdateDesignerTailor $request,$id)
    {
        $data = Employee::findOrFail($id);
        $data->update($request->all());
        session()->flash('messageT', 'تم التعديل بنجاح ');
        return back();
    }

    // destroy  tailor  //

   /* public function destroy(Request $request)
    {
        $ids = $request->input('delete_tailor');
        if (empty($ids)) {    // if no select tailor
            session()->flash('messageF', 'يجب اختيار موظف ');
            return back();

        } else {
            foreach ($ids as $id) {  // tailor Directed by id

                Employee::destroy($id);
            }
            session()->flash('messageT', 'تم حذف الموظف بنجاح ');
            return back();

        }
    }*/
}

