<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warehouse;
use App\Http\Helper;
use App\Http\Requests\WarehouseRequest;

class WarehouseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //

    public function index()
    {
        $data = Warehouse::get();
        return view('warehouses.view', compact('data'));
    }


    public function create()
    {
        return view('warehouses.create');
    }


    public function insert(WarehouseRequest $request)
    {
        $data = Warehouse::create($request->all());
        if ($data == true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }


    public function edit($id)
    {
        $data = Warehouse::findOrFail($id);
        return view('warehouses.edit', compact('data'));
    }


    public function update(WarehouseRequest $request, $id)
    {
        $data = Warehouse::findOrFail($id);
        $update = $data->update($request->all());
        if ($update == true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }


}
