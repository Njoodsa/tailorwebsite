<?php

namespace App\Http\Controllers;
use App\Hour;
use App\Http\Helper;
use Illuminate\Http\Request;
use App\Http\Requests\HourRequest;

class HourController extends Controller
{
    public function index()
    {
        $hours = Hour::get();
        return view('hours.index',compact('hours'));
    }

    public function create()
    {
        return view('hours.create');
    }
    public function insert(HourRequest $request)
    {
        Helper::check(Hour::create(request(['hour'])));
        return back();
    }
    public function destroy(Hour $Hour)
    {
        $Hour->delete();
        return back();
    }
}
