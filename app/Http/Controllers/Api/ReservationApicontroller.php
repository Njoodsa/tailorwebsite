<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helper;
use App\Date;
use App\Reservation;
use IlluminateIllumina \Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
class ReservationApicontroller extends Controller
{

      public function __construct()
      {
          $this->middleware('auth:api_customer');
      }

    /**
     * create reservation.
     * @return array
     */
      public function store(Request $request)
      {
          $validator = $this->validator($request);
          if($validator->fails())
            return response()->json(['errors'=>$validator->errors()]);
          $date_hour = Helper::transformDateToTimestamp(request('date'),request('time'));
          if($this->CheckIfdateOrTimeBlocked($date_hour))
            return $this->CheckIfdateOrTimeBlocked($date_hour);
          $createReservation = auth('api_customer')->user()->reservations()->create($request->all() +
              ['date_time'=>$date_hour,'state'=>'مؤكد']);
          //$this->confirmedReservationSMS();
          return $createReservation ;
      }

      /**
       * Display customer reservations .
       *
       * @param  int  $id customer
       */
      public function showCustomerReservations ($id)
      {
       return response()->json([Reservation::whereIdCustomer($id)->latest()->get()]);
      }

      /**
       * Update the specified resource in storage.
       */
      public function update(Request $request, Reservation $reservation)
        {
          if($reservation->id_customer != auth('api_customer')->user()->id)
             return  response()->json(['The page not found ']);
          $validator =  $this->validator($request);
          if($validator->fails())
             return response()->json(['errord'=>$validator->errors()]);
          $date_hour = Helper::transformDateToTimestamp(request('date'),request('time'));
          if($date_hour != date($reservation->date_time))
              if($this->CheckIfdateOrTimeBlocked($date_hour)){
                 return $this->CheckIfdateOrTimeBlocked($date_hour);}
          $reservation->update($request->all() + ['date_time'=>$date_hour]);
         return $reservation;
      }

      /**
       * Show the form for editing the specified resource.
       */
      public function edit($id)
      {
        return Reservation::findOrFail($id);
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  model  $reservation
       * @return \Illuminate\Http\Response
       */
      public function cancel(Reservation $reservation)
      {
        if($reservation->id_customer != auth('api_customer')->user()->id)
          return  response()->json(['The page not found ']);
          $reservation->update(['state'=>'ملغي']);
          return response()->json(['code' => 200 , 'message' => 'تم إلغاء الحجز ']);
      }

      /**
       * Check if date blocked from admin befor customer reservation.
       *
       */
      protected function CheckIfdateOrTimeBlocked($date_hour)
      {
        $dateTimeBlocked  = Date::select('date','type')
                            ->whereRaw('DATE(date) = ?', [request('date')])
                            ->get();
        $blockedAllDay    = $dateTimeBlocked->where('type', 1);
        if(!$blockedAllDay->isEmpty())
          return response()->json(['code' => 404, 'message' => 'يرجى اختيار يوم اخر ']);
        $blockedTimeOfDay = $dateTimeBlocked->where('date',$date_hour);
        if(!$blockedTimeOfDay->isEmpty())
          return response()->json(['code' => 404, 'message' => 'يرجى اختبار وقت اخر']);
        return $this->CheckIfTimeReserved($date_hour);
      }

      /**
       * Check if time is reserved from other customer.
       *
       */
      protected function CheckIfTimeReserved($date_hour)
      {

        $PreviouslyReservedTime = Reservation::where([['date_time',$date_hour],['state','مؤكد']])
                                   ->get();

        if(!$PreviouslyReservedTime->isEmpty())
           return response()->json(['code' => 409, 'message' => ' الوقت الذي تم اختياره محجوزمسبقاً ']);
      }


      /**
       * Get a validator for an incoming reservation request.
       * @param  array  $request
       *
       */

      public function validator($request)
      {
        $validator = \Validator::make($request->all(), [
           'name' => 'required|string|min:3|max:20',
           'phone'=>['required','regex:/^05[0-9]{8}$/'],
           'lan'  => 'required|string',
           'lat'  => 'required|string',
           'date' => 'required|date|after:today|date_format:Y-m-d',
           'time' => 'required|date_format:g:i|exists:hours,hour',
        ]);
         return $validator;
      }

      protected function confirmedReservationSMS()
      {
        $phone = request('phone');
        $date  = request('date');
        $time  = request('time');
        $msg = urlencode("تم تاكيد حجزك ليوم :$date الوقت :$time
        ماوان للخياطة الرجالية ");
        file_get_contents("http://www.mobily.ws/api/msgSend.php?mobile=966596591130&password=77885544NJODsa&numbers=$phone&sender=Mawan&msg=".$msg."&applicationType=68&68&lang=3");
          
      }

}
