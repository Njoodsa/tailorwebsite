<?php

namespace App\Http\Controllers\Api\Auth;
use App\Http\Controllers\Controller;
use IlluminateIllumina\Support\Facades\Validator;
use App\MobileNumbers;
use Illuminate\Http\Request;
use App\Http\ApiHelper;
class ResetPasswordBySMSController extends Controller
{
  /**
   *
   *
   * @return void
   */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function __invoke($id)
    {
        return view('user.profile',
            [                            'user' =>   User::findOrFail($id)]
        );
    }

    public function ResetsPasswords()
    {
      if($this->validationPhone()) return $this->validationPhone();
      $FindPhone = MobileNumbers::wherephone(request('phone'))->first();
      if(!$FindPhone)
         return response()->json(['code' => 404, 'message' => 'رقم الجوال غير مسجل']);
      session(['mobileSession'=>request('phone')]);
      ApiHelper::sendSMSCode(request('phone'));
      return [ 'code' => 200 , 'phone'=>session('mobileSession'),'message' => 'تم ارسال رمز التحقق الي جوالك '];
    }

    protected function validationPhone()
    {
      $validator = \Validator::make(request()->all(),
      ['phone'=>['regex:/^05[0-9]{8}$/','required']]);
      if($validator->fails())
         return response()->json(['error'=>$validator->errors()]);
    }


    public function ResendCod($phone)
    {
          ApiHelper::sendSMSCode($phone);
    }

    public function confirmed()
    {
      if($this->validationSmsCode())return $this->validationSmsCode();
        if(request('mobile_code') != session('sms_code'))
            return response()->json(['code' => 404 , 'message' => 'رمز التحقق غير صحيح ']);
        return response()->json(['code' => 200 , 'message'=>'succuss']);
    }

    public function updatePasswords(Request $request)
    {

      if(!session('mobileSession'))
         return  response()->json(['error'=>'something wrong, try again ']);
      if($this->validationPassword()) return $this->validationPassword();
        $FindPhone = MobileNumbers::wherephone(session('mobileSession'))->first();
        $FindPhone->password = bcrypt(request('password'));
         if($FindPhone->save())
          return (['code' => 200 ,'message' => 'تم تحديث كلمة المرور بنجاح يمكنك تسجيل الدخول الان']);
        else
          return (['message' => 'حدث خطاء الرجاء المحاولة مرة اخرى ']);

    }

    protected function validationSmsCode()
    {
      $validator = \Validator::make(request()->all(),
      ['mobile_code' => 'required|numeric',]);
      if($validator->fails())
      return response()->json(['errors'=>$validator->errors()]);
    }
    protected function validationPassword()
    {
      $validator = \Validator::make(request()->all(),[
            'password'=>'required|min:6|string ',
        ]);

        if($validator->fails())
        return response()->json(['error'=>$validator->errors()]);
    }

}
