<?php

namespace App\Http\Controllers\Api\Auth ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use IlluminateIllumina \Support\Facades\Validator;
class LoginCustomerController extends Controller
{
  /*
 |--------------------------------------------------------------------------
 | Login Controller
 |--------------------------------------------------------------------------
 |
 | This controller handles authenticating users for the application and
 | redirecting them to your home screen. The controller uses a trait
 | to conveniently provide its functionality to your applications.
 |
 */
  /**
   * Handle a login request to the application.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
   */


  public function login(Request $request)
  {
    $validator = \Validator::make($request->all(), [
      'phone'=>['required', ' regex:/^05[0-9]{8}$/'],
      'password'=>'required | string | min:6'
    ]);
    if($validator->fails())
      return response()->json(['errors'=>$validator->errors()]);


      if($this->attemptLogin($request)) {
          $this->attemptLogin($request);
          $user = $this->guard()->user();
           $user->generateToken();
          return (['api_token'=>$user['api_token'],'phone'=>$user['phone'],['id' => $user['id'] ]]);
      }

      return response()->json(['errors'=>'تاكد من البيانات المدخله ']);

  }

  protected function validateLogin($request)
  {
    $validator = \Validator::make($request->all(), [
      'phone'=>['required', ' regex:/^05[0-9]{8}$/'],
      'password'=>'required | string | min:6'
    ]);
    if($validator->fails())
      return response()->json(['errors'=>$validator->errors()]);
  }



  public function attemptLogin(Request $request)
  {
      return $this->guard()->attempt(
          $this->credentials($request)
      );
  }

  protected function guard()
  {
     return Auth::guard('customer');

  }
  protected function credentials(Request $request)
  {
      return $request->only('phone', 'password');
  }


    public function logout(Request $request)
    {
       $user =  Auth::guard('api_customer')->user();
         if ($user) {
            $user->api_token = null;
            $user->save();
          }
      return response()->json(['data' => 'User logged out.'], 200);

  }
  
}
