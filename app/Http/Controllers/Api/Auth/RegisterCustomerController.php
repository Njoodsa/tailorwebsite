<?php

namespace App\Http\Controllers\Api\Auth;
use App\Customer;
use Illuminate\Http\Request;
use App\MobileNumbers;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use IlluminateIllumina \Support\Facades\Validator;
use App\Http\ApiHelper;
class RegisterCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
    * @return
    * send sms code  to customer
    *
    **/

    public function store(Request $request)
    {
        if($this->validateInformation()) return $this->validateInformation();
        $this->setPublicSession();

        $FindPhone = MobileNumbers::wherephone(request('phone'))->first();
        if((request('type') != "formerCustomer") && (request('type') != "newCustomer"))
            return response()->json(' Errore');
        if(request('type') == 'formerCustomer'){
            if (!$FindPhone)
                return response()->json(['code' => 404, 'message' => ' رقم الجوال غير موجود']);
            if($FindPhone->password)
                return response()->json(['code' => 409, 'message' => 'رقم الجوال مسجل مسبقاً يمكنك تسجيل الدخول']);
        }else{ // newCustomer
            if ($FindPhone)
                return response()->json(['code' => 409, 'message' => 'رقم الجوال  موجود  ']);
            if($this->validationName()) return $this->validationName();
                  session(['userName' => request('name')]);
                  session(['type' => 1 ]); // 1 = new customer to check when
                                              // insert user
        }
        ApiHelper::sendSMSCode(request('phone'));
        return ['code'=>200 ,'sms_code' => session('sms_code'),  'message'=>'تم ارسال رمز التفعيل الى رقم الجوال '];
    }

    /**
    * @return  void
    * re-send sms code  to customer
    *
    **/
    public function ResendCod($phone)
    {
        ApiHelper::sendSMSCode($phone);
    }
    /**
    * @return  message
    * store customer in db after confirm code
    *
    **/
    public function confirmed()
    {
        return DB::transaction(function (){
          if($this->validationSmsCode()) return $this->validationSmsCode();
        if(request('mobile_code') != session('sms_code'))
            return response()->json(['code' => 404, 'message' => 'رمز التحقق غير صحيح ']);
            if(session('type') != 1) {
            $done = MobileNumbers::wherephone(session('mobile'))->update(['password' => session('password')]);
        }else{
           if(MobileNumbers::wherephone(session('mobile'))->first())
            return ['code' => 409, 'message' => 'تم تسجيل رقم الجوال مسبقاً '];
            $createMobile = MobileNumbers::create(['phone'=>session('mobile'),'password'=>session('password') ]);
            $done = $createMobile->addCustomer(['name'=>session('userName'),'MobileNumbers_id'=>$createMobile->id]);
        }
         if($done ){ return ['code' => 201 ,'message' => 'تم تسجيلك بنجاح يمكنك تسجيل الدخول الان '];}
      });

    }

    protected function validationSmsCode()
    {
      $validator = \Validator::make(request()->all(),
      ['mobile_code' => 'required|numeric',]);
      if($validator->fails())
      return response()->json(['errors'=>$validator->errors()]);
    }

    protected function validateInformation()
    {
    $validator = \Validator::make(request()->all(),[
            'phone'=>['regex:/^05[0-9]{8}$/', 'required'],
            'password'=>'required|min:6|string ',
        ],['phone.regex' =>'ادخل رقم الهاتف بشكل صحيح / 05999999999',
            'phone.required' =>'ادخل رقم الجوال']);

            if($validator->fails())
            {
              return response()->json(['errors'=>$validator->errors()]);
            }
    }

    protected function validationName(){
      $validator =  \Validator::make(request()->all(),[
            'name'=>'required|string|max:20|min:4'
          ],['name.required' =>'الاسم مطلوب',
            'name.string' =>'ادخل الاسم بشكل صحيح ']);
            if ($validator->fails())
              return response()->json(['errors'=>$validator->errors()]);

    }

    protected function setPublicSession()
    {
      session(['mobile' => request('phone')]);
      session(['password' =>bcrypt(request('password'))]);
    }


}
