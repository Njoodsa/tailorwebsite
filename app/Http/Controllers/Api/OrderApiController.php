<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helper;
use App\Measurement;
use App\Measurement_order;
use App\Order;
use App\Reservation;
use App\Customer;
use App\warehouses_fabric;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\ProductToOrderRequest;
use App\Order_detail;
use App\Bill;
use DB;
USE App\Http\Resources\OrderInformationRescorce;
USE App\Http\Resources\api\customer\ListOrderInformationCollection;
USE App\Http\Resources\OrderDetailResource;
use App\Order_Information;
use Illuminate\Support\Facades\Input;
use IlluminateIllumina \Support\Facades\Validator;
USE App\Http\Resources\api\ListOrderInformationCollection as ListOrderInformationForEmployee;

class OrderApiController extends Controller
{
  /**
  *
  * @return array  order information
  * view list of order state ending the lifting of customer's measurement
  */
    public function ViewTheOrderList()
    {

        $Order_details = Order_Information::select('id', 'id_customer', 'id_bill', 'created_at')
            ->where([['state', 2], ['type', 'fabirc']])
            ->with(['customer'
                => function ($query){$query->select('id','name','MobileNumbers_id');},
                'customer.mobile'
                => function ($query) {  $query->select('id','phone');},
                'bill.order'
                => function ($query) {$query->select('Bill_id','number')
                  ->pluck('Bill_id'); }
                ])
            ->latest()->get();
        return ListOrderInformationForEmployee::collection($Order_details);
    }

    /**
    *
    * @return array  detals order , order information , bill , order
    *
    */
    public function edit($id)
    {

        $data = Order_Information::select('id', 'id_customer', 'id_bill')
            ->with(
                ['customer.mobile' => function ($query) {
                    $query->select('id', 'phone');
                },
                    'order_detail.measurement', 'bill.order', 'order_detail.design', 'order_detail.sleeve'
                    , 'order_detail.neck', 'order_detail.pocket'])
            ->where([['state', 2], ['type', 'fabirc']])
            ->find($id);

        if (!$data) return response()->json('error, order not found ');
        return new OrderInformationRescorce($data);

    }

    /**
    *
    * @return message  success
    * update measurement and insert or update yeardh
    * update state of information to in work
    */
    public function updateMeasurement_order($id, Request $request)
    {
        // order id for order to bill

        $basic_order_information = order_information::where([['state', 2], ['type', 'fabirc']])->find($id);

        if (!$basic_order_information) return response()->json('erroe, the order not found ');

        return DB::transaction(function () use ($basic_order_information, $request) {
            if ($this->validateYeardh()) return $this->validateYeardh();

            if ($this->validateMeasurement()) return $this->validateMeasurement();

            for ($i = 0; $i < count(request('order_id')); $i++) {
                 $yards     = Helper::arabic_w2e(request('yards'))[$i];

                $if_error = null;
                $order = Order::findOrFail(request('order_id')[$i]);

                // if user do not add yeardh yet

                if ($order->yards == null) {
                    $total_yeardh = $yards * $order->number;
                    $if_error = warehouses_fabric::decrementWarehouses($order->id_fabric, 'fabirc', $total_yeardh);


                } elseif ($order->yards != null) {

                    $oldQuantity = $order->number * $order->yards; // check if change yeard
                    $Quantity = $yards * $order->number;
                    $if_error = warehouses_fabric::checkWarehouses($order->id_fabric, $order->id_fabric, $Quantity, $oldQuantity, 'fabirc');

                }

                if ($if_error == 'error') {
                    return response()->json('رصيد المخزون لا يكفي لعدد الياردات المدخلة ');
                }

                $order->update(['yards' => $yards]);

            }

            // measurement
            for ($i = 0; $i < count(request('measurement_id')); $i++) {
                $update_measurement = Measurement_order::findOrFail(request('measurement_id')[$i]);
                $update_measurement->updateMeasurement_order($i);
            }
            // update order information
            $basic_order_information->updateOrderInformation(['state' => 3]);
            return response()->json('success');

        });
    }


    public function validateYeardh()
    {

        $validator = \Validator::make(request()->all(), [
            "yards.*" => ' required |arabic_numbers ',
        ], [
            'yards.required' => ' ادخل  عدد الياردات ',
            'yards.arabic_numbers' => ' ادخل  عدد الياردات ',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

    }

    public function validateMeasurement()
    {
        $validator = \Validator::make(request()->all(), [
            'Length.*' => 'required|numeric',
            'ShlR.*' => 'required|numeric',
            'ShlL.*' => 'required |numeric',
            'Slv_w.*' => 'required|numeric',
            'Chest.*' => 'required|numeric',
            'Waist.*' => 'required|numeric',
            'Bottom.*' => 'required|numeric',
            'CollarGalab.*' => 'required|numeric',
            'FrontLonger.*' => 'required|numeric',
            'FakeButton.*' => 'required|numeric',
            'Cuffs.*' => 'required|string',
            'ElbowSlvSada.*' => 'required|numeric'
            , 'Sada.*' => 'required |string',
            'Bar.*' => 'required |string',
            'SIDE.*' => 'required|numeric',
            'MIDDLE.*' => 'required |numeric',
            'Back.*' => 'required |numeric',
            'CollarSada.*' => 'required|string',
            'SnpIN.*' => 'required|numeric',
            'SlvSada.*' => 'required|numeric',
            'Shl.*' => 'required|numeric',
            //  'ElbowCuffs'=>'required|numeric',
            'Armhole.*' => 'required|numeric',
            'FrontChest.*' => 'required|numeric',
            'HIPS.*' => 'required|numeric',
            'id_cutter.*' => 'required|numeric'

        ], [
            'required' => ' ادخل القياسات  ',
            'numeric' => 'ادخل القياسات بشكل صحيح',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
    }



                  //---------(  methods for customer  )----------//


    /**
    * methods for customer
    * id_customer : Registered customers with the same mobile number
    *
    */

    public function orders_customer()
    {
       $id_customers = $this->getCustomerIDsWithSamePhone()->pluck('id');
       $findOrder    = Order_Information::whereIn('id_customer',$id_customers)
       ->with(['bill'
       =>function($q){$q->select('id','price','discount');}])
       ->get();
       if($findOrder)
       return ListOrderInformationCollection::collection($findOrder);
    }

    /**
    *
    *  @return array
    * $id : id order
    *
    */
  public function detail_order($id)
  {
     $order = Order_Information::with('bill.order')->findOrFail($id);

     return new OrderDetailResource($order);
  }

  /**
  *
  * @return array
  * home page : get all order , current order
  */
  public function totalCustomerOrder()
  {
     $id_customers = $this->getCustomerIDsWithSamePhone()->pluck('id');
    $names         = $this->getCustomerIDsWithSamePhone()->pluck('name');
    $orders        = Order_Information::whereIn(
                       'id_customer',$id_customers
                     )->orderBy('created_at','desc')->get();
    $lastReservation   = Reservation::where('id_customer',auth('api_customer')->user()->id)
     ->orderBy('created_at','desc')->first();
    return [ 'details' => $this->organizeInformationInArray($orders ,$lastReservation) , 'names' => $names ];

  }

  /**
  *
  * @return IDs customer
  * all customers register in mawan by the same mobile number
  */

  public function getCustomerIDsWithSamePhone()
  {
    return Customer::select('id','name')->
    where('MobileNumbers_id',1)->get(); //auth('api_customer')->user()->id
  }

  /**
  *
  * @return array
  *  all customers register in mawan by the same mobile number
  * [0] :  last order , we get order as desc
  */
  public function organizeInformationInArray($p_orders ,$p_reservation)
  {
    $arr = [] ;
    if(!$p_orders->isEmpty())
        $arr = ['order_number'=>$p_orders->count(),
             'current_order' =>$p_orders->whereNotIn('state',[5,6,7])->count(),
             'last_order_date'=> $p_orders[0]->created_at,'last_visit'=>null
           ];
    if($p_reservation)
         $arr['last_visit'] = $p_reservation->date_time->format('Y-m-d');
    return $arr ;
  }


}
