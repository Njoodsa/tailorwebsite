<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Http\Helper;
use App\Measurement;
use DB;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\MeasurementsRequest;
use App\MobileNumbers;
use App\Employee_data;
class CustomerApiController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth:api');
    }

    public function index()
    {
        $data = Customer::with('mobile')->get();
        return $data;
    }


    public function insert(CustomerRequest $request, MeasurementsRequest $myRequest)
    {
        return DB::transaction(function () use ($request, $myRequest) {

            $phone = $request->input('phone');
            $name = $request->input('name');
            if ($request->input('select') == 'old') {// if the number is exist
                $number = MobileNumbers::select('id')->where('phone', $phone)->first();
                if ($number) {
                    $number_id = $number->id;
                    $find_customer = Customer::where([['MobileNumbers_id', $number_id], ['name', $name]])->first();
                    if ($find_customer) {
                        return response()->json(['العميل موجود مسبقاً '], 400);
                    }
                } else { // if the number of customer does not exist
                    return response()->json(['لرقم غير موجود مسبقاًً '], 400);
                }
            } elseif ($request->input('select') == 'new') { // if customer is new
                $this->validate($request, [
                    'phone' => 'unique:mobile_numbers,phone'], ['unique' => 'الرقم موجود مسبقاً'
                ]);
                $create_number = MobileNumbers::create([
                    'phone' => $phone,
                ]);
                $number_id = $create_number->id;
            }
            $data_customers = [
                'name' => $name,
                'MobileNumbers_id' => $number_id,
                'note' => $request->input('note'),
            ];
            $id_customer = Customer::create($data_customers);
            $this->addMeasurment($myRequest, $id_customer->id);

            if ($id_customer) {
                Helper::true();
            } else {
                Helper::false();
            }
            return response()->json($id_customer, 201);
            // return redirect('newMeasurement/'.$id_customer->id);
        });

    }

    public function addMeasurment($myRequest, $id)
    {

        $data_measurement = [
            'Length' => $myRequest->input('Length'),//
            'ShlR' => $myRequest->input('ShlR'),//
            'ShlL' => $myRequest->input('ShlL'),//
            'Slv_w' => $myRequest->input('Slv_w'),//
            'Chest' => $myRequest->input('Chest'),//
            'Waist' => $myRequest->input('Waist'),//
            'Bottom' => $myRequest->input('Bottom'),//
            'CollarGalab' => $myRequest->input('CollarGalab'),//
            'FrontLonger' => $myRequest->input('FrontLonger'),//
            'FakeButton' => $myRequest->input('FakeButton'),//
            'ElbowCuffs' => 1,//
            'Cuffs' => $myRequest->input('Cuffs'),//
            'ElbowSlvSada' => $myRequest->input('ElbowSlvSada'),//
            'Sada' => $myRequest->input('Sada'),//
            'Bar' => $myRequest->input('Bar'),//
            'SIDE' => $myRequest->input('SIDE'),//
            'MIDDLE' => $myRequest->input('MIDDLE'),//
            'Back' => $myRequest->input('Back'),//
            'CollarSada' => $myRequest->input('CollarSada'),//
            'Shl' => $myRequest->input('Shl'),//
            'SnpIN' => $myRequest->input('SnpIN'),//
            'HIPS' => $myRequest->input('HIPS'),//
            'note' => $myRequest->input('note'),//
            'SlvSada' => $myRequest->input('SlvSada'),//
            'customer_id' => $id,
            'Armhole' => $myRequest->input('Armhole'),
            'FrontChest' => $myRequest->input('FrontChest'),
            'id_cutter' => $myRequest->input('id_cutter')
        ];
        Measurement::create($data_measurement);
    }


    public function edit($id)
    {
        $data = Customer::with('measurement.cutters')->FindOrFail($id);
        $cutters = Employee_data::cache_cutter();
        return ['customer'=>$data, 'cutters'=>$cutters];
    }

    public function update_information(CustomerRequest $request, $id)
    {

        $data = Customer::FindOrFail($id);
        $phone = $data->MobileNumbers_id;
        $number = MobileNumbers::FindOrFail($phone);
         $this->validate($request, [
            'phone' => 'unique:mobile_numbers,phone,' . $phone,
        ]);


        $number->phone = $request->input('phone');
        $number->save();
        $data->name = $request->input('name');
        $data->note = $request->input('note');
        $data->save();
        return response()->json($data, 200);
    }

    public function update_measurement(MeasurementsRequest $request, $id)
    {

        // ID measurement = id_meas
        $measurement_update = Measurement::findOrfail($id);
        $measurement_customer = [

            'Length' => $request->input('Length'),//
            'ShlR' => $request->input('ShlR'),//
            'ShlL' => $request->input('ShlL'),//
            'Slv_w' => $request->input('Slv_w'),//
            'Chest' => $request->input('Chest'),//
            'Waist' => $request->input('Waist'),//
            'Bottom' => $request->input('Bottom'),//
            'HIPS' => $request->input('HIPS'),//
            'CollarGalab' => $request->input('CollarGalab'),//
            'FrontLonger' => $request->input('FrontLonger'),//
            'FakeButton' => $request->input('FakeButton'),//
            'ElbowCuffs' => 1,//
            'Cuffs' => $request->input('Cuffs'),//
            'ElbowSlvSada' => $request->input('ElbowSlvSada'),//
            'Sada' => $request->input('Sada'),//
            'Bar' => $request->input('Bar'),//
            'SIDE' => $request->input('SIDE'),//
            'MIDDLE' => $request->input('MIDDLE'),//
            'Back' => $request->input('Back'),//
            'CollarSada' => $request->input('CollarSada'),//
            'Shl' => $request->input('Shl'),//
            'SnpIN' => $request->input('SnpIN'),//
            'SlvSada' => $request->input('SlvSada'),
            'note' => $request->input('note'),//
            'Armhole' => $request->input('Armhole'),
            'FrontChest' => $request->input('FrontChest'),
            'id_cutter' => $request->input('id_cutter')

        ];

        $true = $measurement_update->update($measurement_customer);
        return response()->json($true, 200);
    }

    public function search_name(Request $request)
    {
         $this->validate($request, [
            'search' => 'string|required'], ['string' => 'ادخل الاسم بشكل الصحيح '
        ]);
        $data = Customer::with(array('mobile'=>function($q)  {
            $q->select('id','phone');}))->select('name','created_at','id','note','MobileNumbers_id')->where('name', 'like', '%' . $request->input('search') . '%')->get();
        return $data;

    }


    // search numbber
    public function search(Request $request){
       $this->validate($request,[
            'search'=>'numeric|digits:10 |required'],['numeric'=>'ادخل الرقم بشكل الصحيح'
            ,'digits'=>'يجب ان يتكون الرقم من ١٠ ارقام '
        ]);

        $data = MobileNumbers::where('phone',$request->search)->first();
        return $data;
    }

    public function update_his_information()
    {

    }
}
