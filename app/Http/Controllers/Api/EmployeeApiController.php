<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Helper;
use App\Employee_data;
use App\Http\Requests\EmployeeProfileRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
class EmployeeApiController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth:api');
    }
    public function index()
    {
        return Auth::user()->employee_datas;
    }

    public function update(EmployeeProfileRequest $request, $id)
    {
        $user = User::findOrFail($id);
        if (empty($request->input('password'))) {
            $user->update(array('email' => $request->Input('email')));
        } else {
            $user->update($request->all());
        }
        $id_employee = $user->employee_datas->id;
        $user_date = Employee_data::findOrFail($id_employee);
        $user_date->update($request->all());
        if ($user_date->update($request->all())) {
            Helper::true();
        } else {
            Helper::false();
        }
       return $user_date ;
    }

}
