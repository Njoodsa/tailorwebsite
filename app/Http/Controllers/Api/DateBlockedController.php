<?php

namespace App\Http\Controllers\api;

use App\Date;
use App\Http\Helper;
use Illuminate\Http\Request;
use App\Hour;
use Carbon\Carbon;
use App\Http\Requests\DateRequest;
use App\Reservation;
use App\Http\Controllers\Controller;

class DateBlockedController extends Controller
{
    
    /**
    * @return array
    * get the days , times blocked or reserved to disable it.
    *
    **/
    public function index()
    {
      $today           = Carbon::today();

      $hoursAvailablly = Hour::all()->pluck('hour');

      $daysBlocked     = Date::select('date')->where('date','>=',$today)
                              ->whereType(1)->pluck('date') // 1 all day blocked
                              ->map(function ($date) {
                              return $date->format('Y-m-d');
                              });

      $daysTimeBlocked = Date::select('date')->where('date','>=',$today)
                               ->whereType(0)->pluck('date')
                               ->map(function ($date) {
                                  return $date->format('Y-m-d g:i');
                               });

       $reserved       = Reservation::whereDate('date_time','>=',$today)->pluck('date_time')
                            ->map(function ($date) {
                               return $date->format('Y-m-d g:i');
                              });
        return [
          'hours available'     =>$hoursAvailablly,
          'all day bloked'      =>$daysBlocked,
          'Time of day blocked' =>$daysTimeBlocked,
          'reserved'            =>$reserved
        ];

    }

}
