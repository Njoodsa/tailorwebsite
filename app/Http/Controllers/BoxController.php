<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Box;
use App\Http\Helper;
use App\Branch;
use App\Http\Requests\BoxRequest;
class BoxController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data         = Box::get();
        return view('boxes.view', compact('data'));
    }


    public function create()
    {
        $branches   =Branch::get();
        return view('boxes.create', compact('branches'));
    }


    public function insert(BoxRequest $request)
    {
        $data = Box::create($request->all());
        if ($data == true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }


    public function edit($id)
    {
        $data = Box::findOrFail($id);
        $branches   = Branch::get();
        return view('boxes.edit', compact('data', 'branches'));
    }


    public function update(BoxRequest $request, $id)
    {
        $data = Box::findOrFail($id);
        $update = $data->update($request->all());
        if ($update == true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }

}
