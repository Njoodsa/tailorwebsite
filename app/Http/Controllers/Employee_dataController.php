<?php

namespace App\Http\Controllers;
use App\Employee_data;
use App\Employee_type;
use App\Http\Requests\Employee_dataRequest;
use App\Http\Requests\Employee_dataUpdateRequest;
use App\Http\Helper;
use Illuminate\Support\Facades\Cache;
class Employee_dataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = Employee_data::get();
        return view('employee_data.view', compact('data'));
    }

    public function create()
    {
        $data = Employee_type::get();
        return view('employee_data.create', compact('data'));
    }

    public function insert(Employee_dataRequest $request)
    {

        $data = Employee_data::create($request->all());
        if($request->input('employee_type_id') == 1){
            Cache::forget('cache_tailors');
            Employee_data::cache_tailor();
        }elseif($request->input('employee_type_id') == 4){
            Cache::forget('cache_designs');
            Employee_data::cache_cutter();
        }elseif($request->input('employee_type_id') == 2) {
            Cache::forget('cache_cutters ');
            Employee_data::cache_design();
        }
        if ($data == true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }

    public function edit($id)
    {
        $data = Employee_data::findOrFail($id);
        $employee_types = Employee_type::get();
        return view('employee_data.edit', compact('data','employee_types'));
    }

    public function update(Employee_dataRequest $request, $id)
    {
        $data = Employee_data::findOrFail($id);

        if($request->input('employee_type_id') == 1){
            Cache::forget('cache_tailors');
            Employee_data::cache_tailor();
        }elseif($request->input('employee_type_id') == 4){
            Cache::forget('cache_designs');
            Employee_data::cache_cutter();
        }elseif($request->input('employee_type_id') == 2) {
            Cache::forget('cache_cutters');
            Employee_data::cache_design();
        }
        $update = $data->update($request->all());
        if ($update == true) {
            Helper::true();
        } else {
            Helper::false();
        }
        return back();
    }
}
