<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OrdersCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request){

    if($this->type == 'product'){
      return[
          'price_item'=>$this->price,
          'product'=>$this->products->name,
          'type'=>$this->type,
          'number'=>$this->number,
        ];

    }else{

      return[
          'price_item'=>$this->price,
          'fabric'=>$this->fabrics->name,
          'type'=>$this->type,
          'number'=>$this->number,
        ];

}
    }
  }
