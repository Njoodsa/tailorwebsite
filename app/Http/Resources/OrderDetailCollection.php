<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OrderDetailCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'Style'         => $this->design->name,
          'Sleeve'        => $this->sleeve->name,
          'Neck'          => $this->neck->name,
          'Pocket'        => $this->pocket->name,
          'button_neck'   => $this->button_neck,
          'Bar'           => $this->button->name,
          'Inside pocket' => $this->detail_pocket,
          'Filling button'=> $this->button_detail,
          'Filling collar'=> $this->collar_detail,
          'Fabric'        => $this->fabric->name,
          'measurement'   => $this->measurement,
          'cutter name'   => $this->measurement->cutters->name,
        ];
    }
}
