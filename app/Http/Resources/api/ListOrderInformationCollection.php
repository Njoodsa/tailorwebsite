<?php

namespace App\Http\Resources\api;

use Illuminate\Http\Resources\Json\Resource;

class ListOrderInformationCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id_order'      => $this->id,
          'created'       => $this->created_at,
          'Number of Thop'=> $this->bill->order->sum('number'),
          'customer name' => $this->customer->name,
          'customer phone'=> $this->customer->mobile->phone,
        ];
    }
}
