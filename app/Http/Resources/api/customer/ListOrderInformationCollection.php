<?php

namespace App\Http\Resources\api\customer;

use Illuminate\Http\Resources\Json\Resource;

class ListOrderInformationCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        'id_order' => $this->id,
        //'state' => $this->state,
        $this->mergeWhen($this->state == 1, [
           'state' => 'تحت المعالجة',
       ]),
       $this->mergeWhen($this->state == 3, [
          'state' => 'قيد العمل',
       ]),
       $this->mergeWhen($this->state == 4, [
          'state' => 'بإنتظار العميل لإستلام الطلب',
       ]),
       $this->mergeWhen($this->state == 2, [
          'state' => 'تم رفع القياسات',
       ]),
       $this->mergeWhen($this->state == 5, [
          'state' => 'ملغي',
       ]),
       $this->mergeWhen($this->state == 6, [
          'state' => 'تم تسليم الطلب',
       ]),
       $this->mergeWhen($this->state == 7, [
          'state' => 'طلب غير مكتمل',
       ]),
        'date' => $this->created_at,
        'price' => number_format((($this->bill->price + ($this->bill->price * 0.05)) - $this->bill->discount)),

      ];
    }
}
