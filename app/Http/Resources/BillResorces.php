<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BillResorces extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'paid_up'         => $this->paid_up,
          'Residual'        => $this->Residual,
          'price'           => $this->price,
          'payment method'  => $this->payment_method->name,
          'delivery date'   => $this->end_date,
          'order'           => Orders::collection($this->order)

          ];
    }
}
