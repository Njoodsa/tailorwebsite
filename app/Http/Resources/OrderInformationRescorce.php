<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderInformationRescorce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      
        return [

          'order_Information_id' => $this->id,
          'customer_name'        => $this->customer->name  ,
          'phone'                => $this->customer->mobile->phone,
          'order_detail'         => OrderDetailCollection::collection($this->order_detail),
          'bill'                 => new BillResorces($this->bill),
        ];
    }
}
