<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Orders extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'order_id'      => $this->id,
        'addition'      => $this->additions->name,
        'addition price'      => $this->additions->price,
        'fabric'        => $this->fabrics->name,
        'yards'         => $this->yards,
        'price'         => $this->price,
        'number'        => $this->number,
        'type'        => $this->type,
        'number column' => $this->number_column,

      ];
    }
}
