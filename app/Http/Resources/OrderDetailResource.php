<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'order_id'   =>$this->id,
        'total_price'=>number_format((($this->bill->price + ($this->bill->price * 0.05)) - $this->bill->discount)),
        'date'       =>$this->created_at,
        'type_order' =>$this->type,
        'order'      =>  OrdersCollection::collection($this->bill->order),
      ];
    }
}
