<?php

namespace App\Http;
use Storage;
class Helper
{



    function flash_success($message)
    {
        session()->flash('message_success', $message );
    }

    function flash_fail($message)
    {
        session()->flash('message_fail', $message );
    }

    function flash_if_success_or_fail($item)
    {
        $item ? flash_success('تمت العملية بنجاح ') : flash_fail('هناك خطاء يرجى المحاولة في وقت اخر ' );
    }





    public static function true (){

        session()->flash('messageT', 'تمة العمليه بنجاح  ');
    }
    public static function false (){

        session()->flash('messageF', 'حدث خطا   ');

    }

    public static function check($success)
    {
        ($success? Helper::true():Helper::false());
    }
    public static function image($file , $images,$place ){

        storage::disk('s3')->put($place .  $file ,file_get_contents($images->getRealPath()));

    }

    public static function arabic_w2e($str)
    {
        $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($arabic_eastern,$arabic_western, $str);
    }



    public static function stateOrder()
    {
          return (request('finally_price') == (request('paid_up')+ request('paid_up_card') ) && request('Residual') == 0) ?
               ['state_bill'=> 1, 'state_order'=> 6] : ['state_bill'=> 0, 'state_order'=> 7];
    }

    public static function transformDateToTimestamp($p_date , $p_hour)
    {
      return date('Y-m-d H:i:s', strtotime($p_date.''.$p_hour));
    }


}
