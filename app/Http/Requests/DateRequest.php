<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $today= now()->format('Y-m-d');
        return [
            'date'=>['required','date','date_format:Y-m-d','unique:dates,date','after:today'],
            'hour'=>['nullable','regex:/^(([1-9]|1[0-2]):[0-59]{2}$)/']
        ];
    }

    public function messages()
    {
        return [
            'date.required'=>'التاريخ مطلوب ',
            'after'=>'التاريخ المدخل مضى    ',
            'date.date'=>'تاكد من صحة التاريخ المدخل ',
            'date.date_format'=>'تاكد من ادخال التاري خبشكل Y-M-D ',
            'hour.regex'=>'ادخل الساعه بشكل صحيح مثل 2:30 او 10:00',
            'date.unique'=>'تم تعطيل هذ التاريخ سابقاً'
        ];
    }
}
