<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddMeasurmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules( )
    {
        $rules = [
            'measurement_id' => 'required|arabic_numbers',
            ];
        foreach($this->request->get('ShlR') as $key => $val)
        {
            $rules['ShlR.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('ShlL') as $key => $val)
        {
            $rules['ShlL.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('Slv_w') as $key => $val)
        {
            $rules['Slv_w.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('Chest') as $key => $val)
        {
            $rules['Chest.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('Waist') as $key => $val)
        {
            $rules['Waist.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('Bottom') as $key => $val)
        {
            $rules['Bottom.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('CollarGalab') as $key => $val)
        {
            $rules['CollarGalab.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('FrontLonger') as $key => $val)
        {
            $rules['FrontLonger.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('FakeButton') as $key => $val)
        {
            $rules['FakeButton.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('Cuffs') as $key => $val)
        {
            $rules['Cuffs.'.$key] = 'required|string';
        }
        foreach($this->request->get('ElbowSlvSada') as $key => $val)
        {
            $rules['ElbowSlvSada.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('Sada') as $key => $val)
        {
            $rules['Sada.'.$key] = 'required|string';
        }
        foreach($this->request->get('Bar') as $key => $val)
        {
            $rules['Bar.'.$key] = 'required|string';
        }
        foreach($this->request->get('SIDE') as $key => $val)
        {
            $rules['SIDE.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('MIDDLE') as $key => $val)
        {
            $rules['MIDDLE.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('Back') as $key => $val)
        {
            $rules['Back.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('CollarSada') as $key => $val)
        {
            $rules['CollarSada.'.$key] = 'required|string';
        }
        foreach($this->request->get('SnpIN') as $key => $val)
        {
            $rules['SnpIN.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('SlvSada') as $key => $val)
        {
            $rules['SlvSada.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('Shl') as $key => $val)
        {
            $rules['Shl.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('Length') as $key => $val)
        {
            $rules['Length.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('HIPS') as $key => $val)
        {
            $rules['HIPS.'.$key] = 'required|arabic_numbers';
        }
        foreach($this->request->get('FrontChest') as $key => $val)
        {
            $rules['FrontChest.'.$key] = 'required|arabic_numbers';
        }

        foreach($this->request->get('ElbowCuffs') as $key => $val)
        {
            $rules['ElbowCuffs.'.$key] = 'required|arabic_numbers';
        }
        return $rules;

    }
    public function  messages(){
        return [

            'measurement_id.required'=>'اختر القياسات',
            'required'=> 'جميع الحقول مطلوبة',
            'arabic_numbers'=>'ادخل القياسات بشكل صحيح'
        ];
    }
}
