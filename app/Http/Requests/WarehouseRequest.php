<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required |string| max:20|min:3| unique:warehouses,name,' . $this->get('id'),
            'cell_phone' => ' nullable| regex:/[0-9]{10}/',
            'phone' => ' required | regex:/[0-9]{10}/',
            'fax' => ' nullable | numeric',
            'mailbox' => 'nullable | numeric',
            'note' => 'nullable | string',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'الاسم مطلوب',
            'phone' => ' رقم الهاتف مطلوب',
            'name.min' => 'الاسم اقل من٣ حروف ',
            'name.max' => 'الاسم طويل جداً ',
            'phone.regex' => ' رقم الهاتف يجب ان يحتوي علي عشر ارقام ',
            'cell_phone.regex' => ' رقم الجوال يجب ان يحتوي علي عشر ارقام ',
            'unique' => ' المستودع موجود ',
            'fax.numeric' => 'ادخل الفاكس بشكل صحيح ',
            'mailbox.numeric' => ' ادخل صندوق البريد بشكل صحيح ',
            'name.string'=>'ادخل الاسم بالشكل الصحيح'

        ];
    }
}
