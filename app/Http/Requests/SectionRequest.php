<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required |string|max:20|min:3|unique:sections,name,' . $this->get('id'),

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'ادخل الاسم ',
            'name.string' => 'الاسم يجب ان يكون حروف',
            'name.unique' => 'االاسم موجود مسبقاً',
            'name.min' => 'الاسم يجب ان لايقل عن ٣ حروف ',
            'name.max' => 'الاسم طويل جداً ',
            'name.max'=>'الاسم يجب ان يكون اقل من ٢٠ حرف',

        ];
    }
}
