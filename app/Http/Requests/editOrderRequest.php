<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class editOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'id_fabric_to_order' => 'nullable |array ',
            "end_date" => "date|required",
            // "Taking_measurements" => "nullable | numeric",
            "finally_price" => 'required |numeric',
            "payment_method_id" => "required | numeric",
            "paid_up" => "required | numeric",
            "Residual" => "required | numeric",
            "id_fabric" => "required |array ",
            'number.*' => 'required|integer|min:1',
            'price.*'=>'required |numeric',
            'id_button.*' => 'nullable |numeric',
            "id_design.*" =>"required | numeric",
            "id_sleeve.*" => "required | numeric",
            "id_pockets.*" => "nullable | numeric",
            "collar_detail" =>"required | array",
            "detail_pocket" =>"required | array",
            "button_detail" => "required | array",
            "number_detail" =>"required | array",
            "number_column.*" => "required | numeric",
            ];

    }

    public function messages()
    {
        return  [
            'phone.required' => 'ادخل الرقم ',
            'phone.numeric' => 'ادخل الرقم بشكل صحيح ',
            'name.required' => 'الاسم مطلوب ',
            'name.string' => 'ادخل الاسم بالشكل الصحيح ',
            'Residual.required' => 'الحقل مطلوب',
            'paid_up.required' => 'الحقل مطلوب ',
            'Residual.numeric' => '  المدخل يجب ان يكون ارقام للمتبقي',
            'paid_up.numeric' => 'المدخل يجب ان تكون ارقام ',
            'id_fabric_to_order.array' => 'ادخل القماش  بشكل صحيح  ',
            'id_fabric.required' => 'ادخل القماش    ',
            // 'id_tailor.required'=>'اختر الخياط ',
            //'Taking_measurements.required'=>'اختر اخذ المقاسات ',
            "payment_method_id.required" => 'اختر طريقة الدفع  ',
            "end_date" => "ادخل تاريخ النهاية بشكل الصحيح",
            "start_date" => "ادخل تاريخ البداية بشكل الصحيح",
            'required' => 'جميع الحقول مطلوبة',
            'arabic_numbers' => 'تاكد من اختيار القياسات و ادخالها  بشكل صحيح',
            'id_design.required' => ' ادخل نوع التصميم ',
            'id_design.*' => ' ادخل نوع التصميم بشكل الصحيح ',
            'id_sleeve.required' => ' ادخل الاكمام ',
            'id_sleeve.*' => ' ادخل الاكمام بشكل الصحيح ',
            'id_button.required' => ' ادخل نوع الازرار ',
            'id_button.*' => ' ادخل الازرار بشكل الصحيح ',
            'id_neck.required' => ' ادخل نوع الرقبة ',
            'id_neck.*' => ' ادخل نوع الرقبة بشكل الصحيح ',
            'id_pockets.required' => ' ادخل نوع الجيب ',
            'id_pockets.*' => ' ادخل نوع الجيب بشكل الصحيح ',
            'collar_detail.required' => ' ادخل  حشوة الياقة  ',
            'collar_detail.array' => ' ادخل  حشوة الياقة  بشكل الصحيح ',
            'detail_pocket.required' => ' ادخل الجيب  ',
            'detail_pocket' => ' ادخل  الجيب  بشكل الصحيح ',
            'button_detail.required' => ' ادخل  حشوة الكبك  ',
            'button_detail.array' => ' ادخل   حشوة الكبك  بشكل الصحيح ',
            'finally_price.numeric' => ' ادخل الإجمالي بشكل صحيح',
            'number.*' => 'عدد المنتج يجب ان لا يكون اقل من 1 ',
            'price.required'=>'ادخل سعر المنتج ',
            'price.numeric'=>'ادخل سعر المنتج بشكل صحيح ',


        ];
    }

}
