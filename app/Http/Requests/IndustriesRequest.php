<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndustriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | regex:/^[\pL\s\-]+$/u| min:3|max:20|unique:industries,name,' . $this->get('id'),
            'note'=> 'nullable |regex:/^[\pL\s\-]+$/u | min:3|max:30'
        ];
    }

    public function messages()
    {
        return [
            'name.string' => 'ادخل الاسم بشكل صحيح ',
            'name.required' => 'الاسم مطلوب ',
            'name.min' => 'يجب ان لايقل الاسم عن ٣ حروف',
            'name.max' => 'الاسم طويل جداً ',
            'name.unique' => 'االاسم موجود مسبقاً',
            'note.string' => 'ادخل الملاحظات بشكل صحيح ',
            'note.max' => 'االملاحظه طويله جداً ',

        ];
    }
}
