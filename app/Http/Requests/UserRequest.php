<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branches_id'=>'required',
            'employee_datas_id'=>'required | numeric |  unique:users,employee_datas_id',
            'password'=>'required',
            'email'=>'required | email | unique:users,email',
        ];
    }

    public function messages()
    {
       return[
           'required'=>' اكمل جميع الحقول ',
           'employee_datas_id.unique'=>'العميل موجود مسبقاً',
           'email.unique'=>'الايميل موجود مسبقاٍ',

       ];
    }
}
