<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DesignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | string| min:3|max:20|unique:designs,name,' . $this->get('id'),
            'note'=> 'nullable |string | min:3|max:30'
        ];
    }

    public function messages()
    {
        return [
            'name.string' => 'ادخل الاسم بشكل صحيح ',
            'name.required' => 'ادخل الاسم ',
            'name.min' => 'الاسم اقل من٣ حروف ',
            'name.max' => 'الاسم طويل جداً ',
            'name.unique' => 'االاسم موجود مسبقاً',
            'note.string' => 'ادخل الملاحظات بشكل صحيح ',
            'note.max' => 'االملاحظه طويله جداً ',

        ];
    }
}
