<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required | min:3|max:30|alpha',
            'phone_number'=>'required | regex:/[0-9]{10}/|unique:employees,phone_number,'.$this->get('id_number'),
            'email'=>'required | email|unique:users,email,'.$this->get('id_user'),

        ];
    }
    public function messages()
    {
        return[
            'required'=>'  اكمل جميع الحقول المطلوبه ',
            'alpha'=>'ادخل الاسم بشكل صحيح ',
            'min'=>'الاسم يجب ان لا يقل عن ٣ احرف  ',
            'max'=>'الاسم يجب ان لايزيد عن٣٠ احرف ',
            'regex'=>'ادخل رقم الهاتف بشكل صحيح ',
            'email'=>'ادخل الايميل بشكل صحيح',
            'email.unique'=>'الايميل موجود مسبقاً',
             'phone_number.unique'=>'رقم الهاتف موجود مسبقاً'

            ,
        ];
    }
}
