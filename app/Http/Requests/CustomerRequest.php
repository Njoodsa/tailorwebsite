<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required |string|max:50|min:3',
            'phone'=>'required |numeric|regex:/[0-9]{10}/|digits:10',
            'note'=>'nullable| string | max:250',




        ];
    }

    public function messages(){
        return[
            'name.required'=>'الاسم مطلوب',
            'name.min'=>'يجب ان لا يقل الاسم عن ٣ حروف',
            'name.string'=>' ادخل الاسم بشكل صحيح',
            'name.max'=>' الاسم طويل ',
            'phone.required'=>' الرقم مطلوب',
            'phone.unique'=>'الرقم موجود مسبقاً',
            'phone.numeric'=>'ادخل ارقام فقط',
            'regex'=>'ادخل رقم الجوال بشكل صحيح',
            'digits'=>' رقم الجوال يجب ان يكون عشرة ارقام',
            'note.string'=>' ادخل الملاحظات بشكل الصحيح',
            'note.max'=>' الملاحظة طويلة جداً'


        ];
    }
}
