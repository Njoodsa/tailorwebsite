<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductToOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_fabric_to_order' =>'nullable |array ',
            "end_date" => "date|required",
            //"id_cutter" => "nullable | numeric",
            // "Taking_measurements" => "nullable | numeric",
            "finally_price"=>'required | numeric',
            "payment_method_id" => "required | numeric",
            "paid_up" => "required | numeric",
            "Residual" => "required | numeric",
            'number.*' => 'required|integer|min:1',
            "discount" => "required | numeric",
        ];
        return $rules;

    }
    public function messages()
    {
        $messages = [
            'phone.required'=>'ادخل الرقم ',
            'phone.numeric'=>'ادخل الرقم بشكل صحيح ',
            'name.required'=>'الاسم مطلوب ',
            'name.string'=>'ادخل الاسم بالشكل الصحيح ',
            'Residual.required'=>'الحقل مطلوب',
            'paid_up.required'=>'الحقل مطلوب ',
            'Residual.numeric'=>'  المدخل يجب ان يكون ارقام للمتبقي',
            'paid_up.numeric'=>'المدخل يجب ان تكون ارقام ',
            'id_fabric_to_order.array'=>'ادخل القماش  بشكل صحيح  ',
            "payment_method_id.required"=>'اختر طريقة الدفع  ',
            "end_date"=> "ادخل تاريخ النهاية بشكل الصحيح",
            "start_date"=> "ادخل تاريخ البداية بشكل الصحيح",
            'number.*'=>'عدد المنتج يجب ان لا يكون اقل من 1 ',
            'discount.required'=>'ادخل مقدار الخصم  ',
            'discount.numeric'=>'  المدخل يجب ان يكون ارقام للخصم ',

        ];
        return $messages;
    }

}
