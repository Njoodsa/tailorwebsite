<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'id_fabric_to_order' => 'nullable |array ',
            "end_date" => "date|required",
            "finally_price" => 'required |numeric',
            "payment_method_id" => "required | numeric",
            "paid_up" => "required | numeric",
            "Residual" => "required | numeric",
            "id_fabric" => "required |array ",
            'number.*' => 'required|integer|min:1',
            'price.*'=>'required |numeric',
            "discount" => "required | numeric",


        ];

        if (($this->get('type')) == 'fabrics') {
            $rules["id_button"] = 'array';
            $rules["id_design"] = 'nullable |array';
            $rules["id_sleeve"] = 'nullable |array';
            $rules["id_pockets"] = 'nullable |array';
            $rules["collar_detail"] = 'nullable |array';
            $rules["detail_pocket"] = 'nullable |array';
            $rules["button_detail"] = 'nullable |array';
            $rules["number_detail"] = 'nullable |array';
            $rules["number_column"] = 'nullable |array';
        }
        return $rules;

    }

    public function messages()
    {
        $messages = [
            'phone.required' => 'ادخل الرقم ',
            'phone.numeric' => 'ادخل الرقم بشكل صحيح ',
            'name.required' => 'الاسم مطلوب ',
            'name.string' => 'ادخل الاسم بالشكل الصحيح ',
            'Residual.required' => 'الحقل مطلوب',
            'paid_up.required' => 'الحقل مطلوب ',
            'Residual.numeric' => '  المدخل يجب ان يكون ارقام للمتبقي',
            'paid_up.numeric' => 'المدخل يجب ان تكون ارقام ',
            'id_fabric_to_order.array' => 'ادخل القماش  بشكل صحيح  ',
            'id_fabric.required' => 'ادخل القماش    ',
            // 'id_tailor.required'=>'اختر الخياط ',
            //'Taking_measurements.required'=>'اختر اخذ المقاسات ',
            "payment_method_id.required" => 'اختر طريقة الدفع  ',
            "end_date" => "ادخل تاريخ النهاية بشكل الصحيح",
            "start_date" => "ادخل تاريخ البداية بشكل الصحيح",
            'required' => 'جميع الحقول مطلوبة',
            'arabic_numbers' => 'تاكد من اختيار القياسات و ادخالها  بشكل صحيح',
            'finally_price.numeric' => ' ادخل الإجمالي بشكل صحيح',
            'number.*' => 'عدد المنتج يجب ان لا يكون اقل من 1 ',
            'price.required'=>'ادخل سعر المنتج ',
            'price.numeric'=>'ادخل سعر المنتج بشكل صحيح ',
            'discount.required'=>'ادخل مقدار الخصم  ',
            'discount.numeric'=>'  المدخل يجب ان يكون ارقام للخصم ',


        ];
        return $messages;
    }

}
