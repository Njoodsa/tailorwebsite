<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:60 |string| unique:products,name,'.$this->get('id') ,
            'unit'=> 'required|string',
            'price'=> 'required|numeric',
            'section_id'=> 'required|numeric',
            'barcode_number'=> 'nullable|numeric|unique:products,barcode_number,'.$this->get('id') ,
            'note'=> 'nullable|string ',

        ];
    }

    public function messages()
    {
        return [
            'required' => 'اكمل جميع الحقول  ',
            'unique'=>'المنتج موجود مسبقاً',
            'name.max'=>'الاسم طويل جداً',
            'name.min'=>'الاسم يجب ان يكون اكثر من ثلاث حروف',
            'name.unique'=>"المنتج  موجود سابقاً",
            'name.string'=>'ادخل اسم القماش بشكل الصحيح ',
            'note.string'=>'ادخل  الملاحظات بشكل الصحيح ',
            'unit.string'=>'ادخل  الملاحظات بشكل الصحيح '

        ];
    }
}
