<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PocketsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required |string|max:20|min:3|max:20|unique:pockets,name,' . $this->get('id'),
            'note'=> 'nullable |string | min:3|max:30'

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'ادخل الاسم ',
            'name.unique' => 'االاسم موجود مسبقاً',
            'name.min' => 'الاسم اقل من٣ حروف ',
            'name.max' => 'الاسم طويل جداً ',
            'name.string' => 'ادخل الاسم بشكل صحيح ',
            'image.mimes'=>'نوع الصوره يجب ان يكون jpeg,jpg,png  ',
            'name.max'=>'الاسم يجب ان يكون اقل من ٢٠ حرف',
            'note.string' => 'ادخل الملاحظات بشكل صحيح ',
            'note.max' => 'االملاحظه طويله جداً ',
        ];
    }
}
