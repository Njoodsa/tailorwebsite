<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Employee_dataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required |min:3|max:30|string',
            'phone' => 'required | regex:/[0-9]{10}/ |digits:10 | unique:employee_datas,phone,' . $this->get('id'),
            'employee_type_id' => 'required|max:3 ',
        ];
    }

    public function messages()
    {
        return [
            'name.string' => 'ادخل الاسم بشكل صحيح ',
            'name.required' => 'الاسم مطلوب',
            'phone.required' => 'رقم الجوال مطلوب ',
            'digits'=>' رقم الجوال يجب ان يكون عشرة ارقام',
            'employee_type_id.required' => 'الوظيفه مطلوبه',
            'phone.regex' => 'ادخل رقم الجوال بشكل صحيح ',
            'phone.unique' => 'رقم الجوال موجود مسبقاً ',
            'name.min' => 'الاسم اقل من٣ حروف ',
            'name.max' => 'الاسم طويل جداً ',
        ];
    }
}
