<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Payment_methodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required |string| unique:payment_methods,name,' . $this->get('id'),
        ];
    }
    public function messages(){

        return[
            'required' => 'الاسم مطلوب' ,
            'unique'   => ' الاسم موجود مسبقاً',
            'string' =>'ادخل الاسم بشكل الصحيح'
        ];
    }
}
