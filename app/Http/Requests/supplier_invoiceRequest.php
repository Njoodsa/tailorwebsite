<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class supplier_invoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              "supplier_id" => "required | numeric  " ,
              "date" => "required | date " ,
              "id_item" =>"required | array " ,
              "warehouses_id" => "required | array" ,
              "number" => "required | array " ,
              "price" => "required | array " ,
              "discount" =>"required | numeric  " ,
              "finally_price" => "required | numeric  " ,
              "payment_method_id" => "required | numeric " ,
              "paid_up" =>"required | numeric  " ,
              "Residual" =>"required | numeric  " ,
              "state" => "required | numeric   " ,
        ];
    }

    public function messages()
    {
     return [
         'required'=>'اكمل جميع الحقول ',
         'date.date'=>' ادخل جميع الحقول بشكل الصحيح ',
         'discount' => 'ادخل الخصم بشكل الصحيح ',

     ];
    }
}
