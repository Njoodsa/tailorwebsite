<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HourRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hour'=>['regex:/^(([1-9]|1[0-2]):[0-59]{2}$)/','required','unique:hours,hour']
        ];

    }
    public function messages()
    {
        return [
            'hour.required'=>'ادخل الساعه ',
            'hour.regex'=>'ادخل الساعه بشكل صحيح مثل 2:30 او 10:00 ',
            'hour.unique'=>'تم إضافة هذه الساعة مسبقاً '
        ];

    }
}
