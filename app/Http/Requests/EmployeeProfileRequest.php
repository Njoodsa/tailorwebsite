<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()

    {
        return [
            'name'=>'required | min:3|max:30|string',
            'phone'=>'required | regex:/[0-9]{10}/|unique:employee_datas,phone,'.$this->get('id_number'),
            'email'=>'required | email|unique:users,email,'.$this->get('id'),

        ];
    }
    public function messages()
    {
        return[
            'required'=>'  اكمل جميع الحقول المطلوبه ',
            'min'=>'الاسم يجب ان لا يقل عن ٣ احرف  ',
            'max'=>'الاسم يجب ان لايزيد عن٣٠ احرف ',
            'regex'=>'ادخل رقم الهاتف بشكل صحيح ',
            'email'=>'ادخل الايميل بشكل صحيح',
            'email.unique'=>'الايميل موجود مسبقاً',
            'phone.unique'=>'رقم الهاتف موجود مسبقاً'

            ,
        ];
    }
}
