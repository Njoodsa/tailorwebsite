<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|string| min:3|max:20| unique:units,name,' . $this->get('id'),
        ];
    }
    public function messages(){

        return[
            'required' => 'الاسم مطلوب' ,
            'unique'   => ' الاسم موجود مسبقاً',
            'min' => 'الاسم اقل من٣ حروف ',
            'max' => 'الاسم طويل جداً ',
            'string'=>'ادخل الاسم بشكل الصحيح'
        ];
    }
}
