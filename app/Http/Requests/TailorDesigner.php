<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TailorDesigner extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required |alpha|min:3|max:30',
            'phone_number'=>'required |unique:employees|regex:/[0-9]{10}/',
        ];
    }
    public function messages()
    {
        return[
            'required'=>'  اكمل جميع الحقول المطلوبه ',
            'alpha'=>'ادخل الاسم بشكل صحيح ',
            'min'=>'الاسم يجب ان لا يقل عن ٣ احرف ',
            'max'=>'الاسم يجب ان لايزيد عن ٣٠ خانه',
            'unique' =>'رقم الجوال موجود مسبقاً',
            'regex' =>'ادخل رقم الجوال بشكل صحيح',
        ];
    }
}
