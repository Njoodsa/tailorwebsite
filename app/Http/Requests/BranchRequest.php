<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required |string| max:30 | min:3| unique:branches,name,' . $this->get('id'),
            'cell_phone' => 'nullable|regex:/[0-9]{10}/',
            'phone' => ' required|regex:/^05[0-9]{8}$/',
            'fax' => ' nullable | numeric |regex:/[0-9]{10}/',
            'mailbox' => 'nullable |string| numeric |size:10',
            'warehouses_id'=>'required | numeric',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'الاسم مطلوب',
            'phone' => ' رقم الهاتف مطلوب',
            'name.min' => 'الاسم اقل من٣ حروف ',
            'name.max' => 'الاسم طويل جداً ',
            'phone.regex' => 'ادخل رقم الهاتف بشكل صحيح ',
            'cell_phone.regex' => ' رقم الجوال يجب ان يحتوي علي عشر ارقام ',
            'unique' => ' الفرع موجود ',
            'fax.numeric' => 'ادخل الفاكس بشكل صحيح ',
            'mailbox.numeric' => ' ادخل صندوق البريد بشكل صحيح ',
            'warehouses_id.required'=> 'ادخل الفرع ',
            'string'=>'ادخل الاسم بشكل الصحيح'
        ];
    }
}
