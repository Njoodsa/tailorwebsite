<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ButtonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required |string|max:20|min:3|unique:buttons,name,' . $this->get('id'),
            'note'=> 'nullable |string | min:3|max:30'

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'ادخل الاسم ',
            'name.string' => 'الاسم يجب ان يكون حروف',
            'name.unique' => 'االاسم موجود مسبقاً',
            'name.min' => 'الاسم يجب ان لايقل عن ٣ حروف ',
            'name.max' => 'الاسم طويل جداً ',
            'image.max'=>'الصوره كبيره جدا',
            'image.mimes'=>'نوع الصوره يجب ان يكون jpeg,jpg,png  ',
            'name.max'=>'الاسم يجب ان يكون اقل من ٢٠ حرف',
            'note.string' => 'ادخل الملاحظات بشكل صحيح ',
            'note.max' => 'االملاحظه طويله جداً ',
        ];
    }
}
