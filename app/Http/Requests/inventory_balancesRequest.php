<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class inventory_balancesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

              'demand_limit'=> 'required | numeric',


        ];
    }
    public function messages(){
        return[

            'demand_limit.required'=>' تاكد انك ادخلت الحد الادنى لطلب ',
            'demand_limit.numeric'=>'  ادخلت الحد الادنى لطلب بشكل صحيح',



        ];
    }



}
