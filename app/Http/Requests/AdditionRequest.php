<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdditionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required | string | max:250|unique:additions,name,' . $this->get('id'),
            'price'=>'numeric |required' ,
        ];
    }

    public function messages()
    {
        return[
            'required' =>'ادخل جميع البيانات',
            'string'=>'ادخل الاسم بشكل صحيح',
            'numeric'=>' ادخل السعر بشكل صحيح ',
            'unique'=>'الاسم موجود مسبقاً'

        ];
    }
}
