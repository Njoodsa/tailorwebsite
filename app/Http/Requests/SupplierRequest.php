<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|string|max:20|min:3',
            'phone'=>'required | regex:/[0-9]{10}/|unique:suppliers,phone,' . $this->get('id'),
            'email'=> 'email | nullable |unique:suppliers,email,' . $this->get('id'),
        ];
    }

    public function messages(){
        return[
            'name.required'=>'الاسم مطلوب',
            'phone.regex'=>' ادخل الجوال بشكل صحيح',
            'phone.required'=>' ادخل رقم الجوال',
            'email'=>'اادخل الايميل بشكل صحيح ',
            'name.min' => 'الاسم اقل من٣ حروف ',
            'name.max' => 'الاسم طويل جداً ',
            'name.string'=>'ادخل الاسم بشكل الصحيح'

        ];
    }
}
