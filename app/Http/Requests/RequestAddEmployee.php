<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestAddEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name'=>'required|min:3|max:30',
            'phone_number'=>'required |regex:/[0-9]{10}/',
            'email'=>'required | email|unique:users',
            'password'=>'required ',
        ];
    }
    public function messages()
    {
        return[
            'required'=>'  اكمل جميع الحقول المطلوبه ',
            'min'=>'الاسم يجب ان لا يقل عن ٣ احرف ',
            'max'=>'الاسم يجب ان لايزيد عن ٣٠ خانه',
            'regex' =>'ادخل رقم الجوال بشكل صحيح',
            'unique'=>'لايميل موجود مسبقاً',
             'email'=>'ادخل الايميل بشكل صحيح',

        ];
    }
}
