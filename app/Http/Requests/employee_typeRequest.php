<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class employee_typeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string|min:3|max:20|unique:employee_types,name,' . $this->get('id'),
        ];
    }

    public function messages(){
        return [
            'required' => ' يرجى ادخال الاسم ',
            'unique'=>'الاسم موجود سابقاً',
            'regex'=> ' ادخل الاسم بشكل صحيح',
            'name.min' => 'الاسم اقل من٣ حروف ',
            'name.max' => 'الاسم طويل جداً ',
            'name.string' => 'ادخل الاسم بشكل صحيح ',


        ];
    }
}
