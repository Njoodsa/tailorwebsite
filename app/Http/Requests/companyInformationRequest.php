<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class companyInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location'=>'required | min:3|max:60',
            'work_days'=>'required | min:3|max:30' ,
            'work_hours'=>'required | min:3|max:30',
            'phone'=>'required',
            'email'=>'email|nullable',
            'fax'=>'nullable|numeric'
        ];
    }
    public function messages()
    {
        return[
            'location.required'=>' ادخل الموقع ',
            'work_days.required'=>' ادخل ايام العمل',
            'work_hours.required'=>' ادخل ساعات العمل',
            'phone.required'=>' ادخال رقم الجول',
            'location.max'=>'القيمة المدخله للموقع  كبيره جدا ',
            'location.min'=>'وصف الموقع لا يمكن ان يكون اقل من٣ حروف  ',
            "work_days.max"=>'النص المدخل لايام العمل كبير جدا',
            "work_hours.min"=>'النص المدخل لساعات العمل صغير جدا',
            "work_days.min"=>'النص المدخل لايام العمل صغير جدا',
            "work_hours.max"=>'النص المدخل لساعات العمل كبير جدا',
            "fax.numeric"=>'ادخل رقم الفاكس بشكل صحيح',
            "fax.max"=>'رقم الفاكس  كبير جدا',
            "fax.min"=>'زقم الفاكس صغير جدا',


        ];
    }
}
