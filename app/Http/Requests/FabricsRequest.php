<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FabricsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:30 |string| unique:fabrics,name,'.$this->get('id') ,
            'Equation'=> 'required|array',
            'price'=> 'required|array',
            'unit_id'=> 'required|array',
            'industries_id'=> 'required|numeric',
            'lowest_price'=> 'required|numeric',
            'fabric_width'=> 'required | string | max:10',
            'note'=> 'nullable|string ',

        ];
    }

    public function messages()
    {
        return [
            'required' => 'اكمل جميع الحقول  ',
            'unique'=>'القماش موجود مسبقاً',
            'name.max'=>'الاسم طويل جداً',
            'name.min'=>'الاسم يجب ان يكون اكثر من ثلاث حروف',
            'name.unique'=>"نوع القماش موجود سابقاً",
            'name.string'=>'ادخل اسم القماش بشكل الصحيح ',
            'note.string'=>'ادخل  الملاحظات بشكل الصحيح '

        ];
    }
}
