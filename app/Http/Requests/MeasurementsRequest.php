<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeasurementsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'Length' => 'required|numeric',
            'ShlR' => 'required|numeric',
            'ShlL' => 'required |numeric',
            'Slv_w' => 'required|numeric',
            'Chest' => 'required|numeric',
            'Waist' => 'required|numeric',
            'Bottom' => 'required|numeric',
            'CollarGalab' => 'required|numeric',
            'FrontLonger' => 'required|numeric',
            'FakeButton' => 'required|numeric',
            'Cuffs' => 'required|string',
            'ElbowSlvSada' => 'required|numeric'
            , 'Sada' => 'required |string',
            'Bar' => 'required |string',
            'SIDE' => 'required|numeric',
            'Back' => 'required |numeric',
            'CollarSada' => 'required|string',
            'SnpIN' => 'required|numeric',
            'SlvSada' => 'required|numeric',
            'Shl' => 'required|numeric',
          //  'ElbowCuffs'=>'required|numeric',
            'Armhole'=>'required|numeric',
            'FrontChest'=>'required|numeric',
            'HIPS'=>'required|numeric',
            'Shafa121'=>'required|numeric',
            'Cuff57'=>'required|numeric',
            'Elpw'=>'required|numeric',
        ];
    }

    public function messages()
    {
        return[
            'required' => 'يرجى ادخال جميع البيانات',
            'numeric' => 'ادخل القياس بشكل صحيح ',
            'id_cutter'=>'اختر اسم القصاص '
        ];

    }
}

