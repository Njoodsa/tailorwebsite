<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDesignerTailor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|alpha|min:3|max:30',
            'phone_number'=>'required|regex:/[0-9]{10}/|unique:employees,phone_number,'.$this->get('id'),
        ];
    }
    public function messages()
    {
        return[
            'alpha'     =>'ادخل الاسم بشكل صحيح ',
            'min'       =>'الاسم يجب ان لا يقل عن 3 احرف ',
            'max'       =>'الاسم يجب ان لايزيد عن ٣٠ خانه',
            'required'  =>' اكمل جميع الحقول المطلوبه ',
            'regex'     =>'ادخل رقم الجوال بشكل صحيح',
            'unique'    =>"رقم الجوال موجود مسبقاً"
        ];
    }
}
