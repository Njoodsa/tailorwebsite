<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
  protected $fillable = ['name'];

  public function products(){
      return $this->hasMany(Product::class,'section_id');
  }
}
