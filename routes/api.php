<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//----------- DateBlocked ------//


Route::group(['middleware' => 'api', 'namespace' => 'Api'], function () {
    //----------------- Login -----------//
    Route::post('login', 'Auth\LoginApiController@login');
    Route::post('logout', 'Auth\LoginApiController@logout');
    //---------------- CUSTOMER -------//
    Route::get('Customer', 'CustomerApiController@index');
    Route::get('editCustomer/{id}', 'CustomerApiController@edit');
    Route::post('update_information/{id}', 'CustomerApiController@update_information');
    Route::post('insertCustomer', 'CustomerApiController@insert');
    Route::post('update_measurement/{id}', 'CustomerApiController@update_measurement');
    Route::post('search_name', 'CustomerApiController@search_name');
    Route::post('search_number', 'CustomerApiController@search');
    //------------- MEASUREMENT ----//
    Route::get('newMeasurement/{id}', 'MeasurementApiController@NewMeasurement'); // new measurement for old customer
    Route::post('insertNewMeasurement/{id}', 'MeasurementApiController@insertNewMeasurement');
    //-------------  ORDER  -------//
    Route::get('pending_review', 'OrderApiController@ViewTheOrderList');
    Route::get('editOrder/{id}', 'OrderApiController@edit');
    Route::post('updateMeasurementOrder/{id}', 'OrderApiController@updateMeasurement_order');
    //------------ EMPLOYEE ------//
    Route::get('Profile', 'EmployeeApiController@index');
    Route::post('update_employeeInformation/{id}', 'EmployeeApiController@update');
    Route::post('logout', 'Auth\LoginApiController@logout');
    //----------- DateBlocked ------//
    Route::get('dataHourBlocked', 'DateBlockedController@index');

});



Route::group(['middleware' => 'auth:api_customer', 'namespace' => 'Api'], function () {
      //----------------RESERVATION-------//
  Route::post('reservation/{reservation}', 'ReservationApicontroller@update')->name('reservation.update');
  Route::post('cancelReservation/{reservation}', 'ReservationApicontroller@cancel');
  Route::get('displayReservation/{id}', 'ReservationApicontroller@showCustomerReservations');
  Route::get('editReservation/{id}', 'ReservationApicontroller@edit');
  Route::resource('reservation','ReservationApicontroller')->except(['show','index','destroy','create']);
     //----------------ORDER CUSTOMER -------//
  Route::get('customer_orders', 'OrderApiController@orders_customer');
  Route::get('viewDetailOrder/{id}', 'OrderApiController@detail_order');
     //----------------Home page for customer  -------//
  Route::get('homePageCustomer', 'OrderApiController@totalCustomerOrder');


});

Route::group([ 'middleware' => 'api', 'namespace' => 'Api\Auth' ], function () {
    //----------------register customer-------//
Route::post('RegisterCustomer', 'RegisterCustomerController@store');
Route::post('confirmed', 'RegisterCustomerController@confirmed');
Route::get('ResendCode/{phone}', 'RegisterCustomerController@ResendCod');
    // ---------- log out  -------  //
    Route::post('logoutCustomer', 'LoginCustomerController@logout');

   // ---------- login customer -------  //
Route::post('loginCustomer', 'LoginCustomerController@login');
Route::get('loginCustomer', 'RegisterCustomerController@login1');
  // ---------- reset password  customer -------  //
Route::post('resePassword', 'ResetPasswordBySMSController@ResetsPasswords');
Route::post('confirmSMS', 'ResetPasswordBySMSController@confirmed');
Route::post('updatePassword', 'ResetPasswordBySMSController@updatePasswords');
Route::post('ResendCodeReset/{phone}', 'ResetPasswordBySMSController@ResendCod');

});
