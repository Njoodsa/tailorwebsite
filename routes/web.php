<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CompanyInformationController@index');
Route::get('/', 'CompanyInformationController@index');
Route::view('/mawan/request-visit', 'request_visitor');
Auth::routes();
Route::match(['get', 'post'], 'register', function () {
    return redirect('/');
});
Route::group(['middleware' => 'isAdmin'], function () {
    // information company
    Route::resource('companyInformation', 'CompanyInformationController')->except(['create', 'store', 'destroy', 'show']);
    //Branches
    Route::resource('Branch', 'BranchController')->except(['destroy']);

    // message
    Route::get('send_massage', 'CustomerController@view_send');
    Route::post('send', 'CustomerController@send_massage');

    Route::get('Admin', 'AdminController@index');
    Route::post('update_hisInformation/{id}', 'AdminController@update');
    Route::get('downloadExcel/{type}', 'CustomerController@downloadExcel');
    // design
    Route::get('design', 'designController@index');
    Route::post('delete/design/{id}', 'designController@destroy');
    Route::get('createDesign', 'designController@create');
    Route::post('insertDesign', 'designController@insert');
    Route::get('editDesign/{data}', 'designController@edit');
    Route::post('updateDesign/{design}', 'designController@update');
    // fabrics
    Route::get('fabrics', 'FabricsController@index');
    Route::get('createFabric', 'FabricsController@create');
    Route::post('insertFabric', 'FabricsController@insert');
    Route::get('editFabric/{data}', 'FabricsController@edit');
    Route::post('updateFabric/{fabrics}', 'FabricsController@update');
    Route::post('delete/fabric/{id}', 'FabricsController@destroy');

    // products
    Route::get('products', 'ProductController@index');
    Route::get('createProduct', 'ProductController@create');
    Route::post('insertProduct', 'ProductController@insert');
    Route::get('editProduct/{id}', 'ProductController@edit');
    Route::post('updateProduct/{id}', 'ProductController@update');
    Route::get('ViewSection/{id}/{name}', 'ProductController@viewSection');
    // industries
    Route::get('industries', 'IndustriesController@index');
    Route::get('createIndustry', 'IndustriesController@create');
    Route::post('insertIndustry', 'IndustriesController@insert');
    Route::get('editIndustry/{id}', 'IndustriesController@edit');
    Route::post('updateIndustry/{id}', 'IndustriesController@update');
    Route::post('delete/industry/{id}', 'IndustriesController@destroy');
    // Button
    Route::get('buttons', 'ButtonController@index');
    Route::get('createButton', 'ButtonController@create');
    Route::post('insertButton', 'ButtonController@insert');
    Route::get('editButton/{data}', 'ButtonController@edit');
    Route::post('updateButton/{button}', 'ButtonController@update');
    // Addition
    Route::get('Addition', 'AdditionController@index');
    Route::get('createAddition', 'AdditionController@create');
    Route::post('insertAddition', 'AdditionController@insert');
    Route::get('editAddition/{data}', 'AdditionController@edit');
    Route::post('updateAddition/{addition}', 'AdditionController@update');
    // Sleeve
    Route::get('sleeve', 'SleevesController@index');
    Route::get('createSleeve', 'SleevesController@create');
    Route::post('insertSleeve', 'SleevesController@insert');
    Route::get('editSleeve/{sleeve}', 'SleevesController@edit');
    Route::post('updateSleeve/{sleeve}', 'SleevesController@update');
    Route::post('delete/Sleeve/{id}', 'SleevesController@destroy');
    // Pocket
    Route::get('pockets', 'PocketController@index');
    Route::get('createPocket', 'PocketController@create');
    Route::post('insertPocket', 'PocketController@insert');
    Route::get('editPocket/{data}', 'PocketController@edit');
    Route::post('updatePocket/{pocket}', 'PocketController@update');
    //Necks
    Route::get('necks', 'NeckController@index');
    Route::get('createNeck', 'NeckController@create');
    Route::post('insertNeck', 'NeckController@insert');
    Route::get('editNeck/{data}', 'NeckController@edit');
    Route::post('updateNeck/{neck}', 'NeckController@update');
    //employee type
    Route::get('employeeType', 'Employee_typeController@view');
    Route::get('createEmployeeType', 'Employee_typeController@create');
    Route::post('insertEmployeeType', 'Employee_typeController@insert');
    Route::get('editEmployeeType/{id}', 'Employee_typeController@edit');
    Route::post('updateEmployeeType/{id}', 'Employee_typeController@update');
    Route::post('delete/employeeType/{id}', 'Employee_typeController@destroy');
    //discount
    Route::get('Discount', 'DiscountController@edit');
    Route::post('updateDiscount/{id}', 'DiscountController@update');
    //employee data
    Route::get('employeeData', 'Employee_dataController@index');
    Route::get('createEmployeeData', 'Employee_dataController@create');
    Route::post('insertEmployeeData', 'Employee_dataController@insert');
    Route::get('editEmployeeData/{id}', 'Employee_dataController@edit');
    Route::post('updateEmployeeData/{id}', 'Employee_dataController@update');
    Route::post('delete/employeeData/{id}', 'Employee_dataController@destroy');
    Route::get('EmployeeDeleted', 'Employee_dataController@indexDeleted');
    // unit
    Route::get('Unit', 'UnitController@index');
    Route::get('createUnit', 'UnitController@create');
    Route::post('insertUnit', 'UnitController@insert');
    Route::get('editUnit/{data}', 'UnitController@edit');
    Route::post('updateUnit/{unit}', 'UnitController@update');
    Route::post('delete/unit/{id}', 'UnitController@destroy');
    //Payment_methods
    Route::get('Payment_method', 'Payment_methodController@index');
    Route::get('createPaymentMethod', 'Payment_methodController@create');
    Route::post('insertPaymentMethod', 'Payment_methodController@insert');
    Route::get('editPaymentMethod/{id}', 'Payment_methodController@edit');
    Route::post('updatePaymentMethod/{payment_method}', 'Payment_methodController@update');
    Route::post('delete/PaymentMethod/{id}', 'Payment_methodController@destroy');
    //Warehouses
    Route::get('Warehouses', 'WarehouseController@index');
    Route::get('createWarehouse', 'WarehouseController@create');
    Route::post('insertWarehouseBesic', 'WarehouseController@insert');
    Route::get('editWarehouse/{id}', 'WarehouseController@edit');
    Route::post('updateWarehouse/{id}', 'WarehouseController@update');
    Route::post('delete/Warehouse/{id}', 'WarehouseController@destroy');
    //section
    Route::get('Sections', 'SectionController@index');
    Route::get('createSection', 'SectionController@create');
    Route::post('insertSection', 'SectionController@insert');
    Route::get('editSection/{id}', 'SectionController@edit');
    Route::post('updateSection/{id}', 'SectionController@update');
    //box
    Route::get('Boxes', 'BoxController@index');
    Route::get('createBox', 'BoxController@create');
    Route::post('insertBox', 'BoxController@insert');
    Route::get('editBox/{id}', 'BoxController@edit');
    Route::post('updateBox/{id}', 'BoxController@update');
    Route::post('delete/Box/{id}', 'BoxController@destroy');
    //Fabrics
    Route::get('inventory_balance', 'Warehouses_fabricsController@index');
    Route::get('inventory_balance_product', 'Warehouses_fabricsController@index_product');
    //Route::get('inventory_balanceCreate', 'Warehouses_fabricsController@create');
    Route::get('GetFabrics/ajax/{fabrics}', 'Warehouses_fabricsController@GetFabrics_create');
    //Route::post('insertWarehouse', 'Warehouses_fabricsController@insert');
    Route::get('editInventory_balance/{id}', 'Warehouses_fabricsController@edit');
    Route::get('editInventory_balance_product/{id}', 'Warehouses_fabricsController@edit_product');
    Route::post('updateInventory_balance/{id}', 'Warehouses_fabricsController@update');
    //number
    Route::get('GetPhone/{phone}', 'MobileNumbersController@GetPhone');
    Route::get('GetPhone2/{phone}', 'MobileNumbersController@GetPhone2');
    //employee supplier
    Route::get('suppliers', 'SupplierController@index');
    Route::get('createSupplier', 'SupplierController@create');
    Route::post('insertSuppliers', 'SupplierController@insert');
    Route::get('editSuppliers/{id}', 'SupplierController@edit');
    Route::post('updateSuppliers/{id}', 'SupplierController@update');
    Route::post('delete/Suppliers/{id}', 'SupplierController@destroy');
    //users
    Route::get('users', 'UserController@index');
    Route::get('createUser', 'UserController@create');
    Route::post('insertUser', 'UserController@insert');
    Route::get('access0/{id}', 'UserController@access0');
    Route::get('access2/{id}', 'UserController@access2');
    //supplier_invoice
    Route::get('invoice', 'Supplier_invoiceController@index');
    Route::get('createSupplier_invoice', 'Supplier_invoiceController@create');
    Route::post('insertInvoice', 'Supplier_invoiceController@insert');
    Route::get('detailInvoice/{id}', 'Supplier_invoiceController@detailCompleted');
    Route::get('detailCompleted_product/{id}', 'Supplier_invoiceController@detailCompleted_product');
    Route::get('invoiceUncompleted', 'Supplier_invoiceController@indexUncompleted');
    Route::get('InvoiceUncompleted/{id}', 'Supplier_invoiceController@detailUnCompleted');
    Route::get('InvoiceUncompleted_product/{id}', 'Supplier_invoiceController@detailUnCompleted_product');
    Route::post('updateInvoice/{id}', 'Supplier_invoiceController@update');
    //supplier_invoice
    Route::get('Revenue', 'RevenueController@index');
    Route::get('AllBills/{year}', 'RevenueController@bills');
    //supplier_invoice
    Route::get('purchases', 'PurchasesController@index');
    Route::get('AllPurchases/{year}', 'PurchasesController@purchases');
});
Route::group(['middleware' => 'isEmployee'], function () {
    Route::get('Profile', 'EmployeeProfileController@index');
    Route::post('update_employeeInformation/{id}', 'EmployeeProfileController@update');
});
Route::group([['middleware' => 'isEmployee'], ['middleware' => 'isAdmin']], function () {
    //Report
    Route::get('report/search/fabric', 'ReportController@search_fabric');
    Route::get('report/search/product', 'ReportController@search_product');
    Route::post('report/result/fabric', 'ReportController@result_search_fabric');
    Route::post('report/result/product', 'ReportController@result_search_product');
    Route::get('report/warehouses/fabric', 'ReportController@warehouses_fabric');
    Route::get('report/warehouses/product', 'ReportController@warehouses_product');


    //hours
    Route::get('hours', 'HourController@index');
    Route::get('add_hour', 'HourController@create');
    Route::post('insertHour', 'HourController@insert');
    Route::post('deleteHour/{Hour}', 'HourController@destroy');
    //date
    Route::get('dates', 'DateController@index');
    Route::get('displayDate', 'DateController@create');
    Route::post('displayDate', 'DateController@insert');
    Route::post('deleteDate/{Date}', 'DateController@destroy');
    Route::get('AjaxGetHour/{Date}', 'DateController@AjaxGetHour');
    // search order
    Route::post('search_order', 'Order_detailController@search');
    //MEASUREMENT
    Route::get('newMeasurement/{id}', 'MeasurementController@NewMeasurement'); // new measurement for old customer
    Route::post('insertNewMeasurement/{id}', 'MeasurementController@insertNewMeasurement');
    Route::post('delete/Measurement/{id}', 'MeasurementController@destroy');
    Route::get('viewMeasurement/{id}', 'MeasurementController@viewOneMeasurement');
    // customer
    Route::get('Customer', 'CustomerController@index');
    Route::get('createCustomer', 'CustomerController@create');
    Route::post('insertCustomer', 'CustomerController@insert');
    Route::get('editCustomer/{id}', 'CustomerController@edit');
    Route::post('update_information/{id}', 'CustomerController@update_information');
    Route::post('update_measurement/{id}', 'CustomerController@update_measurement');
    Route::get('viewMeasurement/{id}', 'MeasurementController@viewOneMeasurement');
    Route::post('search_number', 'MobileNumbersController@search');
    Route::post('search_name', 'CustomerController@search_name');
    Route::get('autocomplete', 'CustomerController@autocomplete');
    //order
    Route::get('orders', 'Order_detailController@index');
    Route::get('check_style/{design}', 'Order_detailController@checkStyle');
    Route::get('createOrder/{id}', 'Order_detailController@create');
    Route::get('editOrder/{id}', 'Order_detailController@edit');
    Route::post('updateOrder/{id}', 'Order_detailController@review');
    Route::get('GetName/{phone}', 'Order_detailController@GetName');
    Route::get('Check_number/{phone}', 'Order_detailController@Check_number');
    Route::get('GetPrice/{Fabric_number}', 'Order_detailController@GetPrice');
    Route::get('GetProductPrice/{product}', 'Order_detailController@getProductPrice');
    Route::get('GetLowestPrice/{fabrics}', 'Order_detailController@getLowestPrice');
    Route::get('GetPriceAddition/{addition}', 'Order_detailController@getPriceAddition');
    Route::get('orders_completed', 'Order_detailController@orders_completed');
    Route::get('orders_canceled', 'Order_detailController@orders_canceled');
    Route::get('detailDelivered/{id}', 'Order_detailController@detailDelivered');
    Route::get('order_canceled/{id}', 'Order_detailController@order_canceled');
    Route::post('insertOrder/{id}', 'Order_detailController@insert');
    Route::post('delete/user/{id}', 'Order_detailController@destroy');
    Route::get('order_delivered', 'Order_detailController@order_delivered');
    Route::post('It_is_deliver/{id}', 'Order_detailController@it_is_deliver');
    Route::get('billCompleted/{id}', 'Order_detailController@viewBillCompleted');
    Route::get('pending_review', 'Order_detailController@viewReviewMeasurement'); // review_measurement
    Route::get('editOrder/{id}', 'Order_detailController@edit'); //  detail review_measurement
    Route::get('DetailWorked_order/{id}', 'Order_detailController@DetailWorked_order'); //  detail review_measurement
    Route::post('check_complete_review/{id}', 'Order_detailController@completed_or_review'); //  detail review_measurement
    Route::get('detail_orders_completed/{id}', 'Order_detailController@detail_orders_completed'); //  detail review_measurement
    Route::get('view_product_unCompleted', 'Order_detailController@productUnCompleted'); // stste = 7
    Route::get('editOrderProduct/{id}', 'Order_detailController@editOrderProduct'); // stste = 7
    Route::post('updateOrderProduct/{id}', 'Order_detailController@updateProductUncompleted'); // update product
    Route::get('View_product_delivered', 'Order_detailController@product_delivered'); // update product
    Route::get('detailProduct_delivered/{id}', 'Order_detailController@detailProduct_delivered'); // update product
    Route::get('viewProduct_cancel', 'Order_detailController@viewProduct_cancel'); // update product
    Route::get('detailorder_canceled_product/{id}', 'Order_detailController@detailorder_canceled_product'); // update product
    Route::get('GetProductAJax', 'Supplier_invoiceController@GetProductAJax');
    Route::get('GetFabricsAJax', 'Supplier_invoiceController@GetFabricsAJax');
    Route::get('GetMeasurement/{id}', 'Order_detailController@GetMeasurement');
    Route::get('GetProductAJaxToOrder', 'Order_detailController@GetProductAJax');
    //Reservation
    Route::get('reservations', 'ReservationController@index');
    Route::get('reservation/{reservation}', 'ReservationController@show');
    Route::get('visited/{reservation}', 'ReservationController@visited');


});
Route::group(['middleware' => 'isEmployeeNoAccess'], function () {

    Route::get('ERRORE', 'EmployeeProfileController@noAccess');
});
